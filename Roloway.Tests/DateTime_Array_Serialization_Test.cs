﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class DateTime_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new DateTimeArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (DateTimeArrayClass) s2.Deserialize(typeof (DateTimeArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr0, o2.PrFieldDateTimeArr0);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr1, o2.PrFieldDateTimeArr1);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr2, o2.PrFieldDateTimeArr2);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr3, o2.PrFieldDateTimeArr3);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr4, o2.PrFieldDateTimeArr4);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr0, o2._pubFieldDateTimeArr0);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr1, o2._pubFieldDateTimeArr1);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr2, o2._pubFieldDateTimeArr2);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr3, o2._pubFieldDateTimeArr3);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr4, o2._pubFieldDateTimeArr4);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr0, o2.PropertyDateTimeArr0);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr1, o2.PropertyDateTimeArr1);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr2, o2.PropertyDateTimeArr2);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr3, o2.PropertyDateTimeArr3);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr4, o2.PropertyDateTimeArr4);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr5, o2.PropertyDateTimeArr5);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr6, o2.PropertyDateTimeArr6);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr7, o2.PropertyDateTimeArr7);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr8, o2.PropertyDateTimeArr8);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr9, o2.PropertyDateTimeArr9);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr10, o2.PropertyDateTimeArr10);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr11, o2.PropertyDateTimeArr11);
            }
        }
    }

    [Serializable]
    internal class DateTimeArrayClass {
        private DateTime[] _prFieldDateTimeArr0;
        private DateTime[] _prFieldDateTimeArr1;
        private DateTime[] _prFieldDateTimeArr2;
        private DateTime[] _prFieldDateTimeArr3;
        private DateTime[] _prFieldDateTimeArr4;

        public DateTime[] PrFieldDateTimeArr0 {
            get { return _prFieldDateTimeArr0; }
        }

        public DateTime[] PrFieldDateTimeArr1 {
            get { return _prFieldDateTimeArr1; }
        }

        public DateTime[] PrFieldDateTimeArr2 {
            get { return _prFieldDateTimeArr2; }
        }

        public DateTime[] PrFieldDateTimeArr3 {
            get { return _prFieldDateTimeArr3; }
        }

        public DateTime[] PrFieldDateTimeArr4 {
            get { return _prFieldDateTimeArr4; }
        }

        public DateTime[] _pubFieldDateTimeArr0;
        public DateTime[] _pubFieldDateTimeArr1;
        public DateTime[] _pubFieldDateTimeArr2;
        public DateTime[] _pubFieldDateTimeArr3;
        public DateTime[] _pubFieldDateTimeArr4;
        public DateTime[] PropertyDateTimeArr0 { get; set; }
        public DateTime[] PropertyDateTimeArr1 { get; set; }
        public DateTime[] PropertyDateTimeArr2 { get; set; }
        public DateTime[] PropertyDateTimeArr3 { get; set; }
        public DateTime[] PropertyDateTimeArr4 { get; set; }
        public DateTime[] PropertyDateTimeArr5 { get; set; }
        public DateTime[] PropertyDateTimeArr6 { get; set; }
        public DateTime[] PropertyDateTimeArr7 { get; set; }
        public DateTime[] PropertyDateTimeArr8 { get; set; }
        public DateTime[] PropertyDateTimeArr9 { get; set; }
        public DateTime[] PropertyDateTimeArr10 { get; set; }
        public DateTime[] PropertyDateTimeArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldDateTimeArr0 = Utils.Default<DateTime[]>();
            _prFieldDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            _prFieldDateTimeArr2 = Utils.Default<DateTime[]>();
            _prFieldDateTimeArr3 = Utils.EmptyArray<DateTime>();
            _prFieldDateTimeArr4 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeArr0 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            _pubFieldDateTimeArr2 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeArr3 = Utils.EmptyArray<DateTime>();
            _pubFieldDateTimeArr4 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr0 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeArr2 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr3 = Utils.EmptyArray<DateTime>();
            PropertyDateTimeArr4 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr5 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeArr6 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr7 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeArr8 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr9 = Utils.EmptyArray<DateTime>();
            PropertyDateTimeArr10 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr11 = Utils.RandomArray<DateTime>(r);
        }
    }
}