using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt16_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListUInt16Class();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListUInt16Class) s2.Deserialize(typeof (ListUInt16Class));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListUInt16Class {
        private List<UInt16> _prFieldListList10;
        private List<UInt16> _prFieldListList11;
        private List<UInt16> _prFieldListList12;
        private List<UInt16> _prFieldListList13;
        private List<UInt16> _prFieldListList14;

        public List<UInt16> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<UInt16> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<UInt16> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<UInt16> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<UInt16> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<UInt16> _pubFieldListList10;
        public List<UInt16> _pubFieldListList11;
        public List<UInt16> _pubFieldListList12;
        public List<UInt16> _pubFieldListList13;
        public List<UInt16> _pubFieldListList14;
        public List<UInt16> PropertyListList10 { get; set; }
        public List<UInt16> PropertyListList11 { get; set; }
        public List<UInt16> PropertyListList12 { get; set; }
        public List<UInt16> PropertyListList13 { get; set; }
        public List<UInt16> PropertyListList14 { get; set; }
        public List<UInt16> PropertyListList15 { get; set; }
        public List<UInt16> PropertyListList16 { get; set; }
        public List<UInt16> PropertyListList17 { get; set; }
        public List<UInt16> PropertyListList18 { get; set; }
        public List<UInt16> PropertyListList19 { get; set; }
        public List<UInt16> PropertyListList110 { get; set; }
        public List<UInt16> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<UInt16>>();
            _prFieldListList11 = Utils.RandomList<UInt16>(r);
            _prFieldListList12 = Utils.Default<List<UInt16>>();
            _prFieldListList13 = Utils.EmptyList<UInt16>();
            _prFieldListList14 = Utils.Default<List<UInt16>>();
            _pubFieldListList10 = Utils.Default<List<UInt16>>();
            _pubFieldListList11 = Utils.RandomList<UInt16>(r);
            _pubFieldListList12 = Utils.Default<List<UInt16>>();
            _pubFieldListList13 = Utils.EmptyList<UInt16>();
            _pubFieldListList14 = Utils.Default<List<UInt16>>();
            PropertyListList10 = Utils.Default<List<UInt16>>();
            PropertyListList11 = Utils.RandomList<UInt16>(r);
            PropertyListList12 = Utils.Default<List<UInt16>>();
            PropertyListList13 = Utils.EmptyList<UInt16>();
            PropertyListList14 = Utils.Default<List<UInt16>>();
            PropertyListList15 = Utils.RandomList<UInt16>(r);
            PropertyListList16 = Utils.Default<List<UInt16>>();
            PropertyListList17 = Utils.RandomList<UInt16>(r);
            PropertyListList18 = Utils.Default<List<UInt16>>();
            PropertyListList19 = Utils.EmptyList<UInt16>();
            PropertyListList110 = Utils.Default<List<UInt16>>();
            PropertyListList111 = Utils.RandomList<UInt16>(r);
        }
    }
}