﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Char_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new CharArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (CharArrayClass) s2.Deserialize(typeof (CharArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldCharArr0, o2.PrFieldCharArr0);
                CollectionAssert.AreEqual(o1.PrFieldCharArr1, o2.PrFieldCharArr1);
                CollectionAssert.AreEqual(o1.PrFieldCharArr2, o2.PrFieldCharArr2);
                CollectionAssert.AreEqual(o1.PrFieldCharArr3, o2.PrFieldCharArr3);
                CollectionAssert.AreEqual(o1.PrFieldCharArr4, o2.PrFieldCharArr4);
                CollectionAssert.AreEqual(o1._pubFieldCharArr0, o2._pubFieldCharArr0);
                CollectionAssert.AreEqual(o1._pubFieldCharArr1, o2._pubFieldCharArr1);
                CollectionAssert.AreEqual(o1._pubFieldCharArr2, o2._pubFieldCharArr2);
                CollectionAssert.AreEqual(o1._pubFieldCharArr3, o2._pubFieldCharArr3);
                CollectionAssert.AreEqual(o1._pubFieldCharArr4, o2._pubFieldCharArr4);
                CollectionAssert.AreEqual(o1.PropertyCharArr0, o2.PropertyCharArr0);
                CollectionAssert.AreEqual(o1.PropertyCharArr1, o2.PropertyCharArr1);
                CollectionAssert.AreEqual(o1.PropertyCharArr2, o2.PropertyCharArr2);
                CollectionAssert.AreEqual(o1.PropertyCharArr3, o2.PropertyCharArr3);
                CollectionAssert.AreEqual(o1.PropertyCharArr4, o2.PropertyCharArr4);
                CollectionAssert.AreEqual(o1.PropertyCharArr5, o2.PropertyCharArr5);
                CollectionAssert.AreEqual(o1.PropertyCharArr6, o2.PropertyCharArr6);
                CollectionAssert.AreEqual(o1.PropertyCharArr7, o2.PropertyCharArr7);
                CollectionAssert.AreEqual(o1.PropertyCharArr8, o2.PropertyCharArr8);
                CollectionAssert.AreEqual(o1.PropertyCharArr9, o2.PropertyCharArr9);
                CollectionAssert.AreEqual(o1.PropertyCharArr10, o2.PropertyCharArr10);
                CollectionAssert.AreEqual(o1.PropertyCharArr11, o2.PropertyCharArr11);
            }
        }
    }

    [Serializable]
    internal class CharArrayClass {
        private Char[] _prFieldCharArr0;
        private Char[] _prFieldCharArr1;
        private Char[] _prFieldCharArr2;
        private Char[] _prFieldCharArr3;
        private Char[] _prFieldCharArr4;

        public Char[] PrFieldCharArr0 {
            get { return _prFieldCharArr0; }
        }

        public Char[] PrFieldCharArr1 {
            get { return _prFieldCharArr1; }
        }

        public Char[] PrFieldCharArr2 {
            get { return _prFieldCharArr2; }
        }

        public Char[] PrFieldCharArr3 {
            get { return _prFieldCharArr3; }
        }

        public Char[] PrFieldCharArr4 {
            get { return _prFieldCharArr4; }
        }

        public Char[] _pubFieldCharArr0;
        public Char[] _pubFieldCharArr1;
        public Char[] _pubFieldCharArr2;
        public Char[] _pubFieldCharArr3;
        public Char[] _pubFieldCharArr4;
        public Char[] PropertyCharArr0 { get; set; }
        public Char[] PropertyCharArr1 { get; set; }
        public Char[] PropertyCharArr2 { get; set; }
        public Char[] PropertyCharArr3 { get; set; }
        public Char[] PropertyCharArr4 { get; set; }
        public Char[] PropertyCharArr5 { get; set; }
        public Char[] PropertyCharArr6 { get; set; }
        public Char[] PropertyCharArr7 { get; set; }
        public Char[] PropertyCharArr8 { get; set; }
        public Char[] PropertyCharArr9 { get; set; }
        public Char[] PropertyCharArr10 { get; set; }
        public Char[] PropertyCharArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldCharArr0 = Utils.Default<Char[]>();
            _prFieldCharArr1 = Utils.RandomArray<Char>(r);
            _prFieldCharArr2 = Utils.Default<Char[]>();
            _prFieldCharArr3 = Utils.EmptyArray<Char>();
            _prFieldCharArr4 = Utils.Default<Char[]>();
            _pubFieldCharArr0 = Utils.Default<Char[]>();
            _pubFieldCharArr1 = Utils.RandomArray<Char>(r);
            _pubFieldCharArr2 = Utils.Default<Char[]>();
            _pubFieldCharArr3 = Utils.EmptyArray<Char>();
            _pubFieldCharArr4 = Utils.Default<Char[]>();
            PropertyCharArr0 = Utils.Default<Char[]>();
            PropertyCharArr1 = Utils.RandomArray<Char>(r);
            PropertyCharArr2 = Utils.Default<Char[]>();
            PropertyCharArr3 = Utils.EmptyArray<Char>();
            PropertyCharArr4 = Utils.Default<Char[]>();
            PropertyCharArr5 = Utils.RandomArray<Char>(r);
            PropertyCharArr6 = Utils.Default<Char[]>();
            PropertyCharArr7 = Utils.RandomArray<Char>(r);
            PropertyCharArr8 = Utils.Default<Char[]>();
            PropertyCharArr9 = Utils.EmptyArray<Char>();
            PropertyCharArr10 = Utils.Default<Char[]>();
            PropertyCharArr11 = Utils.RandomArray<Char>(r);
        }
    }
}