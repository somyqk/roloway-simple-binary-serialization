using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Int32_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListInt32Class();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListInt32Class) s2.Deserialize(typeof (ListInt32Class));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListInt32Class {
        private List<Int32> _prFieldListList10;
        private List<Int32> _prFieldListList11;
        private List<Int32> _prFieldListList12;
        private List<Int32> _prFieldListList13;
        private List<Int32> _prFieldListList14;

        public List<Int32> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<Int32> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<Int32> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<Int32> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<Int32> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<Int32> _pubFieldListList10;
        public List<Int32> _pubFieldListList11;
        public List<Int32> _pubFieldListList12;
        public List<Int32> _pubFieldListList13;
        public List<Int32> _pubFieldListList14;
        public List<Int32> PropertyListList10 { get; set; }
        public List<Int32> PropertyListList11 { get; set; }
        public List<Int32> PropertyListList12 { get; set; }
        public List<Int32> PropertyListList13 { get; set; }
        public List<Int32> PropertyListList14 { get; set; }
        public List<Int32> PropertyListList15 { get; set; }
        public List<Int32> PropertyListList16 { get; set; }
        public List<Int32> PropertyListList17 { get; set; }
        public List<Int32> PropertyListList18 { get; set; }
        public List<Int32> PropertyListList19 { get; set; }
        public List<Int32> PropertyListList110 { get; set; }
        public List<Int32> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<Int32>>();
            _prFieldListList11 = Utils.RandomList<Int32>(r);
            _prFieldListList12 = Utils.Default<List<Int32>>();
            _prFieldListList13 = Utils.EmptyList<Int32>();
            _prFieldListList14 = Utils.Default<List<Int32>>();
            _pubFieldListList10 = Utils.Default<List<Int32>>();
            _pubFieldListList11 = Utils.RandomList<Int32>(r);
            _pubFieldListList12 = Utils.Default<List<Int32>>();
            _pubFieldListList13 = Utils.EmptyList<Int32>();
            _pubFieldListList14 = Utils.Default<List<Int32>>();
            PropertyListList10 = Utils.Default<List<Int32>>();
            PropertyListList11 = Utils.RandomList<Int32>(r);
            PropertyListList12 = Utils.Default<List<Int32>>();
            PropertyListList13 = Utils.EmptyList<Int32>();
            PropertyListList14 = Utils.Default<List<Int32>>();
            PropertyListList15 = Utils.RandomList<Int32>(r);
            PropertyListList16 = Utils.Default<List<Int32>>();
            PropertyListList17 = Utils.RandomList<Int32>(r);
            PropertyListList18 = Utils.Default<List<Int32>>();
            PropertyListList19 = Utils.EmptyList<Int32>();
            PropertyListList110 = Utils.Default<List<Int32>>();
            PropertyListList111 = Utils.RandomList<Int32>(r);
        }
    }
}