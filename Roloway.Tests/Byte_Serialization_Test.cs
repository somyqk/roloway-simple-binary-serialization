﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Byte_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new BytesClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (BytesClass) s2.Deserialize(typeof (BytesClass));

                Assert.AreEqual(o1.PrFieldByte0, o2.PrFieldByte0);
                Assert.AreEqual(o1.PrFieldByte1, o2.PrFieldByte1);
                Assert.AreEqual(o1.PrFieldByte2, o2.PrFieldByte2);
                Assert.AreEqual(o1.PrFieldByte3, o2.PrFieldByte3);
                Assert.AreEqual(o1.PrFieldByte4, o2.PrFieldByte4);
                Assert.AreEqual(o1._pubFieldByte0, o2._pubFieldByte0);
                Assert.AreEqual(o1._pubFieldByte1, o2._pubFieldByte1);
                Assert.AreEqual(o1._pubFieldByte2, o2._pubFieldByte2);
                Assert.AreEqual(o1._pubFieldByte3, o2._pubFieldByte3);
                Assert.AreEqual(o1._pubFieldByte4, o2._pubFieldByte4);
                Assert.AreEqual(o1.PropertyByte0, o2.PropertyByte0);
                Assert.AreEqual(o1.PropertyByte1, o2.PropertyByte1);
                Assert.AreEqual(o1.PropertyByte2, o2.PropertyByte2);
                Assert.AreEqual(o1.PropertyByte3, o2.PropertyByte3);
                Assert.AreEqual(o1.PropertyByte4, o2.PropertyByte4);
                Assert.AreEqual(o1.PropertyByte5, o2.PropertyByte5);
                Assert.AreEqual(o1.PropertyByte6, o2.PropertyByte6);
                Assert.AreEqual(o1.PropertyByte7, o2.PropertyByte7);
                Assert.AreEqual(o1.PropertyByte8, o2.PropertyByte8);
                Assert.AreEqual(o1.PropertyByte9, o2.PropertyByte9);
                Assert.AreEqual(o1.PropertyByte10, o2.PropertyByte10);
                Assert.AreEqual(o1.PropertyByte11, o2.PropertyByte11);
            }
        }
    }

    [Serializable]
    internal class BytesClass {
        private Byte _prFieldByte0;
        private Byte _prFieldByte1;
        private Byte _prFieldByte2;
        private Byte _prFieldByte3;
        private Byte _prFieldByte4;

        public Byte PrFieldByte0 {
            get { return _prFieldByte0; }
        }

        public Byte PrFieldByte1 {
            get { return _prFieldByte1; }
        }

        public Byte PrFieldByte2 {
            get { return _prFieldByte2; }
        }

        public Byte PrFieldByte3 {
            get { return _prFieldByte3; }
        }

        public Byte PrFieldByte4 {
            get { return _prFieldByte4; }
        }

        public Byte _pubFieldByte0;
        public Byte _pubFieldByte1;
        public Byte _pubFieldByte2;
        public Byte _pubFieldByte3;
        public Byte _pubFieldByte4;
        public Byte PropertyByte0 { get; set; }
        public Byte PropertyByte1 { get; set; }
        public Byte PropertyByte2 { get; set; }
        public Byte PropertyByte3 { get; set; }
        public Byte PropertyByte4 { get; set; }
        public Byte PropertyByte5 { get; set; }
        public Byte PropertyByte6 { get; set; }
        public Byte PropertyByte7 { get; set; }
        public Byte PropertyByte8 { get; set; }
        public Byte PropertyByte9 { get; set; }
        public Byte PropertyByte10 { get; set; }
        public Byte PropertyByte11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldByte0 = Utils.Default<Byte>();
            _prFieldByte1 = Utils.RandomValue<Byte>(r);
            _prFieldByte2 = Utils.Default<Byte>();
            _prFieldByte3 = Utils.RandomValue<Byte>(r);
            _prFieldByte4 = Utils.Default<Byte>();
            _pubFieldByte0 = Utils.Default<Byte>();
            _pubFieldByte1 = Utils.RandomValue<Byte>(r);
            _pubFieldByte2 = Utils.Default<Byte>();
            _pubFieldByte3 = Utils.RandomValue<Byte>(r);
            _pubFieldByte4 = Utils.Default<Byte>();
            PropertyByte0 = Utils.Default<Byte>();
            PropertyByte1 = Utils.RandomValue<Byte>(r);
            PropertyByte2 = Utils.RandomValue<Byte>(r);
            PropertyByte3 = Utils.Default<Byte>();
            PropertyByte4 = Utils.RandomValue<Byte>(r);
            PropertyByte5 = Utils.RandomValue<Byte>(r);
            PropertyByte6 = Utils.Default<Byte>();
            PropertyByte7 = Utils.RandomValue<Byte>(r);
            PropertyByte8 = Utils.RandomValue<Byte>(r);
            PropertyByte9 = Utils.Default<Byte>();
            PropertyByte10 = Utils.RandomValue<Byte>(r);
            PropertyByte11 = Utils.RandomValue<Byte>(r);
        }
    }
}