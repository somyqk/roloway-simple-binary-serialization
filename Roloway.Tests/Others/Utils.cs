﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roloway.Tests.Others {
    public static class Utils {
        public static T RandomValue<T>(Random random) {
            string chars = "abcd efgh ijkl mnop qrsu vwxy zABC DEFG HIJK LMNO PQRS TUVW XYZ0 1234 5678 9!@# $%^& *()- _=+{ }:\" <>., /?|\\`";
            object res = null;

            if (typeof (IRandomizable).IsAssignableFrom(typeof (T))) {
                var randomizable = (IRandomizable) Activator.CreateInstance<T>();
                randomizable.Randomize(random);

                res = randomizable;
            }
            else if (typeof (T).IsEnum) {
                var values = typeof (T).GetEnumValues();
                res = values.GetValue(random.Next(0, values.Length - 1));
            }
            else if (typeof (T) == typeof (Byte)) {
                res = (Byte) random.Next(Byte.MinValue, Byte.MaxValue);
            }
            else if (typeof (T) == typeof (SByte)) {
                res = (SByte) random.Next(SByte.MinValue, SByte.MaxValue);
            }
            else if (typeof (T) == typeof (UInt16)) {
                res = (UInt16) random.Next(UInt16.MinValue, UInt16.MaxValue);
            }
            else if (typeof (T) == typeof (Int16)) {
                res = (Int16) random.Next(Int16.MinValue, Int16.MaxValue);
            }
            else if (typeof (T) == typeof (UInt32)) {
                var bytes = new byte[4];
                random.NextBytes(bytes);
                res = BitConverter.ToUInt32(bytes, 0);
            }
            else if (typeof (T) == typeof (Int32)) {
                res = random.Next(Int32.MinValue, Int32.MaxValue);
            }
            else if (typeof (T) == typeof (UInt64)) {
                var bytes = new byte[8];
                random.NextBytes(bytes);
                res = BitConverter.ToUInt64(bytes, 0);
            }
            else if (typeof (T) == typeof (Int64)) {
                var bytes = new byte[8];
                random.NextBytes(bytes);
                res = BitConverter.ToInt64(bytes, 0);
            }
            else if (typeof (T) == typeof (Single)) {
                var bytes = new byte[4];
                random.NextBytes(bytes);
                res = BitConverter.ToSingle(bytes, 0);
            }
            else if (typeof (T) == typeof (Double)) {
                var bytes = new byte[8];
                random.NextBytes(bytes);
                res = BitConverter.ToDouble(bytes, 0);
            }
            else if (typeof (T) == typeof (Boolean)) {
                res = random.Next()%2 == 0;
            }
            else if (typeof (T) == typeof (Char)) {
                res = chars[random.Next(0, chars.Length - 1)];
            }
            else if (typeof (T) == typeof (DateTime)) {
                long l = DateTime.MinValue.Ticks;
                while (l <= DateTime.MinValue.Ticks || l >= DateTime.MaxValue.Ticks) {
                    l = RandomValue<long>(random);
                }
                res = DateTime.FromBinary(l);
            }
            else if (typeof (T) == typeof (Decimal)) {
                res = NextDecimal(random);
            }
            else if (typeof (T) == typeof (String)) {
                var length = random.Next(0, 1000);
                var builder = new StringBuilder();
                for (int i = 0; i < length; i++) {
                    builder.Append(chars[random.Next(0, chars.Length - 1)]);
                }
                res = builder.ToString();
            }
            return (T) res;
        }

        public static T[] EmptyArray<T>() {
            return new T[0];
        }

        public static List<T> EmptyList<T>() {
            return new List<T>();
        }

        public static T[] RandomArray<T>(Random random) {
            var res = new T[100];
            for (int i = 0; i < 100; i++) {
                res[i] = RandomValue<T>(random);
            }

            return res;
        }

        public static List<T> RandomList<T>(Random random) {
            var res = new List<T>();
            for (int i = 0; i < 100; i++) {
                res.Add(RandomValue<T>(random));
            }
            return res;
        }

        public static T Default<T>() {
            return default(T);
        }

        private static int NextInt32(Random random) {
            unchecked {
                int firstBits = random.Next(0, 1 << 4) << 28;
                int lastBits = random.Next(0, 1 << 28);
                return firstBits | lastBits;
            }
        }

        private static decimal NextDecimal(Random random) {
            byte scale = (byte) random.Next(29);
            bool sign = random.Next(2) == 1;
            return new decimal(NextInt32(random), NextInt32(random), NextInt32(random), sign, scale);
        }
    }
}