﻿using System;

namespace Roloway.Tests.Others {
    internal interface IRandomizable {
        void Randomize(Random random);
    }

    [Serializable]
    internal struct RandomizableStruct : IRandomizable {
        private int field1;
        private double field2;
        private string field3;
        private long[] field4;

        public void Randomize(Random random) {
            field1 = Utils.RandomValue<Int32>(random);
            field2 = Utils.RandomValue<Double>(random);
            field3 = Utils.RandomValue<String>(random);
            field4 = Utils.RandomArray<Int64>(random);
        }

        public override bool Equals(object obj) {
            if (obj == null) {
                return false;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            return Equals((RandomizableStruct) obj);
        }

        public bool Equals(RandomizableStruct other) {
            var equal = field1 == other.field1 && field2.Equals(other.field2) && field3 == other.field3;
            if (equal) {
                if (field4 == null) {
                    return other.field4 == null;
                }

                for (int i = 0; i < field4.Length; i++) {
                    if (field4[i] != other.field4[i]) {
                        return false;
                    }
                }
            }

            return equal;
        }
    }

    [Serializable]
    internal class Randomizable : IRandomizable {
        private int field1;
        private double field2;
        private string field3;
        private long[] field4;

        public void Randomize(Random random) {
            field1 = Utils.RandomValue<Int32>(random);
            field2 = Utils.RandomValue<Double>(random);
            field3 = Utils.RandomValue<String>(random);
            field4 = Utils.RandomArray<Int64>(random);
        }

        public override bool Equals(object obj) {
            if (obj == null) {
                return false;
            }
            if (obj.GetType() != GetType()) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            return Equals((Randomizable) obj);
        }

        public bool Equals(Randomizable other) {
            if (other == null) {
                return false;
            }
            var equal = field1 == other.field1 && field2.Equals(other.field2) && field3 == other.field3;
            if (equal) {
                for (int i = 0; i < field4.Length; i++) {
                    if (field4[i] != other.field4[i]) {
                        return false;
                    }
                }
            }

            return equal;
        }
    }
}