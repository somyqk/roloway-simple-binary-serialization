﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt16_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new UInt16sClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (UInt16sClass) s2.Deserialize(typeof (UInt16sClass));

                Assert.AreEqual(o1.PrFieldUInt160, o2.PrFieldUInt160);
                Assert.AreEqual(o1.PrFieldUInt161, o2.PrFieldUInt161);
                Assert.AreEqual(o1.PrFieldUInt162, o2.PrFieldUInt162);
                Assert.AreEqual(o1.PrFieldUInt163, o2.PrFieldUInt163);
                Assert.AreEqual(o1.PrFieldUInt164, o2.PrFieldUInt164);
                Assert.AreEqual(o1._pubFieldUInt160, o2._pubFieldUInt160);
                Assert.AreEqual(o1._pubFieldUInt161, o2._pubFieldUInt161);
                Assert.AreEqual(o1._pubFieldUInt162, o2._pubFieldUInt162);
                Assert.AreEqual(o1._pubFieldUInt163, o2._pubFieldUInt163);
                Assert.AreEqual(o1._pubFieldUInt164, o2._pubFieldUInt164);
                Assert.AreEqual(o1.PropertyUInt160, o2.PropertyUInt160);
                Assert.AreEqual(o1.PropertyUInt161, o2.PropertyUInt161);
                Assert.AreEqual(o1.PropertyUInt162, o2.PropertyUInt162);
                Assert.AreEqual(o1.PropertyUInt163, o2.PropertyUInt163);
                Assert.AreEqual(o1.PropertyUInt164, o2.PropertyUInt164);
                Assert.AreEqual(o1.PropertyUInt165, o2.PropertyUInt165);
                Assert.AreEqual(o1.PropertyUInt166, o2.PropertyUInt166);
                Assert.AreEqual(o1.PropertyUInt167, o2.PropertyUInt167);
                Assert.AreEqual(o1.PropertyUInt168, o2.PropertyUInt168);
                Assert.AreEqual(o1.PropertyUInt169, o2.PropertyUInt169);
                Assert.AreEqual(o1.PropertyUInt1610, o2.PropertyUInt1610);
                Assert.AreEqual(o1.PropertyUInt1611, o2.PropertyUInt1611);
            }
        }
    }

    [Serializable]
    internal class UInt16sClass {
        private UInt16 _prFieldUInt160;
        private UInt16 _prFieldUInt161;
        private UInt16 _prFieldUInt162;
        private UInt16 _prFieldUInt163;
        private UInt16 _prFieldUInt164;

        public UInt16 PrFieldUInt160 {
            get { return _prFieldUInt160; }
        }

        public UInt16 PrFieldUInt161 {
            get { return _prFieldUInt161; }
        }

        public UInt16 PrFieldUInt162 {
            get { return _prFieldUInt162; }
        }

        public UInt16 PrFieldUInt163 {
            get { return _prFieldUInt163; }
        }

        public UInt16 PrFieldUInt164 {
            get { return _prFieldUInt164; }
        }

        public UInt16 _pubFieldUInt160;
        public UInt16 _pubFieldUInt161;
        public UInt16 _pubFieldUInt162;
        public UInt16 _pubFieldUInt163;
        public UInt16 _pubFieldUInt164;
        public UInt16 PropertyUInt160 { get; set; }
        public UInt16 PropertyUInt161 { get; set; }
        public UInt16 PropertyUInt162 { get; set; }
        public UInt16 PropertyUInt163 { get; set; }
        public UInt16 PropertyUInt164 { get; set; }
        public UInt16 PropertyUInt165 { get; set; }
        public UInt16 PropertyUInt166 { get; set; }
        public UInt16 PropertyUInt167 { get; set; }
        public UInt16 PropertyUInt168 { get; set; }
        public UInt16 PropertyUInt169 { get; set; }
        public UInt16 PropertyUInt1610 { get; set; }
        public UInt16 PropertyUInt1611 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldUInt160 = Utils.Default<UInt16>();
            _prFieldUInt161 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt162 = Utils.Default<UInt16>();
            _prFieldUInt163 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt164 = Utils.Default<UInt16>();
            _pubFieldUInt160 = Utils.Default<UInt16>();
            _pubFieldUInt161 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt162 = Utils.Default<UInt16>();
            _pubFieldUInt163 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt164 = Utils.Default<UInt16>();
            PropertyUInt160 = Utils.Default<UInt16>();
            PropertyUInt161 = Utils.RandomValue<UInt16>(r);
            PropertyUInt162 = Utils.RandomValue<UInt16>(r);
            PropertyUInt163 = Utils.Default<UInt16>();
            PropertyUInt164 = Utils.RandomValue<UInt16>(r);
            PropertyUInt165 = Utils.RandomValue<UInt16>(r);
            PropertyUInt166 = Utils.Default<UInt16>();
            PropertyUInt167 = Utils.RandomValue<UInt16>(r);
            PropertyUInt168 = Utils.RandomValue<UInt16>(r);
            PropertyUInt169 = Utils.Default<UInt16>();
            PropertyUInt1610 = Utils.RandomValue<UInt16>(r);
            PropertyUInt1611 = Utils.RandomValue<UInt16>(r);
        }
    }
}