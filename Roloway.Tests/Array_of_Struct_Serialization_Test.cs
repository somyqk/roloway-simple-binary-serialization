﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Array_of_Struct_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ArrayStructClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ArrayStructClass) s2.Deserialize(typeof (ArrayStructClass));

                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr0, o2.PrFieldRandomizableStructArr0);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr1, o2.PrFieldRandomizableStructArr1);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr2, o2.PrFieldRandomizableStructArr2);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr3, o2.PrFieldRandomizableStructArr3);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr4, o2.PrFieldRandomizableStructArr4);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr0, o2._pubFieldRandomizableStructArr0);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr1, o2._pubFieldRandomizableStructArr1);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr2, o2._pubFieldRandomizableStructArr2);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr3, o2._pubFieldRandomizableStructArr3);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr4, o2._pubFieldRandomizableStructArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr0, o2.PropertyRandomizableStructArr0);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr1, o2.PropertyRandomizableStructArr1);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr2, o2.PropertyRandomizableStructArr2);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr3, o2.PropertyRandomizableStructArr3);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr4, o2.PropertyRandomizableStructArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr5, o2.PropertyRandomizableStructArr5);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr6, o2.PropertyRandomizableStructArr6);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr7, o2.PropertyRandomizableStructArr7);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr8, o2.PropertyRandomizableStructArr8);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr9, o2.PropertyRandomizableStructArr9);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr10, o2.PropertyRandomizableStructArr10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr11, o2.PropertyRandomizableStructArr11);
            }
        }
    }

    [Serializable]
    internal class ArrayStructClass {
        private RandomizableStruct[] _prFieldRandomizableStructArr0;
        private RandomizableStruct[] _prFieldRandomizableStructArr1;
        private RandomizableStruct[] _prFieldRandomizableStructArr2;
        private RandomizableStruct[] _prFieldRandomizableStructArr3;
        private RandomizableStruct[] _prFieldRandomizableStructArr4;

        public RandomizableStruct[] PrFieldRandomizableStructArr0 {
            get { return _prFieldRandomizableStructArr0; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr1 {
            get { return _prFieldRandomizableStructArr1; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr2 {
            get { return _prFieldRandomizableStructArr2; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr3 {
            get { return _prFieldRandomizableStructArr3; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr4 {
            get { return _prFieldRandomizableStructArr4; }
        }

        public RandomizableStruct[] _pubFieldRandomizableStructArr0;
        public RandomizableStruct[] _pubFieldRandomizableStructArr1;
        public RandomizableStruct[] _pubFieldRandomizableStructArr2;
        public RandomizableStruct[] _pubFieldRandomizableStructArr3;
        public RandomizableStruct[] _pubFieldRandomizableStructArr4;
        public RandomizableStruct[] PropertyRandomizableStructArr0 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr1 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr2 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr3 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr4 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr5 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr6 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr7 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr8 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr9 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr10 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            _prFieldRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            _prFieldRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            _prFieldRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            _prFieldRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            _pubFieldRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            _pubFieldRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            PropertyRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr5 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructArr6 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr7 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructArr8 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr9 = Utils.EmptyArray<RandomizableStruct>();
            PropertyRandomizableStructArr10 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr11 = Utils.RandomArray<RandomizableStruct>(r);
        }
    }
}