﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Int64_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new Int64sClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (Int64sClass) s2.Deserialize(typeof (Int64sClass));

                Assert.AreEqual(o1.PrFieldInt640, o2.PrFieldInt640);
                Assert.AreEqual(o1.PrFieldInt641, o2.PrFieldInt641);
                Assert.AreEqual(o1.PrFieldInt642, o2.PrFieldInt642);
                Assert.AreEqual(o1.PrFieldInt643, o2.PrFieldInt643);
                Assert.AreEqual(o1.PrFieldInt644, o2.PrFieldInt644);
                Assert.AreEqual(o1._pubFieldInt640, o2._pubFieldInt640);
                Assert.AreEqual(o1._pubFieldInt641, o2._pubFieldInt641);
                Assert.AreEqual(o1._pubFieldInt642, o2._pubFieldInt642);
                Assert.AreEqual(o1._pubFieldInt643, o2._pubFieldInt643);
                Assert.AreEqual(o1._pubFieldInt644, o2._pubFieldInt644);
                Assert.AreEqual(o1.PropertyInt640, o2.PropertyInt640);
                Assert.AreEqual(o1.PropertyInt641, o2.PropertyInt641);
                Assert.AreEqual(o1.PropertyInt642, o2.PropertyInt642);
                Assert.AreEqual(o1.PropertyInt643, o2.PropertyInt643);
                Assert.AreEqual(o1.PropertyInt644, o2.PropertyInt644);
                Assert.AreEqual(o1.PropertyInt645, o2.PropertyInt645);
                Assert.AreEqual(o1.PropertyInt646, o2.PropertyInt646);
                Assert.AreEqual(o1.PropertyInt647, o2.PropertyInt647);
                Assert.AreEqual(o1.PropertyInt648, o2.PropertyInt648);
                Assert.AreEqual(o1.PropertyInt649, o2.PropertyInt649);
                Assert.AreEqual(o1.PropertyInt6410, o2.PropertyInt6410);
                Assert.AreEqual(o1.PropertyInt6411, o2.PropertyInt6411);
            }
        }
    }

    [Serializable]
    internal class Int64sClass {
        private Int64 _prFieldInt640;
        private Int64 _prFieldInt641;
        private Int64 _prFieldInt642;
        private Int64 _prFieldInt643;
        private Int64 _prFieldInt644;

        public Int64 PrFieldInt640 {
            get { return _prFieldInt640; }
        }

        public Int64 PrFieldInt641 {
            get { return _prFieldInt641; }
        }

        public Int64 PrFieldInt642 {
            get { return _prFieldInt642; }
        }

        public Int64 PrFieldInt643 {
            get { return _prFieldInt643; }
        }

        public Int64 PrFieldInt644 {
            get { return _prFieldInt644; }
        }

        public Int64 _pubFieldInt640;
        public Int64 _pubFieldInt641;
        public Int64 _pubFieldInt642;
        public Int64 _pubFieldInt643;
        public Int64 _pubFieldInt644;
        public Int64 PropertyInt640 { get; set; }
        public Int64 PropertyInt641 { get; set; }
        public Int64 PropertyInt642 { get; set; }
        public Int64 PropertyInt643 { get; set; }
        public Int64 PropertyInt644 { get; set; }
        public Int64 PropertyInt645 { get; set; }
        public Int64 PropertyInt646 { get; set; }
        public Int64 PropertyInt647 { get; set; }
        public Int64 PropertyInt648 { get; set; }
        public Int64 PropertyInt649 { get; set; }
        public Int64 PropertyInt6410 { get; set; }
        public Int64 PropertyInt6411 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldInt640 = Utils.Default<Int64>();
            _prFieldInt641 = Utils.RandomValue<Int64>(r);
            _prFieldInt642 = Utils.Default<Int64>();
            _prFieldInt643 = Utils.RandomValue<Int64>(r);
            _prFieldInt644 = Utils.Default<Int64>();
            _pubFieldInt640 = Utils.Default<Int64>();
            _pubFieldInt641 = Utils.RandomValue<Int64>(r);
            _pubFieldInt642 = Utils.Default<Int64>();
            _pubFieldInt643 = Utils.RandomValue<Int64>(r);
            _pubFieldInt644 = Utils.Default<Int64>();
            PropertyInt640 = Utils.Default<Int64>();
            PropertyInt641 = Utils.RandomValue<Int64>(r);
            PropertyInt642 = Utils.RandomValue<Int64>(r);
            PropertyInt643 = Utils.Default<Int64>();
            PropertyInt644 = Utils.RandomValue<Int64>(r);
            PropertyInt645 = Utils.RandomValue<Int64>(r);
            PropertyInt646 = Utils.Default<Int64>();
            PropertyInt647 = Utils.RandomValue<Int64>(r);
            PropertyInt648 = Utils.RandomValue<Int64>(r);
            PropertyInt649 = Utils.Default<Int64>();
            PropertyInt6410 = Utils.RandomValue<Int64>(r);
            PropertyInt6411 = Utils.RandomValue<Int64>(r);
        }
    }
}