﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt16_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new UInt16ArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (UInt16ArrayClass) s2.Deserialize(typeof (UInt16ArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr0, o2.PrFieldUInt16Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr1, o2.PrFieldUInt16Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr2, o2.PrFieldUInt16Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr3, o2.PrFieldUInt16Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr4, o2.PrFieldUInt16Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr0, o2._pubFieldUInt16Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr1, o2._pubFieldUInt16Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr2, o2._pubFieldUInt16Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr3, o2._pubFieldUInt16Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr4, o2._pubFieldUInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr0, o2.PropertyUInt16Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr1, o2.PropertyUInt16Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr2, o2.PropertyUInt16Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr3, o2.PropertyUInt16Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr4, o2.PropertyUInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr5, o2.PropertyUInt16Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr6, o2.PropertyUInt16Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr7, o2.PropertyUInt16Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr8, o2.PropertyUInt16Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr9, o2.PropertyUInt16Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr10, o2.PropertyUInt16Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr11, o2.PropertyUInt16Arr11);
            }
        }
    }

    [Serializable]
    internal class UInt16ArrayClass {
        private UInt16[] _prFieldUInt16Arr0;
        private UInt16[] _prFieldUInt16Arr1;
        private UInt16[] _prFieldUInt16Arr2;
        private UInt16[] _prFieldUInt16Arr3;
        private UInt16[] _prFieldUInt16Arr4;

        public UInt16[] PrFieldUInt16Arr0 {
            get { return _prFieldUInt16Arr0; }
        }

        public UInt16[] PrFieldUInt16Arr1 {
            get { return _prFieldUInt16Arr1; }
        }

        public UInt16[] PrFieldUInt16Arr2 {
            get { return _prFieldUInt16Arr2; }
        }

        public UInt16[] PrFieldUInt16Arr3 {
            get { return _prFieldUInt16Arr3; }
        }

        public UInt16[] PrFieldUInt16Arr4 {
            get { return _prFieldUInt16Arr4; }
        }

        public UInt16[] _pubFieldUInt16Arr0;
        public UInt16[] _pubFieldUInt16Arr1;
        public UInt16[] _pubFieldUInt16Arr2;
        public UInt16[] _pubFieldUInt16Arr3;
        public UInt16[] _pubFieldUInt16Arr4;
        public UInt16[] PropertyUInt16Arr0 { get; set; }
        public UInt16[] PropertyUInt16Arr1 { get; set; }
        public UInt16[] PropertyUInt16Arr2 { get; set; }
        public UInt16[] PropertyUInt16Arr3 { get; set; }
        public UInt16[] PropertyUInt16Arr4 { get; set; }
        public UInt16[] PropertyUInt16Arr5 { get; set; }
        public UInt16[] PropertyUInt16Arr6 { get; set; }
        public UInt16[] PropertyUInt16Arr7 { get; set; }
        public UInt16[] PropertyUInt16Arr8 { get; set; }
        public UInt16[] PropertyUInt16Arr9 { get; set; }
        public UInt16[] PropertyUInt16Arr10 { get; set; }
        public UInt16[] PropertyUInt16Arr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldUInt16Arr0 = Utils.Default<UInt16[]>();
            _prFieldUInt16Arr1 = Utils.RandomArray<UInt16>(r);
            _prFieldUInt16Arr2 = Utils.Default<UInt16[]>();
            _prFieldUInt16Arr3 = Utils.EmptyArray<UInt16>();
            _prFieldUInt16Arr4 = Utils.Default<UInt16[]>();
            _pubFieldUInt16Arr0 = Utils.Default<UInt16[]>();
            _pubFieldUInt16Arr1 = Utils.RandomArray<UInt16>(r);
            _pubFieldUInt16Arr2 = Utils.Default<UInt16[]>();
            _pubFieldUInt16Arr3 = Utils.EmptyArray<UInt16>();
            _pubFieldUInt16Arr4 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr0 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr1 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16Arr2 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr3 = Utils.EmptyArray<UInt16>();
            PropertyUInt16Arr4 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr5 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16Arr6 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr7 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16Arr8 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr9 = Utils.EmptyArray<UInt16>();
            PropertyUInt16Arr10 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr11 = Utils.RandomArray<UInt16>(r);
        }
    }
}