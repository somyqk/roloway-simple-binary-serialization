﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Single_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new SingleArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (SingleArrayClass) s2.Deserialize(typeof (SingleArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldSingleArr0, o2.PrFieldSingleArr0);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr1, o2.PrFieldSingleArr1);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr2, o2.PrFieldSingleArr2);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr3, o2.PrFieldSingleArr3);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr4, o2.PrFieldSingleArr4);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr0, o2._pubFieldSingleArr0);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr1, o2._pubFieldSingleArr1);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr2, o2._pubFieldSingleArr2);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr3, o2._pubFieldSingleArr3);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr4, o2._pubFieldSingleArr4);
                CollectionAssert.AreEqual(o1.PropertySingleArr0, o2.PropertySingleArr0);
                CollectionAssert.AreEqual(o1.PropertySingleArr1, o2.PropertySingleArr1);
                CollectionAssert.AreEqual(o1.PropertySingleArr2, o2.PropertySingleArr2);
                CollectionAssert.AreEqual(o1.PropertySingleArr3, o2.PropertySingleArr3);
                CollectionAssert.AreEqual(o1.PropertySingleArr4, o2.PropertySingleArr4);
                CollectionAssert.AreEqual(o1.PropertySingleArr5, o2.PropertySingleArr5);
                CollectionAssert.AreEqual(o1.PropertySingleArr6, o2.PropertySingleArr6);
                CollectionAssert.AreEqual(o1.PropertySingleArr7, o2.PropertySingleArr7);
                CollectionAssert.AreEqual(o1.PropertySingleArr8, o2.PropertySingleArr8);
                CollectionAssert.AreEqual(o1.PropertySingleArr9, o2.PropertySingleArr9);
                CollectionAssert.AreEqual(o1.PropertySingleArr10, o2.PropertySingleArr10);
                CollectionAssert.AreEqual(o1.PropertySingleArr11, o2.PropertySingleArr11);
            }
        }
    }

    [Serializable]
    internal class SingleArrayClass {
        private Single[] _prFieldSingleArr0;
        private Single[] _prFieldSingleArr1;
        private Single[] _prFieldSingleArr2;
        private Single[] _prFieldSingleArr3;
        private Single[] _prFieldSingleArr4;

        public Single[] PrFieldSingleArr0 {
            get { return _prFieldSingleArr0; }
        }

        public Single[] PrFieldSingleArr1 {
            get { return _prFieldSingleArr1; }
        }

        public Single[] PrFieldSingleArr2 {
            get { return _prFieldSingleArr2; }
        }

        public Single[] PrFieldSingleArr3 {
            get { return _prFieldSingleArr3; }
        }

        public Single[] PrFieldSingleArr4 {
            get { return _prFieldSingleArr4; }
        }

        public Single[] _pubFieldSingleArr0;
        public Single[] _pubFieldSingleArr1;
        public Single[] _pubFieldSingleArr2;
        public Single[] _pubFieldSingleArr3;
        public Single[] _pubFieldSingleArr4;
        public Single[] PropertySingleArr0 { get; set; }
        public Single[] PropertySingleArr1 { get; set; }
        public Single[] PropertySingleArr2 { get; set; }
        public Single[] PropertySingleArr3 { get; set; }
        public Single[] PropertySingleArr4 { get; set; }
        public Single[] PropertySingleArr5 { get; set; }
        public Single[] PropertySingleArr6 { get; set; }
        public Single[] PropertySingleArr7 { get; set; }
        public Single[] PropertySingleArr8 { get; set; }
        public Single[] PropertySingleArr9 { get; set; }
        public Single[] PropertySingleArr10 { get; set; }
        public Single[] PropertySingleArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldSingleArr0 = Utils.Default<Single[]>();
            _prFieldSingleArr1 = Utils.RandomArray<Single>(r);
            _prFieldSingleArr2 = Utils.Default<Single[]>();
            _prFieldSingleArr3 = Utils.EmptyArray<Single>();
            _prFieldSingleArr4 = Utils.Default<Single[]>();
            _pubFieldSingleArr0 = Utils.Default<Single[]>();
            _pubFieldSingleArr1 = Utils.RandomArray<Single>(r);
            _pubFieldSingleArr2 = Utils.Default<Single[]>();
            _pubFieldSingleArr3 = Utils.EmptyArray<Single>();
            _pubFieldSingleArr4 = Utils.Default<Single[]>();
            PropertySingleArr0 = Utils.Default<Single[]>();
            PropertySingleArr1 = Utils.RandomArray<Single>(r);
            PropertySingleArr2 = Utils.Default<Single[]>();
            PropertySingleArr3 = Utils.EmptyArray<Single>();
            PropertySingleArr4 = Utils.Default<Single[]>();
            PropertySingleArr5 = Utils.RandomArray<Single>(r);
            PropertySingleArr6 = Utils.Default<Single[]>();
            PropertySingleArr7 = Utils.RandomArray<Single>(r);
            PropertySingleArr8 = Utils.Default<Single[]>();
            PropertySingleArr9 = Utils.EmptyArray<Single>();
            PropertySingleArr10 = Utils.Default<Single[]>();
            PropertySingleArr11 = Utils.RandomArray<Single>(r);
        }
    }
}