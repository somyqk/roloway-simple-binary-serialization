﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Class_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ClassClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ClassClass) s2.Deserialize(typeof (ClassClass));

                Assert.AreEqual(o1.PrFieldRandomizable0, o2.PrFieldRandomizable0);
                Assert.AreEqual(o1.PrFieldRandomizable1, o2.PrFieldRandomizable1);
                Assert.AreEqual(o1.PrFieldRandomizable2, o2.PrFieldRandomizable2);
                Assert.AreEqual(o1.PrFieldRandomizable3, o2.PrFieldRandomizable3);
                Assert.AreEqual(o1.PrFieldRandomizable4, o2.PrFieldRandomizable4);
                Assert.AreEqual(o1._pubFieldRandomizable0, o2._pubFieldRandomizable0);
                Assert.AreEqual(o1._pubFieldRandomizable1, o2._pubFieldRandomizable1);
                Assert.AreEqual(o1._pubFieldRandomizable2, o2._pubFieldRandomizable2);
                Assert.AreEqual(o1._pubFieldRandomizable3, o2._pubFieldRandomizable3);
                Assert.AreEqual(o1._pubFieldRandomizable4, o2._pubFieldRandomizable4);
                Assert.AreEqual(o1.PropertyRandomizable0, o2.PropertyRandomizable0);
                Assert.AreEqual(o1.PropertyRandomizable1, o2.PropertyRandomizable1);
                Assert.AreEqual(o1.PropertyRandomizable2, o2.PropertyRandomizable2);
                Assert.AreEqual(o1.PropertyRandomizable3, o2.PropertyRandomizable3);
                Assert.AreEqual(o1.PropertyRandomizable4, o2.PropertyRandomizable4);
                Assert.AreEqual(o1.PropertyRandomizable5, o2.PropertyRandomizable5);
                Assert.AreEqual(o1.PropertyRandomizable6, o2.PropertyRandomizable6);
                Assert.AreEqual(o1.PropertyRandomizable7, o2.PropertyRandomizable7);
                Assert.AreEqual(o1.PropertyRandomizable8, o2.PropertyRandomizable8);
                Assert.AreEqual(o1.PropertyRandomizable9, o2.PropertyRandomizable9);
                Assert.AreEqual(o1.PropertyRandomizable10, o2.PropertyRandomizable10);
                Assert.AreEqual(o1.PropertyRandomizable11, o2.PropertyRandomizable11);
            }
        }
    }

    [Serializable]
    internal class ClassClass {
        private Randomizable _prFieldRandomizable0;
        private Randomizable _prFieldRandomizable1;
        private Randomizable _prFieldRandomizable2;
        private Randomizable _prFieldRandomizable3;
        private Randomizable _prFieldRandomizable4;

        public Randomizable PrFieldRandomizable0 {
            get { return _prFieldRandomizable0; }
        }

        public Randomizable PrFieldRandomizable1 {
            get { return _prFieldRandomizable1; }
        }

        public Randomizable PrFieldRandomizable2 {
            get { return _prFieldRandomizable2; }
        }

        public Randomizable PrFieldRandomizable3 {
            get { return _prFieldRandomizable3; }
        }

        public Randomizable PrFieldRandomizable4 {
            get { return _prFieldRandomizable4; }
        }

        public Randomizable _pubFieldRandomizable0;
        public Randomizable _pubFieldRandomizable1;
        public Randomizable _pubFieldRandomizable2;
        public Randomizable _pubFieldRandomizable3;
        public Randomizable _pubFieldRandomizable4;
        public Randomizable PropertyRandomizable0 { get; set; }
        public Randomizable PropertyRandomizable1 { get; set; }
        public Randomizable PropertyRandomizable2 { get; set; }
        public Randomizable PropertyRandomizable3 { get; set; }
        public Randomizable PropertyRandomizable4 { get; set; }
        public Randomizable PropertyRandomizable5 { get; set; }
        public Randomizable PropertyRandomizable6 { get; set; }
        public Randomizable PropertyRandomizable7 { get; set; }
        public Randomizable PropertyRandomizable8 { get; set; }
        public Randomizable PropertyRandomizable9 { get; set; }
        public Randomizable PropertyRandomizable10 { get; set; }
        public Randomizable PropertyRandomizable11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldRandomizable0 = Utils.Default<Randomizable>();
            _prFieldRandomizable1 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizable2 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizable3 = Utils.Default<Randomizable>();
            _prFieldRandomizable4 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable0 = Utils.Default<Randomizable>();
            _pubFieldRandomizable1 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable2 = Utils.Default<Randomizable>();
            _pubFieldRandomizable3 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable4 = Utils.Default<Randomizable>();
            PropertyRandomizable0 = Utils.Default<Randomizable>();
            PropertyRandomizable1 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable2 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable3 = Utils.Default<Randomizable>();
            PropertyRandomizable4 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable5 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable6 = Utils.Default<Randomizable>();
            PropertyRandomizable7 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable8 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable9 = Utils.Default<Randomizable>();
            PropertyRandomizable10 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable11 = Utils.RandomValue<Randomizable>(r);
        }
    }
}