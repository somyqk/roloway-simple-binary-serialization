using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class SByte_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListSByteClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListSByteClass) s2.Deserialize(typeof (ListSByteClass));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListSByteClass {
        private List<SByte> _prFieldListList10;
        private List<SByte> _prFieldListList11;
        private List<SByte> _prFieldListList12;
        private List<SByte> _prFieldListList13;
        private List<SByte> _prFieldListList14;

        public List<SByte> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<SByte> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<SByte> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<SByte> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<SByte> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<SByte> _pubFieldListList10;
        public List<SByte> _pubFieldListList11;
        public List<SByte> _pubFieldListList12;
        public List<SByte> _pubFieldListList13;
        public List<SByte> _pubFieldListList14;
        public List<SByte> PropertyListList10 { get; set; }
        public List<SByte> PropertyListList11 { get; set; }
        public List<SByte> PropertyListList12 { get; set; }
        public List<SByte> PropertyListList13 { get; set; }
        public List<SByte> PropertyListList14 { get; set; }
        public List<SByte> PropertyListList15 { get; set; }
        public List<SByte> PropertyListList16 { get; set; }
        public List<SByte> PropertyListList17 { get; set; }
        public List<SByte> PropertyListList18 { get; set; }
        public List<SByte> PropertyListList19 { get; set; }
        public List<SByte> PropertyListList110 { get; set; }
        public List<SByte> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<SByte>>();
            _prFieldListList11 = Utils.RandomList<SByte>(r);
            _prFieldListList12 = Utils.Default<List<SByte>>();
            _prFieldListList13 = Utils.EmptyList<SByte>();
            _prFieldListList14 = Utils.Default<List<SByte>>();
            _pubFieldListList10 = Utils.Default<List<SByte>>();
            _pubFieldListList11 = Utils.RandomList<SByte>(r);
            _pubFieldListList12 = Utils.Default<List<SByte>>();
            _pubFieldListList13 = Utils.EmptyList<SByte>();
            _pubFieldListList14 = Utils.Default<List<SByte>>();
            PropertyListList10 = Utils.Default<List<SByte>>();
            PropertyListList11 = Utils.RandomList<SByte>(r);
            PropertyListList12 = Utils.Default<List<SByte>>();
            PropertyListList13 = Utils.EmptyList<SByte>();
            PropertyListList14 = Utils.Default<List<SByte>>();
            PropertyListList15 = Utils.RandomList<SByte>(r);
            PropertyListList16 = Utils.Default<List<SByte>>();
            PropertyListList17 = Utils.RandomList<SByte>(r);
            PropertyListList18 = Utils.Default<List<SByte>>();
            PropertyListList19 = Utils.EmptyList<SByte>();
            PropertyListList110 = Utils.Default<List<SByte>>();
            PropertyListList111 = Utils.RandomList<SByte>(r);
        }
    }
}