using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Boolean_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListBooleanClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListBooleanClass) s2.Deserialize(typeof (ListBooleanClass));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListBooleanClass {
        private List<Boolean> _prFieldListList10;
        private List<Boolean> _prFieldListList11;
        private List<Boolean> _prFieldListList12;
        private List<Boolean> _prFieldListList13;
        private List<Boolean> _prFieldListList14;

        public List<Boolean> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<Boolean> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<Boolean> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<Boolean> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<Boolean> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<Boolean> _pubFieldListList10;
        public List<Boolean> _pubFieldListList11;
        public List<Boolean> _pubFieldListList12;
        public List<Boolean> _pubFieldListList13;
        public List<Boolean> _pubFieldListList14;
        public List<Boolean> PropertyListList10 { get; set; }
        public List<Boolean> PropertyListList11 { get; set; }
        public List<Boolean> PropertyListList12 { get; set; }
        public List<Boolean> PropertyListList13 { get; set; }
        public List<Boolean> PropertyListList14 { get; set; }
        public List<Boolean> PropertyListList15 { get; set; }
        public List<Boolean> PropertyListList16 { get; set; }
        public List<Boolean> PropertyListList17 { get; set; }
        public List<Boolean> PropertyListList18 { get; set; }
        public List<Boolean> PropertyListList19 { get; set; }
        public List<Boolean> PropertyListList110 { get; set; }
        public List<Boolean> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<Boolean>>();
            _prFieldListList11 = Utils.RandomList<Boolean>(r);
            _prFieldListList12 = Utils.Default<List<Boolean>>();
            _prFieldListList13 = Utils.EmptyList<Boolean>();
            _prFieldListList14 = Utils.Default<List<Boolean>>();
            _pubFieldListList10 = Utils.Default<List<Boolean>>();
            _pubFieldListList11 = Utils.RandomList<Boolean>(r);
            _pubFieldListList12 = Utils.Default<List<Boolean>>();
            _pubFieldListList13 = Utils.EmptyList<Boolean>();
            _pubFieldListList14 = Utils.Default<List<Boolean>>();
            PropertyListList10 = Utils.Default<List<Boolean>>();
            PropertyListList11 = Utils.RandomList<Boolean>(r);
            PropertyListList12 = Utils.Default<List<Boolean>>();
            PropertyListList13 = Utils.EmptyList<Boolean>();
            PropertyListList14 = Utils.Default<List<Boolean>>();
            PropertyListList15 = Utils.RandomList<Boolean>(r);
            PropertyListList16 = Utils.Default<List<Boolean>>();
            PropertyListList17 = Utils.RandomList<Boolean>(r);
            PropertyListList18 = Utils.Default<List<Boolean>>();
            PropertyListList19 = Utils.EmptyList<Boolean>();
            PropertyListList110 = Utils.Default<List<Boolean>>();
            PropertyListList111 = Utils.RandomList<Boolean>(r);
        }
    }
}