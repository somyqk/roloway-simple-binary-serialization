﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class All_Types_Mixed_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new MixedClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (MixedClass) s2.Deserialize(typeof (MixedClass));

                Assert.AreEqual(o1.PrFieldByte0, o2.PrFieldByte0);
                Assert.AreEqual(o1.PrFieldByte1, o2.PrFieldByte1);
                Assert.AreEqual(o1.PrFieldByte2, o2.PrFieldByte2);
                Assert.AreEqual(o1.PrFieldByte3, o2.PrFieldByte3);
                Assert.AreEqual(o1.PrFieldByte4, o2.PrFieldByte4);
                Assert.AreEqual(o1._pubFieldByte0, o2._pubFieldByte0);
                Assert.AreEqual(o1._pubFieldByte1, o2._pubFieldByte1);
                Assert.AreEqual(o1._pubFieldByte2, o2._pubFieldByte2);
                Assert.AreEqual(o1._pubFieldByte3, o2._pubFieldByte3);
                Assert.AreEqual(o1._pubFieldByte4, o2._pubFieldByte4);
                Assert.AreEqual(o1.PropertyByte0, o2.PropertyByte0);
                Assert.AreEqual(o1.PropertyByte1, o2.PropertyByte1);
                Assert.AreEqual(o1.PropertyByte2, o2.PropertyByte2);
                Assert.AreEqual(o1.PropertyByte3, o2.PropertyByte3);
                Assert.AreEqual(o1.PropertyByte4, o2.PropertyByte4);
                Assert.AreEqual(o1.PropertyByte5, o2.PropertyByte5);
                Assert.AreEqual(o1.PropertyByte6, o2.PropertyByte6);
                Assert.AreEqual(o1.PropertyByte7, o2.PropertyByte7);
                Assert.AreEqual(o1.PropertyByte8, o2.PropertyByte8);
                Assert.AreEqual(o1.PropertyByte9, o2.PropertyByte9);
                Assert.AreEqual(o1.PropertyByte10, o2.PropertyByte10);
                Assert.AreEqual(o1.PropertyByte11, o2.PropertyByte11);
                Assert.AreEqual(o1.PrFieldSByte0, o2.PrFieldSByte0);
                Assert.AreEqual(o1.PrFieldSByte1, o2.PrFieldSByte1);
                Assert.AreEqual(o1.PrFieldSByte2, o2.PrFieldSByte2);
                Assert.AreEqual(o1.PrFieldSByte3, o2.PrFieldSByte3);
                Assert.AreEqual(o1.PrFieldSByte4, o2.PrFieldSByte4);
                Assert.AreEqual(o1._pubFieldSByte0, o2._pubFieldSByte0);
                Assert.AreEqual(o1._pubFieldSByte1, o2._pubFieldSByte1);
                Assert.AreEqual(o1._pubFieldSByte2, o2._pubFieldSByte2);
                Assert.AreEqual(o1._pubFieldSByte3, o2._pubFieldSByte3);
                Assert.AreEqual(o1._pubFieldSByte4, o2._pubFieldSByte4);
                Assert.AreEqual(o1.PropertySByte0, o2.PropertySByte0);
                Assert.AreEqual(o1.PropertySByte1, o2.PropertySByte1);
                Assert.AreEqual(o1.PropertySByte2, o2.PropertySByte2);
                Assert.AreEqual(o1.PropertySByte3, o2.PropertySByte3);
                Assert.AreEqual(o1.PropertySByte4, o2.PropertySByte4);
                Assert.AreEqual(o1.PropertySByte5, o2.PropertySByte5);
                Assert.AreEqual(o1.PropertySByte6, o2.PropertySByte6);
                Assert.AreEqual(o1.PropertySByte7, o2.PropertySByte7);
                Assert.AreEqual(o1.PropertySByte8, o2.PropertySByte8);
                Assert.AreEqual(o1.PropertySByte9, o2.PropertySByte9);
                Assert.AreEqual(o1.PropertySByte10, o2.PropertySByte10);
                Assert.AreEqual(o1.PropertySByte11, o2.PropertySByte11);
                Assert.AreEqual(o1.PrFieldUInt160, o2.PrFieldUInt160);
                Assert.AreEqual(o1.PrFieldUInt161, o2.PrFieldUInt161);
                Assert.AreEqual(o1.PrFieldUInt162, o2.PrFieldUInt162);
                Assert.AreEqual(o1.PrFieldUInt163, o2.PrFieldUInt163);
                Assert.AreEqual(o1.PrFieldUInt164, o2.PrFieldUInt164);
                Assert.AreEqual(o1._pubFieldUInt160, o2._pubFieldUInt160);
                Assert.AreEqual(o1._pubFieldUInt161, o2._pubFieldUInt161);
                Assert.AreEqual(o1._pubFieldUInt162, o2._pubFieldUInt162);
                Assert.AreEqual(o1._pubFieldUInt163, o2._pubFieldUInt163);
                Assert.AreEqual(o1._pubFieldUInt164, o2._pubFieldUInt164);
                Assert.AreEqual(o1.PropertyUInt160, o2.PropertyUInt160);
                Assert.AreEqual(o1.PropertyUInt161, o2.PropertyUInt161);
                Assert.AreEqual(o1.PropertyUInt162, o2.PropertyUInt162);
                Assert.AreEqual(o1.PropertyUInt163, o2.PropertyUInt163);
                Assert.AreEqual(o1.PropertyUInt164, o2.PropertyUInt164);
                Assert.AreEqual(o1.PropertyUInt165, o2.PropertyUInt165);
                Assert.AreEqual(o1.PropertyUInt166, o2.PropertyUInt166);
                Assert.AreEqual(o1.PropertyUInt167, o2.PropertyUInt167);
                Assert.AreEqual(o1.PropertyUInt168, o2.PropertyUInt168);
                Assert.AreEqual(o1.PropertyUInt169, o2.PropertyUInt169);
                Assert.AreEqual(o1.PropertyUInt1610, o2.PropertyUInt1610);
                Assert.AreEqual(o1.PropertyUInt1611, o2.PropertyUInt1611);
                Assert.AreEqual(o1.PrFieldInt160, o2.PrFieldInt160);
                Assert.AreEqual(o1.PrFieldInt161, o2.PrFieldInt161);
                Assert.AreEqual(o1.PrFieldInt162, o2.PrFieldInt162);
                Assert.AreEqual(o1.PrFieldInt163, o2.PrFieldInt163);
                Assert.AreEqual(o1.PrFieldInt164, o2.PrFieldInt164);
                Assert.AreEqual(o1._pubFieldInt160, o2._pubFieldInt160);
                Assert.AreEqual(o1._pubFieldInt161, o2._pubFieldInt161);
                Assert.AreEqual(o1._pubFieldInt162, o2._pubFieldInt162);
                Assert.AreEqual(o1._pubFieldInt163, o2._pubFieldInt163);
                Assert.AreEqual(o1._pubFieldInt164, o2._pubFieldInt164);
                Assert.AreEqual(o1.PropertyInt160, o2.PropertyInt160);
                Assert.AreEqual(o1.PropertyInt161, o2.PropertyInt161);
                Assert.AreEqual(o1.PropertyInt162, o2.PropertyInt162);
                Assert.AreEqual(o1.PropertyInt163, o2.PropertyInt163);
                Assert.AreEqual(o1.PropertyInt164, o2.PropertyInt164);
                Assert.AreEqual(o1.PropertyInt165, o2.PropertyInt165);
                Assert.AreEqual(o1.PropertyInt166, o2.PropertyInt166);
                Assert.AreEqual(o1.PropertyInt167, o2.PropertyInt167);
                Assert.AreEqual(o1.PropertyInt168, o2.PropertyInt168);
                Assert.AreEqual(o1.PropertyInt169, o2.PropertyInt169);
                Assert.AreEqual(o1.PropertyInt1610, o2.PropertyInt1610);
                Assert.AreEqual(o1.PropertyInt1611, o2.PropertyInt1611);
                Assert.AreEqual(o1.PrFieldUInt320, o2.PrFieldUInt320);
                Assert.AreEqual(o1.PrFieldUInt321, o2.PrFieldUInt321);
                Assert.AreEqual(o1.PrFieldUInt322, o2.PrFieldUInt322);
                Assert.AreEqual(o1.PrFieldUInt323, o2.PrFieldUInt323);
                Assert.AreEqual(o1.PrFieldUInt324, o2.PrFieldUInt324);
                Assert.AreEqual(o1._pubFieldUInt320, o2._pubFieldUInt320);
                Assert.AreEqual(o1._pubFieldUInt321, o2._pubFieldUInt321);
                Assert.AreEqual(o1._pubFieldUInt322, o2._pubFieldUInt322);
                Assert.AreEqual(o1._pubFieldUInt323, o2._pubFieldUInt323);
                Assert.AreEqual(o1._pubFieldUInt324, o2._pubFieldUInt324);
                Assert.AreEqual(o1.PropertyUInt320, o2.PropertyUInt320);
                Assert.AreEqual(o1.PropertyUInt321, o2.PropertyUInt321);
                Assert.AreEqual(o1.PropertyUInt322, o2.PropertyUInt322);
                Assert.AreEqual(o1.PropertyUInt323, o2.PropertyUInt323);
                Assert.AreEqual(o1.PropertyUInt324, o2.PropertyUInt324);
                Assert.AreEqual(o1.PropertyUInt325, o2.PropertyUInt325);
                Assert.AreEqual(o1.PropertyUInt326, o2.PropertyUInt326);
                Assert.AreEqual(o1.PropertyUInt327, o2.PropertyUInt327);
                Assert.AreEqual(o1.PropertyUInt328, o2.PropertyUInt328);
                Assert.AreEqual(o1.PropertyUInt329, o2.PropertyUInt329);
                Assert.AreEqual(o1.PropertyUInt3210, o2.PropertyUInt3210);
                Assert.AreEqual(o1.PropertyUInt3211, o2.PropertyUInt3211);
                Assert.AreEqual(o1.PrFieldInt320, o2.PrFieldInt320);
                Assert.AreEqual(o1.PrFieldInt321, o2.PrFieldInt321);
                Assert.AreEqual(o1.PrFieldInt322, o2.PrFieldInt322);
                Assert.AreEqual(o1.PrFieldInt323, o2.PrFieldInt323);
                Assert.AreEqual(o1.PrFieldInt324, o2.PrFieldInt324);
                Assert.AreEqual(o1._pubFieldInt320, o2._pubFieldInt320);
                Assert.AreEqual(o1._pubFieldInt321, o2._pubFieldInt321);
                Assert.AreEqual(o1._pubFieldInt322, o2._pubFieldInt322);
                Assert.AreEqual(o1._pubFieldInt323, o2._pubFieldInt323);
                Assert.AreEqual(o1._pubFieldInt324, o2._pubFieldInt324);
                Assert.AreEqual(o1.PropertyInt320, o2.PropertyInt320);
                Assert.AreEqual(o1.PropertyInt321, o2.PropertyInt321);
                Assert.AreEqual(o1.PropertyInt322, o2.PropertyInt322);
                Assert.AreEqual(o1.PropertyInt323, o2.PropertyInt323);
                Assert.AreEqual(o1.PropertyInt324, o2.PropertyInt324);
                Assert.AreEqual(o1.PropertyInt325, o2.PropertyInt325);
                Assert.AreEqual(o1.PropertyInt326, o2.PropertyInt326);
                Assert.AreEqual(o1.PropertyInt327, o2.PropertyInt327);
                Assert.AreEqual(o1.PropertyInt328, o2.PropertyInt328);
                Assert.AreEqual(o1.PropertyInt329, o2.PropertyInt329);
                Assert.AreEqual(o1.PropertyInt3210, o2.PropertyInt3210);
                Assert.AreEqual(o1.PropertyInt3211, o2.PropertyInt3211);
                Assert.AreEqual(o1.PrFieldUInt640, o2.PrFieldUInt640);
                Assert.AreEqual(o1.PrFieldUInt641, o2.PrFieldUInt641);
                Assert.AreEqual(o1.PrFieldUInt642, o2.PrFieldUInt642);
                Assert.AreEqual(o1.PrFieldUInt643, o2.PrFieldUInt643);
                Assert.AreEqual(o1.PrFieldUInt644, o2.PrFieldUInt644);
                Assert.AreEqual(o1._pubFieldUInt640, o2._pubFieldUInt640);
                Assert.AreEqual(o1._pubFieldUInt641, o2._pubFieldUInt641);
                Assert.AreEqual(o1._pubFieldUInt642, o2._pubFieldUInt642);
                Assert.AreEqual(o1._pubFieldUInt643, o2._pubFieldUInt643);
                Assert.AreEqual(o1._pubFieldUInt644, o2._pubFieldUInt644);
                Assert.AreEqual(o1.PropertyUInt640, o2.PropertyUInt640);
                Assert.AreEqual(o1.PropertyUInt641, o2.PropertyUInt641);
                Assert.AreEqual(o1.PropertyUInt642, o2.PropertyUInt642);
                Assert.AreEqual(o1.PropertyUInt643, o2.PropertyUInt643);
                Assert.AreEqual(o1.PropertyUInt644, o2.PropertyUInt644);
                Assert.AreEqual(o1.PropertyUInt645, o2.PropertyUInt645);
                Assert.AreEqual(o1.PropertyUInt646, o2.PropertyUInt646);
                Assert.AreEqual(o1.PropertyUInt647, o2.PropertyUInt647);
                Assert.AreEqual(o1.PropertyUInt648, o2.PropertyUInt648);
                Assert.AreEqual(o1.PropertyUInt649, o2.PropertyUInt649);
                Assert.AreEqual(o1.PropertyUInt6410, o2.PropertyUInt6410);
                Assert.AreEqual(o1.PropertyUInt6411, o2.PropertyUInt6411);
                Assert.AreEqual(o1.PrFieldInt640, o2.PrFieldInt640);
                Assert.AreEqual(o1.PrFieldInt641, o2.PrFieldInt641);
                Assert.AreEqual(o1.PrFieldInt642, o2.PrFieldInt642);
                Assert.AreEqual(o1.PrFieldInt643, o2.PrFieldInt643);
                Assert.AreEqual(o1.PrFieldInt644, o2.PrFieldInt644);
                Assert.AreEqual(o1._pubFieldInt640, o2._pubFieldInt640);
                Assert.AreEqual(o1._pubFieldInt641, o2._pubFieldInt641);
                Assert.AreEqual(o1._pubFieldInt642, o2._pubFieldInt642);
                Assert.AreEqual(o1._pubFieldInt643, o2._pubFieldInt643);
                Assert.AreEqual(o1._pubFieldInt644, o2._pubFieldInt644);
                Assert.AreEqual(o1.PropertyInt640, o2.PropertyInt640);
                Assert.AreEqual(o1.PropertyInt641, o2.PropertyInt641);
                Assert.AreEqual(o1.PropertyInt642, o2.PropertyInt642);
                Assert.AreEqual(o1.PropertyInt643, o2.PropertyInt643);
                Assert.AreEqual(o1.PropertyInt644, o2.PropertyInt644);
                Assert.AreEqual(o1.PropertyInt645, o2.PropertyInt645);
                Assert.AreEqual(o1.PropertyInt646, o2.PropertyInt646);
                Assert.AreEqual(o1.PropertyInt647, o2.PropertyInt647);
                Assert.AreEqual(o1.PropertyInt648, o2.PropertyInt648);
                Assert.AreEqual(o1.PropertyInt649, o2.PropertyInt649);
                Assert.AreEqual(o1.PropertyInt6410, o2.PropertyInt6410);
                Assert.AreEqual(o1.PropertyInt6411, o2.PropertyInt6411);
                Assert.AreEqual(o1.PrFieldSingle0, o2.PrFieldSingle0);
                Assert.AreEqual(o1.PrFieldSingle1, o2.PrFieldSingle1);
                Assert.AreEqual(o1.PrFieldSingle2, o2.PrFieldSingle2);
                Assert.AreEqual(o1.PrFieldSingle3, o2.PrFieldSingle3);
                Assert.AreEqual(o1.PrFieldSingle4, o2.PrFieldSingle4);
                Assert.AreEqual(o1._pubFieldSingle0, o2._pubFieldSingle0);
                Assert.AreEqual(o1._pubFieldSingle1, o2._pubFieldSingle1);
                Assert.AreEqual(o1._pubFieldSingle2, o2._pubFieldSingle2);
                Assert.AreEqual(o1._pubFieldSingle3, o2._pubFieldSingle3);
                Assert.AreEqual(o1._pubFieldSingle4, o2._pubFieldSingle4);
                Assert.AreEqual(o1.PropertySingle0, o2.PropertySingle0);
                Assert.AreEqual(o1.PropertySingle1, o2.PropertySingle1);
                Assert.AreEqual(o1.PropertySingle2, o2.PropertySingle2);
                Assert.AreEqual(o1.PropertySingle3, o2.PropertySingle3);
                Assert.AreEqual(o1.PropertySingle4, o2.PropertySingle4);
                Assert.AreEqual(o1.PropertySingle5, o2.PropertySingle5);
                Assert.AreEqual(o1.PropertySingle6, o2.PropertySingle6);
                Assert.AreEqual(o1.PropertySingle7, o2.PropertySingle7);
                Assert.AreEqual(o1.PropertySingle8, o2.PropertySingle8);
                Assert.AreEqual(o1.PropertySingle9, o2.PropertySingle9);
                Assert.AreEqual(o1.PropertySingle10, o2.PropertySingle10);
                Assert.AreEqual(o1.PropertySingle11, o2.PropertySingle11);
                Assert.AreEqual(o1.PrFieldDouble0, o2.PrFieldDouble0);
                Assert.AreEqual(o1.PrFieldDouble1, o2.PrFieldDouble1);
                Assert.AreEqual(o1.PrFieldDouble2, o2.PrFieldDouble2);
                Assert.AreEqual(o1.PrFieldDouble3, o2.PrFieldDouble3);
                Assert.AreEqual(o1.PrFieldDouble4, o2.PrFieldDouble4);
                Assert.AreEqual(o1._pubFieldDouble0, o2._pubFieldDouble0);
                Assert.AreEqual(o1._pubFieldDouble1, o2._pubFieldDouble1);
                Assert.AreEqual(o1._pubFieldDouble2, o2._pubFieldDouble2);
                Assert.AreEqual(o1._pubFieldDouble3, o2._pubFieldDouble3);
                Assert.AreEqual(o1._pubFieldDouble4, o2._pubFieldDouble4);
                Assert.AreEqual(o1.PropertyDouble0, o2.PropertyDouble0);
                Assert.AreEqual(o1.PropertyDouble1, o2.PropertyDouble1);
                Assert.AreEqual(o1.PropertyDouble2, o2.PropertyDouble2);
                Assert.AreEqual(o1.PropertyDouble3, o2.PropertyDouble3);
                Assert.AreEqual(o1.PropertyDouble4, o2.PropertyDouble4);
                Assert.AreEqual(o1.PropertyDouble5, o2.PropertyDouble5);
                Assert.AreEqual(o1.PropertyDouble6, o2.PropertyDouble6);
                Assert.AreEqual(o1.PropertyDouble7, o2.PropertyDouble7);
                Assert.AreEqual(o1.PropertyDouble8, o2.PropertyDouble8);
                Assert.AreEqual(o1.PropertyDouble9, o2.PropertyDouble9);
                Assert.AreEqual(o1.PropertyDouble10, o2.PropertyDouble10);
                Assert.AreEqual(o1.PropertyDouble11, o2.PropertyDouble11);
                Assert.AreEqual(o1.PrFieldBoolean0, o2.PrFieldBoolean0);
                Assert.AreEqual(o1.PrFieldBoolean1, o2.PrFieldBoolean1);
                Assert.AreEqual(o1.PrFieldBoolean2, o2.PrFieldBoolean2);
                Assert.AreEqual(o1.PrFieldBoolean3, o2.PrFieldBoolean3);
                Assert.AreEqual(o1.PrFieldBoolean4, o2.PrFieldBoolean4);
                Assert.AreEqual(o1._pubFieldBoolean0, o2._pubFieldBoolean0);
                Assert.AreEqual(o1._pubFieldBoolean1, o2._pubFieldBoolean1);
                Assert.AreEqual(o1._pubFieldBoolean2, o2._pubFieldBoolean2);
                Assert.AreEqual(o1._pubFieldBoolean3, o2._pubFieldBoolean3);
                Assert.AreEqual(o1._pubFieldBoolean4, o2._pubFieldBoolean4);
                Assert.AreEqual(o1.PropertyBoolean0, o2.PropertyBoolean0);
                Assert.AreEqual(o1.PropertyBoolean1, o2.PropertyBoolean1);
                Assert.AreEqual(o1.PropertyBoolean2, o2.PropertyBoolean2);
                Assert.AreEqual(o1.PropertyBoolean3, o2.PropertyBoolean3);
                Assert.AreEqual(o1.PropertyBoolean4, o2.PropertyBoolean4);
                Assert.AreEqual(o1.PropertyBoolean5, o2.PropertyBoolean5);
                Assert.AreEqual(o1.PropertyBoolean6, o2.PropertyBoolean6);
                Assert.AreEqual(o1.PropertyBoolean7, o2.PropertyBoolean7);
                Assert.AreEqual(o1.PropertyBoolean8, o2.PropertyBoolean8);
                Assert.AreEqual(o1.PropertyBoolean9, o2.PropertyBoolean9);
                Assert.AreEqual(o1.PropertyBoolean10, o2.PropertyBoolean10);
                Assert.AreEqual(o1.PropertyBoolean11, o2.PropertyBoolean11);
                Assert.AreEqual(o1.PrFieldChar0, o2.PrFieldChar0);
                Assert.AreEqual(o1.PrFieldChar1, o2.PrFieldChar1);
                Assert.AreEqual(o1.PrFieldChar2, o2.PrFieldChar2);
                Assert.AreEqual(o1.PrFieldChar3, o2.PrFieldChar3);
                Assert.AreEqual(o1.PrFieldChar4, o2.PrFieldChar4);
                Assert.AreEqual(o1._pubFieldChar0, o2._pubFieldChar0);
                Assert.AreEqual(o1._pubFieldChar1, o2._pubFieldChar1);
                Assert.AreEqual(o1._pubFieldChar2, o2._pubFieldChar2);
                Assert.AreEqual(o1._pubFieldChar3, o2._pubFieldChar3);
                Assert.AreEqual(o1._pubFieldChar4, o2._pubFieldChar4);
                Assert.AreEqual(o1.PropertyChar0, o2.PropertyChar0);
                Assert.AreEqual(o1.PropertyChar1, o2.PropertyChar1);
                Assert.AreEqual(o1.PropertyChar2, o2.PropertyChar2);
                Assert.AreEqual(o1.PropertyChar3, o2.PropertyChar3);
                Assert.AreEqual(o1.PropertyChar4, o2.PropertyChar4);
                Assert.AreEqual(o1.PropertyChar5, o2.PropertyChar5);
                Assert.AreEqual(o1.PropertyChar6, o2.PropertyChar6);
                Assert.AreEqual(o1.PropertyChar7, o2.PropertyChar7);
                Assert.AreEqual(o1.PropertyChar8, o2.PropertyChar8);
                Assert.AreEqual(o1.PropertyChar9, o2.PropertyChar9);
                Assert.AreEqual(o1.PropertyChar10, o2.PropertyChar10);
                Assert.AreEqual(o1.PropertyChar11, o2.PropertyChar11);
                Assert.AreEqual(o1.PrFieldDateTime0, o2.PrFieldDateTime0);
                Assert.AreEqual(o1.PrFieldDateTime1, o2.PrFieldDateTime1);
                Assert.AreEqual(o1.PrFieldDateTime2, o2.PrFieldDateTime2);
                Assert.AreEqual(o1.PrFieldDateTime3, o2.PrFieldDateTime3);
                Assert.AreEqual(o1.PrFieldDateTime4, o2.PrFieldDateTime4);
                Assert.AreEqual(o1._pubFieldDateTime0, o2._pubFieldDateTime0);
                Assert.AreEqual(o1._pubFieldDateTime1, o2._pubFieldDateTime1);
                Assert.AreEqual(o1._pubFieldDateTime2, o2._pubFieldDateTime2);
                Assert.AreEqual(o1._pubFieldDateTime3, o2._pubFieldDateTime3);
                Assert.AreEqual(o1._pubFieldDateTime4, o2._pubFieldDateTime4);
                Assert.AreEqual(o1.PropertyDateTime0, o2.PropertyDateTime0);
                Assert.AreEqual(o1.PropertyDateTime1, o2.PropertyDateTime1);
                Assert.AreEqual(o1.PropertyDateTime2, o2.PropertyDateTime2);
                Assert.AreEqual(o1.PropertyDateTime3, o2.PropertyDateTime3);
                Assert.AreEqual(o1.PropertyDateTime4, o2.PropertyDateTime4);
                Assert.AreEqual(o1.PropertyDateTime5, o2.PropertyDateTime5);
                Assert.AreEqual(o1.PropertyDateTime6, o2.PropertyDateTime6);
                Assert.AreEqual(o1.PropertyDateTime7, o2.PropertyDateTime7);
                Assert.AreEqual(o1.PropertyDateTime8, o2.PropertyDateTime8);
                Assert.AreEqual(o1.PropertyDateTime9, o2.PropertyDateTime9);
                Assert.AreEqual(o1.PropertyDateTime10, o2.PropertyDateTime10);
                Assert.AreEqual(o1.PropertyDateTime11, o2.PropertyDateTime11);
                Assert.AreEqual(o1.PrFieldDecimal0, o2.PrFieldDecimal0);
                Assert.AreEqual(o1.PrFieldDecimal1, o2.PrFieldDecimal1);
                Assert.AreEqual(o1.PrFieldDecimal2, o2.PrFieldDecimal2);
                Assert.AreEqual(o1.PrFieldDecimal3, o2.PrFieldDecimal3);
                Assert.AreEqual(o1.PrFieldDecimal4, o2.PrFieldDecimal4);
                Assert.AreEqual(o1._pubFieldDecimal0, o2._pubFieldDecimal0);
                Assert.AreEqual(o1._pubFieldDecimal1, o2._pubFieldDecimal1);
                Assert.AreEqual(o1._pubFieldDecimal2, o2._pubFieldDecimal2);
                Assert.AreEqual(o1._pubFieldDecimal3, o2._pubFieldDecimal3);
                Assert.AreEqual(o1._pubFieldDecimal4, o2._pubFieldDecimal4);
                Assert.AreEqual(o1.PropertyDecimal0, o2.PropertyDecimal0);
                Assert.AreEqual(o1.PropertyDecimal1, o2.PropertyDecimal1);
                Assert.AreEqual(o1.PropertyDecimal2, o2.PropertyDecimal2);
                Assert.AreEqual(o1.PropertyDecimal3, o2.PropertyDecimal3);
                Assert.AreEqual(o1.PropertyDecimal4, o2.PropertyDecimal4);
                Assert.AreEqual(o1.PropertyDecimal5, o2.PropertyDecimal5);
                Assert.AreEqual(o1.PropertyDecimal6, o2.PropertyDecimal6);
                Assert.AreEqual(o1.PropertyDecimal7, o2.PropertyDecimal7);
                Assert.AreEqual(o1.PropertyDecimal8, o2.PropertyDecimal8);
                Assert.AreEqual(o1.PropertyDecimal9, o2.PropertyDecimal9);
                Assert.AreEqual(o1.PropertyDecimal10, o2.PropertyDecimal10);
                Assert.AreEqual(o1.PropertyDecimal11, o2.PropertyDecimal11);
                Assert.AreEqual(o1.PrFieldSomeEnum0, o2.PrFieldSomeEnum0);
                Assert.AreEqual(o1.PrFieldSomeEnum1, o2.PrFieldSomeEnum1);
                Assert.AreEqual(o1.PrFieldSomeEnum2, o2.PrFieldSomeEnum2);
                Assert.AreEqual(o1.PrFieldSomeEnum3, o2.PrFieldSomeEnum3);
                Assert.AreEqual(o1.PrFieldSomeEnum4, o2.PrFieldSomeEnum4);
                Assert.AreEqual(o1._pubFieldSomeEnum0, o2._pubFieldSomeEnum0);
                Assert.AreEqual(o1._pubFieldSomeEnum1, o2._pubFieldSomeEnum1);
                Assert.AreEqual(o1._pubFieldSomeEnum2, o2._pubFieldSomeEnum2);
                Assert.AreEqual(o1._pubFieldSomeEnum3, o2._pubFieldSomeEnum3);
                Assert.AreEqual(o1._pubFieldSomeEnum4, o2._pubFieldSomeEnum4);
                Assert.AreEqual(o1.PropertySomeEnum0, o2.PropertySomeEnum0);
                Assert.AreEqual(o1.PropertySomeEnum1, o2.PropertySomeEnum1);
                Assert.AreEqual(o1.PropertySomeEnum2, o2.PropertySomeEnum2);
                Assert.AreEqual(o1.PropertySomeEnum3, o2.PropertySomeEnum3);
                Assert.AreEqual(o1.PropertySomeEnum4, o2.PropertySomeEnum4);
                Assert.AreEqual(o1.PropertySomeEnum5, o2.PropertySomeEnum5);
                Assert.AreEqual(o1.PropertySomeEnum6, o2.PropertySomeEnum6);
                Assert.AreEqual(o1.PropertySomeEnum7, o2.PropertySomeEnum7);
                Assert.AreEqual(o1.PropertySomeEnum8, o2.PropertySomeEnum8);
                Assert.AreEqual(o1.PropertySomeEnum9, o2.PropertySomeEnum9);
                Assert.AreEqual(o1.PropertySomeEnum10, o2.PropertySomeEnum10);
                Assert.AreEqual(o1.PropertySomeEnum11, o2.PropertySomeEnum11);
                Assert.AreEqual(o1.PrFieldRandomizableStruct0, o2.PrFieldRandomizableStruct0);
                Assert.AreEqual(o1.PrFieldRandomizableStruct1, o2.PrFieldRandomizableStruct1);
                Assert.AreEqual(o1.PrFieldRandomizableStruct2, o2.PrFieldRandomizableStruct2);
                Assert.AreEqual(o1.PrFieldRandomizableStruct3, o2.PrFieldRandomizableStruct3);
                Assert.AreEqual(o1.PrFieldRandomizableStruct4, o2.PrFieldRandomizableStruct4);
                Assert.AreEqual(o1._pubFieldRandomizableStruct0, o2._pubFieldRandomizableStruct0);
                Assert.AreEqual(o1._pubFieldRandomizableStruct1, o2._pubFieldRandomizableStruct1);
                Assert.AreEqual(o1._pubFieldRandomizableStruct2, o2._pubFieldRandomizableStruct2);
                Assert.AreEqual(o1._pubFieldRandomizableStruct3, o2._pubFieldRandomizableStruct3);
                Assert.AreEqual(o1._pubFieldRandomizableStruct4, o2._pubFieldRandomizableStruct4);
                Assert.AreEqual(o1.PropertyRandomizableStruct0, o2.PropertyRandomizableStruct0);
                Assert.AreEqual(o1.PropertyRandomizableStruct1, o2.PropertyRandomizableStruct1);
                Assert.AreEqual(o1.PropertyRandomizableStruct2, o2.PropertyRandomizableStruct2);
                Assert.AreEqual(o1.PropertyRandomizableStruct3, o2.PropertyRandomizableStruct3);
                Assert.AreEqual(o1.PropertyRandomizableStruct4, o2.PropertyRandomizableStruct4);
                Assert.AreEqual(o1.PropertyRandomizableStruct5, o2.PropertyRandomizableStruct5);
                Assert.AreEqual(o1.PropertyRandomizableStruct6, o2.PropertyRandomizableStruct6);
                Assert.AreEqual(o1.PropertyRandomizableStruct7, o2.PropertyRandomizableStruct7);
                Assert.AreEqual(o1.PropertyRandomizableStruct8, o2.PropertyRandomizableStruct8);
                Assert.AreEqual(o1.PropertyRandomizableStruct9, o2.PropertyRandomizableStruct9);
                Assert.AreEqual(o1.PropertyRandomizableStruct10, o2.PropertyRandomizableStruct10);
                Assert.AreEqual(o1.PropertyRandomizableStruct11, o2.PropertyRandomizableStruct11);
                Assert.AreEqual(o1.PrFieldRandomizable0, o2.PrFieldRandomizable0);
                Assert.AreEqual(o1.PrFieldRandomizable1, o2.PrFieldRandomizable1);
                Assert.AreEqual(o1.PrFieldRandomizable2, o2.PrFieldRandomizable2);
                Assert.AreEqual(o1.PrFieldRandomizable3, o2.PrFieldRandomizable3);
                Assert.AreEqual(o1.PrFieldRandomizable4, o2.PrFieldRandomizable4);
                Assert.AreEqual(o1._pubFieldRandomizable0, o2._pubFieldRandomizable0);
                Assert.AreEqual(o1._pubFieldRandomizable1, o2._pubFieldRandomizable1);
                Assert.AreEqual(o1._pubFieldRandomizable2, o2._pubFieldRandomizable2);
                Assert.AreEqual(o1._pubFieldRandomizable3, o2._pubFieldRandomizable3);
                Assert.AreEqual(o1._pubFieldRandomizable4, o2._pubFieldRandomizable4);
                Assert.AreEqual(o1.PropertyRandomizable0, o2.PropertyRandomizable0);
                Assert.AreEqual(o1.PropertyRandomizable1, o2.PropertyRandomizable1);
                Assert.AreEqual(o1.PropertyRandomizable2, o2.PropertyRandomizable2);
                Assert.AreEqual(o1.PropertyRandomizable3, o2.PropertyRandomizable3);
                Assert.AreEqual(o1.PropertyRandomizable4, o2.PropertyRandomizable4);
                Assert.AreEqual(o1.PropertyRandomizable5, o2.PropertyRandomizable5);
                Assert.AreEqual(o1.PropertyRandomizable6, o2.PropertyRandomizable6);
                Assert.AreEqual(o1.PropertyRandomizable7, o2.PropertyRandomizable7);
                Assert.AreEqual(o1.PropertyRandomizable8, o2.PropertyRandomizable8);
                Assert.AreEqual(o1.PropertyRandomizable9, o2.PropertyRandomizable9);
                Assert.AreEqual(o1.PropertyRandomizable10, o2.PropertyRandomizable10);
                Assert.AreEqual(o1.PropertyRandomizable11, o2.PropertyRandomizable11);
                Assert.AreEqual(o1.PrFieldString0, o2.PrFieldString0);
                Assert.AreEqual(o1.PrFieldString1, o2.PrFieldString1);
                Assert.AreEqual(o1.PrFieldString2, o2.PrFieldString2);
                Assert.AreEqual(o1.PrFieldString3, o2.PrFieldString3);
                Assert.AreEqual(o1.PrFieldString4, o2.PrFieldString4);
                Assert.AreEqual(o1._pubFieldString0, o2._pubFieldString0);
                Assert.AreEqual(o1._pubFieldString1, o2._pubFieldString1);
                Assert.AreEqual(o1._pubFieldString2, o2._pubFieldString2);
                Assert.AreEqual(o1._pubFieldString3, o2._pubFieldString3);
                Assert.AreEqual(o1._pubFieldString4, o2._pubFieldString4);
                Assert.AreEqual(o1.PropertyString0, o2.PropertyString0);
                Assert.AreEqual(o1.PropertyString1, o2.PropertyString1);
                Assert.AreEqual(o1.PropertyString2, o2.PropertyString2);
                Assert.AreEqual(o1.PropertyString3, o2.PropertyString3);
                Assert.AreEqual(o1.PropertyString4, o2.PropertyString4);
                Assert.AreEqual(o1.PropertyString5, o2.PropertyString5);
                Assert.AreEqual(o1.PropertyString6, o2.PropertyString6);
                Assert.AreEqual(o1.PropertyString7, o2.PropertyString7);
                Assert.AreEqual(o1.PropertyString8, o2.PropertyString8);
                Assert.AreEqual(o1.PropertyString9, o2.PropertyString9);
                Assert.AreEqual(o1.PropertyString10, o2.PropertyString10);
                Assert.AreEqual(o1.PropertyString11, o2.PropertyString11);
                CollectionAssert.AreEqual(o1.PrFieldByteArr0, o2.PrFieldByteArr0);
                CollectionAssert.AreEqual(o1.PrFieldByteArr1, o2.PrFieldByteArr1);
                CollectionAssert.AreEqual(o1.PrFieldByteArr2, o2.PrFieldByteArr2);
                CollectionAssert.AreEqual(o1.PrFieldByteArr3, o2.PrFieldByteArr3);
                CollectionAssert.AreEqual(o1.PrFieldByteArr4, o2.PrFieldByteArr4);
                CollectionAssert.AreEqual(o1._pubFieldByteArr0, o2._pubFieldByteArr0);
                CollectionAssert.AreEqual(o1._pubFieldByteArr1, o2._pubFieldByteArr1);
                CollectionAssert.AreEqual(o1._pubFieldByteArr2, o2._pubFieldByteArr2);
                CollectionAssert.AreEqual(o1._pubFieldByteArr3, o2._pubFieldByteArr3);
                CollectionAssert.AreEqual(o1._pubFieldByteArr4, o2._pubFieldByteArr4);
                CollectionAssert.AreEqual(o1.PropertyByteArr0, o2.PropertyByteArr0);
                CollectionAssert.AreEqual(o1.PropertyByteArr1, o2.PropertyByteArr1);
                CollectionAssert.AreEqual(o1.PropertyByteArr2, o2.PropertyByteArr2);
                CollectionAssert.AreEqual(o1.PropertyByteArr3, o2.PropertyByteArr3);
                CollectionAssert.AreEqual(o1.PropertyByteArr4, o2.PropertyByteArr4);
                CollectionAssert.AreEqual(o1.PropertyByteArr5, o2.PropertyByteArr5);
                CollectionAssert.AreEqual(o1.PropertyByteArr6, o2.PropertyByteArr6);
                CollectionAssert.AreEqual(o1.PropertyByteArr7, o2.PropertyByteArr7);
                CollectionAssert.AreEqual(o1.PropertyByteArr8, o2.PropertyByteArr8);
                CollectionAssert.AreEqual(o1.PropertyByteArr9, o2.PropertyByteArr9);
                CollectionAssert.AreEqual(o1.PropertyByteArr10, o2.PropertyByteArr10);
                CollectionAssert.AreEqual(o1.PropertyByteArr11, o2.PropertyByteArr11);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr0, o2.PrFieldSByteArr0);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr1, o2.PrFieldSByteArr1);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr2, o2.PrFieldSByteArr2);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr3, o2.PrFieldSByteArr3);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr4, o2.PrFieldSByteArr4);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr0, o2._pubFieldSByteArr0);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr1, o2._pubFieldSByteArr1);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr2, o2._pubFieldSByteArr2);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr3, o2._pubFieldSByteArr3);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr4, o2._pubFieldSByteArr4);
                CollectionAssert.AreEqual(o1.PropertySByteArr0, o2.PropertySByteArr0);
                CollectionAssert.AreEqual(o1.PropertySByteArr1, o2.PropertySByteArr1);
                CollectionAssert.AreEqual(o1.PropertySByteArr2, o2.PropertySByteArr2);
                CollectionAssert.AreEqual(o1.PropertySByteArr3, o2.PropertySByteArr3);
                CollectionAssert.AreEqual(o1.PropertySByteArr4, o2.PropertySByteArr4);
                CollectionAssert.AreEqual(o1.PropertySByteArr5, o2.PropertySByteArr5);
                CollectionAssert.AreEqual(o1.PropertySByteArr6, o2.PropertySByteArr6);
                CollectionAssert.AreEqual(o1.PropertySByteArr7, o2.PropertySByteArr7);
                CollectionAssert.AreEqual(o1.PropertySByteArr8, o2.PropertySByteArr8);
                CollectionAssert.AreEqual(o1.PropertySByteArr9, o2.PropertySByteArr9);
                CollectionAssert.AreEqual(o1.PropertySByteArr10, o2.PropertySByteArr10);
                CollectionAssert.AreEqual(o1.PropertySByteArr11, o2.PropertySByteArr11);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr0, o2.PrFieldUInt16Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr1, o2.PrFieldUInt16Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr2, o2.PrFieldUInt16Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr3, o2.PrFieldUInt16Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt16Arr4, o2.PrFieldUInt16Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr0, o2._pubFieldUInt16Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr1, o2._pubFieldUInt16Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr2, o2._pubFieldUInt16Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr3, o2._pubFieldUInt16Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt16Arr4, o2._pubFieldUInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr0, o2.PropertyUInt16Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr1, o2.PropertyUInt16Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr2, o2.PropertyUInt16Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr3, o2.PropertyUInt16Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr4, o2.PropertyUInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr5, o2.PropertyUInt16Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr6, o2.PropertyUInt16Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr7, o2.PropertyUInt16Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr8, o2.PropertyUInt16Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr9, o2.PropertyUInt16Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr10, o2.PropertyUInt16Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt16Arr11, o2.PropertyUInt16Arr11);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr0, o2.PrFieldInt16Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr1, o2.PrFieldInt16Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr2, o2.PrFieldInt16Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr3, o2.PrFieldInt16Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr4, o2.PrFieldInt16Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr0, o2._pubFieldInt16Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr1, o2._pubFieldInt16Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr2, o2._pubFieldInt16Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr3, o2._pubFieldInt16Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr4, o2._pubFieldInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr0, o2.PropertyInt16Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr1, o2.PropertyInt16Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr2, o2.PropertyInt16Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr3, o2.PropertyInt16Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr4, o2.PropertyInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr5, o2.PropertyInt16Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr6, o2.PropertyInt16Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr7, o2.PropertyInt16Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr8, o2.PropertyInt16Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr9, o2.PropertyInt16Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr10, o2.PropertyInt16Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr11, o2.PropertyInt16Arr11);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr0, o2.PrFieldUInt32Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr1, o2.PrFieldUInt32Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr2, o2.PrFieldUInt32Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr3, o2.PrFieldUInt32Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr4, o2.PrFieldUInt32Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr0, o2._pubFieldUInt32Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr1, o2._pubFieldUInt32Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr2, o2._pubFieldUInt32Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr3, o2._pubFieldUInt32Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr4, o2._pubFieldUInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr0, o2.PropertyUInt32Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr1, o2.PropertyUInt32Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr2, o2.PropertyUInt32Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr3, o2.PropertyUInt32Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr4, o2.PropertyUInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr5, o2.PropertyUInt32Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr6, o2.PropertyUInt32Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr7, o2.PropertyUInt32Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr8, o2.PropertyUInt32Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr9, o2.PropertyUInt32Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr10, o2.PropertyUInt32Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr11, o2.PropertyUInt32Arr11);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr0, o2.PrFieldInt32Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr1, o2.PrFieldInt32Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr2, o2.PrFieldInt32Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr3, o2.PrFieldInt32Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr4, o2.PrFieldInt32Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr0, o2._pubFieldInt32Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr1, o2._pubFieldInt32Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr2, o2._pubFieldInt32Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr3, o2._pubFieldInt32Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr4, o2._pubFieldInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr0, o2.PropertyInt32Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr1, o2.PropertyInt32Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr2, o2.PropertyInt32Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr3, o2.PropertyInt32Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr4, o2.PropertyInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr5, o2.PropertyInt32Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr6, o2.PropertyInt32Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr7, o2.PropertyInt32Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr8, o2.PropertyInt32Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr9, o2.PropertyInt32Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr10, o2.PropertyInt32Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr11, o2.PropertyInt32Arr11);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr0, o2.PrFieldUInt64Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr1, o2.PrFieldUInt64Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr2, o2.PrFieldUInt64Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr3, o2.PrFieldUInt64Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr4, o2.PrFieldUInt64Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr0, o2._pubFieldUInt64Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr1, o2._pubFieldUInt64Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr2, o2._pubFieldUInt64Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr3, o2._pubFieldUInt64Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr4, o2._pubFieldUInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr0, o2.PropertyUInt64Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr1, o2.PropertyUInt64Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr2, o2.PropertyUInt64Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr3, o2.PropertyUInt64Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr4, o2.PropertyUInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr5, o2.PropertyUInt64Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr6, o2.PropertyUInt64Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr7, o2.PropertyUInt64Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr8, o2.PropertyUInt64Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr9, o2.PropertyUInt64Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr10, o2.PropertyUInt64Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr11, o2.PropertyUInt64Arr11);
                CollectionAssert.AreEqual(o1.PrFieldInt64Arr0, o2.PrFieldInt64Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt64Arr1, o2.PrFieldInt64Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt64Arr2, o2.PrFieldInt64Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt64Arr3, o2.PrFieldInt64Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt64Arr4, o2.PrFieldInt64Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt64Arr0, o2._pubFieldInt64Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt64Arr1, o2._pubFieldInt64Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt64Arr2, o2._pubFieldInt64Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt64Arr3, o2._pubFieldInt64Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt64Arr4, o2._pubFieldInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr0, o2.PropertyInt64Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr1, o2.PropertyInt64Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr2, o2.PropertyInt64Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr3, o2.PropertyInt64Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr4, o2.PropertyInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr5, o2.PropertyInt64Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr6, o2.PropertyInt64Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr7, o2.PropertyInt64Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr8, o2.PropertyInt64Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr9, o2.PropertyInt64Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr10, o2.PropertyInt64Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt64Arr11, o2.PropertyInt64Arr11);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr0, o2.PrFieldSingleArr0);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr1, o2.PrFieldSingleArr1);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr2, o2.PrFieldSingleArr2);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr3, o2.PrFieldSingleArr3);
                CollectionAssert.AreEqual(o1.PrFieldSingleArr4, o2.PrFieldSingleArr4);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr0, o2._pubFieldSingleArr0);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr1, o2._pubFieldSingleArr1);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr2, o2._pubFieldSingleArr2);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr3, o2._pubFieldSingleArr3);
                CollectionAssert.AreEqual(o1._pubFieldSingleArr4, o2._pubFieldSingleArr4);
                CollectionAssert.AreEqual(o1.PropertySingleArr0, o2.PropertySingleArr0);
                CollectionAssert.AreEqual(o1.PropertySingleArr1, o2.PropertySingleArr1);
                CollectionAssert.AreEqual(o1.PropertySingleArr2, o2.PropertySingleArr2);
                CollectionAssert.AreEqual(o1.PropertySingleArr3, o2.PropertySingleArr3);
                CollectionAssert.AreEqual(o1.PropertySingleArr4, o2.PropertySingleArr4);
                CollectionAssert.AreEqual(o1.PropertySingleArr5, o2.PropertySingleArr5);
                CollectionAssert.AreEqual(o1.PropertySingleArr6, o2.PropertySingleArr6);
                CollectionAssert.AreEqual(o1.PropertySingleArr7, o2.PropertySingleArr7);
                CollectionAssert.AreEqual(o1.PropertySingleArr8, o2.PropertySingleArr8);
                CollectionAssert.AreEqual(o1.PropertySingleArr9, o2.PropertySingleArr9);
                CollectionAssert.AreEqual(o1.PropertySingleArr10, o2.PropertySingleArr10);
                CollectionAssert.AreEqual(o1.PropertySingleArr11, o2.PropertySingleArr11);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr0, o2.PrFieldDoubleArr0);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr1, o2.PrFieldDoubleArr1);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr2, o2.PrFieldDoubleArr2);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr3, o2.PrFieldDoubleArr3);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr4, o2.PrFieldDoubleArr4);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr0, o2._pubFieldDoubleArr0);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr1, o2._pubFieldDoubleArr1);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr2, o2._pubFieldDoubleArr2);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr3, o2._pubFieldDoubleArr3);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr4, o2._pubFieldDoubleArr4);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr0, o2.PropertyDoubleArr0);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr1, o2.PropertyDoubleArr1);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr2, o2.PropertyDoubleArr2);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr3, o2.PropertyDoubleArr3);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr4, o2.PropertyDoubleArr4);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr5, o2.PropertyDoubleArr5);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr6, o2.PropertyDoubleArr6);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr7, o2.PropertyDoubleArr7);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr8, o2.PropertyDoubleArr8);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr9, o2.PropertyDoubleArr9);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr10, o2.PropertyDoubleArr10);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr11, o2.PropertyDoubleArr11);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr0, o2.PrFieldBooleanArr0);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr1, o2.PrFieldBooleanArr1);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr2, o2.PrFieldBooleanArr2);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr3, o2.PrFieldBooleanArr3);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr4, o2.PrFieldBooleanArr4);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr0, o2._pubFieldBooleanArr0);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr1, o2._pubFieldBooleanArr1);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr2, o2._pubFieldBooleanArr2);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr3, o2._pubFieldBooleanArr3);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr4, o2._pubFieldBooleanArr4);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr0, o2.PropertyBooleanArr0);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr1, o2.PropertyBooleanArr1);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr2, o2.PropertyBooleanArr2);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr3, o2.PropertyBooleanArr3);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr4, o2.PropertyBooleanArr4);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr5, o2.PropertyBooleanArr5);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr6, o2.PropertyBooleanArr6);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr7, o2.PropertyBooleanArr7);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr8, o2.PropertyBooleanArr8);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr9, o2.PropertyBooleanArr9);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr10, o2.PropertyBooleanArr10);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr11, o2.PropertyBooleanArr11);
                CollectionAssert.AreEqual(o1.PrFieldCharArr0, o2.PrFieldCharArr0);
                CollectionAssert.AreEqual(o1.PrFieldCharArr1, o2.PrFieldCharArr1);
                CollectionAssert.AreEqual(o1.PrFieldCharArr2, o2.PrFieldCharArr2);
                CollectionAssert.AreEqual(o1.PrFieldCharArr3, o2.PrFieldCharArr3);
                CollectionAssert.AreEqual(o1.PrFieldCharArr4, o2.PrFieldCharArr4);
                CollectionAssert.AreEqual(o1._pubFieldCharArr0, o2._pubFieldCharArr0);
                CollectionAssert.AreEqual(o1._pubFieldCharArr1, o2._pubFieldCharArr1);
                CollectionAssert.AreEqual(o1._pubFieldCharArr2, o2._pubFieldCharArr2);
                CollectionAssert.AreEqual(o1._pubFieldCharArr3, o2._pubFieldCharArr3);
                CollectionAssert.AreEqual(o1._pubFieldCharArr4, o2._pubFieldCharArr4);
                CollectionAssert.AreEqual(o1.PropertyCharArr0, o2.PropertyCharArr0);
                CollectionAssert.AreEqual(o1.PropertyCharArr1, o2.PropertyCharArr1);
                CollectionAssert.AreEqual(o1.PropertyCharArr2, o2.PropertyCharArr2);
                CollectionAssert.AreEqual(o1.PropertyCharArr3, o2.PropertyCharArr3);
                CollectionAssert.AreEqual(o1.PropertyCharArr4, o2.PropertyCharArr4);
                CollectionAssert.AreEqual(o1.PropertyCharArr5, o2.PropertyCharArr5);
                CollectionAssert.AreEqual(o1.PropertyCharArr6, o2.PropertyCharArr6);
                CollectionAssert.AreEqual(o1.PropertyCharArr7, o2.PropertyCharArr7);
                CollectionAssert.AreEqual(o1.PropertyCharArr8, o2.PropertyCharArr8);
                CollectionAssert.AreEqual(o1.PropertyCharArr9, o2.PropertyCharArr9);
                CollectionAssert.AreEqual(o1.PropertyCharArr10, o2.PropertyCharArr10);
                CollectionAssert.AreEqual(o1.PropertyCharArr11, o2.PropertyCharArr11);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr0, o2.PrFieldDateTimeArr0);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr1, o2.PrFieldDateTimeArr1);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr2, o2.PrFieldDateTimeArr2);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr3, o2.PrFieldDateTimeArr3);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeArr4, o2.PrFieldDateTimeArr4);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr0, o2._pubFieldDateTimeArr0);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr1, o2._pubFieldDateTimeArr1);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr2, o2._pubFieldDateTimeArr2);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr3, o2._pubFieldDateTimeArr3);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeArr4, o2._pubFieldDateTimeArr4);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr0, o2.PropertyDateTimeArr0);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr1, o2.PropertyDateTimeArr1);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr2, o2.PropertyDateTimeArr2);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr3, o2.PropertyDateTimeArr3);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr4, o2.PropertyDateTimeArr4);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr5, o2.PropertyDateTimeArr5);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr6, o2.PropertyDateTimeArr6);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr7, o2.PropertyDateTimeArr7);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr8, o2.PropertyDateTimeArr8);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr9, o2.PropertyDateTimeArr9);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr10, o2.PropertyDateTimeArr10);
                CollectionAssert.AreEqual(o1.PropertyDateTimeArr11, o2.PropertyDateTimeArr11);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr0, o2.PrFieldDecimalArr0);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr1, o2.PrFieldDecimalArr1);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr2, o2.PrFieldDecimalArr2);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr3, o2.PrFieldDecimalArr3);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr4, o2.PrFieldDecimalArr4);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr0, o2._pubFieldDecimalArr0);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr1, o2._pubFieldDecimalArr1);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr2, o2._pubFieldDecimalArr2);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr3, o2._pubFieldDecimalArr3);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr4, o2._pubFieldDecimalArr4);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr0, o2.PropertyDecimalArr0);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr1, o2.PropertyDecimalArr1);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr2, o2.PropertyDecimalArr2);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr3, o2.PropertyDecimalArr3);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr4, o2.PropertyDecimalArr4);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr5, o2.PropertyDecimalArr5);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr6, o2.PropertyDecimalArr6);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr7, o2.PropertyDecimalArr7);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr8, o2.PropertyDecimalArr8);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr9, o2.PropertyDecimalArr9);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr10, o2.PropertyDecimalArr10);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr11, o2.PropertyDecimalArr11);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr0, o2.PrFieldSomeEnumArr0);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr1, o2.PrFieldSomeEnumArr1);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr2, o2.PrFieldSomeEnumArr2);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr3, o2.PrFieldSomeEnumArr3);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr4, o2.PrFieldSomeEnumArr4);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr0, o2._pubFieldSomeEnumArr0);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr1, o2._pubFieldSomeEnumArr1);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr2, o2._pubFieldSomeEnumArr2);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr3, o2._pubFieldSomeEnumArr3);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr4, o2._pubFieldSomeEnumArr4);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr0, o2.PropertySomeEnumArr0);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr1, o2.PropertySomeEnumArr1);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr2, o2.PropertySomeEnumArr2);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr3, o2.PropertySomeEnumArr3);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr4, o2.PropertySomeEnumArr4);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr5, o2.PropertySomeEnumArr5);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr6, o2.PropertySomeEnumArr6);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr7, o2.PropertySomeEnumArr7);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr8, o2.PropertySomeEnumArr8);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr9, o2.PropertySomeEnumArr9);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr10, o2.PropertySomeEnumArr10);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr11, o2.PropertySomeEnumArr11);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr0, o2.PrFieldRandomizableStructArr0);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr1, o2.PrFieldRandomizableStructArr1);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr2, o2.PrFieldRandomizableStructArr2);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr3, o2.PrFieldRandomizableStructArr3);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructArr4, o2.PrFieldRandomizableStructArr4);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr0, o2._pubFieldRandomizableStructArr0);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr1, o2._pubFieldRandomizableStructArr1);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr2, o2._pubFieldRandomizableStructArr2);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr3, o2._pubFieldRandomizableStructArr3);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructArr4, o2._pubFieldRandomizableStructArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr0, o2.PropertyRandomizableStructArr0);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr1, o2.PropertyRandomizableStructArr1);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr2, o2.PropertyRandomizableStructArr2);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr3, o2.PropertyRandomizableStructArr3);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr4, o2.PropertyRandomizableStructArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr5, o2.PropertyRandomizableStructArr5);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr6, o2.PropertyRandomizableStructArr6);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr7, o2.PropertyRandomizableStructArr7);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr8, o2.PropertyRandomizableStructArr8);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr9, o2.PropertyRandomizableStructArr9);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr10, o2.PropertyRandomizableStructArr10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructArr11, o2.PropertyRandomizableStructArr11);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr0, o2.PrFieldRandomizableArr0);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr1, o2.PrFieldRandomizableArr1);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr2, o2.PrFieldRandomizableArr2);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr3, o2.PrFieldRandomizableArr3);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr4, o2.PrFieldRandomizableArr4);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr0, o2._pubFieldRandomizableArr0);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr1, o2._pubFieldRandomizableArr1);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr2, o2._pubFieldRandomizableArr2);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr3, o2._pubFieldRandomizableArr3);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr4, o2._pubFieldRandomizableArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr0, o2.PropertyRandomizableArr0);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr1, o2.PropertyRandomizableArr1);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr2, o2.PropertyRandomizableArr2);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr3, o2.PropertyRandomizableArr3);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr4, o2.PropertyRandomizableArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr5, o2.PropertyRandomizableArr5);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr6, o2.PropertyRandomizableArr6);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr7, o2.PropertyRandomizableArr7);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr8, o2.PropertyRandomizableArr8);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr9, o2.PropertyRandomizableArr9);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr10, o2.PropertyRandomizableArr10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr11, o2.PropertyRandomizableArr11);
                CollectionAssert.AreEqual(o1.PrFieldStringArr0, o2.PrFieldStringArr0);
                CollectionAssert.AreEqual(o1.PrFieldStringArr1, o2.PrFieldStringArr1);
                CollectionAssert.AreEqual(o1.PrFieldStringArr2, o2.PrFieldStringArr2);
                CollectionAssert.AreEqual(o1.PrFieldStringArr3, o2.PrFieldStringArr3);
                CollectionAssert.AreEqual(o1.PrFieldStringArr4, o2.PrFieldStringArr4);
                CollectionAssert.AreEqual(o1._pubFieldStringArr0, o2._pubFieldStringArr0);
                CollectionAssert.AreEqual(o1._pubFieldStringArr1, o2._pubFieldStringArr1);
                CollectionAssert.AreEqual(o1._pubFieldStringArr2, o2._pubFieldStringArr2);
                CollectionAssert.AreEqual(o1._pubFieldStringArr3, o2._pubFieldStringArr3);
                CollectionAssert.AreEqual(o1._pubFieldStringArr4, o2._pubFieldStringArr4);
                CollectionAssert.AreEqual(o1.PropertyStringArr0, o2.PropertyStringArr0);
                CollectionAssert.AreEqual(o1.PropertyStringArr1, o2.PropertyStringArr1);
                CollectionAssert.AreEqual(o1.PropertyStringArr2, o2.PropertyStringArr2);
                CollectionAssert.AreEqual(o1.PropertyStringArr3, o2.PropertyStringArr3);
                CollectionAssert.AreEqual(o1.PropertyStringArr4, o2.PropertyStringArr4);
                CollectionAssert.AreEqual(o1.PropertyStringArr5, o2.PropertyStringArr5);
                CollectionAssert.AreEqual(o1.PropertyStringArr6, o2.PropertyStringArr6);
                CollectionAssert.AreEqual(o1.PropertyStringArr7, o2.PropertyStringArr7);
                CollectionAssert.AreEqual(o1.PropertyStringArr8, o2.PropertyStringArr8);
                CollectionAssert.AreEqual(o1.PropertyStringArr9, o2.PropertyStringArr9);
                CollectionAssert.AreEqual(o1.PropertyStringArr10, o2.PropertyStringArr10);
                CollectionAssert.AreEqual(o1.PropertyStringArr11, o2.PropertyStringArr11);
            }
        }
    }

    [Serializable]
    internal class MixedClass {
        private Byte _prFieldByte0;
        private Byte _prFieldByte1;
        private Byte _prFieldByte2;
        private Byte _prFieldByte3;
        private Byte _prFieldByte4;

        public Byte PrFieldByte0 {
            get { return _prFieldByte0; }
        }

        public Byte PrFieldByte1 {
            get { return _prFieldByte1; }
        }

        public Byte PrFieldByte2 {
            get { return _prFieldByte2; }
        }

        public Byte PrFieldByte3 {
            get { return _prFieldByte3; }
        }

        public Byte PrFieldByte4 {
            get { return _prFieldByte4; }
        }

        public Byte _pubFieldByte0;
        public Byte _pubFieldByte1;
        public Byte _pubFieldByte2;
        public Byte _pubFieldByte3;
        public Byte _pubFieldByte4;
        public Byte PropertyByte0 { get; set; }
        public Byte PropertyByte1 { get; set; }
        public Byte PropertyByte2 { get; set; }
        public Byte PropertyByte3 { get; set; }
        public Byte PropertyByte4 { get; set; }
        public Byte PropertyByte5 { get; set; }
        public Byte PropertyByte6 { get; set; }
        public Byte PropertyByte7 { get; set; }
        public Byte PropertyByte8 { get; set; }
        public Byte PropertyByte9 { get; set; }
        public Byte PropertyByte10 { get; set; }
        public Byte PropertyByte11 { get; set; }
        private SByte _prFieldSByte0;
        private SByte _prFieldSByte1;
        private SByte _prFieldSByte2;
        private SByte _prFieldSByte3;
        private SByte _prFieldSByte4;

        public SByte PrFieldSByte0 {
            get { return _prFieldSByte0; }
        }

        public SByte PrFieldSByte1 {
            get { return _prFieldSByte1; }
        }

        public SByte PrFieldSByte2 {
            get { return _prFieldSByte2; }
        }

        public SByte PrFieldSByte3 {
            get { return _prFieldSByte3; }
        }

        public SByte PrFieldSByte4 {
            get { return _prFieldSByte4; }
        }

        public SByte _pubFieldSByte0;
        public SByte _pubFieldSByte1;
        public SByte _pubFieldSByte2;
        public SByte _pubFieldSByte3;
        public SByte _pubFieldSByte4;
        public SByte PropertySByte0 { get; set; }
        public SByte PropertySByte1 { get; set; }
        public SByte PropertySByte2 { get; set; }
        public SByte PropertySByte3 { get; set; }
        public SByte PropertySByte4 { get; set; }
        public SByte PropertySByte5 { get; set; }
        public SByte PropertySByte6 { get; set; }
        public SByte PropertySByte7 { get; set; }
        public SByte PropertySByte8 { get; set; }
        public SByte PropertySByte9 { get; set; }
        public SByte PropertySByte10 { get; set; }
        public SByte PropertySByte11 { get; set; }
        private UInt16 _prFieldUInt160;
        private UInt16 _prFieldUInt161;
        private UInt16 _prFieldUInt162;
        private UInt16 _prFieldUInt163;
        private UInt16 _prFieldUInt164;

        public UInt16 PrFieldUInt160 {
            get { return _prFieldUInt160; }
        }

        public UInt16 PrFieldUInt161 {
            get { return _prFieldUInt161; }
        }

        public UInt16 PrFieldUInt162 {
            get { return _prFieldUInt162; }
        }

        public UInt16 PrFieldUInt163 {
            get { return _prFieldUInt163; }
        }

        public UInt16 PrFieldUInt164 {
            get { return _prFieldUInt164; }
        }

        public UInt16 _pubFieldUInt160;
        public UInt16 _pubFieldUInt161;
        public UInt16 _pubFieldUInt162;
        public UInt16 _pubFieldUInt163;
        public UInt16 _pubFieldUInt164;
        public UInt16 PropertyUInt160 { get; set; }
        public UInt16 PropertyUInt161 { get; set; }
        public UInt16 PropertyUInt162 { get; set; }
        public UInt16 PropertyUInt163 { get; set; }
        public UInt16 PropertyUInt164 { get; set; }
        public UInt16 PropertyUInt165 { get; set; }
        public UInt16 PropertyUInt166 { get; set; }
        public UInt16 PropertyUInt167 { get; set; }
        public UInt16 PropertyUInt168 { get; set; }
        public UInt16 PropertyUInt169 { get; set; }
        public UInt16 PropertyUInt1610 { get; set; }
        public UInt16 PropertyUInt1611 { get; set; }
        private Int16 _prFieldInt160;
        private Int16 _prFieldInt161;
        private Int16 _prFieldInt162;
        private Int16 _prFieldInt163;
        private Int16 _prFieldInt164;

        public Int16 PrFieldInt160 {
            get { return _prFieldInt160; }
        }

        public Int16 PrFieldInt161 {
            get { return _prFieldInt161; }
        }

        public Int16 PrFieldInt162 {
            get { return _prFieldInt162; }
        }

        public Int16 PrFieldInt163 {
            get { return _prFieldInt163; }
        }

        public Int16 PrFieldInt164 {
            get { return _prFieldInt164; }
        }

        public Int16 _pubFieldInt160;
        public Int16 _pubFieldInt161;
        public Int16 _pubFieldInt162;
        public Int16 _pubFieldInt163;
        public Int16 _pubFieldInt164;
        public Int16 PropertyInt160 { get; set; }
        public Int16 PropertyInt161 { get; set; }
        public Int16 PropertyInt162 { get; set; }
        public Int16 PropertyInt163 { get; set; }
        public Int16 PropertyInt164 { get; set; }
        public Int16 PropertyInt165 { get; set; }
        public Int16 PropertyInt166 { get; set; }
        public Int16 PropertyInt167 { get; set; }
        public Int16 PropertyInt168 { get; set; }
        public Int16 PropertyInt169 { get; set; }
        public Int16 PropertyInt1610 { get; set; }
        public Int16 PropertyInt1611 { get; set; }
        private UInt32 _prFieldUInt320;
        private UInt32 _prFieldUInt321;
        private UInt32 _prFieldUInt322;
        private UInt32 _prFieldUInt323;
        private UInt32 _prFieldUInt324;

        public UInt32 PrFieldUInt320 {
            get { return _prFieldUInt320; }
        }

        public UInt32 PrFieldUInt321 {
            get { return _prFieldUInt321; }
        }

        public UInt32 PrFieldUInt322 {
            get { return _prFieldUInt322; }
        }

        public UInt32 PrFieldUInt323 {
            get { return _prFieldUInt323; }
        }

        public UInt32 PrFieldUInt324 {
            get { return _prFieldUInt324; }
        }

        public UInt32 _pubFieldUInt320;
        public UInt32 _pubFieldUInt321;
        public UInt32 _pubFieldUInt322;
        public UInt32 _pubFieldUInt323;
        public UInt32 _pubFieldUInt324;
        public UInt32 PropertyUInt320 { get; set; }
        public UInt32 PropertyUInt321 { get; set; }
        public UInt32 PropertyUInt322 { get; set; }
        public UInt32 PropertyUInt323 { get; set; }
        public UInt32 PropertyUInt324 { get; set; }
        public UInt32 PropertyUInt325 { get; set; }
        public UInt32 PropertyUInt326 { get; set; }
        public UInt32 PropertyUInt327 { get; set; }
        public UInt32 PropertyUInt328 { get; set; }
        public UInt32 PropertyUInt329 { get; set; }
        public UInt32 PropertyUInt3210 { get; set; }
        public UInt32 PropertyUInt3211 { get; set; }
        private Int32 _prFieldInt320;
        private Int32 _prFieldInt321;
        private Int32 _prFieldInt322;
        private Int32 _prFieldInt323;
        private Int32 _prFieldInt324;

        public Int32 PrFieldInt320 {
            get { return _prFieldInt320; }
        }

        public Int32 PrFieldInt321 {
            get { return _prFieldInt321; }
        }

        public Int32 PrFieldInt322 {
            get { return _prFieldInt322; }
        }

        public Int32 PrFieldInt323 {
            get { return _prFieldInt323; }
        }

        public Int32 PrFieldInt324 {
            get { return _prFieldInt324; }
        }

        public Int32 _pubFieldInt320;
        public Int32 _pubFieldInt321;
        public Int32 _pubFieldInt322;
        public Int32 _pubFieldInt323;
        public Int32 _pubFieldInt324;
        public Int32 PropertyInt320 { get; set; }
        public Int32 PropertyInt321 { get; set; }
        public Int32 PropertyInt322 { get; set; }
        public Int32 PropertyInt323 { get; set; }
        public Int32 PropertyInt324 { get; set; }
        public Int32 PropertyInt325 { get; set; }
        public Int32 PropertyInt326 { get; set; }
        public Int32 PropertyInt327 { get; set; }
        public Int32 PropertyInt328 { get; set; }
        public Int32 PropertyInt329 { get; set; }
        public Int32 PropertyInt3210 { get; set; }
        public Int32 PropertyInt3211 { get; set; }
        private UInt64 _prFieldUInt640;
        private UInt64 _prFieldUInt641;
        private UInt64 _prFieldUInt642;
        private UInt64 _prFieldUInt643;
        private UInt64 _prFieldUInt644;

        public UInt64 PrFieldUInt640 {
            get { return _prFieldUInt640; }
        }

        public UInt64 PrFieldUInt641 {
            get { return _prFieldUInt641; }
        }

        public UInt64 PrFieldUInt642 {
            get { return _prFieldUInt642; }
        }

        public UInt64 PrFieldUInt643 {
            get { return _prFieldUInt643; }
        }

        public UInt64 PrFieldUInt644 {
            get { return _prFieldUInt644; }
        }

        public UInt64 _pubFieldUInt640;
        public UInt64 _pubFieldUInt641;
        public UInt64 _pubFieldUInt642;
        public UInt64 _pubFieldUInt643;
        public UInt64 _pubFieldUInt644;
        public UInt64 PropertyUInt640 { get; set; }
        public UInt64 PropertyUInt641 { get; set; }
        public UInt64 PropertyUInt642 { get; set; }
        public UInt64 PropertyUInt643 { get; set; }
        public UInt64 PropertyUInt644 { get; set; }
        public UInt64 PropertyUInt645 { get; set; }
        public UInt64 PropertyUInt646 { get; set; }
        public UInt64 PropertyUInt647 { get; set; }
        public UInt64 PropertyUInt648 { get; set; }
        public UInt64 PropertyUInt649 { get; set; }
        public UInt64 PropertyUInt6410 { get; set; }
        public UInt64 PropertyUInt6411 { get; set; }
        private Int64 _prFieldInt640;
        private Int64 _prFieldInt641;
        private Int64 _prFieldInt642;
        private Int64 _prFieldInt643;
        private Int64 _prFieldInt644;

        public Int64 PrFieldInt640 {
            get { return _prFieldInt640; }
        }

        public Int64 PrFieldInt641 {
            get { return _prFieldInt641; }
        }

        public Int64 PrFieldInt642 {
            get { return _prFieldInt642; }
        }

        public Int64 PrFieldInt643 {
            get { return _prFieldInt643; }
        }

        public Int64 PrFieldInt644 {
            get { return _prFieldInt644; }
        }

        public Int64 _pubFieldInt640;
        public Int64 _pubFieldInt641;
        public Int64 _pubFieldInt642;
        public Int64 _pubFieldInt643;
        public Int64 _pubFieldInt644;
        public Int64 PropertyInt640 { get; set; }
        public Int64 PropertyInt641 { get; set; }
        public Int64 PropertyInt642 { get; set; }
        public Int64 PropertyInt643 { get; set; }
        public Int64 PropertyInt644 { get; set; }
        public Int64 PropertyInt645 { get; set; }
        public Int64 PropertyInt646 { get; set; }
        public Int64 PropertyInt647 { get; set; }
        public Int64 PropertyInt648 { get; set; }
        public Int64 PropertyInt649 { get; set; }
        public Int64 PropertyInt6410 { get; set; }
        public Int64 PropertyInt6411 { get; set; }
        private Single _prFieldSingle0;
        private Single _prFieldSingle1;
        private Single _prFieldSingle2;
        private Single _prFieldSingle3;
        private Single _prFieldSingle4;

        public Single PrFieldSingle0 {
            get { return _prFieldSingle0; }
        }

        public Single PrFieldSingle1 {
            get { return _prFieldSingle1; }
        }

        public Single PrFieldSingle2 {
            get { return _prFieldSingle2; }
        }

        public Single PrFieldSingle3 {
            get { return _prFieldSingle3; }
        }

        public Single PrFieldSingle4 {
            get { return _prFieldSingle4; }
        }

        public Single _pubFieldSingle0;
        public Single _pubFieldSingle1;
        public Single _pubFieldSingle2;
        public Single _pubFieldSingle3;
        public Single _pubFieldSingle4;
        public Single PropertySingle0 { get; set; }
        public Single PropertySingle1 { get; set; }
        public Single PropertySingle2 { get; set; }
        public Single PropertySingle3 { get; set; }
        public Single PropertySingle4 { get; set; }
        public Single PropertySingle5 { get; set; }
        public Single PropertySingle6 { get; set; }
        public Single PropertySingle7 { get; set; }
        public Single PropertySingle8 { get; set; }
        public Single PropertySingle9 { get; set; }
        public Single PropertySingle10 { get; set; }
        public Single PropertySingle11 { get; set; }
        private Double _prFieldDouble0;
        private Double _prFieldDouble1;
        private Double _prFieldDouble2;
        private Double _prFieldDouble3;
        private Double _prFieldDouble4;

        public Double PrFieldDouble0 {
            get { return _prFieldDouble0; }
        }

        public Double PrFieldDouble1 {
            get { return _prFieldDouble1; }
        }

        public Double PrFieldDouble2 {
            get { return _prFieldDouble2; }
        }

        public Double PrFieldDouble3 {
            get { return _prFieldDouble3; }
        }

        public Double PrFieldDouble4 {
            get { return _prFieldDouble4; }
        }

        public Double _pubFieldDouble0;
        public Double _pubFieldDouble1;
        public Double _pubFieldDouble2;
        public Double _pubFieldDouble3;
        public Double _pubFieldDouble4;
        public Double PropertyDouble0 { get; set; }
        public Double PropertyDouble1 { get; set; }
        public Double PropertyDouble2 { get; set; }
        public Double PropertyDouble3 { get; set; }
        public Double PropertyDouble4 { get; set; }
        public Double PropertyDouble5 { get; set; }
        public Double PropertyDouble6 { get; set; }
        public Double PropertyDouble7 { get; set; }
        public Double PropertyDouble8 { get; set; }
        public Double PropertyDouble9 { get; set; }
        public Double PropertyDouble10 { get; set; }
        public Double PropertyDouble11 { get; set; }
        private Boolean _prFieldBoolean0;
        private Boolean _prFieldBoolean1;
        private Boolean _prFieldBoolean2;
        private Boolean _prFieldBoolean3;
        private Boolean _prFieldBoolean4;

        public Boolean PrFieldBoolean0 {
            get { return _prFieldBoolean0; }
        }

        public Boolean PrFieldBoolean1 {
            get { return _prFieldBoolean1; }
        }

        public Boolean PrFieldBoolean2 {
            get { return _prFieldBoolean2; }
        }

        public Boolean PrFieldBoolean3 {
            get { return _prFieldBoolean3; }
        }

        public Boolean PrFieldBoolean4 {
            get { return _prFieldBoolean4; }
        }

        public Boolean _pubFieldBoolean0;
        public Boolean _pubFieldBoolean1;
        public Boolean _pubFieldBoolean2;
        public Boolean _pubFieldBoolean3;
        public Boolean _pubFieldBoolean4;
        public Boolean PropertyBoolean0 { get; set; }
        public Boolean PropertyBoolean1 { get; set; }
        public Boolean PropertyBoolean2 { get; set; }
        public Boolean PropertyBoolean3 { get; set; }
        public Boolean PropertyBoolean4 { get; set; }
        public Boolean PropertyBoolean5 { get; set; }
        public Boolean PropertyBoolean6 { get; set; }
        public Boolean PropertyBoolean7 { get; set; }
        public Boolean PropertyBoolean8 { get; set; }
        public Boolean PropertyBoolean9 { get; set; }
        public Boolean PropertyBoolean10 { get; set; }
        public Boolean PropertyBoolean11 { get; set; }
        private Char _prFieldChar0;
        private Char _prFieldChar1;
        private Char _prFieldChar2;
        private Char _prFieldChar3;
        private Char _prFieldChar4;

        public Char PrFieldChar0 {
            get { return _prFieldChar0; }
        }

        public Char PrFieldChar1 {
            get { return _prFieldChar1; }
        }

        public Char PrFieldChar2 {
            get { return _prFieldChar2; }
        }

        public Char PrFieldChar3 {
            get { return _prFieldChar3; }
        }

        public Char PrFieldChar4 {
            get { return _prFieldChar4; }
        }

        public Char _pubFieldChar0;
        public Char _pubFieldChar1;
        public Char _pubFieldChar2;
        public Char _pubFieldChar3;
        public Char _pubFieldChar4;
        public Char PropertyChar0 { get; set; }
        public Char PropertyChar1 { get; set; }
        public Char PropertyChar2 { get; set; }
        public Char PropertyChar3 { get; set; }
        public Char PropertyChar4 { get; set; }
        public Char PropertyChar5 { get; set; }
        public Char PropertyChar6 { get; set; }
        public Char PropertyChar7 { get; set; }
        public Char PropertyChar8 { get; set; }
        public Char PropertyChar9 { get; set; }
        public Char PropertyChar10 { get; set; }
        public Char PropertyChar11 { get; set; }
        private DateTime _prFieldDateTime0;
        private DateTime _prFieldDateTime1;
        private DateTime _prFieldDateTime2;
        private DateTime _prFieldDateTime3;
        private DateTime _prFieldDateTime4;

        public DateTime PrFieldDateTime0 {
            get { return _prFieldDateTime0; }
        }

        public DateTime PrFieldDateTime1 {
            get { return _prFieldDateTime1; }
        }

        public DateTime PrFieldDateTime2 {
            get { return _prFieldDateTime2; }
        }

        public DateTime PrFieldDateTime3 {
            get { return _prFieldDateTime3; }
        }

        public DateTime PrFieldDateTime4 {
            get { return _prFieldDateTime4; }
        }

        public DateTime _pubFieldDateTime0;
        public DateTime _pubFieldDateTime1;
        public DateTime _pubFieldDateTime2;
        public DateTime _pubFieldDateTime3;
        public DateTime _pubFieldDateTime4;
        public DateTime PropertyDateTime0 { get; set; }
        public DateTime PropertyDateTime1 { get; set; }
        public DateTime PropertyDateTime2 { get; set; }
        public DateTime PropertyDateTime3 { get; set; }
        public DateTime PropertyDateTime4 { get; set; }
        public DateTime PropertyDateTime5 { get; set; }
        public DateTime PropertyDateTime6 { get; set; }
        public DateTime PropertyDateTime7 { get; set; }
        public DateTime PropertyDateTime8 { get; set; }
        public DateTime PropertyDateTime9 { get; set; }
        public DateTime PropertyDateTime10 { get; set; }
        public DateTime PropertyDateTime11 { get; set; }
        private Decimal _prFieldDecimal0;
        private Decimal _prFieldDecimal1;
        private Decimal _prFieldDecimal2;
        private Decimal _prFieldDecimal3;
        private Decimal _prFieldDecimal4;

        public Decimal PrFieldDecimal0 {
            get { return _prFieldDecimal0; }
        }

        public Decimal PrFieldDecimal1 {
            get { return _prFieldDecimal1; }
        }

        public Decimal PrFieldDecimal2 {
            get { return _prFieldDecimal2; }
        }

        public Decimal PrFieldDecimal3 {
            get { return _prFieldDecimal3; }
        }

        public Decimal PrFieldDecimal4 {
            get { return _prFieldDecimal4; }
        }

        public Decimal _pubFieldDecimal0;
        public Decimal _pubFieldDecimal1;
        public Decimal _pubFieldDecimal2;
        public Decimal _pubFieldDecimal3;
        public Decimal _pubFieldDecimal4;
        public Decimal PropertyDecimal0 { get; set; }
        public Decimal PropertyDecimal1 { get; set; }
        public Decimal PropertyDecimal2 { get; set; }
        public Decimal PropertyDecimal3 { get; set; }
        public Decimal PropertyDecimal4 { get; set; }
        public Decimal PropertyDecimal5 { get; set; }
        public Decimal PropertyDecimal6 { get; set; }
        public Decimal PropertyDecimal7 { get; set; }
        public Decimal PropertyDecimal8 { get; set; }
        public Decimal PropertyDecimal9 { get; set; }
        public Decimal PropertyDecimal10 { get; set; }
        public Decimal PropertyDecimal11 { get; set; }
        private SomeEnum _prFieldSomeEnum0;
        private SomeEnum _prFieldSomeEnum1;
        private SomeEnum _prFieldSomeEnum2;
        private SomeEnum _prFieldSomeEnum3;
        private SomeEnum _prFieldSomeEnum4;

        public SomeEnum PrFieldSomeEnum0 {
            get { return _prFieldSomeEnum0; }
        }

        public SomeEnum PrFieldSomeEnum1 {
            get { return _prFieldSomeEnum1; }
        }

        public SomeEnum PrFieldSomeEnum2 {
            get { return _prFieldSomeEnum2; }
        }

        public SomeEnum PrFieldSomeEnum3 {
            get { return _prFieldSomeEnum3; }
        }

        public SomeEnum PrFieldSomeEnum4 {
            get { return _prFieldSomeEnum4; }
        }

        public SomeEnum _pubFieldSomeEnum0;
        public SomeEnum _pubFieldSomeEnum1;
        public SomeEnum _pubFieldSomeEnum2;
        public SomeEnum _pubFieldSomeEnum3;
        public SomeEnum _pubFieldSomeEnum4;
        public SomeEnum PropertySomeEnum0 { get; set; }
        public SomeEnum PropertySomeEnum1 { get; set; }
        public SomeEnum PropertySomeEnum2 { get; set; }
        public SomeEnum PropertySomeEnum3 { get; set; }
        public SomeEnum PropertySomeEnum4 { get; set; }
        public SomeEnum PropertySomeEnum5 { get; set; }
        public SomeEnum PropertySomeEnum6 { get; set; }
        public SomeEnum PropertySomeEnum7 { get; set; }
        public SomeEnum PropertySomeEnum8 { get; set; }
        public SomeEnum PropertySomeEnum9 { get; set; }
        public SomeEnum PropertySomeEnum10 { get; set; }
        public SomeEnum PropertySomeEnum11 { get; set; }
        private RandomizableStruct _prFieldRandomizableStruct0;
        private RandomizableStruct _prFieldRandomizableStruct1;
        private RandomizableStruct _prFieldRandomizableStruct2;
        private RandomizableStruct _prFieldRandomizableStruct3;
        private RandomizableStruct _prFieldRandomizableStruct4;

        public RandomizableStruct PrFieldRandomizableStruct0 {
            get { return _prFieldRandomizableStruct0; }
        }

        public RandomizableStruct PrFieldRandomizableStruct1 {
            get { return _prFieldRandomizableStruct1; }
        }

        public RandomizableStruct PrFieldRandomizableStruct2 {
            get { return _prFieldRandomizableStruct2; }
        }

        public RandomizableStruct PrFieldRandomizableStruct3 {
            get { return _prFieldRandomizableStruct3; }
        }

        public RandomizableStruct PrFieldRandomizableStruct4 {
            get { return _prFieldRandomizableStruct4; }
        }

        public RandomizableStruct _pubFieldRandomizableStruct0;
        public RandomizableStruct _pubFieldRandomizableStruct1;
        public RandomizableStruct _pubFieldRandomizableStruct2;
        public RandomizableStruct _pubFieldRandomizableStruct3;
        public RandomizableStruct _pubFieldRandomizableStruct4;
        public RandomizableStruct PropertyRandomizableStruct0 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct1 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct2 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct3 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct4 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct5 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct6 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct7 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct8 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct9 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct10 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct11 { get; set; }
        private Randomizable _prFieldRandomizable0;
        private Randomizable _prFieldRandomizable1;
        private Randomizable _prFieldRandomizable2;
        private Randomizable _prFieldRandomizable3;
        private Randomizable _prFieldRandomizable4;

        public Randomizable PrFieldRandomizable0 {
            get { return _prFieldRandomizable0; }
        }

        public Randomizable PrFieldRandomizable1 {
            get { return _prFieldRandomizable1; }
        }

        public Randomizable PrFieldRandomizable2 {
            get { return _prFieldRandomizable2; }
        }

        public Randomizable PrFieldRandomizable3 {
            get { return _prFieldRandomizable3; }
        }

        public Randomizable PrFieldRandomizable4 {
            get { return _prFieldRandomizable4; }
        }

        public Randomizable _pubFieldRandomizable0;
        public Randomizable _pubFieldRandomizable1;
        public Randomizable _pubFieldRandomizable2;
        public Randomizable _pubFieldRandomizable3;
        public Randomizable _pubFieldRandomizable4;
        public Randomizable PropertyRandomizable0 { get; set; }
        public Randomizable PropertyRandomizable1 { get; set; }
        public Randomizable PropertyRandomizable2 { get; set; }
        public Randomizable PropertyRandomizable3 { get; set; }
        public Randomizable PropertyRandomizable4 { get; set; }
        public Randomizable PropertyRandomizable5 { get; set; }
        public Randomizable PropertyRandomizable6 { get; set; }
        public Randomizable PropertyRandomizable7 { get; set; }
        public Randomizable PropertyRandomizable8 { get; set; }
        public Randomizable PropertyRandomizable9 { get; set; }
        public Randomizable PropertyRandomizable10 { get; set; }
        public Randomizable PropertyRandomizable11 { get; set; }
        private String _prFieldString0;
        private String _prFieldString1;
        private String _prFieldString2;
        private String _prFieldString3;
        private String _prFieldString4;

        public String PrFieldString0 {
            get { return _prFieldString0; }
        }

        public String PrFieldString1 {
            get { return _prFieldString1; }
        }

        public String PrFieldString2 {
            get { return _prFieldString2; }
        }

        public String PrFieldString3 {
            get { return _prFieldString3; }
        }

        public String PrFieldString4 {
            get { return _prFieldString4; }
        }

        public String _pubFieldString0;
        public String _pubFieldString1;
        public String _pubFieldString2;
        public String _pubFieldString3;
        public String _pubFieldString4;
        public String PropertyString0 { get; set; }
        public String PropertyString1 { get; set; }
        public String PropertyString2 { get; set; }
        public String PropertyString3 { get; set; }
        public String PropertyString4 { get; set; }
        public String PropertyString5 { get; set; }
        public String PropertyString6 { get; set; }
        public String PropertyString7 { get; set; }
        public String PropertyString8 { get; set; }
        public String PropertyString9 { get; set; }
        public String PropertyString10 { get; set; }
        public String PropertyString11 { get; set; }
        private Byte[] _prFieldByteArr0;
        private Byte[] _prFieldByteArr1;
        private Byte[] _prFieldByteArr2;
        private Byte[] _prFieldByteArr3;
        private Byte[] _prFieldByteArr4;

        public Byte[] PrFieldByteArr0 {
            get { return _prFieldByteArr0; }
        }

        public Byte[] PrFieldByteArr1 {
            get { return _prFieldByteArr1; }
        }

        public Byte[] PrFieldByteArr2 {
            get { return _prFieldByteArr2; }
        }

        public Byte[] PrFieldByteArr3 {
            get { return _prFieldByteArr3; }
        }

        public Byte[] PrFieldByteArr4 {
            get { return _prFieldByteArr4; }
        }

        public Byte[] _pubFieldByteArr0;
        public Byte[] _pubFieldByteArr1;
        public Byte[] _pubFieldByteArr2;
        public Byte[] _pubFieldByteArr3;
        public Byte[] _pubFieldByteArr4;
        public Byte[] PropertyByteArr0 { get; set; }
        public Byte[] PropertyByteArr1 { get; set; }
        public Byte[] PropertyByteArr2 { get; set; }
        public Byte[] PropertyByteArr3 { get; set; }
        public Byte[] PropertyByteArr4 { get; set; }
        public Byte[] PropertyByteArr5 { get; set; }
        public Byte[] PropertyByteArr6 { get; set; }
        public Byte[] PropertyByteArr7 { get; set; }
        public Byte[] PropertyByteArr8 { get; set; }
        public Byte[] PropertyByteArr9 { get; set; }
        public Byte[] PropertyByteArr10 { get; set; }
        public Byte[] PropertyByteArr11 { get; set; }
        private SByte[] _prFieldSByteArr0;
        private SByte[] _prFieldSByteArr1;
        private SByte[] _prFieldSByteArr2;
        private SByte[] _prFieldSByteArr3;
        private SByte[] _prFieldSByteArr4;

        public SByte[] PrFieldSByteArr0 {
            get { return _prFieldSByteArr0; }
        }

        public SByte[] PrFieldSByteArr1 {
            get { return _prFieldSByteArr1; }
        }

        public SByte[] PrFieldSByteArr2 {
            get { return _prFieldSByteArr2; }
        }

        public SByte[] PrFieldSByteArr3 {
            get { return _prFieldSByteArr3; }
        }

        public SByte[] PrFieldSByteArr4 {
            get { return _prFieldSByteArr4; }
        }

        public SByte[] _pubFieldSByteArr0;
        public SByte[] _pubFieldSByteArr1;
        public SByte[] _pubFieldSByteArr2;
        public SByte[] _pubFieldSByteArr3;
        public SByte[] _pubFieldSByteArr4;
        public SByte[] PropertySByteArr0 { get; set; }
        public SByte[] PropertySByteArr1 { get; set; }
        public SByte[] PropertySByteArr2 { get; set; }
        public SByte[] PropertySByteArr3 { get; set; }
        public SByte[] PropertySByteArr4 { get; set; }
        public SByte[] PropertySByteArr5 { get; set; }
        public SByte[] PropertySByteArr6 { get; set; }
        public SByte[] PropertySByteArr7 { get; set; }
        public SByte[] PropertySByteArr8 { get; set; }
        public SByte[] PropertySByteArr9 { get; set; }
        public SByte[] PropertySByteArr10 { get; set; }
        public SByte[] PropertySByteArr11 { get; set; }
        private UInt16[] _prFieldUInt16Arr0;
        private UInt16[] _prFieldUInt16Arr1;
        private UInt16[] _prFieldUInt16Arr2;
        private UInt16[] _prFieldUInt16Arr3;
        private UInt16[] _prFieldUInt16Arr4;

        public UInt16[] PrFieldUInt16Arr0 {
            get { return _prFieldUInt16Arr0; }
        }

        public UInt16[] PrFieldUInt16Arr1 {
            get { return _prFieldUInt16Arr1; }
        }

        public UInt16[] PrFieldUInt16Arr2 {
            get { return _prFieldUInt16Arr2; }
        }

        public UInt16[] PrFieldUInt16Arr3 {
            get { return _prFieldUInt16Arr3; }
        }

        public UInt16[] PrFieldUInt16Arr4 {
            get { return _prFieldUInt16Arr4; }
        }

        public UInt16[] _pubFieldUInt16Arr0;
        public UInt16[] _pubFieldUInt16Arr1;
        public UInt16[] _pubFieldUInt16Arr2;
        public UInt16[] _pubFieldUInt16Arr3;
        public UInt16[] _pubFieldUInt16Arr4;
        public UInt16[] PropertyUInt16Arr0 { get; set; }
        public UInt16[] PropertyUInt16Arr1 { get; set; }
        public UInt16[] PropertyUInt16Arr2 { get; set; }
        public UInt16[] PropertyUInt16Arr3 { get; set; }
        public UInt16[] PropertyUInt16Arr4 { get; set; }
        public UInt16[] PropertyUInt16Arr5 { get; set; }
        public UInt16[] PropertyUInt16Arr6 { get; set; }
        public UInt16[] PropertyUInt16Arr7 { get; set; }
        public UInt16[] PropertyUInt16Arr8 { get; set; }
        public UInt16[] PropertyUInt16Arr9 { get; set; }
        public UInt16[] PropertyUInt16Arr10 { get; set; }
        public UInt16[] PropertyUInt16Arr11 { get; set; }
        private Int16[] _prFieldInt16Arr0;
        private Int16[] _prFieldInt16Arr1;
        private Int16[] _prFieldInt16Arr2;
        private Int16[] _prFieldInt16Arr3;
        private Int16[] _prFieldInt16Arr4;

        public Int16[] PrFieldInt16Arr0 {
            get { return _prFieldInt16Arr0; }
        }

        public Int16[] PrFieldInt16Arr1 {
            get { return _prFieldInt16Arr1; }
        }

        public Int16[] PrFieldInt16Arr2 {
            get { return _prFieldInt16Arr2; }
        }

        public Int16[] PrFieldInt16Arr3 {
            get { return _prFieldInt16Arr3; }
        }

        public Int16[] PrFieldInt16Arr4 {
            get { return _prFieldInt16Arr4; }
        }

        public Int16[] _pubFieldInt16Arr0;
        public Int16[] _pubFieldInt16Arr1;
        public Int16[] _pubFieldInt16Arr2;
        public Int16[] _pubFieldInt16Arr3;
        public Int16[] _pubFieldInt16Arr4;
        public Int16[] PropertyInt16Arr0 { get; set; }
        public Int16[] PropertyInt16Arr1 { get; set; }
        public Int16[] PropertyInt16Arr2 { get; set; }
        public Int16[] PropertyInt16Arr3 { get; set; }
        public Int16[] PropertyInt16Arr4 { get; set; }
        public Int16[] PropertyInt16Arr5 { get; set; }
        public Int16[] PropertyInt16Arr6 { get; set; }
        public Int16[] PropertyInt16Arr7 { get; set; }
        public Int16[] PropertyInt16Arr8 { get; set; }
        public Int16[] PropertyInt16Arr9 { get; set; }
        public Int16[] PropertyInt16Arr10 { get; set; }
        public Int16[] PropertyInt16Arr11 { get; set; }
        private UInt32[] _prFieldUInt32Arr0;
        private UInt32[] _prFieldUInt32Arr1;
        private UInt32[] _prFieldUInt32Arr2;
        private UInt32[] _prFieldUInt32Arr3;
        private UInt32[] _prFieldUInt32Arr4;

        public UInt32[] PrFieldUInt32Arr0 {
            get { return _prFieldUInt32Arr0; }
        }

        public UInt32[] PrFieldUInt32Arr1 {
            get { return _prFieldUInt32Arr1; }
        }

        public UInt32[] PrFieldUInt32Arr2 {
            get { return _prFieldUInt32Arr2; }
        }

        public UInt32[] PrFieldUInt32Arr3 {
            get { return _prFieldUInt32Arr3; }
        }

        public UInt32[] PrFieldUInt32Arr4 {
            get { return _prFieldUInt32Arr4; }
        }

        public UInt32[] _pubFieldUInt32Arr0;
        public UInt32[] _pubFieldUInt32Arr1;
        public UInt32[] _pubFieldUInt32Arr2;
        public UInt32[] _pubFieldUInt32Arr3;
        public UInt32[] _pubFieldUInt32Arr4;
        public UInt32[] PropertyUInt32Arr0 { get; set; }
        public UInt32[] PropertyUInt32Arr1 { get; set; }
        public UInt32[] PropertyUInt32Arr2 { get; set; }
        public UInt32[] PropertyUInt32Arr3 { get; set; }
        public UInt32[] PropertyUInt32Arr4 { get; set; }
        public UInt32[] PropertyUInt32Arr5 { get; set; }
        public UInt32[] PropertyUInt32Arr6 { get; set; }
        public UInt32[] PropertyUInt32Arr7 { get; set; }
        public UInt32[] PropertyUInt32Arr8 { get; set; }
        public UInt32[] PropertyUInt32Arr9 { get; set; }
        public UInt32[] PropertyUInt32Arr10 { get; set; }
        public UInt32[] PropertyUInt32Arr11 { get; set; }
        private Int32[] _prFieldInt32Arr0;
        private Int32[] _prFieldInt32Arr1;
        private Int32[] _prFieldInt32Arr2;
        private Int32[] _prFieldInt32Arr3;
        private Int32[] _prFieldInt32Arr4;

        public Int32[] PrFieldInt32Arr0 {
            get { return _prFieldInt32Arr0; }
        }

        public Int32[] PrFieldInt32Arr1 {
            get { return _prFieldInt32Arr1; }
        }

        public Int32[] PrFieldInt32Arr2 {
            get { return _prFieldInt32Arr2; }
        }

        public Int32[] PrFieldInt32Arr3 {
            get { return _prFieldInt32Arr3; }
        }

        public Int32[] PrFieldInt32Arr4 {
            get { return _prFieldInt32Arr4; }
        }

        public Int32[] _pubFieldInt32Arr0;
        public Int32[] _pubFieldInt32Arr1;
        public Int32[] _pubFieldInt32Arr2;
        public Int32[] _pubFieldInt32Arr3;
        public Int32[] _pubFieldInt32Arr4;
        public Int32[] PropertyInt32Arr0 { get; set; }
        public Int32[] PropertyInt32Arr1 { get; set; }
        public Int32[] PropertyInt32Arr2 { get; set; }
        public Int32[] PropertyInt32Arr3 { get; set; }
        public Int32[] PropertyInt32Arr4 { get; set; }
        public Int32[] PropertyInt32Arr5 { get; set; }
        public Int32[] PropertyInt32Arr6 { get; set; }
        public Int32[] PropertyInt32Arr7 { get; set; }
        public Int32[] PropertyInt32Arr8 { get; set; }
        public Int32[] PropertyInt32Arr9 { get; set; }
        public Int32[] PropertyInt32Arr10 { get; set; }
        public Int32[] PropertyInt32Arr11 { get; set; }
        private UInt64[] _prFieldUInt64Arr0;
        private UInt64[] _prFieldUInt64Arr1;
        private UInt64[] _prFieldUInt64Arr2;
        private UInt64[] _prFieldUInt64Arr3;
        private UInt64[] _prFieldUInt64Arr4;

        public UInt64[] PrFieldUInt64Arr0 {
            get { return _prFieldUInt64Arr0; }
        }

        public UInt64[] PrFieldUInt64Arr1 {
            get { return _prFieldUInt64Arr1; }
        }

        public UInt64[] PrFieldUInt64Arr2 {
            get { return _prFieldUInt64Arr2; }
        }

        public UInt64[] PrFieldUInt64Arr3 {
            get { return _prFieldUInt64Arr3; }
        }

        public UInt64[] PrFieldUInt64Arr4 {
            get { return _prFieldUInt64Arr4; }
        }

        public UInt64[] _pubFieldUInt64Arr0;
        public UInt64[] _pubFieldUInt64Arr1;
        public UInt64[] _pubFieldUInt64Arr2;
        public UInt64[] _pubFieldUInt64Arr3;
        public UInt64[] _pubFieldUInt64Arr4;
        public UInt64[] PropertyUInt64Arr0 { get; set; }
        public UInt64[] PropertyUInt64Arr1 { get; set; }
        public UInt64[] PropertyUInt64Arr2 { get; set; }
        public UInt64[] PropertyUInt64Arr3 { get; set; }
        public UInt64[] PropertyUInt64Arr4 { get; set; }
        public UInt64[] PropertyUInt64Arr5 { get; set; }
        public UInt64[] PropertyUInt64Arr6 { get; set; }
        public UInt64[] PropertyUInt64Arr7 { get; set; }
        public UInt64[] PropertyUInt64Arr8 { get; set; }
        public UInt64[] PropertyUInt64Arr9 { get; set; }
        public UInt64[] PropertyUInt64Arr10 { get; set; }
        public UInt64[] PropertyUInt64Arr11 { get; set; }
        private Int64[] _prFieldInt64Arr0;
        private Int64[] _prFieldInt64Arr1;
        private Int64[] _prFieldInt64Arr2;
        private Int64[] _prFieldInt64Arr3;
        private Int64[] _prFieldInt64Arr4;

        public Int64[] PrFieldInt64Arr0 {
            get { return _prFieldInt64Arr0; }
        }

        public Int64[] PrFieldInt64Arr1 {
            get { return _prFieldInt64Arr1; }
        }

        public Int64[] PrFieldInt64Arr2 {
            get { return _prFieldInt64Arr2; }
        }

        public Int64[] PrFieldInt64Arr3 {
            get { return _prFieldInt64Arr3; }
        }

        public Int64[] PrFieldInt64Arr4 {
            get { return _prFieldInt64Arr4; }
        }

        public Int64[] _pubFieldInt64Arr0;
        public Int64[] _pubFieldInt64Arr1;
        public Int64[] _pubFieldInt64Arr2;
        public Int64[] _pubFieldInt64Arr3;
        public Int64[] _pubFieldInt64Arr4;
        public Int64[] PropertyInt64Arr0 { get; set; }
        public Int64[] PropertyInt64Arr1 { get; set; }
        public Int64[] PropertyInt64Arr2 { get; set; }
        public Int64[] PropertyInt64Arr3 { get; set; }
        public Int64[] PropertyInt64Arr4 { get; set; }
        public Int64[] PropertyInt64Arr5 { get; set; }
        public Int64[] PropertyInt64Arr6 { get; set; }
        public Int64[] PropertyInt64Arr7 { get; set; }
        public Int64[] PropertyInt64Arr8 { get; set; }
        public Int64[] PropertyInt64Arr9 { get; set; }
        public Int64[] PropertyInt64Arr10 { get; set; }
        public Int64[] PropertyInt64Arr11 { get; set; }
        private Single[] _prFieldSingleArr0;
        private Single[] _prFieldSingleArr1;
        private Single[] _prFieldSingleArr2;
        private Single[] _prFieldSingleArr3;
        private Single[] _prFieldSingleArr4;

        public Single[] PrFieldSingleArr0 {
            get { return _prFieldSingleArr0; }
        }

        public Single[] PrFieldSingleArr1 {
            get { return _prFieldSingleArr1; }
        }

        public Single[] PrFieldSingleArr2 {
            get { return _prFieldSingleArr2; }
        }

        public Single[] PrFieldSingleArr3 {
            get { return _prFieldSingleArr3; }
        }

        public Single[] PrFieldSingleArr4 {
            get { return _prFieldSingleArr4; }
        }

        public Single[] _pubFieldSingleArr0;
        public Single[] _pubFieldSingleArr1;
        public Single[] _pubFieldSingleArr2;
        public Single[] _pubFieldSingleArr3;
        public Single[] _pubFieldSingleArr4;
        public Single[] PropertySingleArr0 { get; set; }
        public Single[] PropertySingleArr1 { get; set; }
        public Single[] PropertySingleArr2 { get; set; }
        public Single[] PropertySingleArr3 { get; set; }
        public Single[] PropertySingleArr4 { get; set; }
        public Single[] PropertySingleArr5 { get; set; }
        public Single[] PropertySingleArr6 { get; set; }
        public Single[] PropertySingleArr7 { get; set; }
        public Single[] PropertySingleArr8 { get; set; }
        public Single[] PropertySingleArr9 { get; set; }
        public Single[] PropertySingleArr10 { get; set; }
        public Single[] PropertySingleArr11 { get; set; }
        private Double[] _prFieldDoubleArr0;
        private Double[] _prFieldDoubleArr1;
        private Double[] _prFieldDoubleArr2;
        private Double[] _prFieldDoubleArr3;
        private Double[] _prFieldDoubleArr4;

        public Double[] PrFieldDoubleArr0 {
            get { return _prFieldDoubleArr0; }
        }

        public Double[] PrFieldDoubleArr1 {
            get { return _prFieldDoubleArr1; }
        }

        public Double[] PrFieldDoubleArr2 {
            get { return _prFieldDoubleArr2; }
        }

        public Double[] PrFieldDoubleArr3 {
            get { return _prFieldDoubleArr3; }
        }

        public Double[] PrFieldDoubleArr4 {
            get { return _prFieldDoubleArr4; }
        }

        public Double[] _pubFieldDoubleArr0;
        public Double[] _pubFieldDoubleArr1;
        public Double[] _pubFieldDoubleArr2;
        public Double[] _pubFieldDoubleArr3;
        public Double[] _pubFieldDoubleArr4;
        public Double[] PropertyDoubleArr0 { get; set; }
        public Double[] PropertyDoubleArr1 { get; set; }
        public Double[] PropertyDoubleArr2 { get; set; }
        public Double[] PropertyDoubleArr3 { get; set; }
        public Double[] PropertyDoubleArr4 { get; set; }
        public Double[] PropertyDoubleArr5 { get; set; }
        public Double[] PropertyDoubleArr6 { get; set; }
        public Double[] PropertyDoubleArr7 { get; set; }
        public Double[] PropertyDoubleArr8 { get; set; }
        public Double[] PropertyDoubleArr9 { get; set; }
        public Double[] PropertyDoubleArr10 { get; set; }
        public Double[] PropertyDoubleArr11 { get; set; }
        private Boolean[] _prFieldBooleanArr0;
        private Boolean[] _prFieldBooleanArr1;
        private Boolean[] _prFieldBooleanArr2;
        private Boolean[] _prFieldBooleanArr3;
        private Boolean[] _prFieldBooleanArr4;

        public Boolean[] PrFieldBooleanArr0 {
            get { return _prFieldBooleanArr0; }
        }

        public Boolean[] PrFieldBooleanArr1 {
            get { return _prFieldBooleanArr1; }
        }

        public Boolean[] PrFieldBooleanArr2 {
            get { return _prFieldBooleanArr2; }
        }

        public Boolean[] PrFieldBooleanArr3 {
            get { return _prFieldBooleanArr3; }
        }

        public Boolean[] PrFieldBooleanArr4 {
            get { return _prFieldBooleanArr4; }
        }

        public Boolean[] _pubFieldBooleanArr0;
        public Boolean[] _pubFieldBooleanArr1;
        public Boolean[] _pubFieldBooleanArr2;
        public Boolean[] _pubFieldBooleanArr3;
        public Boolean[] _pubFieldBooleanArr4;
        public Boolean[] PropertyBooleanArr0 { get; set; }
        public Boolean[] PropertyBooleanArr1 { get; set; }
        public Boolean[] PropertyBooleanArr2 { get; set; }
        public Boolean[] PropertyBooleanArr3 { get; set; }
        public Boolean[] PropertyBooleanArr4 { get; set; }
        public Boolean[] PropertyBooleanArr5 { get; set; }
        public Boolean[] PropertyBooleanArr6 { get; set; }
        public Boolean[] PropertyBooleanArr7 { get; set; }
        public Boolean[] PropertyBooleanArr8 { get; set; }
        public Boolean[] PropertyBooleanArr9 { get; set; }
        public Boolean[] PropertyBooleanArr10 { get; set; }
        public Boolean[] PropertyBooleanArr11 { get; set; }
        private Char[] _prFieldCharArr0;
        private Char[] _prFieldCharArr1;
        private Char[] _prFieldCharArr2;
        private Char[] _prFieldCharArr3;
        private Char[] _prFieldCharArr4;

        public Char[] PrFieldCharArr0 {
            get { return _prFieldCharArr0; }
        }

        public Char[] PrFieldCharArr1 {
            get { return _prFieldCharArr1; }
        }

        public Char[] PrFieldCharArr2 {
            get { return _prFieldCharArr2; }
        }

        public Char[] PrFieldCharArr3 {
            get { return _prFieldCharArr3; }
        }

        public Char[] PrFieldCharArr4 {
            get { return _prFieldCharArr4; }
        }

        public Char[] _pubFieldCharArr0;
        public Char[] _pubFieldCharArr1;
        public Char[] _pubFieldCharArr2;
        public Char[] _pubFieldCharArr3;
        public Char[] _pubFieldCharArr4;
        public Char[] PropertyCharArr0 { get; set; }
        public Char[] PropertyCharArr1 { get; set; }
        public Char[] PropertyCharArr2 { get; set; }
        public Char[] PropertyCharArr3 { get; set; }
        public Char[] PropertyCharArr4 { get; set; }
        public Char[] PropertyCharArr5 { get; set; }
        public Char[] PropertyCharArr6 { get; set; }
        public Char[] PropertyCharArr7 { get; set; }
        public Char[] PropertyCharArr8 { get; set; }
        public Char[] PropertyCharArr9 { get; set; }
        public Char[] PropertyCharArr10 { get; set; }
        public Char[] PropertyCharArr11 { get; set; }
        private DateTime[] _prFieldDateTimeArr0;
        private DateTime[] _prFieldDateTimeArr1;
        private DateTime[] _prFieldDateTimeArr2;
        private DateTime[] _prFieldDateTimeArr3;
        private DateTime[] _prFieldDateTimeArr4;

        public DateTime[] PrFieldDateTimeArr0 {
            get { return _prFieldDateTimeArr0; }
        }

        public DateTime[] PrFieldDateTimeArr1 {
            get { return _prFieldDateTimeArr1; }
        }

        public DateTime[] PrFieldDateTimeArr2 {
            get { return _prFieldDateTimeArr2; }
        }

        public DateTime[] PrFieldDateTimeArr3 {
            get { return _prFieldDateTimeArr3; }
        }

        public DateTime[] PrFieldDateTimeArr4 {
            get { return _prFieldDateTimeArr4; }
        }

        public DateTime[] _pubFieldDateTimeArr0;
        public DateTime[] _pubFieldDateTimeArr1;
        public DateTime[] _pubFieldDateTimeArr2;
        public DateTime[] _pubFieldDateTimeArr3;
        public DateTime[] _pubFieldDateTimeArr4;
        public DateTime[] PropertyDateTimeArr0 { get; set; }
        public DateTime[] PropertyDateTimeArr1 { get; set; }
        public DateTime[] PropertyDateTimeArr2 { get; set; }
        public DateTime[] PropertyDateTimeArr3 { get; set; }
        public DateTime[] PropertyDateTimeArr4 { get; set; }
        public DateTime[] PropertyDateTimeArr5 { get; set; }
        public DateTime[] PropertyDateTimeArr6 { get; set; }
        public DateTime[] PropertyDateTimeArr7 { get; set; }
        public DateTime[] PropertyDateTimeArr8 { get; set; }
        public DateTime[] PropertyDateTimeArr9 { get; set; }
        public DateTime[] PropertyDateTimeArr10 { get; set; }
        public DateTime[] PropertyDateTimeArr11 { get; set; }
        private Decimal[] _prFieldDecimalArr0;
        private Decimal[] _prFieldDecimalArr1;
        private Decimal[] _prFieldDecimalArr2;
        private Decimal[] _prFieldDecimalArr3;
        private Decimal[] _prFieldDecimalArr4;

        public Decimal[] PrFieldDecimalArr0 {
            get { return _prFieldDecimalArr0; }
        }

        public Decimal[] PrFieldDecimalArr1 {
            get { return _prFieldDecimalArr1; }
        }

        public Decimal[] PrFieldDecimalArr2 {
            get { return _prFieldDecimalArr2; }
        }

        public Decimal[] PrFieldDecimalArr3 {
            get { return _prFieldDecimalArr3; }
        }

        public Decimal[] PrFieldDecimalArr4 {
            get { return _prFieldDecimalArr4; }
        }

        public Decimal[] _pubFieldDecimalArr0;
        public Decimal[] _pubFieldDecimalArr1;
        public Decimal[] _pubFieldDecimalArr2;
        public Decimal[] _pubFieldDecimalArr3;
        public Decimal[] _pubFieldDecimalArr4;
        public Decimal[] PropertyDecimalArr0 { get; set; }
        public Decimal[] PropertyDecimalArr1 { get; set; }
        public Decimal[] PropertyDecimalArr2 { get; set; }
        public Decimal[] PropertyDecimalArr3 { get; set; }
        public Decimal[] PropertyDecimalArr4 { get; set; }
        public Decimal[] PropertyDecimalArr5 { get; set; }
        public Decimal[] PropertyDecimalArr6 { get; set; }
        public Decimal[] PropertyDecimalArr7 { get; set; }
        public Decimal[] PropertyDecimalArr8 { get; set; }
        public Decimal[] PropertyDecimalArr9 { get; set; }
        public Decimal[] PropertyDecimalArr10 { get; set; }
        public Decimal[] PropertyDecimalArr11 { get; set; }
        private SomeEnum[] _prFieldSomeEnumArr0;
        private SomeEnum[] _prFieldSomeEnumArr1;
        private SomeEnum[] _prFieldSomeEnumArr2;
        private SomeEnum[] _prFieldSomeEnumArr3;
        private SomeEnum[] _prFieldSomeEnumArr4;

        public SomeEnum[] PrFieldSomeEnumArr0 {
            get { return _prFieldSomeEnumArr0; }
        }

        public SomeEnum[] PrFieldSomeEnumArr1 {
            get { return _prFieldSomeEnumArr1; }
        }

        public SomeEnum[] PrFieldSomeEnumArr2 {
            get { return _prFieldSomeEnumArr2; }
        }

        public SomeEnum[] PrFieldSomeEnumArr3 {
            get { return _prFieldSomeEnumArr3; }
        }

        public SomeEnum[] PrFieldSomeEnumArr4 {
            get { return _prFieldSomeEnumArr4; }
        }

        public SomeEnum[] _pubFieldSomeEnumArr0;
        public SomeEnum[] _pubFieldSomeEnumArr1;
        public SomeEnum[] _pubFieldSomeEnumArr2;
        public SomeEnum[] _pubFieldSomeEnumArr3;
        public SomeEnum[] _pubFieldSomeEnumArr4;
        public SomeEnum[] PropertySomeEnumArr0 { get; set; }
        public SomeEnum[] PropertySomeEnumArr1 { get; set; }
        public SomeEnum[] PropertySomeEnumArr2 { get; set; }
        public SomeEnum[] PropertySomeEnumArr3 { get; set; }
        public SomeEnum[] PropertySomeEnumArr4 { get; set; }
        public SomeEnum[] PropertySomeEnumArr5 { get; set; }
        public SomeEnum[] PropertySomeEnumArr6 { get; set; }
        public SomeEnum[] PropertySomeEnumArr7 { get; set; }
        public SomeEnum[] PropertySomeEnumArr8 { get; set; }
        public SomeEnum[] PropertySomeEnumArr9 { get; set; }
        public SomeEnum[] PropertySomeEnumArr10 { get; set; }
        public SomeEnum[] PropertySomeEnumArr11 { get; set; }
        private RandomizableStruct[] _prFieldRandomizableStructArr0;
        private RandomizableStruct[] _prFieldRandomizableStructArr1;
        private RandomizableStruct[] _prFieldRandomizableStructArr2;
        private RandomizableStruct[] _prFieldRandomizableStructArr3;
        private RandomizableStruct[] _prFieldRandomizableStructArr4;

        public RandomizableStruct[] PrFieldRandomizableStructArr0 {
            get { return _prFieldRandomizableStructArr0; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr1 {
            get { return _prFieldRandomizableStructArr1; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr2 {
            get { return _prFieldRandomizableStructArr2; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr3 {
            get { return _prFieldRandomizableStructArr3; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructArr4 {
            get { return _prFieldRandomizableStructArr4; }
        }

        public RandomizableStruct[] _pubFieldRandomizableStructArr0;
        public RandomizableStruct[] _pubFieldRandomizableStructArr1;
        public RandomizableStruct[] _pubFieldRandomizableStructArr2;
        public RandomizableStruct[] _pubFieldRandomizableStructArr3;
        public RandomizableStruct[] _pubFieldRandomizableStructArr4;
        public RandomizableStruct[] PropertyRandomizableStructArr0 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr1 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr2 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr3 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr4 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr5 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr6 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr7 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr8 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr9 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr10 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructArr11 { get; set; }
        private Randomizable[] _prFieldRandomizableArr0;
        private Randomizable[] _prFieldRandomizableArr1;
        private Randomizable[] _prFieldRandomizableArr2;
        private Randomizable[] _prFieldRandomizableArr3;
        private Randomizable[] _prFieldRandomizableArr4;

        public Randomizable[] PrFieldRandomizableArr0 {
            get { return _prFieldRandomizableArr0; }
        }

        public Randomizable[] PrFieldRandomizableArr1 {
            get { return _prFieldRandomizableArr1; }
        }

        public Randomizable[] PrFieldRandomizableArr2 {
            get { return _prFieldRandomizableArr2; }
        }

        public Randomizable[] PrFieldRandomizableArr3 {
            get { return _prFieldRandomizableArr3; }
        }

        public Randomizable[] PrFieldRandomizableArr4 {
            get { return _prFieldRandomizableArr4; }
        }

        public Randomizable[] _pubFieldRandomizableArr0;
        public Randomizable[] _pubFieldRandomizableArr1;
        public Randomizable[] _pubFieldRandomizableArr2;
        public Randomizable[] _pubFieldRandomizableArr3;
        public Randomizable[] _pubFieldRandomizableArr4;
        public Randomizable[] PropertyRandomizableArr0 { get; set; }
        public Randomizable[] PropertyRandomizableArr1 { get; set; }
        public Randomizable[] PropertyRandomizableArr2 { get; set; }
        public Randomizable[] PropertyRandomizableArr3 { get; set; }
        public Randomizable[] PropertyRandomizableArr4 { get; set; }
        public Randomizable[] PropertyRandomizableArr5 { get; set; }
        public Randomizable[] PropertyRandomizableArr6 { get; set; }
        public Randomizable[] PropertyRandomizableArr7 { get; set; }
        public Randomizable[] PropertyRandomizableArr8 { get; set; }
        public Randomizable[] PropertyRandomizableArr9 { get; set; }
        public Randomizable[] PropertyRandomizableArr10 { get; set; }
        public Randomizable[] PropertyRandomizableArr11 { get; set; }
        private String[] _prFieldStringArr0;
        private String[] _prFieldStringArr1;
        private String[] _prFieldStringArr2;
        private String[] _prFieldStringArr3;
        private String[] _prFieldStringArr4;

        public String[] PrFieldStringArr0 {
            get { return _prFieldStringArr0; }
        }

        public String[] PrFieldStringArr1 {
            get { return _prFieldStringArr1; }
        }

        public String[] PrFieldStringArr2 {
            get { return _prFieldStringArr2; }
        }

        public String[] PrFieldStringArr3 {
            get { return _prFieldStringArr3; }
        }

        public String[] PrFieldStringArr4 {
            get { return _prFieldStringArr4; }
        }

        public String[] _pubFieldStringArr0;
        public String[] _pubFieldStringArr1;
        public String[] _pubFieldStringArr2;
        public String[] _pubFieldStringArr3;
        public String[] _pubFieldStringArr4;
        public String[] PropertyStringArr0 { get; set; }
        public String[] PropertyStringArr1 { get; set; }
        public String[] PropertyStringArr2 { get; set; }
        public String[] PropertyStringArr3 { get; set; }
        public String[] PropertyStringArr4 { get; set; }
        public String[] PropertyStringArr5 { get; set; }
        public String[] PropertyStringArr6 { get; set; }
        public String[] PropertyStringArr7 { get; set; }
        public String[] PropertyStringArr8 { get; set; }
        public String[] PropertyStringArr9 { get; set; }
        public String[] PropertyStringArr10 { get; set; }
        public String[] PropertyStringArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldByte0 = Utils.Default<Byte>();
            _prFieldByte1 = Utils.RandomValue<Byte>(r);
            _prFieldByte2 = Utils.RandomValue<Byte>(r);
            _prFieldByte3 = Utils.Default<Byte>();
            _prFieldByte4 = Utils.RandomValue<Byte>(r);
            _pubFieldByte0 = Utils.Default<Byte>();
            _pubFieldByte1 = Utils.RandomValue<Byte>(r);
            _pubFieldByte2 = Utils.Default<Byte>();
            _pubFieldByte3 = Utils.RandomValue<Byte>(r);
            _pubFieldByte4 = Utils.Default<Byte>();
            PropertyByte0 = Utils.Default<Byte>();
            PropertyByte1 = Utils.RandomValue<Byte>(r);
            PropertyByte2 = Utils.RandomValue<Byte>(r);
            PropertyByte3 = Utils.Default<Byte>();
            PropertyByte4 = Utils.RandomValue<Byte>(r);
            PropertyByte5 = Utils.RandomValue<Byte>(r);
            PropertyByte6 = Utils.Default<Byte>();
            PropertyByte7 = Utils.RandomValue<Byte>(r);
            PropertyByte8 = Utils.RandomValue<Byte>(r);
            PropertyByte9 = Utils.Default<Byte>();
            PropertyByte10 = Utils.RandomValue<Byte>(r);
            PropertyByte11 = Utils.RandomValue<Byte>(r);
            _prFieldSByte0 = Utils.Default<SByte>();
            _prFieldSByte1 = Utils.RandomValue<SByte>(r);
            _prFieldSByte2 = Utils.RandomValue<SByte>(r);
            _prFieldSByte3 = Utils.Default<SByte>();
            _prFieldSByte4 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte0 = Utils.Default<SByte>();
            _pubFieldSByte1 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte2 = Utils.Default<SByte>();
            _pubFieldSByte3 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte4 = Utils.Default<SByte>();
            PropertySByte0 = Utils.Default<SByte>();
            PropertySByte1 = Utils.RandomValue<SByte>(r);
            PropertySByte2 = Utils.RandomValue<SByte>(r);
            PropertySByte3 = Utils.Default<SByte>();
            PropertySByte4 = Utils.RandomValue<SByte>(r);
            PropertySByte5 = Utils.RandomValue<SByte>(r);
            PropertySByte6 = Utils.Default<SByte>();
            PropertySByte7 = Utils.RandomValue<SByte>(r);
            PropertySByte8 = Utils.RandomValue<SByte>(r);
            PropertySByte9 = Utils.Default<SByte>();
            PropertySByte10 = Utils.RandomValue<SByte>(r);
            PropertySByte11 = Utils.RandomValue<SByte>(r);
            _prFieldUInt160 = Utils.Default<UInt16>();
            _prFieldUInt161 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt162 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt163 = Utils.Default<UInt16>();
            _prFieldUInt164 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt160 = Utils.Default<UInt16>();
            _pubFieldUInt161 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt162 = Utils.Default<UInt16>();
            _pubFieldUInt163 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt164 = Utils.Default<UInt16>();
            PropertyUInt160 = Utils.Default<UInt16>();
            PropertyUInt161 = Utils.RandomValue<UInt16>(r);
            PropertyUInt162 = Utils.RandomValue<UInt16>(r);
            PropertyUInt163 = Utils.Default<UInt16>();
            PropertyUInt164 = Utils.RandomValue<UInt16>(r);
            PropertyUInt165 = Utils.RandomValue<UInt16>(r);
            PropertyUInt166 = Utils.Default<UInt16>();
            PropertyUInt167 = Utils.RandomValue<UInt16>(r);
            PropertyUInt168 = Utils.RandomValue<UInt16>(r);
            PropertyUInt169 = Utils.Default<UInt16>();
            PropertyUInt1610 = Utils.RandomValue<UInt16>(r);
            PropertyUInt1611 = Utils.RandomValue<UInt16>(r);
            _prFieldInt160 = Utils.Default<Int16>();
            _prFieldInt161 = Utils.RandomValue<Int16>(r);
            _prFieldInt162 = Utils.RandomValue<Int16>(r);
            _prFieldInt163 = Utils.Default<Int16>();
            _prFieldInt164 = Utils.RandomValue<Int16>(r);
            _pubFieldInt160 = Utils.Default<Int16>();
            _pubFieldInt161 = Utils.RandomValue<Int16>(r);
            _pubFieldInt162 = Utils.Default<Int16>();
            _pubFieldInt163 = Utils.RandomValue<Int16>(r);
            _pubFieldInt164 = Utils.Default<Int16>();
            PropertyInt160 = Utils.Default<Int16>();
            PropertyInt161 = Utils.RandomValue<Int16>(r);
            PropertyInt162 = Utils.RandomValue<Int16>(r);
            PropertyInt163 = Utils.Default<Int16>();
            PropertyInt164 = Utils.RandomValue<Int16>(r);
            PropertyInt165 = Utils.RandomValue<Int16>(r);
            PropertyInt166 = Utils.Default<Int16>();
            PropertyInt167 = Utils.RandomValue<Int16>(r);
            PropertyInt168 = Utils.RandomValue<Int16>(r);
            PropertyInt169 = Utils.Default<Int16>();
            PropertyInt1610 = Utils.RandomValue<Int16>(r);
            PropertyInt1611 = Utils.RandomValue<Int16>(r);
            _prFieldUInt320 = Utils.Default<UInt32>();
            _prFieldUInt321 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt322 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt323 = Utils.Default<UInt32>();
            _prFieldUInt324 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt320 = Utils.Default<UInt32>();
            _pubFieldUInt321 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt322 = Utils.Default<UInt32>();
            _pubFieldUInt323 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt324 = Utils.Default<UInt32>();
            PropertyUInt320 = Utils.Default<UInt32>();
            PropertyUInt321 = Utils.RandomValue<UInt32>(r);
            PropertyUInt322 = Utils.RandomValue<UInt32>(r);
            PropertyUInt323 = Utils.Default<UInt32>();
            PropertyUInt324 = Utils.RandomValue<UInt32>(r);
            PropertyUInt325 = Utils.RandomValue<UInt32>(r);
            PropertyUInt326 = Utils.Default<UInt32>();
            PropertyUInt327 = Utils.RandomValue<UInt32>(r);
            PropertyUInt328 = Utils.RandomValue<UInt32>(r);
            PropertyUInt329 = Utils.Default<UInt32>();
            PropertyUInt3210 = Utils.RandomValue<UInt32>(r);
            PropertyUInt3211 = Utils.RandomValue<UInt32>(r);
            _prFieldInt320 = Utils.Default<Int32>();
            _prFieldInt321 = Utils.RandomValue<Int32>(r);
            _prFieldInt322 = Utils.RandomValue<Int32>(r);
            _prFieldInt323 = Utils.Default<Int32>();
            _prFieldInt324 = Utils.RandomValue<Int32>(r);
            _pubFieldInt320 = Utils.Default<Int32>();
            _pubFieldInt321 = Utils.RandomValue<Int32>(r);
            _pubFieldInt322 = Utils.Default<Int32>();
            _pubFieldInt323 = Utils.RandomValue<Int32>(r);
            _pubFieldInt324 = Utils.Default<Int32>();
            PropertyInt320 = Utils.Default<Int32>();
            PropertyInt321 = Utils.RandomValue<Int32>(r);
            PropertyInt322 = Utils.RandomValue<Int32>(r);
            PropertyInt323 = Utils.Default<Int32>();
            PropertyInt324 = Utils.RandomValue<Int32>(r);
            PropertyInt325 = Utils.RandomValue<Int32>(r);
            PropertyInt326 = Utils.Default<Int32>();
            PropertyInt327 = Utils.RandomValue<Int32>(r);
            PropertyInt328 = Utils.RandomValue<Int32>(r);
            PropertyInt329 = Utils.Default<Int32>();
            PropertyInt3210 = Utils.RandomValue<Int32>(r);
            PropertyInt3211 = Utils.RandomValue<Int32>(r);
            _prFieldUInt640 = Utils.Default<UInt64>();
            _prFieldUInt641 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt642 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt643 = Utils.Default<UInt64>();
            _prFieldUInt644 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt640 = Utils.Default<UInt64>();
            _pubFieldUInt641 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt642 = Utils.Default<UInt64>();
            _pubFieldUInt643 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt644 = Utils.Default<UInt64>();
            PropertyUInt640 = Utils.Default<UInt64>();
            PropertyUInt641 = Utils.RandomValue<UInt64>(r);
            PropertyUInt642 = Utils.RandomValue<UInt64>(r);
            PropertyUInt643 = Utils.Default<UInt64>();
            PropertyUInt644 = Utils.RandomValue<UInt64>(r);
            PropertyUInt645 = Utils.RandomValue<UInt64>(r);
            PropertyUInt646 = Utils.Default<UInt64>();
            PropertyUInt647 = Utils.RandomValue<UInt64>(r);
            PropertyUInt648 = Utils.RandomValue<UInt64>(r);
            PropertyUInt649 = Utils.Default<UInt64>();
            PropertyUInt6410 = Utils.RandomValue<UInt64>(r);
            PropertyUInt6411 = Utils.RandomValue<UInt64>(r);
            _prFieldInt640 = Utils.Default<Int64>();
            _prFieldInt641 = Utils.RandomValue<Int64>(r);
            _prFieldInt642 = Utils.RandomValue<Int64>(r);
            _prFieldInt643 = Utils.Default<Int64>();
            _prFieldInt644 = Utils.RandomValue<Int64>(r);
            _pubFieldInt640 = Utils.Default<Int64>();
            _pubFieldInt641 = Utils.RandomValue<Int64>(r);
            _pubFieldInt642 = Utils.Default<Int64>();
            _pubFieldInt643 = Utils.RandomValue<Int64>(r);
            _pubFieldInt644 = Utils.Default<Int64>();
            PropertyInt640 = Utils.Default<Int64>();
            PropertyInt641 = Utils.RandomValue<Int64>(r);
            PropertyInt642 = Utils.RandomValue<Int64>(r);
            PropertyInt643 = Utils.Default<Int64>();
            PropertyInt644 = Utils.RandomValue<Int64>(r);
            PropertyInt645 = Utils.RandomValue<Int64>(r);
            PropertyInt646 = Utils.Default<Int64>();
            PropertyInt647 = Utils.RandomValue<Int64>(r);
            PropertyInt648 = Utils.RandomValue<Int64>(r);
            PropertyInt649 = Utils.Default<Int64>();
            PropertyInt6410 = Utils.RandomValue<Int64>(r);
            PropertyInt6411 = Utils.RandomValue<Int64>(r);
            _prFieldSingle0 = Utils.Default<Single>();
            _prFieldSingle1 = Utils.RandomValue<Single>(r);
            _prFieldSingle2 = Utils.RandomValue<Single>(r);
            _prFieldSingle3 = Utils.Default<Single>();
            _prFieldSingle4 = Utils.RandomValue<Single>(r);
            _pubFieldSingle0 = Utils.Default<Single>();
            _pubFieldSingle1 = Utils.RandomValue<Single>(r);
            _pubFieldSingle2 = Utils.Default<Single>();
            _pubFieldSingle3 = Utils.RandomValue<Single>(r);
            _pubFieldSingle4 = Utils.Default<Single>();
            PropertySingle0 = Utils.Default<Single>();
            PropertySingle1 = Utils.RandomValue<Single>(r);
            PropertySingle2 = Utils.RandomValue<Single>(r);
            PropertySingle3 = Utils.Default<Single>();
            PropertySingle4 = Utils.RandomValue<Single>(r);
            PropertySingle5 = Utils.RandomValue<Single>(r);
            PropertySingle6 = Utils.Default<Single>();
            PropertySingle7 = Utils.RandomValue<Single>(r);
            PropertySingle8 = Utils.RandomValue<Single>(r);
            PropertySingle9 = Utils.Default<Single>();
            PropertySingle10 = Utils.RandomValue<Single>(r);
            PropertySingle11 = Utils.RandomValue<Single>(r);
            _prFieldDouble0 = Utils.Default<Double>();
            _prFieldDouble1 = Utils.RandomValue<Double>(r);
            _prFieldDouble2 = Utils.RandomValue<Double>(r);
            _prFieldDouble3 = Utils.Default<Double>();
            _prFieldDouble4 = Utils.RandomValue<Double>(r);
            _pubFieldDouble0 = Utils.Default<Double>();
            _pubFieldDouble1 = Utils.RandomValue<Double>(r);
            _pubFieldDouble2 = Utils.Default<Double>();
            _pubFieldDouble3 = Utils.RandomValue<Double>(r);
            _pubFieldDouble4 = Utils.Default<Double>();
            PropertyDouble0 = Utils.Default<Double>();
            PropertyDouble1 = Utils.RandomValue<Double>(r);
            PropertyDouble2 = Utils.RandomValue<Double>(r);
            PropertyDouble3 = Utils.Default<Double>();
            PropertyDouble4 = Utils.RandomValue<Double>(r);
            PropertyDouble5 = Utils.RandomValue<Double>(r);
            PropertyDouble6 = Utils.Default<Double>();
            PropertyDouble7 = Utils.RandomValue<Double>(r);
            PropertyDouble8 = Utils.RandomValue<Double>(r);
            PropertyDouble9 = Utils.Default<Double>();
            PropertyDouble10 = Utils.RandomValue<Double>(r);
            PropertyDouble11 = Utils.RandomValue<Double>(r);
            _prFieldBoolean0 = Utils.Default<Boolean>();
            _prFieldBoolean1 = Utils.RandomValue<Boolean>(r);
            _prFieldBoolean2 = Utils.RandomValue<Boolean>(r);
            _prFieldBoolean3 = Utils.Default<Boolean>();
            _prFieldBoolean4 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean0 = Utils.Default<Boolean>();
            _pubFieldBoolean1 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean2 = Utils.Default<Boolean>();
            _pubFieldBoolean3 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean4 = Utils.Default<Boolean>();
            PropertyBoolean0 = Utils.Default<Boolean>();
            PropertyBoolean1 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean2 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean3 = Utils.Default<Boolean>();
            PropertyBoolean4 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean5 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean6 = Utils.Default<Boolean>();
            PropertyBoolean7 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean8 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean9 = Utils.Default<Boolean>();
            PropertyBoolean10 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean11 = Utils.RandomValue<Boolean>(r);
            _prFieldChar0 = Utils.Default<Char>();
            _prFieldChar1 = Utils.RandomValue<Char>(r);
            _prFieldChar2 = Utils.RandomValue<Char>(r);
            _prFieldChar3 = Utils.Default<Char>();
            _prFieldChar4 = Utils.RandomValue<Char>(r);
            _pubFieldChar0 = Utils.Default<Char>();
            _pubFieldChar1 = Utils.RandomValue<Char>(r);
            _pubFieldChar2 = Utils.Default<Char>();
            _pubFieldChar3 = Utils.RandomValue<Char>(r);
            _pubFieldChar4 = Utils.Default<Char>();
            PropertyChar0 = Utils.Default<Char>();
            PropertyChar1 = Utils.RandomValue<Char>(r);
            PropertyChar2 = Utils.RandomValue<Char>(r);
            PropertyChar3 = Utils.Default<Char>();
            PropertyChar4 = Utils.RandomValue<Char>(r);
            PropertyChar5 = Utils.RandomValue<Char>(r);
            PropertyChar6 = Utils.Default<Char>();
            PropertyChar7 = Utils.RandomValue<Char>(r);
            PropertyChar8 = Utils.RandomValue<Char>(r);
            PropertyChar9 = Utils.Default<Char>();
            PropertyChar10 = Utils.RandomValue<Char>(r);
            PropertyChar11 = Utils.RandomValue<Char>(r);
            _prFieldDateTime0 = Utils.Default<DateTime>();
            _prFieldDateTime1 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTime2 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTime3 = Utils.Default<DateTime>();
            _prFieldDateTime4 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime0 = Utils.Default<DateTime>();
            _pubFieldDateTime1 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime2 = Utils.Default<DateTime>();
            _pubFieldDateTime3 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime4 = Utils.Default<DateTime>();
            PropertyDateTime0 = Utils.Default<DateTime>();
            PropertyDateTime1 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime2 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime3 = Utils.Default<DateTime>();
            PropertyDateTime4 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime5 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime6 = Utils.Default<DateTime>();
            PropertyDateTime7 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime8 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime9 = Utils.Default<DateTime>();
            PropertyDateTime10 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime11 = Utils.RandomValue<DateTime>(r);
            _prFieldDecimal0 = Utils.Default<Decimal>();
            _prFieldDecimal1 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimal2 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimal3 = Utils.Default<Decimal>();
            _prFieldDecimal4 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal0 = Utils.Default<Decimal>();
            _pubFieldDecimal1 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal2 = Utils.Default<Decimal>();
            _pubFieldDecimal3 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal4 = Utils.Default<Decimal>();
            PropertyDecimal0 = Utils.Default<Decimal>();
            PropertyDecimal1 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal2 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal3 = Utils.Default<Decimal>();
            PropertyDecimal4 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal5 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal6 = Utils.Default<Decimal>();
            PropertyDecimal7 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal8 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal9 = Utils.Default<Decimal>();
            PropertyDecimal10 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal11 = Utils.RandomValue<Decimal>(r);
            _prFieldSomeEnum0 = Utils.Default<SomeEnum>();
            _prFieldSomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnum2 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnum3 = Utils.Default<SomeEnum>();
            _prFieldSomeEnum4 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum0 = Utils.Default<SomeEnum>();
            _pubFieldSomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum2 = Utils.Default<SomeEnum>();
            _pubFieldSomeEnum3 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum4 = Utils.Default<SomeEnum>();
            PropertySomeEnum0 = Utils.Default<SomeEnum>();
            PropertySomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum2 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum3 = Utils.Default<SomeEnum>();
            PropertySomeEnum4 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum5 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum6 = Utils.Default<SomeEnum>();
            PropertySomeEnum7 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum8 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum9 = Utils.Default<SomeEnum>();
            PropertySomeEnum10 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum11 = Utils.RandomValue<SomeEnum>(r);
            _prFieldRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            _prFieldRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStruct2 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStruct3 = Utils.Default<RandomizableStruct>();
            _prFieldRandomizableStruct4 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            _pubFieldRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct2 = Utils.Default<RandomizableStruct>();
            _pubFieldRandomizableStruct3 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct4 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct2 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct3 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct4 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct5 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct6 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct7 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct8 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct9 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct10 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct11 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizable0 = Utils.Default<Randomizable>();
            _prFieldRandomizable1 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizable2 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizable3 = Utils.Default<Randomizable>();
            _prFieldRandomizable4 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable0 = Utils.Default<Randomizable>();
            _pubFieldRandomizable1 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable2 = Utils.Default<Randomizable>();
            _pubFieldRandomizable3 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable4 = Utils.Default<Randomizable>();
            PropertyRandomizable0 = Utils.Default<Randomizable>();
            PropertyRandomizable1 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable2 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable3 = Utils.Default<Randomizable>();
            PropertyRandomizable4 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable5 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable6 = Utils.Default<Randomizable>();
            PropertyRandomizable7 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable8 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable9 = Utils.Default<Randomizable>();
            PropertyRandomizable10 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable11 = Utils.RandomValue<Randomizable>(r);
            _prFieldString0 = Utils.Default<String>();
            _prFieldString1 = Utils.RandomValue<String>(r);
            _prFieldString2 = Utils.RandomValue<String>(r);
            _prFieldString3 = Utils.Default<String>();
            _prFieldString4 = Utils.RandomValue<String>(r);
            _pubFieldString0 = Utils.Default<String>();
            _pubFieldString1 = Utils.RandomValue<String>(r);
            _pubFieldString2 = Utils.Default<String>();
            _pubFieldString3 = Utils.RandomValue<String>(r);
            _pubFieldString4 = Utils.Default<String>();
            PropertyString0 = Utils.Default<String>();
            PropertyString1 = Utils.RandomValue<String>(r);
            PropertyString2 = Utils.RandomValue<String>(r);
            PropertyString3 = Utils.Default<String>();
            PropertyString4 = Utils.RandomValue<String>(r);
            PropertyString5 = Utils.RandomValue<String>(r);
            PropertyString6 = Utils.Default<String>();
            PropertyString7 = Utils.RandomValue<String>(r);
            PropertyString8 = Utils.RandomValue<String>(r);
            PropertyString9 = Utils.Default<String>();
            PropertyString10 = Utils.RandomValue<String>(r);
            PropertyString11 = Utils.RandomValue<String>(r);
            _prFieldByteArr0 = Utils.Default<Byte[]>();
            _prFieldByteArr1 = Utils.RandomArray<Byte>(r);
            _prFieldByteArr2 = Utils.Default<Byte[]>();
            _prFieldByteArr3 = Utils.EmptyArray<Byte>();
            _prFieldByteArr4 = Utils.Default<Byte[]>();
            _pubFieldByteArr0 = Utils.Default<Byte[]>();
            _pubFieldByteArr1 = Utils.RandomArray<Byte>(r);
            _pubFieldByteArr2 = Utils.Default<Byte[]>();
            _pubFieldByteArr3 = Utils.EmptyArray<Byte>();
            _pubFieldByteArr4 = Utils.Default<Byte[]>();
            PropertyByteArr0 = Utils.Default<Byte[]>();
            PropertyByteArr1 = Utils.RandomArray<Byte>(r);
            PropertyByteArr2 = Utils.Default<Byte[]>();
            PropertyByteArr3 = Utils.EmptyArray<Byte>();
            PropertyByteArr4 = Utils.Default<Byte[]>();
            PropertyByteArr5 = Utils.RandomArray<Byte>(r);
            PropertyByteArr6 = Utils.Default<Byte[]>();
            PropertyByteArr7 = Utils.RandomArray<Byte>(r);
            PropertyByteArr8 = Utils.Default<Byte[]>();
            PropertyByteArr9 = Utils.EmptyArray<Byte>();
            PropertyByteArr10 = Utils.Default<Byte[]>();
            PropertyByteArr11 = Utils.RandomArray<Byte>(r);
            _prFieldSByteArr0 = Utils.Default<SByte[]>();
            _prFieldSByteArr1 = Utils.RandomArray<SByte>(r);
            _prFieldSByteArr2 = Utils.Default<SByte[]>();
            _prFieldSByteArr3 = Utils.EmptyArray<SByte>();
            _prFieldSByteArr4 = Utils.Default<SByte[]>();
            _pubFieldSByteArr0 = Utils.Default<SByte[]>();
            _pubFieldSByteArr1 = Utils.RandomArray<SByte>(r);
            _pubFieldSByteArr2 = Utils.Default<SByte[]>();
            _pubFieldSByteArr3 = Utils.EmptyArray<SByte>();
            _pubFieldSByteArr4 = Utils.Default<SByte[]>();
            PropertySByteArr0 = Utils.Default<SByte[]>();
            PropertySByteArr1 = Utils.RandomArray<SByte>(r);
            PropertySByteArr2 = Utils.Default<SByte[]>();
            PropertySByteArr3 = Utils.EmptyArray<SByte>();
            PropertySByteArr4 = Utils.Default<SByte[]>();
            PropertySByteArr5 = Utils.RandomArray<SByte>(r);
            PropertySByteArr6 = Utils.Default<SByte[]>();
            PropertySByteArr7 = Utils.RandomArray<SByte>(r);
            PropertySByteArr8 = Utils.Default<SByte[]>();
            PropertySByteArr9 = Utils.EmptyArray<SByte>();
            PropertySByteArr10 = Utils.Default<SByte[]>();
            PropertySByteArr11 = Utils.RandomArray<SByte>(r);
            _prFieldUInt16Arr0 = Utils.Default<UInt16[]>();
            _prFieldUInt16Arr1 = Utils.RandomArray<UInt16>(r);
            _prFieldUInt16Arr2 = Utils.Default<UInt16[]>();
            _prFieldUInt16Arr3 = Utils.EmptyArray<UInt16>();
            _prFieldUInt16Arr4 = Utils.Default<UInt16[]>();
            _pubFieldUInt16Arr0 = Utils.Default<UInt16[]>();
            _pubFieldUInt16Arr1 = Utils.RandomArray<UInt16>(r);
            _pubFieldUInt16Arr2 = Utils.Default<UInt16[]>();
            _pubFieldUInt16Arr3 = Utils.EmptyArray<UInt16>();
            _pubFieldUInt16Arr4 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr0 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr1 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16Arr2 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr3 = Utils.EmptyArray<UInt16>();
            PropertyUInt16Arr4 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr5 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16Arr6 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr7 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16Arr8 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr9 = Utils.EmptyArray<UInt16>();
            PropertyUInt16Arr10 = Utils.Default<UInt16[]>();
            PropertyUInt16Arr11 = Utils.RandomArray<UInt16>(r);
            _prFieldInt16Arr0 = Utils.Default<Int16[]>();
            _prFieldInt16Arr1 = Utils.RandomArray<Int16>(r);
            _prFieldInt16Arr2 = Utils.Default<Int16[]>();
            _prFieldInt16Arr3 = Utils.EmptyArray<Int16>();
            _prFieldInt16Arr4 = Utils.Default<Int16[]>();
            _pubFieldInt16Arr0 = Utils.Default<Int16[]>();
            _pubFieldInt16Arr1 = Utils.RandomArray<Int16>(r);
            _pubFieldInt16Arr2 = Utils.Default<Int16[]>();
            _pubFieldInt16Arr3 = Utils.EmptyArray<Int16>();
            _pubFieldInt16Arr4 = Utils.Default<Int16[]>();
            PropertyInt16Arr0 = Utils.Default<Int16[]>();
            PropertyInt16Arr1 = Utils.RandomArray<Int16>(r);
            PropertyInt16Arr2 = Utils.Default<Int16[]>();
            PropertyInt16Arr3 = Utils.EmptyArray<Int16>();
            PropertyInt16Arr4 = Utils.Default<Int16[]>();
            PropertyInt16Arr5 = Utils.RandomArray<Int16>(r);
            PropertyInt16Arr6 = Utils.Default<Int16[]>();
            PropertyInt16Arr7 = Utils.RandomArray<Int16>(r);
            PropertyInt16Arr8 = Utils.Default<Int16[]>();
            PropertyInt16Arr9 = Utils.EmptyArray<Int16>();
            PropertyInt16Arr10 = Utils.Default<Int16[]>();
            PropertyInt16Arr11 = Utils.RandomArray<Int16>(r);
            _prFieldUInt32Arr0 = Utils.Default<UInt32[]>();
            _prFieldUInt32Arr1 = Utils.RandomArray<UInt32>(r);
            _prFieldUInt32Arr2 = Utils.Default<UInt32[]>();
            _prFieldUInt32Arr3 = Utils.EmptyArray<UInt32>();
            _prFieldUInt32Arr4 = Utils.Default<UInt32[]>();
            _pubFieldUInt32Arr0 = Utils.Default<UInt32[]>();
            _pubFieldUInt32Arr1 = Utils.RandomArray<UInt32>(r);
            _pubFieldUInt32Arr2 = Utils.Default<UInt32[]>();
            _pubFieldUInt32Arr3 = Utils.EmptyArray<UInt32>();
            _pubFieldUInt32Arr4 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr0 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr1 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32Arr2 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr3 = Utils.EmptyArray<UInt32>();
            PropertyUInt32Arr4 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr5 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32Arr6 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr7 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32Arr8 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr9 = Utils.EmptyArray<UInt32>();
            PropertyUInt32Arr10 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr11 = Utils.RandomArray<UInt32>(r);
            _prFieldInt32Arr0 = Utils.Default<Int32[]>();
            _prFieldInt32Arr1 = Utils.RandomArray<Int32>(r);
            _prFieldInt32Arr2 = Utils.Default<Int32[]>();
            _prFieldInt32Arr3 = Utils.EmptyArray<Int32>();
            _prFieldInt32Arr4 = Utils.Default<Int32[]>();
            _pubFieldInt32Arr0 = Utils.Default<Int32[]>();
            _pubFieldInt32Arr1 = Utils.RandomArray<Int32>(r);
            _pubFieldInt32Arr2 = Utils.Default<Int32[]>();
            _pubFieldInt32Arr3 = Utils.EmptyArray<Int32>();
            _pubFieldInt32Arr4 = Utils.Default<Int32[]>();
            PropertyInt32Arr0 = Utils.Default<Int32[]>();
            PropertyInt32Arr1 = Utils.RandomArray<Int32>(r);
            PropertyInt32Arr2 = Utils.Default<Int32[]>();
            PropertyInt32Arr3 = Utils.EmptyArray<Int32>();
            PropertyInt32Arr4 = Utils.Default<Int32[]>();
            PropertyInt32Arr5 = Utils.RandomArray<Int32>(r);
            PropertyInt32Arr6 = Utils.Default<Int32[]>();
            PropertyInt32Arr7 = Utils.RandomArray<Int32>(r);
            PropertyInt32Arr8 = Utils.Default<Int32[]>();
            PropertyInt32Arr9 = Utils.EmptyArray<Int32>();
            PropertyInt32Arr10 = Utils.Default<Int32[]>();
            PropertyInt32Arr11 = Utils.RandomArray<Int32>(r);
            _prFieldUInt64Arr0 = Utils.Default<UInt64[]>();
            _prFieldUInt64Arr1 = Utils.RandomArray<UInt64>(r);
            _prFieldUInt64Arr2 = Utils.Default<UInt64[]>();
            _prFieldUInt64Arr3 = Utils.EmptyArray<UInt64>();
            _prFieldUInt64Arr4 = Utils.Default<UInt64[]>();
            _pubFieldUInt64Arr0 = Utils.Default<UInt64[]>();
            _pubFieldUInt64Arr1 = Utils.RandomArray<UInt64>(r);
            _pubFieldUInt64Arr2 = Utils.Default<UInt64[]>();
            _pubFieldUInt64Arr3 = Utils.EmptyArray<UInt64>();
            _pubFieldUInt64Arr4 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr0 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr1 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64Arr2 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr3 = Utils.EmptyArray<UInt64>();
            PropertyUInt64Arr4 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr5 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64Arr6 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr7 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64Arr8 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr9 = Utils.EmptyArray<UInt64>();
            PropertyUInt64Arr10 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr11 = Utils.RandomArray<UInt64>(r);
            _prFieldInt64Arr0 = Utils.Default<Int64[]>();
            _prFieldInt64Arr1 = Utils.RandomArray<Int64>(r);
            _prFieldInt64Arr2 = Utils.Default<Int64[]>();
            _prFieldInt64Arr3 = Utils.EmptyArray<Int64>();
            _prFieldInt64Arr4 = Utils.Default<Int64[]>();
            _pubFieldInt64Arr0 = Utils.Default<Int64[]>();
            _pubFieldInt64Arr1 = Utils.RandomArray<Int64>(r);
            _pubFieldInt64Arr2 = Utils.Default<Int64[]>();
            _pubFieldInt64Arr3 = Utils.EmptyArray<Int64>();
            _pubFieldInt64Arr4 = Utils.Default<Int64[]>();
            PropertyInt64Arr0 = Utils.Default<Int64[]>();
            PropertyInt64Arr1 = Utils.RandomArray<Int64>(r);
            PropertyInt64Arr2 = Utils.Default<Int64[]>();
            PropertyInt64Arr3 = Utils.EmptyArray<Int64>();
            PropertyInt64Arr4 = Utils.Default<Int64[]>();
            PropertyInt64Arr5 = Utils.RandomArray<Int64>(r);
            PropertyInt64Arr6 = Utils.Default<Int64[]>();
            PropertyInt64Arr7 = Utils.RandomArray<Int64>(r);
            PropertyInt64Arr8 = Utils.Default<Int64[]>();
            PropertyInt64Arr9 = Utils.EmptyArray<Int64>();
            PropertyInt64Arr10 = Utils.Default<Int64[]>();
            PropertyInt64Arr11 = Utils.RandomArray<Int64>(r);
            _prFieldSingleArr0 = Utils.Default<Single[]>();
            _prFieldSingleArr1 = Utils.RandomArray<Single>(r);
            _prFieldSingleArr2 = Utils.Default<Single[]>();
            _prFieldSingleArr3 = Utils.EmptyArray<Single>();
            _prFieldSingleArr4 = Utils.Default<Single[]>();
            _pubFieldSingleArr0 = Utils.Default<Single[]>();
            _pubFieldSingleArr1 = Utils.RandomArray<Single>(r);
            _pubFieldSingleArr2 = Utils.Default<Single[]>();
            _pubFieldSingleArr3 = Utils.EmptyArray<Single>();
            _pubFieldSingleArr4 = Utils.Default<Single[]>();
            PropertySingleArr0 = Utils.Default<Single[]>();
            PropertySingleArr1 = Utils.RandomArray<Single>(r);
            PropertySingleArr2 = Utils.Default<Single[]>();
            PropertySingleArr3 = Utils.EmptyArray<Single>();
            PropertySingleArr4 = Utils.Default<Single[]>();
            PropertySingleArr5 = Utils.RandomArray<Single>(r);
            PropertySingleArr6 = Utils.Default<Single[]>();
            PropertySingleArr7 = Utils.RandomArray<Single>(r);
            PropertySingleArr8 = Utils.Default<Single[]>();
            PropertySingleArr9 = Utils.EmptyArray<Single>();
            PropertySingleArr10 = Utils.Default<Single[]>();
            PropertySingleArr11 = Utils.RandomArray<Single>(r);
            _prFieldDoubleArr0 = Utils.Default<Double[]>();
            _prFieldDoubleArr1 = Utils.RandomArray<Double>(r);
            _prFieldDoubleArr2 = Utils.Default<Double[]>();
            _prFieldDoubleArr3 = Utils.EmptyArray<Double>();
            _prFieldDoubleArr4 = Utils.Default<Double[]>();
            _pubFieldDoubleArr0 = Utils.Default<Double[]>();
            _pubFieldDoubleArr1 = Utils.RandomArray<Double>(r);
            _pubFieldDoubleArr2 = Utils.Default<Double[]>();
            _pubFieldDoubleArr3 = Utils.EmptyArray<Double>();
            _pubFieldDoubleArr4 = Utils.Default<Double[]>();
            PropertyDoubleArr0 = Utils.Default<Double[]>();
            PropertyDoubleArr1 = Utils.RandomArray<Double>(r);
            PropertyDoubleArr2 = Utils.Default<Double[]>();
            PropertyDoubleArr3 = Utils.EmptyArray<Double>();
            PropertyDoubleArr4 = Utils.Default<Double[]>();
            PropertyDoubleArr5 = Utils.RandomArray<Double>(r);
            PropertyDoubleArr6 = Utils.Default<Double[]>();
            PropertyDoubleArr7 = Utils.RandomArray<Double>(r);
            PropertyDoubleArr8 = Utils.Default<Double[]>();
            PropertyDoubleArr9 = Utils.EmptyArray<Double>();
            PropertyDoubleArr10 = Utils.Default<Double[]>();
            PropertyDoubleArr11 = Utils.RandomArray<Double>(r);
            _prFieldBooleanArr0 = Utils.Default<Boolean[]>();
            _prFieldBooleanArr1 = Utils.RandomArray<Boolean>(r);
            _prFieldBooleanArr2 = Utils.Default<Boolean[]>();
            _prFieldBooleanArr3 = Utils.EmptyArray<Boolean>();
            _prFieldBooleanArr4 = Utils.Default<Boolean[]>();
            _pubFieldBooleanArr0 = Utils.Default<Boolean[]>();
            _pubFieldBooleanArr1 = Utils.RandomArray<Boolean>(r);
            _pubFieldBooleanArr2 = Utils.Default<Boolean[]>();
            _pubFieldBooleanArr3 = Utils.EmptyArray<Boolean>();
            _pubFieldBooleanArr4 = Utils.Default<Boolean[]>();
            PropertyBooleanArr0 = Utils.Default<Boolean[]>();
            PropertyBooleanArr1 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanArr2 = Utils.Default<Boolean[]>();
            PropertyBooleanArr3 = Utils.EmptyArray<Boolean>();
            PropertyBooleanArr4 = Utils.Default<Boolean[]>();
            PropertyBooleanArr5 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanArr6 = Utils.Default<Boolean[]>();
            PropertyBooleanArr7 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanArr8 = Utils.Default<Boolean[]>();
            PropertyBooleanArr9 = Utils.EmptyArray<Boolean>();
            PropertyBooleanArr10 = Utils.Default<Boolean[]>();
            PropertyBooleanArr11 = Utils.RandomArray<Boolean>(r);
            _prFieldCharArr0 = Utils.Default<Char[]>();
            _prFieldCharArr1 = Utils.RandomArray<Char>(r);
            _prFieldCharArr2 = Utils.Default<Char[]>();
            _prFieldCharArr3 = Utils.EmptyArray<Char>();
            _prFieldCharArr4 = Utils.Default<Char[]>();
            _pubFieldCharArr0 = Utils.Default<Char[]>();
            _pubFieldCharArr1 = Utils.RandomArray<Char>(r);
            _pubFieldCharArr2 = Utils.Default<Char[]>();
            _pubFieldCharArr3 = Utils.EmptyArray<Char>();
            _pubFieldCharArr4 = Utils.Default<Char[]>();
            PropertyCharArr0 = Utils.Default<Char[]>();
            PropertyCharArr1 = Utils.RandomArray<Char>(r);
            PropertyCharArr2 = Utils.Default<Char[]>();
            PropertyCharArr3 = Utils.EmptyArray<Char>();
            PropertyCharArr4 = Utils.Default<Char[]>();
            PropertyCharArr5 = Utils.RandomArray<Char>(r);
            PropertyCharArr6 = Utils.Default<Char[]>();
            PropertyCharArr7 = Utils.RandomArray<Char>(r);
            PropertyCharArr8 = Utils.Default<Char[]>();
            PropertyCharArr9 = Utils.EmptyArray<Char>();
            PropertyCharArr10 = Utils.Default<Char[]>();
            PropertyCharArr11 = Utils.RandomArray<Char>(r);
            _prFieldDateTimeArr0 = Utils.Default<DateTime[]>();
            _prFieldDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            _prFieldDateTimeArr2 = Utils.Default<DateTime[]>();
            _prFieldDateTimeArr3 = Utils.EmptyArray<DateTime>();
            _prFieldDateTimeArr4 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeArr0 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            _pubFieldDateTimeArr2 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeArr3 = Utils.EmptyArray<DateTime>();
            _pubFieldDateTimeArr4 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr0 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeArr2 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr3 = Utils.EmptyArray<DateTime>();
            PropertyDateTimeArr4 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr5 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeArr6 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr7 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeArr8 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr9 = Utils.EmptyArray<DateTime>();
            PropertyDateTimeArr10 = Utils.Default<DateTime[]>();
            PropertyDateTimeArr11 = Utils.RandomArray<DateTime>(r);
            _prFieldDecimalArr0 = Utils.Default<Decimal[]>();
            _prFieldDecimalArr1 = Utils.RandomArray<Decimal>(r);
            _prFieldDecimalArr2 = Utils.Default<Decimal[]>();
            _prFieldDecimalArr3 = Utils.EmptyArray<Decimal>();
            _prFieldDecimalArr4 = Utils.Default<Decimal[]>();
            _pubFieldDecimalArr0 = Utils.Default<Decimal[]>();
            _pubFieldDecimalArr1 = Utils.RandomArray<Decimal>(r);
            _pubFieldDecimalArr2 = Utils.Default<Decimal[]>();
            _pubFieldDecimalArr3 = Utils.EmptyArray<Decimal>();
            _pubFieldDecimalArr4 = Utils.Default<Decimal[]>();
            PropertyDecimalArr0 = Utils.Default<Decimal[]>();
            PropertyDecimalArr1 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalArr2 = Utils.Default<Decimal[]>();
            PropertyDecimalArr3 = Utils.EmptyArray<Decimal>();
            PropertyDecimalArr4 = Utils.Default<Decimal[]>();
            PropertyDecimalArr5 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalArr6 = Utils.Default<Decimal[]>();
            PropertyDecimalArr7 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalArr8 = Utils.Default<Decimal[]>();
            PropertyDecimalArr9 = Utils.EmptyArray<Decimal>();
            PropertyDecimalArr10 = Utils.Default<Decimal[]>();
            PropertyDecimalArr11 = Utils.RandomArray<Decimal>(r);
            _prFieldSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            _prFieldSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            _prFieldSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            _prFieldSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            _prFieldSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            _pubFieldSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            _pubFieldSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr0 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumArr2 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            PropertySomeEnumArr4 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr5 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumArr6 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr7 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumArr8 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr9 = Utils.EmptyArray<SomeEnum>();
            PropertySomeEnumArr10 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr11 = Utils.RandomArray<SomeEnum>(r);
            _prFieldRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            _prFieldRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            _prFieldRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            _prFieldRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            _prFieldRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            _pubFieldRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            _pubFieldRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            PropertyRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr5 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructArr6 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr7 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructArr8 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr9 = Utils.EmptyArray<RandomizableStruct>();
            PropertyRandomizableStructArr10 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructArr11 = Utils.RandomArray<RandomizableStruct>(r);
            _prFieldRandomizableArr0 = Utils.Default<Randomizable[]>();
            _prFieldRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            _prFieldRandomizableArr2 = Utils.Default<Randomizable[]>();
            _prFieldRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            _prFieldRandomizableArr4 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableArr0 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            _pubFieldRandomizableArr2 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            _pubFieldRandomizableArr4 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr0 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableArr2 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            PropertyRandomizableArr4 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr5 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableArr6 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr7 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableArr8 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr9 = Utils.EmptyArray<Randomizable>();
            PropertyRandomizableArr10 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr11 = Utils.RandomArray<Randomizable>(r);
            _prFieldStringArr0 = Utils.Default<String[]>();
            _prFieldStringArr1 = Utils.RandomArray<String>(r);
            _prFieldStringArr2 = Utils.Default<String[]>();
            _prFieldStringArr3 = Utils.EmptyArray<String>();
            _prFieldStringArr4 = Utils.Default<String[]>();
            _pubFieldStringArr0 = Utils.Default<String[]>();
            _pubFieldStringArr1 = Utils.RandomArray<String>(r);
            _pubFieldStringArr2 = Utils.Default<String[]>();
            _pubFieldStringArr3 = Utils.EmptyArray<String>();
            _pubFieldStringArr4 = Utils.Default<String[]>();
            PropertyStringArr0 = Utils.Default<String[]>();
            PropertyStringArr1 = Utils.RandomArray<String>(r);
            PropertyStringArr2 = Utils.Default<String[]>();
            PropertyStringArr3 = Utils.EmptyArray<String>();
            PropertyStringArr4 = Utils.Default<String[]>();
            PropertyStringArr5 = Utils.RandomArray<String>(r);
            PropertyStringArr6 = Utils.Default<String[]>();
            PropertyStringArr7 = Utils.RandomArray<String>(r);
            PropertyStringArr8 = Utils.Default<String[]>();
            PropertyStringArr9 = Utils.EmptyArray<String>();
            PropertyStringArr10 = Utils.Default<String[]>();
            PropertyStringArr11 = Utils.RandomArray<String>(r);
        }
    }
}