using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class DateTime_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListDateTimeClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListDateTimeClass) s2.Deserialize(typeof (ListDateTimeClass));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListDateTimeClass {
        private List<DateTime> _prFieldListList10;
        private List<DateTime> _prFieldListList11;
        private List<DateTime> _prFieldListList12;
        private List<DateTime> _prFieldListList13;
        private List<DateTime> _prFieldListList14;

        public List<DateTime> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<DateTime> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<DateTime> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<DateTime> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<DateTime> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<DateTime> _pubFieldListList10;
        public List<DateTime> _pubFieldListList11;
        public List<DateTime> _pubFieldListList12;
        public List<DateTime> _pubFieldListList13;
        public List<DateTime> _pubFieldListList14;
        public List<DateTime> PropertyListList10 { get; set; }
        public List<DateTime> PropertyListList11 { get; set; }
        public List<DateTime> PropertyListList12 { get; set; }
        public List<DateTime> PropertyListList13 { get; set; }
        public List<DateTime> PropertyListList14 { get; set; }
        public List<DateTime> PropertyListList15 { get; set; }
        public List<DateTime> PropertyListList16 { get; set; }
        public List<DateTime> PropertyListList17 { get; set; }
        public List<DateTime> PropertyListList18 { get; set; }
        public List<DateTime> PropertyListList19 { get; set; }
        public List<DateTime> PropertyListList110 { get; set; }
        public List<DateTime> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<DateTime>>();
            _prFieldListList11 = Utils.RandomList<DateTime>(r);
            _prFieldListList12 = Utils.Default<List<DateTime>>();
            _prFieldListList13 = Utils.EmptyList<DateTime>();
            _prFieldListList14 = Utils.Default<List<DateTime>>();
            _pubFieldListList10 = Utils.Default<List<DateTime>>();
            _pubFieldListList11 = Utils.RandomList<DateTime>(r);
            _pubFieldListList12 = Utils.Default<List<DateTime>>();
            _pubFieldListList13 = Utils.EmptyList<DateTime>();
            _pubFieldListList14 = Utils.Default<List<DateTime>>();
            PropertyListList10 = Utils.Default<List<DateTime>>();
            PropertyListList11 = Utils.RandomList<DateTime>(r);
            PropertyListList12 = Utils.Default<List<DateTime>>();
            PropertyListList13 = Utils.EmptyList<DateTime>();
            PropertyListList14 = Utils.Default<List<DateTime>>();
            PropertyListList15 = Utils.RandomList<DateTime>(r);
            PropertyListList16 = Utils.Default<List<DateTime>>();
            PropertyListList17 = Utils.RandomList<DateTime>(r);
            PropertyListList18 = Utils.Default<List<DateTime>>();
            PropertyListList19 = Utils.EmptyList<DateTime>();
            PropertyListList110 = Utils.Default<List<DateTime>>();
            PropertyListList111 = Utils.RandomList<DateTime>(r);
        }
    }
}