using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Mixed_Class_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new NewMixedClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (NewMixedClass) s2.Deserialize(typeof (NewMixedClass));

                CollectionAssert.AreEqual(o1.PrFieldByteByteArr0, o2.PrFieldByteByteArr0);
                CollectionAssert.AreEqual(o1.PrFieldByteByteArr1, o2.PrFieldByteByteArr1);
                CollectionAssert.AreEqual(o1.PrFieldByteByteArr2, o2.PrFieldByteByteArr2);
                CollectionAssert.AreEqual(o1.PrFieldByteByteArr3, o2.PrFieldByteByteArr3);
                CollectionAssert.AreEqual(o1.PrFieldByteByteArr4, o2.PrFieldByteByteArr4);
                CollectionAssert.AreEqual(o1._pubFieldByteByteArr0, o2._pubFieldByteByteArr0);
                CollectionAssert.AreEqual(o1._pubFieldByteByteArr1, o2._pubFieldByteByteArr1);
                CollectionAssert.AreEqual(o1._pubFieldByteByteArr2, o2._pubFieldByteByteArr2);
                CollectionAssert.AreEqual(o1._pubFieldByteByteArr3, o2._pubFieldByteByteArr3);
                CollectionAssert.AreEqual(o1._pubFieldByteByteArr4, o2._pubFieldByteByteArr4);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr0, o2.PropertyByteByteArr0);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr1, o2.PropertyByteByteArr1);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr2, o2.PropertyByteByteArr2);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr3, o2.PropertyByteByteArr3);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr4, o2.PropertyByteByteArr4);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr5, o2.PropertyByteByteArr5);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr6, o2.PropertyByteByteArr6);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr7, o2.PropertyByteByteArr7);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr8, o2.PropertyByteByteArr8);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr9, o2.PropertyByteByteArr9);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr10, o2.PropertyByteByteArr10);
                CollectionAssert.AreEqual(o1.PropertyByteByteArr11, o2.PropertyByteByteArr11);
                Assert.AreEqual(o1.PrFieldByte0, o2.PrFieldByte0);
                Assert.AreEqual(o1.PrFieldByte1, o2.PrFieldByte1);
                Assert.AreEqual(o1.PrFieldByte2, o2.PrFieldByte2);
                Assert.AreEqual(o1.PrFieldByte3, o2.PrFieldByte3);
                Assert.AreEqual(o1.PrFieldByte4, o2.PrFieldByte4);
                Assert.AreEqual(o1._pubFieldByte0, o2._pubFieldByte0);
                Assert.AreEqual(o1._pubFieldByte1, o2._pubFieldByte1);
                Assert.AreEqual(o1._pubFieldByte2, o2._pubFieldByte2);
                Assert.AreEqual(o1._pubFieldByte3, o2._pubFieldByte3);
                Assert.AreEqual(o1._pubFieldByte4, o2._pubFieldByte4);
                Assert.AreEqual(o1.PropertyByte0, o2.PropertyByte0);
                Assert.AreEqual(o1.PropertyByte1, o2.PropertyByte1);
                Assert.AreEqual(o1.PropertyByte2, o2.PropertyByte2);
                Assert.AreEqual(o1.PropertyByte3, o2.PropertyByte3);
                Assert.AreEqual(o1.PropertyByte4, o2.PropertyByte4);
                Assert.AreEqual(o1.PropertyByte5, o2.PropertyByte5);
                Assert.AreEqual(o1.PropertyByte6, o2.PropertyByte6);
                Assert.AreEqual(o1.PropertyByte7, o2.PropertyByte7);
                Assert.AreEqual(o1.PropertyByte8, o2.PropertyByte8);
                Assert.AreEqual(o1.PropertyByte9, o2.PropertyByte9);
                Assert.AreEqual(o1.PropertyByte10, o2.PropertyByte10);
                Assert.AreEqual(o1.PropertyByte11, o2.PropertyByte11);
                CollectionAssert.AreEqual(o1.PrFieldByteListList10, o2.PrFieldByteListList10);
                CollectionAssert.AreEqual(o1.PrFieldByteListList11, o2.PrFieldByteListList11);
                CollectionAssert.AreEqual(o1.PrFieldByteListList12, o2.PrFieldByteListList12);
                CollectionAssert.AreEqual(o1.PrFieldByteListList13, o2.PrFieldByteListList13);
                CollectionAssert.AreEqual(o1.PrFieldByteListList14, o2.PrFieldByteListList14);
                CollectionAssert.AreEqual(o1._pubFieldByteListList10, o2._pubFieldByteListList10);
                CollectionAssert.AreEqual(o1._pubFieldByteListList11, o2._pubFieldByteListList11);
                CollectionAssert.AreEqual(o1._pubFieldByteListList12, o2._pubFieldByteListList12);
                CollectionAssert.AreEqual(o1._pubFieldByteListList13, o2._pubFieldByteListList13);
                CollectionAssert.AreEqual(o1._pubFieldByteListList14, o2._pubFieldByteListList14);
                CollectionAssert.AreEqual(o1.PropertyByteListList10, o2.PropertyByteListList10);
                CollectionAssert.AreEqual(o1.PropertyByteListList11, o2.PropertyByteListList11);
                CollectionAssert.AreEqual(o1.PropertyByteListList12, o2.PropertyByteListList12);
                CollectionAssert.AreEqual(o1.PropertyByteListList13, o2.PropertyByteListList13);
                CollectionAssert.AreEqual(o1.PropertyByteListList14, o2.PropertyByteListList14);
                CollectionAssert.AreEqual(o1.PropertyByteListList15, o2.PropertyByteListList15);
                CollectionAssert.AreEqual(o1.PropertyByteListList16, o2.PropertyByteListList16);
                CollectionAssert.AreEqual(o1.PropertyByteListList17, o2.PropertyByteListList17);
                CollectionAssert.AreEqual(o1.PropertyByteListList18, o2.PropertyByteListList18);
                CollectionAssert.AreEqual(o1.PropertyByteListList19, o2.PropertyByteListList19);
                CollectionAssert.AreEqual(o1.PropertyByteListList110, o2.PropertyByteListList110);
                CollectionAssert.AreEqual(o1.PropertyByteListList111, o2.PropertyByteListList111);
                CollectionAssert.AreEqual(o1.PrFieldSByteSByteArr0, o2.PrFieldSByteSByteArr0);
                CollectionAssert.AreEqual(o1.PrFieldSByteSByteArr1, o2.PrFieldSByteSByteArr1);
                CollectionAssert.AreEqual(o1.PrFieldSByteSByteArr2, o2.PrFieldSByteSByteArr2);
                CollectionAssert.AreEqual(o1.PrFieldSByteSByteArr3, o2.PrFieldSByteSByteArr3);
                CollectionAssert.AreEqual(o1.PrFieldSByteSByteArr4, o2.PrFieldSByteSByteArr4);
                CollectionAssert.AreEqual(o1._pubFieldSByteSByteArr0, o2._pubFieldSByteSByteArr0);
                CollectionAssert.AreEqual(o1._pubFieldSByteSByteArr1, o2._pubFieldSByteSByteArr1);
                CollectionAssert.AreEqual(o1._pubFieldSByteSByteArr2, o2._pubFieldSByteSByteArr2);
                CollectionAssert.AreEqual(o1._pubFieldSByteSByteArr3, o2._pubFieldSByteSByteArr3);
                CollectionAssert.AreEqual(o1._pubFieldSByteSByteArr4, o2._pubFieldSByteSByteArr4);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr0, o2.PropertySByteSByteArr0);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr1, o2.PropertySByteSByteArr1);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr2, o2.PropertySByteSByteArr2);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr3, o2.PropertySByteSByteArr3);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr4, o2.PropertySByteSByteArr4);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr5, o2.PropertySByteSByteArr5);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr6, o2.PropertySByteSByteArr6);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr7, o2.PropertySByteSByteArr7);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr8, o2.PropertySByteSByteArr8);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr9, o2.PropertySByteSByteArr9);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr10, o2.PropertySByteSByteArr10);
                CollectionAssert.AreEqual(o1.PropertySByteSByteArr11, o2.PropertySByteSByteArr11);
                Assert.AreEqual(o1.PrFieldSByte0, o2.PrFieldSByte0);
                Assert.AreEqual(o1.PrFieldSByte1, o2.PrFieldSByte1);
                Assert.AreEqual(o1.PrFieldSByte2, o2.PrFieldSByte2);
                Assert.AreEqual(o1.PrFieldSByte3, o2.PrFieldSByte3);
                Assert.AreEqual(o1.PrFieldSByte4, o2.PrFieldSByte4);
                Assert.AreEqual(o1._pubFieldSByte0, o2._pubFieldSByte0);
                Assert.AreEqual(o1._pubFieldSByte1, o2._pubFieldSByte1);
                Assert.AreEqual(o1._pubFieldSByte2, o2._pubFieldSByte2);
                Assert.AreEqual(o1._pubFieldSByte3, o2._pubFieldSByte3);
                Assert.AreEqual(o1._pubFieldSByte4, o2._pubFieldSByte4);
                Assert.AreEqual(o1.PropertySByte0, o2.PropertySByte0);
                Assert.AreEqual(o1.PropertySByte1, o2.PropertySByte1);
                Assert.AreEqual(o1.PropertySByte2, o2.PropertySByte2);
                Assert.AreEqual(o1.PropertySByte3, o2.PropertySByte3);
                Assert.AreEqual(o1.PropertySByte4, o2.PropertySByte4);
                Assert.AreEqual(o1.PropertySByte5, o2.PropertySByte5);
                Assert.AreEqual(o1.PropertySByte6, o2.PropertySByte6);
                Assert.AreEqual(o1.PropertySByte7, o2.PropertySByte7);
                Assert.AreEqual(o1.PropertySByte8, o2.PropertySByte8);
                Assert.AreEqual(o1.PropertySByte9, o2.PropertySByte9);
                Assert.AreEqual(o1.PropertySByte10, o2.PropertySByte10);
                Assert.AreEqual(o1.PropertySByte11, o2.PropertySByte11);
                CollectionAssert.AreEqual(o1.PrFieldSByteListList10, o2.PrFieldSByteListList10);
                CollectionAssert.AreEqual(o1.PrFieldSByteListList11, o2.PrFieldSByteListList11);
                CollectionAssert.AreEqual(o1.PrFieldSByteListList12, o2.PrFieldSByteListList12);
                CollectionAssert.AreEqual(o1.PrFieldSByteListList13, o2.PrFieldSByteListList13);
                CollectionAssert.AreEqual(o1.PrFieldSByteListList14, o2.PrFieldSByteListList14);
                CollectionAssert.AreEqual(o1._pubFieldSByteListList10, o2._pubFieldSByteListList10);
                CollectionAssert.AreEqual(o1._pubFieldSByteListList11, o2._pubFieldSByteListList11);
                CollectionAssert.AreEqual(o1._pubFieldSByteListList12, o2._pubFieldSByteListList12);
                CollectionAssert.AreEqual(o1._pubFieldSByteListList13, o2._pubFieldSByteListList13);
                CollectionAssert.AreEqual(o1._pubFieldSByteListList14, o2._pubFieldSByteListList14);
                CollectionAssert.AreEqual(o1.PropertySByteListList10, o2.PropertySByteListList10);
                CollectionAssert.AreEqual(o1.PropertySByteListList11, o2.PropertySByteListList11);
                CollectionAssert.AreEqual(o1.PropertySByteListList12, o2.PropertySByteListList12);
                CollectionAssert.AreEqual(o1.PropertySByteListList13, o2.PropertySByteListList13);
                CollectionAssert.AreEqual(o1.PropertySByteListList14, o2.PropertySByteListList14);
                CollectionAssert.AreEqual(o1.PropertySByteListList15, o2.PropertySByteListList15);
                CollectionAssert.AreEqual(o1.PropertySByteListList16, o2.PropertySByteListList16);
                CollectionAssert.AreEqual(o1.PropertySByteListList17, o2.PropertySByteListList17);
                CollectionAssert.AreEqual(o1.PropertySByteListList18, o2.PropertySByteListList18);
                CollectionAssert.AreEqual(o1.PropertySByteListList19, o2.PropertySByteListList19);
                CollectionAssert.AreEqual(o1.PropertySByteListList110, o2.PropertySByteListList110);
                CollectionAssert.AreEqual(o1.PropertySByteListList111, o2.PropertySByteListList111);
                CollectionAssert.AreEqual(o1.PrFieldUInt16UInt16Arr0, o2.PrFieldUInt16UInt16Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt16UInt16Arr1, o2.PrFieldUInt16UInt16Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt16UInt16Arr2, o2.PrFieldUInt16UInt16Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt16UInt16Arr3, o2.PrFieldUInt16UInt16Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt16UInt16Arr4, o2.PrFieldUInt16UInt16Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt16UInt16Arr0, o2._pubFieldUInt16UInt16Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt16UInt16Arr1, o2._pubFieldUInt16UInt16Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt16UInt16Arr2, o2._pubFieldUInt16UInt16Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt16UInt16Arr3, o2._pubFieldUInt16UInt16Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt16UInt16Arr4, o2._pubFieldUInt16UInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr0, o2.PropertyUInt16UInt16Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr1, o2.PropertyUInt16UInt16Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr2, o2.PropertyUInt16UInt16Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr3, o2.PropertyUInt16UInt16Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr4, o2.PropertyUInt16UInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr5, o2.PropertyUInt16UInt16Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr6, o2.PropertyUInt16UInt16Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr7, o2.PropertyUInt16UInt16Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr8, o2.PropertyUInt16UInt16Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr9, o2.PropertyUInt16UInt16Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr10, o2.PropertyUInt16UInt16Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt16UInt16Arr11, o2.PropertyUInt16UInt16Arr11);
                Assert.AreEqual(o1.PrFieldUInt160, o2.PrFieldUInt160);
                Assert.AreEqual(o1.PrFieldUInt161, o2.PrFieldUInt161);
                Assert.AreEqual(o1.PrFieldUInt162, o2.PrFieldUInt162);
                Assert.AreEqual(o1.PrFieldUInt163, o2.PrFieldUInt163);
                Assert.AreEqual(o1.PrFieldUInt164, o2.PrFieldUInt164);
                Assert.AreEqual(o1._pubFieldUInt160, o2._pubFieldUInt160);
                Assert.AreEqual(o1._pubFieldUInt161, o2._pubFieldUInt161);
                Assert.AreEqual(o1._pubFieldUInt162, o2._pubFieldUInt162);
                Assert.AreEqual(o1._pubFieldUInt163, o2._pubFieldUInt163);
                Assert.AreEqual(o1._pubFieldUInt164, o2._pubFieldUInt164);
                Assert.AreEqual(o1.PropertyUInt160, o2.PropertyUInt160);
                Assert.AreEqual(o1.PropertyUInt161, o2.PropertyUInt161);
                Assert.AreEqual(o1.PropertyUInt162, o2.PropertyUInt162);
                Assert.AreEqual(o1.PropertyUInt163, o2.PropertyUInt163);
                Assert.AreEqual(o1.PropertyUInt164, o2.PropertyUInt164);
                Assert.AreEqual(o1.PropertyUInt165, o2.PropertyUInt165);
                Assert.AreEqual(o1.PropertyUInt166, o2.PropertyUInt166);
                Assert.AreEqual(o1.PropertyUInt167, o2.PropertyUInt167);
                Assert.AreEqual(o1.PropertyUInt168, o2.PropertyUInt168);
                Assert.AreEqual(o1.PropertyUInt169, o2.PropertyUInt169);
                Assert.AreEqual(o1.PropertyUInt1610, o2.PropertyUInt1610);
                Assert.AreEqual(o1.PropertyUInt1611, o2.PropertyUInt1611);
                CollectionAssert.AreEqual(o1.PrFieldUInt16ListList10, o2.PrFieldUInt16ListList10);
                CollectionAssert.AreEqual(o1.PrFieldUInt16ListList11, o2.PrFieldUInt16ListList11);
                CollectionAssert.AreEqual(o1.PrFieldUInt16ListList12, o2.PrFieldUInt16ListList12);
                CollectionAssert.AreEqual(o1.PrFieldUInt16ListList13, o2.PrFieldUInt16ListList13);
                CollectionAssert.AreEqual(o1.PrFieldUInt16ListList14, o2.PrFieldUInt16ListList14);
                CollectionAssert.AreEqual(o1._pubFieldUInt16ListList10, o2._pubFieldUInt16ListList10);
                CollectionAssert.AreEqual(o1._pubFieldUInt16ListList11, o2._pubFieldUInt16ListList11);
                CollectionAssert.AreEqual(o1._pubFieldUInt16ListList12, o2._pubFieldUInt16ListList12);
                CollectionAssert.AreEqual(o1._pubFieldUInt16ListList13, o2._pubFieldUInt16ListList13);
                CollectionAssert.AreEqual(o1._pubFieldUInt16ListList14, o2._pubFieldUInt16ListList14);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList10, o2.PropertyUInt16ListList10);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList11, o2.PropertyUInt16ListList11);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList12, o2.PropertyUInt16ListList12);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList13, o2.PropertyUInt16ListList13);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList14, o2.PropertyUInt16ListList14);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList15, o2.PropertyUInt16ListList15);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList16, o2.PropertyUInt16ListList16);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList17, o2.PropertyUInt16ListList17);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList18, o2.PropertyUInt16ListList18);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList19, o2.PropertyUInt16ListList19);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList110, o2.PropertyUInt16ListList110);
                CollectionAssert.AreEqual(o1.PropertyUInt16ListList111, o2.PropertyUInt16ListList111);
                CollectionAssert.AreEqual(o1.PrFieldInt16Int16Arr0, o2.PrFieldInt16Int16Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt16Int16Arr1, o2.PrFieldInt16Int16Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt16Int16Arr2, o2.PrFieldInt16Int16Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt16Int16Arr3, o2.PrFieldInt16Int16Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt16Int16Arr4, o2.PrFieldInt16Int16Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt16Int16Arr0, o2._pubFieldInt16Int16Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt16Int16Arr1, o2._pubFieldInt16Int16Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt16Int16Arr2, o2._pubFieldInt16Int16Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt16Int16Arr3, o2._pubFieldInt16Int16Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt16Int16Arr4, o2._pubFieldInt16Int16Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr0, o2.PropertyInt16Int16Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr1, o2.PropertyInt16Int16Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr2, o2.PropertyInt16Int16Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr3, o2.PropertyInt16Int16Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr4, o2.PropertyInt16Int16Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr5, o2.PropertyInt16Int16Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr6, o2.PropertyInt16Int16Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr7, o2.PropertyInt16Int16Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr8, o2.PropertyInt16Int16Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr9, o2.PropertyInt16Int16Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr10, o2.PropertyInt16Int16Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt16Int16Arr11, o2.PropertyInt16Int16Arr11);
                Assert.AreEqual(o1.PrFieldInt160, o2.PrFieldInt160);
                Assert.AreEqual(o1.PrFieldInt161, o2.PrFieldInt161);
                Assert.AreEqual(o1.PrFieldInt162, o2.PrFieldInt162);
                Assert.AreEqual(o1.PrFieldInt163, o2.PrFieldInt163);
                Assert.AreEqual(o1.PrFieldInt164, o2.PrFieldInt164);
                Assert.AreEqual(o1._pubFieldInt160, o2._pubFieldInt160);
                Assert.AreEqual(o1._pubFieldInt161, o2._pubFieldInt161);
                Assert.AreEqual(o1._pubFieldInt162, o2._pubFieldInt162);
                Assert.AreEqual(o1._pubFieldInt163, o2._pubFieldInt163);
                Assert.AreEqual(o1._pubFieldInt164, o2._pubFieldInt164);
                Assert.AreEqual(o1.PropertyInt160, o2.PropertyInt160);
                Assert.AreEqual(o1.PropertyInt161, o2.PropertyInt161);
                Assert.AreEqual(o1.PropertyInt162, o2.PropertyInt162);
                Assert.AreEqual(o1.PropertyInt163, o2.PropertyInt163);
                Assert.AreEqual(o1.PropertyInt164, o2.PropertyInt164);
                Assert.AreEqual(o1.PropertyInt165, o2.PropertyInt165);
                Assert.AreEqual(o1.PropertyInt166, o2.PropertyInt166);
                Assert.AreEqual(o1.PropertyInt167, o2.PropertyInt167);
                Assert.AreEqual(o1.PropertyInt168, o2.PropertyInt168);
                Assert.AreEqual(o1.PropertyInt169, o2.PropertyInt169);
                Assert.AreEqual(o1.PropertyInt1610, o2.PropertyInt1610);
                Assert.AreEqual(o1.PropertyInt1611, o2.PropertyInt1611);
                CollectionAssert.AreEqual(o1.PrFieldInt16ListList10, o2.PrFieldInt16ListList10);
                CollectionAssert.AreEqual(o1.PrFieldInt16ListList11, o2.PrFieldInt16ListList11);
                CollectionAssert.AreEqual(o1.PrFieldInt16ListList12, o2.PrFieldInt16ListList12);
                CollectionAssert.AreEqual(o1.PrFieldInt16ListList13, o2.PrFieldInt16ListList13);
                CollectionAssert.AreEqual(o1.PrFieldInt16ListList14, o2.PrFieldInt16ListList14);
                CollectionAssert.AreEqual(o1._pubFieldInt16ListList10, o2._pubFieldInt16ListList10);
                CollectionAssert.AreEqual(o1._pubFieldInt16ListList11, o2._pubFieldInt16ListList11);
                CollectionAssert.AreEqual(o1._pubFieldInt16ListList12, o2._pubFieldInt16ListList12);
                CollectionAssert.AreEqual(o1._pubFieldInt16ListList13, o2._pubFieldInt16ListList13);
                CollectionAssert.AreEqual(o1._pubFieldInt16ListList14, o2._pubFieldInt16ListList14);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList10, o2.PropertyInt16ListList10);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList11, o2.PropertyInt16ListList11);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList12, o2.PropertyInt16ListList12);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList13, o2.PropertyInt16ListList13);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList14, o2.PropertyInt16ListList14);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList15, o2.PropertyInt16ListList15);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList16, o2.PropertyInt16ListList16);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList17, o2.PropertyInt16ListList17);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList18, o2.PropertyInt16ListList18);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList19, o2.PropertyInt16ListList19);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList110, o2.PropertyInt16ListList110);
                CollectionAssert.AreEqual(o1.PropertyInt16ListList111, o2.PropertyInt16ListList111);
                CollectionAssert.AreEqual(o1.PrFieldUInt32UInt32Arr0, o2.PrFieldUInt32UInt32Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt32UInt32Arr1, o2.PrFieldUInt32UInt32Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt32UInt32Arr2, o2.PrFieldUInt32UInt32Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt32UInt32Arr3, o2.PrFieldUInt32UInt32Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt32UInt32Arr4, o2.PrFieldUInt32UInt32Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt32UInt32Arr0, o2._pubFieldUInt32UInt32Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt32UInt32Arr1, o2._pubFieldUInt32UInt32Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt32UInt32Arr2, o2._pubFieldUInt32UInt32Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt32UInt32Arr3, o2._pubFieldUInt32UInt32Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt32UInt32Arr4, o2._pubFieldUInt32UInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr0, o2.PropertyUInt32UInt32Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr1, o2.PropertyUInt32UInt32Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr2, o2.PropertyUInt32UInt32Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr3, o2.PropertyUInt32UInt32Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr4, o2.PropertyUInt32UInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr5, o2.PropertyUInt32UInt32Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr6, o2.PropertyUInt32UInt32Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr7, o2.PropertyUInt32UInt32Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr8, o2.PropertyUInt32UInt32Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr9, o2.PropertyUInt32UInt32Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr10, o2.PropertyUInt32UInt32Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt32UInt32Arr11, o2.PropertyUInt32UInt32Arr11);
                Assert.AreEqual(o1.PrFieldUInt320, o2.PrFieldUInt320);
                Assert.AreEqual(o1.PrFieldUInt321, o2.PrFieldUInt321);
                Assert.AreEqual(o1.PrFieldUInt322, o2.PrFieldUInt322);
                Assert.AreEqual(o1.PrFieldUInt323, o2.PrFieldUInt323);
                Assert.AreEqual(o1.PrFieldUInt324, o2.PrFieldUInt324);
                Assert.AreEqual(o1._pubFieldUInt320, o2._pubFieldUInt320);
                Assert.AreEqual(o1._pubFieldUInt321, o2._pubFieldUInt321);
                Assert.AreEqual(o1._pubFieldUInt322, o2._pubFieldUInt322);
                Assert.AreEqual(o1._pubFieldUInt323, o2._pubFieldUInt323);
                Assert.AreEqual(o1._pubFieldUInt324, o2._pubFieldUInt324);
                Assert.AreEqual(o1.PropertyUInt320, o2.PropertyUInt320);
                Assert.AreEqual(o1.PropertyUInt321, o2.PropertyUInt321);
                Assert.AreEqual(o1.PropertyUInt322, o2.PropertyUInt322);
                Assert.AreEqual(o1.PropertyUInt323, o2.PropertyUInt323);
                Assert.AreEqual(o1.PropertyUInt324, o2.PropertyUInt324);
                Assert.AreEqual(o1.PropertyUInt325, o2.PropertyUInt325);
                Assert.AreEqual(o1.PropertyUInt326, o2.PropertyUInt326);
                Assert.AreEqual(o1.PropertyUInt327, o2.PropertyUInt327);
                Assert.AreEqual(o1.PropertyUInt328, o2.PropertyUInt328);
                Assert.AreEqual(o1.PropertyUInt329, o2.PropertyUInt329);
                Assert.AreEqual(o1.PropertyUInt3210, o2.PropertyUInt3210);
                Assert.AreEqual(o1.PropertyUInt3211, o2.PropertyUInt3211);
                CollectionAssert.AreEqual(o1.PrFieldUInt32ListList10, o2.PrFieldUInt32ListList10);
                CollectionAssert.AreEqual(o1.PrFieldUInt32ListList11, o2.PrFieldUInt32ListList11);
                CollectionAssert.AreEqual(o1.PrFieldUInt32ListList12, o2.PrFieldUInt32ListList12);
                CollectionAssert.AreEqual(o1.PrFieldUInt32ListList13, o2.PrFieldUInt32ListList13);
                CollectionAssert.AreEqual(o1.PrFieldUInt32ListList14, o2.PrFieldUInt32ListList14);
                CollectionAssert.AreEqual(o1._pubFieldUInt32ListList10, o2._pubFieldUInt32ListList10);
                CollectionAssert.AreEqual(o1._pubFieldUInt32ListList11, o2._pubFieldUInt32ListList11);
                CollectionAssert.AreEqual(o1._pubFieldUInt32ListList12, o2._pubFieldUInt32ListList12);
                CollectionAssert.AreEqual(o1._pubFieldUInt32ListList13, o2._pubFieldUInt32ListList13);
                CollectionAssert.AreEqual(o1._pubFieldUInt32ListList14, o2._pubFieldUInt32ListList14);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList10, o2.PropertyUInt32ListList10);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList11, o2.PropertyUInt32ListList11);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList12, o2.PropertyUInt32ListList12);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList13, o2.PropertyUInt32ListList13);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList14, o2.PropertyUInt32ListList14);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList15, o2.PropertyUInt32ListList15);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList16, o2.PropertyUInt32ListList16);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList17, o2.PropertyUInt32ListList17);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList18, o2.PropertyUInt32ListList18);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList19, o2.PropertyUInt32ListList19);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList110, o2.PropertyUInt32ListList110);
                CollectionAssert.AreEqual(o1.PropertyUInt32ListList111, o2.PropertyUInt32ListList111);
                CollectionAssert.AreEqual(o1.PrFieldInt32Int32Arr0, o2.PrFieldInt32Int32Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt32Int32Arr1, o2.PrFieldInt32Int32Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt32Int32Arr2, o2.PrFieldInt32Int32Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt32Int32Arr3, o2.PrFieldInt32Int32Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt32Int32Arr4, o2.PrFieldInt32Int32Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt32Int32Arr0, o2._pubFieldInt32Int32Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt32Int32Arr1, o2._pubFieldInt32Int32Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt32Int32Arr2, o2._pubFieldInt32Int32Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt32Int32Arr3, o2._pubFieldInt32Int32Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt32Int32Arr4, o2._pubFieldInt32Int32Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr0, o2.PropertyInt32Int32Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr1, o2.PropertyInt32Int32Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr2, o2.PropertyInt32Int32Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr3, o2.PropertyInt32Int32Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr4, o2.PropertyInt32Int32Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr5, o2.PropertyInt32Int32Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr6, o2.PropertyInt32Int32Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr7, o2.PropertyInt32Int32Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr8, o2.PropertyInt32Int32Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr9, o2.PropertyInt32Int32Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr10, o2.PropertyInt32Int32Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt32Int32Arr11, o2.PropertyInt32Int32Arr11);
                Assert.AreEqual(o1.PrFieldInt320, o2.PrFieldInt320);
                Assert.AreEqual(o1.PrFieldInt321, o2.PrFieldInt321);
                Assert.AreEqual(o1.PrFieldInt322, o2.PrFieldInt322);
                Assert.AreEqual(o1.PrFieldInt323, o2.PrFieldInt323);
                Assert.AreEqual(o1.PrFieldInt324, o2.PrFieldInt324);
                Assert.AreEqual(o1._pubFieldInt320, o2._pubFieldInt320);
                Assert.AreEqual(o1._pubFieldInt321, o2._pubFieldInt321);
                Assert.AreEqual(o1._pubFieldInt322, o2._pubFieldInt322);
                Assert.AreEqual(o1._pubFieldInt323, o2._pubFieldInt323);
                Assert.AreEqual(o1._pubFieldInt324, o2._pubFieldInt324);
                Assert.AreEqual(o1.PropertyInt320, o2.PropertyInt320);
                Assert.AreEqual(o1.PropertyInt321, o2.PropertyInt321);
                Assert.AreEqual(o1.PropertyInt322, o2.PropertyInt322);
                Assert.AreEqual(o1.PropertyInt323, o2.PropertyInt323);
                Assert.AreEqual(o1.PropertyInt324, o2.PropertyInt324);
                Assert.AreEqual(o1.PropertyInt325, o2.PropertyInt325);
                Assert.AreEqual(o1.PropertyInt326, o2.PropertyInt326);
                Assert.AreEqual(o1.PropertyInt327, o2.PropertyInt327);
                Assert.AreEqual(o1.PropertyInt328, o2.PropertyInt328);
                Assert.AreEqual(o1.PropertyInt329, o2.PropertyInt329);
                Assert.AreEqual(o1.PropertyInt3210, o2.PropertyInt3210);
                Assert.AreEqual(o1.PropertyInt3211, o2.PropertyInt3211);
                CollectionAssert.AreEqual(o1.PrFieldInt32ListList10, o2.PrFieldInt32ListList10);
                CollectionAssert.AreEqual(o1.PrFieldInt32ListList11, o2.PrFieldInt32ListList11);
                CollectionAssert.AreEqual(o1.PrFieldInt32ListList12, o2.PrFieldInt32ListList12);
                CollectionAssert.AreEqual(o1.PrFieldInt32ListList13, o2.PrFieldInt32ListList13);
                CollectionAssert.AreEqual(o1.PrFieldInt32ListList14, o2.PrFieldInt32ListList14);
                CollectionAssert.AreEqual(o1._pubFieldInt32ListList10, o2._pubFieldInt32ListList10);
                CollectionAssert.AreEqual(o1._pubFieldInt32ListList11, o2._pubFieldInt32ListList11);
                CollectionAssert.AreEqual(o1._pubFieldInt32ListList12, o2._pubFieldInt32ListList12);
                CollectionAssert.AreEqual(o1._pubFieldInt32ListList13, o2._pubFieldInt32ListList13);
                CollectionAssert.AreEqual(o1._pubFieldInt32ListList14, o2._pubFieldInt32ListList14);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList10, o2.PropertyInt32ListList10);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList11, o2.PropertyInt32ListList11);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList12, o2.PropertyInt32ListList12);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList13, o2.PropertyInt32ListList13);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList14, o2.PropertyInt32ListList14);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList15, o2.PropertyInt32ListList15);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList16, o2.PropertyInt32ListList16);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList17, o2.PropertyInt32ListList17);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList18, o2.PropertyInt32ListList18);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList19, o2.PropertyInt32ListList19);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList110, o2.PropertyInt32ListList110);
                CollectionAssert.AreEqual(o1.PropertyInt32ListList111, o2.PropertyInt32ListList111);
                CollectionAssert.AreEqual(o1.PrFieldUInt64UInt64Arr0, o2.PrFieldUInt64UInt64Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt64UInt64Arr1, o2.PrFieldUInt64UInt64Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt64UInt64Arr2, o2.PrFieldUInt64UInt64Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt64UInt64Arr3, o2.PrFieldUInt64UInt64Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt64UInt64Arr4, o2.PrFieldUInt64UInt64Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt64UInt64Arr0, o2._pubFieldUInt64UInt64Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt64UInt64Arr1, o2._pubFieldUInt64UInt64Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt64UInt64Arr2, o2._pubFieldUInt64UInt64Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt64UInt64Arr3, o2._pubFieldUInt64UInt64Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt64UInt64Arr4, o2._pubFieldUInt64UInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr0, o2.PropertyUInt64UInt64Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr1, o2.PropertyUInt64UInt64Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr2, o2.PropertyUInt64UInt64Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr3, o2.PropertyUInt64UInt64Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr4, o2.PropertyUInt64UInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr5, o2.PropertyUInt64UInt64Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr6, o2.PropertyUInt64UInt64Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr7, o2.PropertyUInt64UInt64Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr8, o2.PropertyUInt64UInt64Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr9, o2.PropertyUInt64UInt64Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr10, o2.PropertyUInt64UInt64Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt64UInt64Arr11, o2.PropertyUInt64UInt64Arr11);
                Assert.AreEqual(o1.PrFieldUInt640, o2.PrFieldUInt640);
                Assert.AreEqual(o1.PrFieldUInt641, o2.PrFieldUInt641);
                Assert.AreEqual(o1.PrFieldUInt642, o2.PrFieldUInt642);
                Assert.AreEqual(o1.PrFieldUInt643, o2.PrFieldUInt643);
                Assert.AreEqual(o1.PrFieldUInt644, o2.PrFieldUInt644);
                Assert.AreEqual(o1._pubFieldUInt640, o2._pubFieldUInt640);
                Assert.AreEqual(o1._pubFieldUInt641, o2._pubFieldUInt641);
                Assert.AreEqual(o1._pubFieldUInt642, o2._pubFieldUInt642);
                Assert.AreEqual(o1._pubFieldUInt643, o2._pubFieldUInt643);
                Assert.AreEqual(o1._pubFieldUInt644, o2._pubFieldUInt644);
                Assert.AreEqual(o1.PropertyUInt640, o2.PropertyUInt640);
                Assert.AreEqual(o1.PropertyUInt641, o2.PropertyUInt641);
                Assert.AreEqual(o1.PropertyUInt642, o2.PropertyUInt642);
                Assert.AreEqual(o1.PropertyUInt643, o2.PropertyUInt643);
                Assert.AreEqual(o1.PropertyUInt644, o2.PropertyUInt644);
                Assert.AreEqual(o1.PropertyUInt645, o2.PropertyUInt645);
                Assert.AreEqual(o1.PropertyUInt646, o2.PropertyUInt646);
                Assert.AreEqual(o1.PropertyUInt647, o2.PropertyUInt647);
                Assert.AreEqual(o1.PropertyUInt648, o2.PropertyUInt648);
                Assert.AreEqual(o1.PropertyUInt649, o2.PropertyUInt649);
                Assert.AreEqual(o1.PropertyUInt6410, o2.PropertyUInt6410);
                Assert.AreEqual(o1.PropertyUInt6411, o2.PropertyUInt6411);
                CollectionAssert.AreEqual(o1.PrFieldUInt64ListList10, o2.PrFieldUInt64ListList10);
                CollectionAssert.AreEqual(o1.PrFieldUInt64ListList11, o2.PrFieldUInt64ListList11);
                CollectionAssert.AreEqual(o1.PrFieldUInt64ListList12, o2.PrFieldUInt64ListList12);
                CollectionAssert.AreEqual(o1.PrFieldUInt64ListList13, o2.PrFieldUInt64ListList13);
                CollectionAssert.AreEqual(o1.PrFieldUInt64ListList14, o2.PrFieldUInt64ListList14);
                CollectionAssert.AreEqual(o1._pubFieldUInt64ListList10, o2._pubFieldUInt64ListList10);
                CollectionAssert.AreEqual(o1._pubFieldUInt64ListList11, o2._pubFieldUInt64ListList11);
                CollectionAssert.AreEqual(o1._pubFieldUInt64ListList12, o2._pubFieldUInt64ListList12);
                CollectionAssert.AreEqual(o1._pubFieldUInt64ListList13, o2._pubFieldUInt64ListList13);
                CollectionAssert.AreEqual(o1._pubFieldUInt64ListList14, o2._pubFieldUInt64ListList14);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList10, o2.PropertyUInt64ListList10);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList11, o2.PropertyUInt64ListList11);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList12, o2.PropertyUInt64ListList12);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList13, o2.PropertyUInt64ListList13);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList14, o2.PropertyUInt64ListList14);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList15, o2.PropertyUInt64ListList15);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList16, o2.PropertyUInt64ListList16);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList17, o2.PropertyUInt64ListList17);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList18, o2.PropertyUInt64ListList18);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList19, o2.PropertyUInt64ListList19);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList110, o2.PropertyUInt64ListList110);
                CollectionAssert.AreEqual(o1.PropertyUInt64ListList111, o2.PropertyUInt64ListList111);
                CollectionAssert.AreEqual(o1.PrFieldInt64Int64Arr0, o2.PrFieldInt64Int64Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt64Int64Arr1, o2.PrFieldInt64Int64Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt64Int64Arr2, o2.PrFieldInt64Int64Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt64Int64Arr3, o2.PrFieldInt64Int64Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt64Int64Arr4, o2.PrFieldInt64Int64Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt64Int64Arr0, o2._pubFieldInt64Int64Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt64Int64Arr1, o2._pubFieldInt64Int64Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt64Int64Arr2, o2._pubFieldInt64Int64Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt64Int64Arr3, o2._pubFieldInt64Int64Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt64Int64Arr4, o2._pubFieldInt64Int64Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr0, o2.PropertyInt64Int64Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr1, o2.PropertyInt64Int64Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr2, o2.PropertyInt64Int64Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr3, o2.PropertyInt64Int64Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr4, o2.PropertyInt64Int64Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr5, o2.PropertyInt64Int64Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr6, o2.PropertyInt64Int64Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr7, o2.PropertyInt64Int64Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr8, o2.PropertyInt64Int64Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr9, o2.PropertyInt64Int64Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr10, o2.PropertyInt64Int64Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt64Int64Arr11, o2.PropertyInt64Int64Arr11);
                Assert.AreEqual(o1.PrFieldInt640, o2.PrFieldInt640);
                Assert.AreEqual(o1.PrFieldInt641, o2.PrFieldInt641);
                Assert.AreEqual(o1.PrFieldInt642, o2.PrFieldInt642);
                Assert.AreEqual(o1.PrFieldInt643, o2.PrFieldInt643);
                Assert.AreEqual(o1.PrFieldInt644, o2.PrFieldInt644);
                Assert.AreEqual(o1._pubFieldInt640, o2._pubFieldInt640);
                Assert.AreEqual(o1._pubFieldInt641, o2._pubFieldInt641);
                Assert.AreEqual(o1._pubFieldInt642, o2._pubFieldInt642);
                Assert.AreEqual(o1._pubFieldInt643, o2._pubFieldInt643);
                Assert.AreEqual(o1._pubFieldInt644, o2._pubFieldInt644);
                Assert.AreEqual(o1.PropertyInt640, o2.PropertyInt640);
                Assert.AreEqual(o1.PropertyInt641, o2.PropertyInt641);
                Assert.AreEqual(o1.PropertyInt642, o2.PropertyInt642);
                Assert.AreEqual(o1.PropertyInt643, o2.PropertyInt643);
                Assert.AreEqual(o1.PropertyInt644, o2.PropertyInt644);
                Assert.AreEqual(o1.PropertyInt645, o2.PropertyInt645);
                Assert.AreEqual(o1.PropertyInt646, o2.PropertyInt646);
                Assert.AreEqual(o1.PropertyInt647, o2.PropertyInt647);
                Assert.AreEqual(o1.PropertyInt648, o2.PropertyInt648);
                Assert.AreEqual(o1.PropertyInt649, o2.PropertyInt649);
                Assert.AreEqual(o1.PropertyInt6410, o2.PropertyInt6410);
                Assert.AreEqual(o1.PropertyInt6411, o2.PropertyInt6411);
                CollectionAssert.AreEqual(o1.PrFieldInt64ListList10, o2.PrFieldInt64ListList10);
                CollectionAssert.AreEqual(o1.PrFieldInt64ListList11, o2.PrFieldInt64ListList11);
                CollectionAssert.AreEqual(o1.PrFieldInt64ListList12, o2.PrFieldInt64ListList12);
                CollectionAssert.AreEqual(o1.PrFieldInt64ListList13, o2.PrFieldInt64ListList13);
                CollectionAssert.AreEqual(o1.PrFieldInt64ListList14, o2.PrFieldInt64ListList14);
                CollectionAssert.AreEqual(o1._pubFieldInt64ListList10, o2._pubFieldInt64ListList10);
                CollectionAssert.AreEqual(o1._pubFieldInt64ListList11, o2._pubFieldInt64ListList11);
                CollectionAssert.AreEqual(o1._pubFieldInt64ListList12, o2._pubFieldInt64ListList12);
                CollectionAssert.AreEqual(o1._pubFieldInt64ListList13, o2._pubFieldInt64ListList13);
                CollectionAssert.AreEqual(o1._pubFieldInt64ListList14, o2._pubFieldInt64ListList14);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList10, o2.PropertyInt64ListList10);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList11, o2.PropertyInt64ListList11);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList12, o2.PropertyInt64ListList12);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList13, o2.PropertyInt64ListList13);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList14, o2.PropertyInt64ListList14);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList15, o2.PropertyInt64ListList15);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList16, o2.PropertyInt64ListList16);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList17, o2.PropertyInt64ListList17);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList18, o2.PropertyInt64ListList18);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList19, o2.PropertyInt64ListList19);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList110, o2.PropertyInt64ListList110);
                CollectionAssert.AreEqual(o1.PropertyInt64ListList111, o2.PropertyInt64ListList111);
                CollectionAssert.AreEqual(o1.PrFieldSingleSingleArr0, o2.PrFieldSingleSingleArr0);
                CollectionAssert.AreEqual(o1.PrFieldSingleSingleArr1, o2.PrFieldSingleSingleArr1);
                CollectionAssert.AreEqual(o1.PrFieldSingleSingleArr2, o2.PrFieldSingleSingleArr2);
                CollectionAssert.AreEqual(o1.PrFieldSingleSingleArr3, o2.PrFieldSingleSingleArr3);
                CollectionAssert.AreEqual(o1.PrFieldSingleSingleArr4, o2.PrFieldSingleSingleArr4);
                CollectionAssert.AreEqual(o1._pubFieldSingleSingleArr0, o2._pubFieldSingleSingleArr0);
                CollectionAssert.AreEqual(o1._pubFieldSingleSingleArr1, o2._pubFieldSingleSingleArr1);
                CollectionAssert.AreEqual(o1._pubFieldSingleSingleArr2, o2._pubFieldSingleSingleArr2);
                CollectionAssert.AreEqual(o1._pubFieldSingleSingleArr3, o2._pubFieldSingleSingleArr3);
                CollectionAssert.AreEqual(o1._pubFieldSingleSingleArr4, o2._pubFieldSingleSingleArr4);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr0, o2.PropertySingleSingleArr0);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr1, o2.PropertySingleSingleArr1);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr2, o2.PropertySingleSingleArr2);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr3, o2.PropertySingleSingleArr3);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr4, o2.PropertySingleSingleArr4);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr5, o2.PropertySingleSingleArr5);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr6, o2.PropertySingleSingleArr6);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr7, o2.PropertySingleSingleArr7);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr8, o2.PropertySingleSingleArr8);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr9, o2.PropertySingleSingleArr9);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr10, o2.PropertySingleSingleArr10);
                CollectionAssert.AreEqual(o1.PropertySingleSingleArr11, o2.PropertySingleSingleArr11);
                Assert.AreEqual(o1.PrFieldSingle0, o2.PrFieldSingle0);
                Assert.AreEqual(o1.PrFieldSingle1, o2.PrFieldSingle1);
                Assert.AreEqual(o1.PrFieldSingle2, o2.PrFieldSingle2);
                Assert.AreEqual(o1.PrFieldSingle3, o2.PrFieldSingle3);
                Assert.AreEqual(o1.PrFieldSingle4, o2.PrFieldSingle4);
                Assert.AreEqual(o1._pubFieldSingle0, o2._pubFieldSingle0);
                Assert.AreEqual(o1._pubFieldSingle1, o2._pubFieldSingle1);
                Assert.AreEqual(o1._pubFieldSingle2, o2._pubFieldSingle2);
                Assert.AreEqual(o1._pubFieldSingle3, o2._pubFieldSingle3);
                Assert.AreEqual(o1._pubFieldSingle4, o2._pubFieldSingle4);
                Assert.AreEqual(o1.PropertySingle0, o2.PropertySingle0);
                Assert.AreEqual(o1.PropertySingle1, o2.PropertySingle1);
                Assert.AreEqual(o1.PropertySingle2, o2.PropertySingle2);
                Assert.AreEqual(o1.PropertySingle3, o2.PropertySingle3);
                Assert.AreEqual(o1.PropertySingle4, o2.PropertySingle4);
                Assert.AreEqual(o1.PropertySingle5, o2.PropertySingle5);
                Assert.AreEqual(o1.PropertySingle6, o2.PropertySingle6);
                Assert.AreEqual(o1.PropertySingle7, o2.PropertySingle7);
                Assert.AreEqual(o1.PropertySingle8, o2.PropertySingle8);
                Assert.AreEqual(o1.PropertySingle9, o2.PropertySingle9);
                Assert.AreEqual(o1.PropertySingle10, o2.PropertySingle10);
                Assert.AreEqual(o1.PropertySingle11, o2.PropertySingle11);
                CollectionAssert.AreEqual(o1.PrFieldSingleListList10, o2.PrFieldSingleListList10);
                CollectionAssert.AreEqual(o1.PrFieldSingleListList11, o2.PrFieldSingleListList11);
                CollectionAssert.AreEqual(o1.PrFieldSingleListList12, o2.PrFieldSingleListList12);
                CollectionAssert.AreEqual(o1.PrFieldSingleListList13, o2.PrFieldSingleListList13);
                CollectionAssert.AreEqual(o1.PrFieldSingleListList14, o2.PrFieldSingleListList14);
                CollectionAssert.AreEqual(o1._pubFieldSingleListList10, o2._pubFieldSingleListList10);
                CollectionAssert.AreEqual(o1._pubFieldSingleListList11, o2._pubFieldSingleListList11);
                CollectionAssert.AreEqual(o1._pubFieldSingleListList12, o2._pubFieldSingleListList12);
                CollectionAssert.AreEqual(o1._pubFieldSingleListList13, o2._pubFieldSingleListList13);
                CollectionAssert.AreEqual(o1._pubFieldSingleListList14, o2._pubFieldSingleListList14);
                CollectionAssert.AreEqual(o1.PropertySingleListList10, o2.PropertySingleListList10);
                CollectionAssert.AreEqual(o1.PropertySingleListList11, o2.PropertySingleListList11);
                CollectionAssert.AreEqual(o1.PropertySingleListList12, o2.PropertySingleListList12);
                CollectionAssert.AreEqual(o1.PropertySingleListList13, o2.PropertySingleListList13);
                CollectionAssert.AreEqual(o1.PropertySingleListList14, o2.PropertySingleListList14);
                CollectionAssert.AreEqual(o1.PropertySingleListList15, o2.PropertySingleListList15);
                CollectionAssert.AreEqual(o1.PropertySingleListList16, o2.PropertySingleListList16);
                CollectionAssert.AreEqual(o1.PropertySingleListList17, o2.PropertySingleListList17);
                CollectionAssert.AreEqual(o1.PropertySingleListList18, o2.PropertySingleListList18);
                CollectionAssert.AreEqual(o1.PropertySingleListList19, o2.PropertySingleListList19);
                CollectionAssert.AreEqual(o1.PropertySingleListList110, o2.PropertySingleListList110);
                CollectionAssert.AreEqual(o1.PropertySingleListList111, o2.PropertySingleListList111);
                CollectionAssert.AreEqual(o1.PrFieldDoubleDoubleArr0, o2.PrFieldDoubleDoubleArr0);
                CollectionAssert.AreEqual(o1.PrFieldDoubleDoubleArr1, o2.PrFieldDoubleDoubleArr1);
                CollectionAssert.AreEqual(o1.PrFieldDoubleDoubleArr2, o2.PrFieldDoubleDoubleArr2);
                CollectionAssert.AreEqual(o1.PrFieldDoubleDoubleArr3, o2.PrFieldDoubleDoubleArr3);
                CollectionAssert.AreEqual(o1.PrFieldDoubleDoubleArr4, o2.PrFieldDoubleDoubleArr4);
                CollectionAssert.AreEqual(o1._pubFieldDoubleDoubleArr0, o2._pubFieldDoubleDoubleArr0);
                CollectionAssert.AreEqual(o1._pubFieldDoubleDoubleArr1, o2._pubFieldDoubleDoubleArr1);
                CollectionAssert.AreEqual(o1._pubFieldDoubleDoubleArr2, o2._pubFieldDoubleDoubleArr2);
                CollectionAssert.AreEqual(o1._pubFieldDoubleDoubleArr3, o2._pubFieldDoubleDoubleArr3);
                CollectionAssert.AreEqual(o1._pubFieldDoubleDoubleArr4, o2._pubFieldDoubleDoubleArr4);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr0, o2.PropertyDoubleDoubleArr0);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr1, o2.PropertyDoubleDoubleArr1);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr2, o2.PropertyDoubleDoubleArr2);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr3, o2.PropertyDoubleDoubleArr3);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr4, o2.PropertyDoubleDoubleArr4);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr5, o2.PropertyDoubleDoubleArr5);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr6, o2.PropertyDoubleDoubleArr6);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr7, o2.PropertyDoubleDoubleArr7);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr8, o2.PropertyDoubleDoubleArr8);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr9, o2.PropertyDoubleDoubleArr9);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr10, o2.PropertyDoubleDoubleArr10);
                CollectionAssert.AreEqual(o1.PropertyDoubleDoubleArr11, o2.PropertyDoubleDoubleArr11);
                Assert.AreEqual(o1.PrFieldDouble0, o2.PrFieldDouble0);
                Assert.AreEqual(o1.PrFieldDouble1, o2.PrFieldDouble1);
                Assert.AreEqual(o1.PrFieldDouble2, o2.PrFieldDouble2);
                Assert.AreEqual(o1.PrFieldDouble3, o2.PrFieldDouble3);
                Assert.AreEqual(o1.PrFieldDouble4, o2.PrFieldDouble4);
                Assert.AreEqual(o1._pubFieldDouble0, o2._pubFieldDouble0);
                Assert.AreEqual(o1._pubFieldDouble1, o2._pubFieldDouble1);
                Assert.AreEqual(o1._pubFieldDouble2, o2._pubFieldDouble2);
                Assert.AreEqual(o1._pubFieldDouble3, o2._pubFieldDouble3);
                Assert.AreEqual(o1._pubFieldDouble4, o2._pubFieldDouble4);
                Assert.AreEqual(o1.PropertyDouble0, o2.PropertyDouble0);
                Assert.AreEqual(o1.PropertyDouble1, o2.PropertyDouble1);
                Assert.AreEqual(o1.PropertyDouble2, o2.PropertyDouble2);
                Assert.AreEqual(o1.PropertyDouble3, o2.PropertyDouble3);
                Assert.AreEqual(o1.PropertyDouble4, o2.PropertyDouble4);
                Assert.AreEqual(o1.PropertyDouble5, o2.PropertyDouble5);
                Assert.AreEqual(o1.PropertyDouble6, o2.PropertyDouble6);
                Assert.AreEqual(o1.PropertyDouble7, o2.PropertyDouble7);
                Assert.AreEqual(o1.PropertyDouble8, o2.PropertyDouble8);
                Assert.AreEqual(o1.PropertyDouble9, o2.PropertyDouble9);
                Assert.AreEqual(o1.PropertyDouble10, o2.PropertyDouble10);
                Assert.AreEqual(o1.PropertyDouble11, o2.PropertyDouble11);
                CollectionAssert.AreEqual(o1.PrFieldDoubleListList10, o2.PrFieldDoubleListList10);
                CollectionAssert.AreEqual(o1.PrFieldDoubleListList11, o2.PrFieldDoubleListList11);
                CollectionAssert.AreEqual(o1.PrFieldDoubleListList12, o2.PrFieldDoubleListList12);
                CollectionAssert.AreEqual(o1.PrFieldDoubleListList13, o2.PrFieldDoubleListList13);
                CollectionAssert.AreEqual(o1.PrFieldDoubleListList14, o2.PrFieldDoubleListList14);
                CollectionAssert.AreEqual(o1._pubFieldDoubleListList10, o2._pubFieldDoubleListList10);
                CollectionAssert.AreEqual(o1._pubFieldDoubleListList11, o2._pubFieldDoubleListList11);
                CollectionAssert.AreEqual(o1._pubFieldDoubleListList12, o2._pubFieldDoubleListList12);
                CollectionAssert.AreEqual(o1._pubFieldDoubleListList13, o2._pubFieldDoubleListList13);
                CollectionAssert.AreEqual(o1._pubFieldDoubleListList14, o2._pubFieldDoubleListList14);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList10, o2.PropertyDoubleListList10);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList11, o2.PropertyDoubleListList11);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList12, o2.PropertyDoubleListList12);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList13, o2.PropertyDoubleListList13);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList14, o2.PropertyDoubleListList14);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList15, o2.PropertyDoubleListList15);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList16, o2.PropertyDoubleListList16);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList17, o2.PropertyDoubleListList17);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList18, o2.PropertyDoubleListList18);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList19, o2.PropertyDoubleListList19);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList110, o2.PropertyDoubleListList110);
                CollectionAssert.AreEqual(o1.PropertyDoubleListList111, o2.PropertyDoubleListList111);
                CollectionAssert.AreEqual(o1.PrFieldBooleanBooleanArr0, o2.PrFieldBooleanBooleanArr0);
                CollectionAssert.AreEqual(o1.PrFieldBooleanBooleanArr1, o2.PrFieldBooleanBooleanArr1);
                CollectionAssert.AreEqual(o1.PrFieldBooleanBooleanArr2, o2.PrFieldBooleanBooleanArr2);
                CollectionAssert.AreEqual(o1.PrFieldBooleanBooleanArr3, o2.PrFieldBooleanBooleanArr3);
                CollectionAssert.AreEqual(o1.PrFieldBooleanBooleanArr4, o2.PrFieldBooleanBooleanArr4);
                CollectionAssert.AreEqual(o1._pubFieldBooleanBooleanArr0, o2._pubFieldBooleanBooleanArr0);
                CollectionAssert.AreEqual(o1._pubFieldBooleanBooleanArr1, o2._pubFieldBooleanBooleanArr1);
                CollectionAssert.AreEqual(o1._pubFieldBooleanBooleanArr2, o2._pubFieldBooleanBooleanArr2);
                CollectionAssert.AreEqual(o1._pubFieldBooleanBooleanArr3, o2._pubFieldBooleanBooleanArr3);
                CollectionAssert.AreEqual(o1._pubFieldBooleanBooleanArr4, o2._pubFieldBooleanBooleanArr4);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr0, o2.PropertyBooleanBooleanArr0);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr1, o2.PropertyBooleanBooleanArr1);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr2, o2.PropertyBooleanBooleanArr2);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr3, o2.PropertyBooleanBooleanArr3);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr4, o2.PropertyBooleanBooleanArr4);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr5, o2.PropertyBooleanBooleanArr5);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr6, o2.PropertyBooleanBooleanArr6);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr7, o2.PropertyBooleanBooleanArr7);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr8, o2.PropertyBooleanBooleanArr8);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr9, o2.PropertyBooleanBooleanArr9);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr10, o2.PropertyBooleanBooleanArr10);
                CollectionAssert.AreEqual(o1.PropertyBooleanBooleanArr11, o2.PropertyBooleanBooleanArr11);
                Assert.AreEqual(o1.PrFieldBoolean0, o2.PrFieldBoolean0);
                Assert.AreEqual(o1.PrFieldBoolean1, o2.PrFieldBoolean1);
                Assert.AreEqual(o1.PrFieldBoolean2, o2.PrFieldBoolean2);
                Assert.AreEqual(o1.PrFieldBoolean3, o2.PrFieldBoolean3);
                Assert.AreEqual(o1.PrFieldBoolean4, o2.PrFieldBoolean4);
                Assert.AreEqual(o1._pubFieldBoolean0, o2._pubFieldBoolean0);
                Assert.AreEqual(o1._pubFieldBoolean1, o2._pubFieldBoolean1);
                Assert.AreEqual(o1._pubFieldBoolean2, o2._pubFieldBoolean2);
                Assert.AreEqual(o1._pubFieldBoolean3, o2._pubFieldBoolean3);
                Assert.AreEqual(o1._pubFieldBoolean4, o2._pubFieldBoolean4);
                Assert.AreEqual(o1.PropertyBoolean0, o2.PropertyBoolean0);
                Assert.AreEqual(o1.PropertyBoolean1, o2.PropertyBoolean1);
                Assert.AreEqual(o1.PropertyBoolean2, o2.PropertyBoolean2);
                Assert.AreEqual(o1.PropertyBoolean3, o2.PropertyBoolean3);
                Assert.AreEqual(o1.PropertyBoolean4, o2.PropertyBoolean4);
                Assert.AreEqual(o1.PropertyBoolean5, o2.PropertyBoolean5);
                Assert.AreEqual(o1.PropertyBoolean6, o2.PropertyBoolean6);
                Assert.AreEqual(o1.PropertyBoolean7, o2.PropertyBoolean7);
                Assert.AreEqual(o1.PropertyBoolean8, o2.PropertyBoolean8);
                Assert.AreEqual(o1.PropertyBoolean9, o2.PropertyBoolean9);
                Assert.AreEqual(o1.PropertyBoolean10, o2.PropertyBoolean10);
                Assert.AreEqual(o1.PropertyBoolean11, o2.PropertyBoolean11);
                CollectionAssert.AreEqual(o1.PrFieldBooleanListList10, o2.PrFieldBooleanListList10);
                CollectionAssert.AreEqual(o1.PrFieldBooleanListList11, o2.PrFieldBooleanListList11);
                CollectionAssert.AreEqual(o1.PrFieldBooleanListList12, o2.PrFieldBooleanListList12);
                CollectionAssert.AreEqual(o1.PrFieldBooleanListList13, o2.PrFieldBooleanListList13);
                CollectionAssert.AreEqual(o1.PrFieldBooleanListList14, o2.PrFieldBooleanListList14);
                CollectionAssert.AreEqual(o1._pubFieldBooleanListList10, o2._pubFieldBooleanListList10);
                CollectionAssert.AreEqual(o1._pubFieldBooleanListList11, o2._pubFieldBooleanListList11);
                CollectionAssert.AreEqual(o1._pubFieldBooleanListList12, o2._pubFieldBooleanListList12);
                CollectionAssert.AreEqual(o1._pubFieldBooleanListList13, o2._pubFieldBooleanListList13);
                CollectionAssert.AreEqual(o1._pubFieldBooleanListList14, o2._pubFieldBooleanListList14);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList10, o2.PropertyBooleanListList10);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList11, o2.PropertyBooleanListList11);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList12, o2.PropertyBooleanListList12);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList13, o2.PropertyBooleanListList13);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList14, o2.PropertyBooleanListList14);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList15, o2.PropertyBooleanListList15);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList16, o2.PropertyBooleanListList16);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList17, o2.PropertyBooleanListList17);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList18, o2.PropertyBooleanListList18);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList19, o2.PropertyBooleanListList19);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList110, o2.PropertyBooleanListList110);
                CollectionAssert.AreEqual(o1.PropertyBooleanListList111, o2.PropertyBooleanListList111);
                CollectionAssert.AreEqual(o1.PrFieldCharCharArr0, o2.PrFieldCharCharArr0);
                CollectionAssert.AreEqual(o1.PrFieldCharCharArr1, o2.PrFieldCharCharArr1);
                CollectionAssert.AreEqual(o1.PrFieldCharCharArr2, o2.PrFieldCharCharArr2);
                CollectionAssert.AreEqual(o1.PrFieldCharCharArr3, o2.PrFieldCharCharArr3);
                CollectionAssert.AreEqual(o1.PrFieldCharCharArr4, o2.PrFieldCharCharArr4);
                CollectionAssert.AreEqual(o1._pubFieldCharCharArr0, o2._pubFieldCharCharArr0);
                CollectionAssert.AreEqual(o1._pubFieldCharCharArr1, o2._pubFieldCharCharArr1);
                CollectionAssert.AreEqual(o1._pubFieldCharCharArr2, o2._pubFieldCharCharArr2);
                CollectionAssert.AreEqual(o1._pubFieldCharCharArr3, o2._pubFieldCharCharArr3);
                CollectionAssert.AreEqual(o1._pubFieldCharCharArr4, o2._pubFieldCharCharArr4);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr0, o2.PropertyCharCharArr0);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr1, o2.PropertyCharCharArr1);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr2, o2.PropertyCharCharArr2);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr3, o2.PropertyCharCharArr3);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr4, o2.PropertyCharCharArr4);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr5, o2.PropertyCharCharArr5);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr6, o2.PropertyCharCharArr6);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr7, o2.PropertyCharCharArr7);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr8, o2.PropertyCharCharArr8);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr9, o2.PropertyCharCharArr9);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr10, o2.PropertyCharCharArr10);
                CollectionAssert.AreEqual(o1.PropertyCharCharArr11, o2.PropertyCharCharArr11);
                Assert.AreEqual(o1.PrFieldChar0, o2.PrFieldChar0);
                Assert.AreEqual(o1.PrFieldChar1, o2.PrFieldChar1);
                Assert.AreEqual(o1.PrFieldChar2, o2.PrFieldChar2);
                Assert.AreEqual(o1.PrFieldChar3, o2.PrFieldChar3);
                Assert.AreEqual(o1.PrFieldChar4, o2.PrFieldChar4);
                Assert.AreEqual(o1._pubFieldChar0, o2._pubFieldChar0);
                Assert.AreEqual(o1._pubFieldChar1, o2._pubFieldChar1);
                Assert.AreEqual(o1._pubFieldChar2, o2._pubFieldChar2);
                Assert.AreEqual(o1._pubFieldChar3, o2._pubFieldChar3);
                Assert.AreEqual(o1._pubFieldChar4, o2._pubFieldChar4);
                Assert.AreEqual(o1.PropertyChar0, o2.PropertyChar0);
                Assert.AreEqual(o1.PropertyChar1, o2.PropertyChar1);
                Assert.AreEqual(o1.PropertyChar2, o2.PropertyChar2);
                Assert.AreEqual(o1.PropertyChar3, o2.PropertyChar3);
                Assert.AreEqual(o1.PropertyChar4, o2.PropertyChar4);
                Assert.AreEqual(o1.PropertyChar5, o2.PropertyChar5);
                Assert.AreEqual(o1.PropertyChar6, o2.PropertyChar6);
                Assert.AreEqual(o1.PropertyChar7, o2.PropertyChar7);
                Assert.AreEqual(o1.PropertyChar8, o2.PropertyChar8);
                Assert.AreEqual(o1.PropertyChar9, o2.PropertyChar9);
                Assert.AreEqual(o1.PropertyChar10, o2.PropertyChar10);
                Assert.AreEqual(o1.PropertyChar11, o2.PropertyChar11);
                CollectionAssert.AreEqual(o1.PrFieldCharListList10, o2.PrFieldCharListList10);
                CollectionAssert.AreEqual(o1.PrFieldCharListList11, o2.PrFieldCharListList11);
                CollectionAssert.AreEqual(o1.PrFieldCharListList12, o2.PrFieldCharListList12);
                CollectionAssert.AreEqual(o1.PrFieldCharListList13, o2.PrFieldCharListList13);
                CollectionAssert.AreEqual(o1.PrFieldCharListList14, o2.PrFieldCharListList14);
                CollectionAssert.AreEqual(o1._pubFieldCharListList10, o2._pubFieldCharListList10);
                CollectionAssert.AreEqual(o1._pubFieldCharListList11, o2._pubFieldCharListList11);
                CollectionAssert.AreEqual(o1._pubFieldCharListList12, o2._pubFieldCharListList12);
                CollectionAssert.AreEqual(o1._pubFieldCharListList13, o2._pubFieldCharListList13);
                CollectionAssert.AreEqual(o1._pubFieldCharListList14, o2._pubFieldCharListList14);
                CollectionAssert.AreEqual(o1.PropertyCharListList10, o2.PropertyCharListList10);
                CollectionAssert.AreEqual(o1.PropertyCharListList11, o2.PropertyCharListList11);
                CollectionAssert.AreEqual(o1.PropertyCharListList12, o2.PropertyCharListList12);
                CollectionAssert.AreEqual(o1.PropertyCharListList13, o2.PropertyCharListList13);
                CollectionAssert.AreEqual(o1.PropertyCharListList14, o2.PropertyCharListList14);
                CollectionAssert.AreEqual(o1.PropertyCharListList15, o2.PropertyCharListList15);
                CollectionAssert.AreEqual(o1.PropertyCharListList16, o2.PropertyCharListList16);
                CollectionAssert.AreEqual(o1.PropertyCharListList17, o2.PropertyCharListList17);
                CollectionAssert.AreEqual(o1.PropertyCharListList18, o2.PropertyCharListList18);
                CollectionAssert.AreEqual(o1.PropertyCharListList19, o2.PropertyCharListList19);
                CollectionAssert.AreEqual(o1.PropertyCharListList110, o2.PropertyCharListList110);
                CollectionAssert.AreEqual(o1.PropertyCharListList111, o2.PropertyCharListList111);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeDateTimeArr0, o2.PrFieldDateTimeDateTimeArr0);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeDateTimeArr1, o2.PrFieldDateTimeDateTimeArr1);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeDateTimeArr2, o2.PrFieldDateTimeDateTimeArr2);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeDateTimeArr3, o2.PrFieldDateTimeDateTimeArr3);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeDateTimeArr4, o2.PrFieldDateTimeDateTimeArr4);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeDateTimeArr0, o2._pubFieldDateTimeDateTimeArr0);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeDateTimeArr1, o2._pubFieldDateTimeDateTimeArr1);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeDateTimeArr2, o2._pubFieldDateTimeDateTimeArr2);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeDateTimeArr3, o2._pubFieldDateTimeDateTimeArr3);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeDateTimeArr4, o2._pubFieldDateTimeDateTimeArr4);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr0, o2.PropertyDateTimeDateTimeArr0);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr1, o2.PropertyDateTimeDateTimeArr1);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr2, o2.PropertyDateTimeDateTimeArr2);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr3, o2.PropertyDateTimeDateTimeArr3);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr4, o2.PropertyDateTimeDateTimeArr4);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr5, o2.PropertyDateTimeDateTimeArr5);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr6, o2.PropertyDateTimeDateTimeArr6);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr7, o2.PropertyDateTimeDateTimeArr7);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr8, o2.PropertyDateTimeDateTimeArr8);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr9, o2.PropertyDateTimeDateTimeArr9);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr10, o2.PropertyDateTimeDateTimeArr10);
                CollectionAssert.AreEqual(o1.PropertyDateTimeDateTimeArr11, o2.PropertyDateTimeDateTimeArr11);
                Assert.AreEqual(o1.PrFieldDateTime0, o2.PrFieldDateTime0);
                Assert.AreEqual(o1.PrFieldDateTime1, o2.PrFieldDateTime1);
                Assert.AreEqual(o1.PrFieldDateTime2, o2.PrFieldDateTime2);
                Assert.AreEqual(o1.PrFieldDateTime3, o2.PrFieldDateTime3);
                Assert.AreEqual(o1.PrFieldDateTime4, o2.PrFieldDateTime4);
                Assert.AreEqual(o1._pubFieldDateTime0, o2._pubFieldDateTime0);
                Assert.AreEqual(o1._pubFieldDateTime1, o2._pubFieldDateTime1);
                Assert.AreEqual(o1._pubFieldDateTime2, o2._pubFieldDateTime2);
                Assert.AreEqual(o1._pubFieldDateTime3, o2._pubFieldDateTime3);
                Assert.AreEqual(o1._pubFieldDateTime4, o2._pubFieldDateTime4);
                Assert.AreEqual(o1.PropertyDateTime0, o2.PropertyDateTime0);
                Assert.AreEqual(o1.PropertyDateTime1, o2.PropertyDateTime1);
                Assert.AreEqual(o1.PropertyDateTime2, o2.PropertyDateTime2);
                Assert.AreEqual(o1.PropertyDateTime3, o2.PropertyDateTime3);
                Assert.AreEqual(o1.PropertyDateTime4, o2.PropertyDateTime4);
                Assert.AreEqual(o1.PropertyDateTime5, o2.PropertyDateTime5);
                Assert.AreEqual(o1.PropertyDateTime6, o2.PropertyDateTime6);
                Assert.AreEqual(o1.PropertyDateTime7, o2.PropertyDateTime7);
                Assert.AreEqual(o1.PropertyDateTime8, o2.PropertyDateTime8);
                Assert.AreEqual(o1.PropertyDateTime9, o2.PropertyDateTime9);
                Assert.AreEqual(o1.PropertyDateTime10, o2.PropertyDateTime10);
                Assert.AreEqual(o1.PropertyDateTime11, o2.PropertyDateTime11);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeListList10, o2.PrFieldDateTimeListList10);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeListList11, o2.PrFieldDateTimeListList11);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeListList12, o2.PrFieldDateTimeListList12);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeListList13, o2.PrFieldDateTimeListList13);
                CollectionAssert.AreEqual(o1.PrFieldDateTimeListList14, o2.PrFieldDateTimeListList14);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeListList10, o2._pubFieldDateTimeListList10);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeListList11, o2._pubFieldDateTimeListList11);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeListList12, o2._pubFieldDateTimeListList12);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeListList13, o2._pubFieldDateTimeListList13);
                CollectionAssert.AreEqual(o1._pubFieldDateTimeListList14, o2._pubFieldDateTimeListList14);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList10, o2.PropertyDateTimeListList10);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList11, o2.PropertyDateTimeListList11);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList12, o2.PropertyDateTimeListList12);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList13, o2.PropertyDateTimeListList13);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList14, o2.PropertyDateTimeListList14);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList15, o2.PropertyDateTimeListList15);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList16, o2.PropertyDateTimeListList16);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList17, o2.PropertyDateTimeListList17);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList18, o2.PropertyDateTimeListList18);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList19, o2.PropertyDateTimeListList19);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList110, o2.PropertyDateTimeListList110);
                CollectionAssert.AreEqual(o1.PropertyDateTimeListList111, o2.PropertyDateTimeListList111);
                CollectionAssert.AreEqual(o1.PrFieldDecimalDecimalArr0, o2.PrFieldDecimalDecimalArr0);
                CollectionAssert.AreEqual(o1.PrFieldDecimalDecimalArr1, o2.PrFieldDecimalDecimalArr1);
                CollectionAssert.AreEqual(o1.PrFieldDecimalDecimalArr2, o2.PrFieldDecimalDecimalArr2);
                CollectionAssert.AreEqual(o1.PrFieldDecimalDecimalArr3, o2.PrFieldDecimalDecimalArr3);
                CollectionAssert.AreEqual(o1.PrFieldDecimalDecimalArr4, o2.PrFieldDecimalDecimalArr4);
                CollectionAssert.AreEqual(o1._pubFieldDecimalDecimalArr0, o2._pubFieldDecimalDecimalArr0);
                CollectionAssert.AreEqual(o1._pubFieldDecimalDecimalArr1, o2._pubFieldDecimalDecimalArr1);
                CollectionAssert.AreEqual(o1._pubFieldDecimalDecimalArr2, o2._pubFieldDecimalDecimalArr2);
                CollectionAssert.AreEqual(o1._pubFieldDecimalDecimalArr3, o2._pubFieldDecimalDecimalArr3);
                CollectionAssert.AreEqual(o1._pubFieldDecimalDecimalArr4, o2._pubFieldDecimalDecimalArr4);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr0, o2.PropertyDecimalDecimalArr0);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr1, o2.PropertyDecimalDecimalArr1);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr2, o2.PropertyDecimalDecimalArr2);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr3, o2.PropertyDecimalDecimalArr3);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr4, o2.PropertyDecimalDecimalArr4);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr5, o2.PropertyDecimalDecimalArr5);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr6, o2.PropertyDecimalDecimalArr6);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr7, o2.PropertyDecimalDecimalArr7);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr8, o2.PropertyDecimalDecimalArr8);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr9, o2.PropertyDecimalDecimalArr9);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr10, o2.PropertyDecimalDecimalArr10);
                CollectionAssert.AreEqual(o1.PropertyDecimalDecimalArr11, o2.PropertyDecimalDecimalArr11);
                Assert.AreEqual(o1.PrFieldDecimal0, o2.PrFieldDecimal0);
                Assert.AreEqual(o1.PrFieldDecimal1, o2.PrFieldDecimal1);
                Assert.AreEqual(o1.PrFieldDecimal2, o2.PrFieldDecimal2);
                Assert.AreEqual(o1.PrFieldDecimal3, o2.PrFieldDecimal3);
                Assert.AreEqual(o1.PrFieldDecimal4, o2.PrFieldDecimal4);
                Assert.AreEqual(o1._pubFieldDecimal0, o2._pubFieldDecimal0);
                Assert.AreEqual(o1._pubFieldDecimal1, o2._pubFieldDecimal1);
                Assert.AreEqual(o1._pubFieldDecimal2, o2._pubFieldDecimal2);
                Assert.AreEqual(o1._pubFieldDecimal3, o2._pubFieldDecimal3);
                Assert.AreEqual(o1._pubFieldDecimal4, o2._pubFieldDecimal4);
                Assert.AreEqual(o1.PropertyDecimal0, o2.PropertyDecimal0);
                Assert.AreEqual(o1.PropertyDecimal1, o2.PropertyDecimal1);
                Assert.AreEqual(o1.PropertyDecimal2, o2.PropertyDecimal2);
                Assert.AreEqual(o1.PropertyDecimal3, o2.PropertyDecimal3);
                Assert.AreEqual(o1.PropertyDecimal4, o2.PropertyDecimal4);
                Assert.AreEqual(o1.PropertyDecimal5, o2.PropertyDecimal5);
                Assert.AreEqual(o1.PropertyDecimal6, o2.PropertyDecimal6);
                Assert.AreEqual(o1.PropertyDecimal7, o2.PropertyDecimal7);
                Assert.AreEqual(o1.PropertyDecimal8, o2.PropertyDecimal8);
                Assert.AreEqual(o1.PropertyDecimal9, o2.PropertyDecimal9);
                Assert.AreEqual(o1.PropertyDecimal10, o2.PropertyDecimal10);
                Assert.AreEqual(o1.PropertyDecimal11, o2.PropertyDecimal11);
                CollectionAssert.AreEqual(o1.PrFieldDecimalListList10, o2.PrFieldDecimalListList10);
                CollectionAssert.AreEqual(o1.PrFieldDecimalListList11, o2.PrFieldDecimalListList11);
                CollectionAssert.AreEqual(o1.PrFieldDecimalListList12, o2.PrFieldDecimalListList12);
                CollectionAssert.AreEqual(o1.PrFieldDecimalListList13, o2.PrFieldDecimalListList13);
                CollectionAssert.AreEqual(o1.PrFieldDecimalListList14, o2.PrFieldDecimalListList14);
                CollectionAssert.AreEqual(o1._pubFieldDecimalListList10, o2._pubFieldDecimalListList10);
                CollectionAssert.AreEqual(o1._pubFieldDecimalListList11, o2._pubFieldDecimalListList11);
                CollectionAssert.AreEqual(o1._pubFieldDecimalListList12, o2._pubFieldDecimalListList12);
                CollectionAssert.AreEqual(o1._pubFieldDecimalListList13, o2._pubFieldDecimalListList13);
                CollectionAssert.AreEqual(o1._pubFieldDecimalListList14, o2._pubFieldDecimalListList14);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList10, o2.PropertyDecimalListList10);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList11, o2.PropertyDecimalListList11);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList12, o2.PropertyDecimalListList12);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList13, o2.PropertyDecimalListList13);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList14, o2.PropertyDecimalListList14);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList15, o2.PropertyDecimalListList15);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList16, o2.PropertyDecimalListList16);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList17, o2.PropertyDecimalListList17);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList18, o2.PropertyDecimalListList18);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList19, o2.PropertyDecimalListList19);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList110, o2.PropertyDecimalListList110);
                CollectionAssert.AreEqual(o1.PropertyDecimalListList111, o2.PropertyDecimalListList111);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumSomeEnumArr0, o2.PrFieldSomeEnumSomeEnumArr0);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumSomeEnumArr1, o2.PrFieldSomeEnumSomeEnumArr1);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumSomeEnumArr2, o2.PrFieldSomeEnumSomeEnumArr2);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumSomeEnumArr3, o2.PrFieldSomeEnumSomeEnumArr3);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumSomeEnumArr4, o2.PrFieldSomeEnumSomeEnumArr4);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumSomeEnumArr0, o2._pubFieldSomeEnumSomeEnumArr0);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumSomeEnumArr1, o2._pubFieldSomeEnumSomeEnumArr1);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumSomeEnumArr2, o2._pubFieldSomeEnumSomeEnumArr2);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumSomeEnumArr3, o2._pubFieldSomeEnumSomeEnumArr3);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumSomeEnumArr4, o2._pubFieldSomeEnumSomeEnumArr4);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr0, o2.PropertySomeEnumSomeEnumArr0);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr1, o2.PropertySomeEnumSomeEnumArr1);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr2, o2.PropertySomeEnumSomeEnumArr2);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr3, o2.PropertySomeEnumSomeEnumArr3);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr4, o2.PropertySomeEnumSomeEnumArr4);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr5, o2.PropertySomeEnumSomeEnumArr5);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr6, o2.PropertySomeEnumSomeEnumArr6);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr7, o2.PropertySomeEnumSomeEnumArr7);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr8, o2.PropertySomeEnumSomeEnumArr8);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr9, o2.PropertySomeEnumSomeEnumArr9);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr10, o2.PropertySomeEnumSomeEnumArr10);
                CollectionAssert.AreEqual(o1.PropertySomeEnumSomeEnumArr11, o2.PropertySomeEnumSomeEnumArr11);
                Assert.AreEqual(o1.PrFieldSomeEnum0, o2.PrFieldSomeEnum0);
                Assert.AreEqual(o1.PrFieldSomeEnum1, o2.PrFieldSomeEnum1);
                Assert.AreEqual(o1.PrFieldSomeEnum2, o2.PrFieldSomeEnum2);
                Assert.AreEqual(o1.PrFieldSomeEnum3, o2.PrFieldSomeEnum3);
                Assert.AreEqual(o1.PrFieldSomeEnum4, o2.PrFieldSomeEnum4);
                Assert.AreEqual(o1._pubFieldSomeEnum0, o2._pubFieldSomeEnum0);
                Assert.AreEqual(o1._pubFieldSomeEnum1, o2._pubFieldSomeEnum1);
                Assert.AreEqual(o1._pubFieldSomeEnum2, o2._pubFieldSomeEnum2);
                Assert.AreEqual(o1._pubFieldSomeEnum3, o2._pubFieldSomeEnum3);
                Assert.AreEqual(o1._pubFieldSomeEnum4, o2._pubFieldSomeEnum4);
                Assert.AreEqual(o1.PropertySomeEnum0, o2.PropertySomeEnum0);
                Assert.AreEqual(o1.PropertySomeEnum1, o2.PropertySomeEnum1);
                Assert.AreEqual(o1.PropertySomeEnum2, o2.PropertySomeEnum2);
                Assert.AreEqual(o1.PropertySomeEnum3, o2.PropertySomeEnum3);
                Assert.AreEqual(o1.PropertySomeEnum4, o2.PropertySomeEnum4);
                Assert.AreEqual(o1.PropertySomeEnum5, o2.PropertySomeEnum5);
                Assert.AreEqual(o1.PropertySomeEnum6, o2.PropertySomeEnum6);
                Assert.AreEqual(o1.PropertySomeEnum7, o2.PropertySomeEnum7);
                Assert.AreEqual(o1.PropertySomeEnum8, o2.PropertySomeEnum8);
                Assert.AreEqual(o1.PropertySomeEnum9, o2.PropertySomeEnum9);
                Assert.AreEqual(o1.PropertySomeEnum10, o2.PropertySomeEnum10);
                Assert.AreEqual(o1.PropertySomeEnum11, o2.PropertySomeEnum11);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumListList10, o2.PrFieldSomeEnumListList10);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumListList11, o2.PrFieldSomeEnumListList11);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumListList12, o2.PrFieldSomeEnumListList12);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumListList13, o2.PrFieldSomeEnumListList13);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumListList14, o2.PrFieldSomeEnumListList14);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumListList10, o2._pubFieldSomeEnumListList10);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumListList11, o2._pubFieldSomeEnumListList11);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumListList12, o2._pubFieldSomeEnumListList12);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumListList13, o2._pubFieldSomeEnumListList13);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumListList14, o2._pubFieldSomeEnumListList14);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList10, o2.PropertySomeEnumListList10);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList11, o2.PropertySomeEnumListList11);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList12, o2.PropertySomeEnumListList12);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList13, o2.PropertySomeEnumListList13);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList14, o2.PropertySomeEnumListList14);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList15, o2.PropertySomeEnumListList15);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList16, o2.PropertySomeEnumListList16);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList17, o2.PropertySomeEnumListList17);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList18, o2.PropertySomeEnumListList18);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList19, o2.PropertySomeEnumListList19);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList110, o2.PropertySomeEnumListList110);
                CollectionAssert.AreEqual(o1.PropertySomeEnumListList111, o2.PropertySomeEnumListList111);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructRandomizableStructArr0, o2.PrFieldRandomizableStructRandomizableStructArr0);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructRandomizableStructArr1, o2.PrFieldRandomizableStructRandomizableStructArr1);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructRandomizableStructArr2, o2.PrFieldRandomizableStructRandomizableStructArr2);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructRandomizableStructArr3, o2.PrFieldRandomizableStructRandomizableStructArr3);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructRandomizableStructArr4, o2.PrFieldRandomizableStructRandomizableStructArr4);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructRandomizableStructArr0, o2._pubFieldRandomizableStructRandomizableStructArr0);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructRandomizableStructArr1, o2._pubFieldRandomizableStructRandomizableStructArr1);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructRandomizableStructArr2, o2._pubFieldRandomizableStructRandomizableStructArr2);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructRandomizableStructArr3, o2._pubFieldRandomizableStructRandomizableStructArr3);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructRandomizableStructArr4, o2._pubFieldRandomizableStructRandomizableStructArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr0, o2.PropertyRandomizableStructRandomizableStructArr0);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr1, o2.PropertyRandomizableStructRandomizableStructArr1);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr2, o2.PropertyRandomizableStructRandomizableStructArr2);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr3, o2.PropertyRandomizableStructRandomizableStructArr3);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr4, o2.PropertyRandomizableStructRandomizableStructArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr5, o2.PropertyRandomizableStructRandomizableStructArr5);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr6, o2.PropertyRandomizableStructRandomizableStructArr6);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr7, o2.PropertyRandomizableStructRandomizableStructArr7);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr8, o2.PropertyRandomizableStructRandomizableStructArr8);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr9, o2.PropertyRandomizableStructRandomizableStructArr9);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr10, o2.PropertyRandomizableStructRandomizableStructArr10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructRandomizableStructArr11, o2.PropertyRandomizableStructRandomizableStructArr11);
                Assert.AreEqual(o1.PrFieldRandomizableStruct0, o2.PrFieldRandomizableStruct0);
                Assert.AreEqual(o1.PrFieldRandomizableStruct1, o2.PrFieldRandomizableStruct1);
                Assert.AreEqual(o1.PrFieldRandomizableStruct2, o2.PrFieldRandomizableStruct2);
                Assert.AreEqual(o1.PrFieldRandomizableStruct3, o2.PrFieldRandomizableStruct3);
                Assert.AreEqual(o1.PrFieldRandomizableStruct4, o2.PrFieldRandomizableStruct4);
                Assert.AreEqual(o1._pubFieldRandomizableStruct0, o2._pubFieldRandomizableStruct0);
                Assert.AreEqual(o1._pubFieldRandomizableStruct1, o2._pubFieldRandomizableStruct1);
                Assert.AreEqual(o1._pubFieldRandomizableStruct2, o2._pubFieldRandomizableStruct2);
                Assert.AreEqual(o1._pubFieldRandomizableStruct3, o2._pubFieldRandomizableStruct3);
                Assert.AreEqual(o1._pubFieldRandomizableStruct4, o2._pubFieldRandomizableStruct4);
                Assert.AreEqual(o1.PropertyRandomizableStruct0, o2.PropertyRandomizableStruct0);
                Assert.AreEqual(o1.PropertyRandomizableStruct1, o2.PropertyRandomizableStruct1);
                Assert.AreEqual(o1.PropertyRandomizableStruct2, o2.PropertyRandomizableStruct2);
                Assert.AreEqual(o1.PropertyRandomizableStruct3, o2.PropertyRandomizableStruct3);
                Assert.AreEqual(o1.PropertyRandomizableStruct4, o2.PropertyRandomizableStruct4);
                Assert.AreEqual(o1.PropertyRandomizableStruct5, o2.PropertyRandomizableStruct5);
                Assert.AreEqual(o1.PropertyRandomizableStruct6, o2.PropertyRandomizableStruct6);
                Assert.AreEqual(o1.PropertyRandomizableStruct7, o2.PropertyRandomizableStruct7);
                Assert.AreEqual(o1.PropertyRandomizableStruct8, o2.PropertyRandomizableStruct8);
                Assert.AreEqual(o1.PropertyRandomizableStruct9, o2.PropertyRandomizableStruct9);
                Assert.AreEqual(o1.PropertyRandomizableStruct10, o2.PropertyRandomizableStruct10);
                Assert.AreEqual(o1.PropertyRandomizableStruct11, o2.PropertyRandomizableStruct11);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructListList10, o2.PrFieldRandomizableStructListList10);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructListList11, o2.PrFieldRandomizableStructListList11);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructListList12, o2.PrFieldRandomizableStructListList12);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructListList13, o2.PrFieldRandomizableStructListList13);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableStructListList14, o2.PrFieldRandomizableStructListList14);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructListList10, o2._pubFieldRandomizableStructListList10);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructListList11, o2._pubFieldRandomizableStructListList11);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructListList12, o2._pubFieldRandomizableStructListList12);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructListList13, o2._pubFieldRandomizableStructListList13);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableStructListList14, o2._pubFieldRandomizableStructListList14);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList10, o2.PropertyRandomizableStructListList10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList11, o2.PropertyRandomizableStructListList11);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList12, o2.PropertyRandomizableStructListList12);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList13, o2.PropertyRandomizableStructListList13);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList14, o2.PropertyRandomizableStructListList14);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList15, o2.PropertyRandomizableStructListList15);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList16, o2.PropertyRandomizableStructListList16);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList17, o2.PropertyRandomizableStructListList17);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList18, o2.PropertyRandomizableStructListList18);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList19, o2.PropertyRandomizableStructListList19);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList110, o2.PropertyRandomizableStructListList110);
                CollectionAssert.AreEqual(o1.PropertyRandomizableStructListList111, o2.PropertyRandomizableStructListList111);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableRandomizableArr0, o2.PrFieldRandomizableRandomizableArr0);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableRandomizableArr1, o2.PrFieldRandomizableRandomizableArr1);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableRandomizableArr2, o2.PrFieldRandomizableRandomizableArr2);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableRandomizableArr3, o2.PrFieldRandomizableRandomizableArr3);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableRandomizableArr4, o2.PrFieldRandomizableRandomizableArr4);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableRandomizableArr0, o2._pubFieldRandomizableRandomizableArr0);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableRandomizableArr1, o2._pubFieldRandomizableRandomizableArr1);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableRandomizableArr2, o2._pubFieldRandomizableRandomizableArr2);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableRandomizableArr3, o2._pubFieldRandomizableRandomizableArr3);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableRandomizableArr4, o2._pubFieldRandomizableRandomizableArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr0, o2.PropertyRandomizableRandomizableArr0);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr1, o2.PropertyRandomizableRandomizableArr1);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr2, o2.PropertyRandomizableRandomizableArr2);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr3, o2.PropertyRandomizableRandomizableArr3);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr4, o2.PropertyRandomizableRandomizableArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr5, o2.PropertyRandomizableRandomizableArr5);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr6, o2.PropertyRandomizableRandomizableArr6);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr7, o2.PropertyRandomizableRandomizableArr7);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr8, o2.PropertyRandomizableRandomizableArr8);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr9, o2.PropertyRandomizableRandomizableArr9);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr10, o2.PropertyRandomizableRandomizableArr10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableRandomizableArr11, o2.PropertyRandomizableRandomizableArr11);
                Assert.AreEqual(o1.PrFieldRandomizable0, o2.PrFieldRandomizable0);
                Assert.AreEqual(o1.PrFieldRandomizable1, o2.PrFieldRandomizable1);
                Assert.AreEqual(o1.PrFieldRandomizable2, o2.PrFieldRandomizable2);
                Assert.AreEqual(o1.PrFieldRandomizable3, o2.PrFieldRandomizable3);
                Assert.AreEqual(o1.PrFieldRandomizable4, o2.PrFieldRandomizable4);
                Assert.AreEqual(o1._pubFieldRandomizable0, o2._pubFieldRandomizable0);
                Assert.AreEqual(o1._pubFieldRandomizable1, o2._pubFieldRandomizable1);
                Assert.AreEqual(o1._pubFieldRandomizable2, o2._pubFieldRandomizable2);
                Assert.AreEqual(o1._pubFieldRandomizable3, o2._pubFieldRandomizable3);
                Assert.AreEqual(o1._pubFieldRandomizable4, o2._pubFieldRandomizable4);
                Assert.AreEqual(o1.PropertyRandomizable0, o2.PropertyRandomizable0);
                Assert.AreEqual(o1.PropertyRandomizable1, o2.PropertyRandomizable1);
                Assert.AreEqual(o1.PropertyRandomizable2, o2.PropertyRandomizable2);
                Assert.AreEqual(o1.PropertyRandomizable3, o2.PropertyRandomizable3);
                Assert.AreEqual(o1.PropertyRandomizable4, o2.PropertyRandomizable4);
                Assert.AreEqual(o1.PropertyRandomizable5, o2.PropertyRandomizable5);
                Assert.AreEqual(o1.PropertyRandomizable6, o2.PropertyRandomizable6);
                Assert.AreEqual(o1.PropertyRandomizable7, o2.PropertyRandomizable7);
                Assert.AreEqual(o1.PropertyRandomizable8, o2.PropertyRandomizable8);
                Assert.AreEqual(o1.PropertyRandomizable9, o2.PropertyRandomizable9);
                Assert.AreEqual(o1.PropertyRandomizable10, o2.PropertyRandomizable10);
                Assert.AreEqual(o1.PropertyRandomizable11, o2.PropertyRandomizable11);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableListList10, o2.PrFieldRandomizableListList10);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableListList11, o2.PrFieldRandomizableListList11);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableListList12, o2.PrFieldRandomizableListList12);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableListList13, o2.PrFieldRandomizableListList13);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableListList14, o2.PrFieldRandomizableListList14);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableListList10, o2._pubFieldRandomizableListList10);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableListList11, o2._pubFieldRandomizableListList11);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableListList12, o2._pubFieldRandomizableListList12);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableListList13, o2._pubFieldRandomizableListList13);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableListList14, o2._pubFieldRandomizableListList14);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList10, o2.PropertyRandomizableListList10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList11, o2.PropertyRandomizableListList11);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList12, o2.PropertyRandomizableListList12);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList13, o2.PropertyRandomizableListList13);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList14, o2.PropertyRandomizableListList14);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList15, o2.PropertyRandomizableListList15);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList16, o2.PropertyRandomizableListList16);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList17, o2.PropertyRandomizableListList17);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList18, o2.PropertyRandomizableListList18);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList19, o2.PropertyRandomizableListList19);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList110, o2.PropertyRandomizableListList110);
                CollectionAssert.AreEqual(o1.PropertyRandomizableListList111, o2.PropertyRandomizableListList111);
                CollectionAssert.AreEqual(o1.PrFieldStringStringArr0, o2.PrFieldStringStringArr0);
                CollectionAssert.AreEqual(o1.PrFieldStringStringArr1, o2.PrFieldStringStringArr1);
                CollectionAssert.AreEqual(o1.PrFieldStringStringArr2, o2.PrFieldStringStringArr2);
                CollectionAssert.AreEqual(o1.PrFieldStringStringArr3, o2.PrFieldStringStringArr3);
                CollectionAssert.AreEqual(o1.PrFieldStringStringArr4, o2.PrFieldStringStringArr4);
                CollectionAssert.AreEqual(o1._pubFieldStringStringArr0, o2._pubFieldStringStringArr0);
                CollectionAssert.AreEqual(o1._pubFieldStringStringArr1, o2._pubFieldStringStringArr1);
                CollectionAssert.AreEqual(o1._pubFieldStringStringArr2, o2._pubFieldStringStringArr2);
                CollectionAssert.AreEqual(o1._pubFieldStringStringArr3, o2._pubFieldStringStringArr3);
                CollectionAssert.AreEqual(o1._pubFieldStringStringArr4, o2._pubFieldStringStringArr4);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr0, o2.PropertyStringStringArr0);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr1, o2.PropertyStringStringArr1);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr2, o2.PropertyStringStringArr2);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr3, o2.PropertyStringStringArr3);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr4, o2.PropertyStringStringArr4);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr5, o2.PropertyStringStringArr5);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr6, o2.PropertyStringStringArr6);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr7, o2.PropertyStringStringArr7);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr8, o2.PropertyStringStringArr8);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr9, o2.PropertyStringStringArr9);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr10, o2.PropertyStringStringArr10);
                CollectionAssert.AreEqual(o1.PropertyStringStringArr11, o2.PropertyStringStringArr11);
                Assert.AreEqual(o1.PrFieldString0, o2.PrFieldString0);
                Assert.AreEqual(o1.PrFieldString1, o2.PrFieldString1);
                Assert.AreEqual(o1.PrFieldString2, o2.PrFieldString2);
                Assert.AreEqual(o1.PrFieldString3, o2.PrFieldString3);
                Assert.AreEqual(o1.PrFieldString4, o2.PrFieldString4);
                Assert.AreEqual(o1._pubFieldString0, o2._pubFieldString0);
                Assert.AreEqual(o1._pubFieldString1, o2._pubFieldString1);
                Assert.AreEqual(o1._pubFieldString2, o2._pubFieldString2);
                Assert.AreEqual(o1._pubFieldString3, o2._pubFieldString3);
                Assert.AreEqual(o1._pubFieldString4, o2._pubFieldString4);
                Assert.AreEqual(o1.PropertyString0, o2.PropertyString0);
                Assert.AreEqual(o1.PropertyString1, o2.PropertyString1);
                Assert.AreEqual(o1.PropertyString2, o2.PropertyString2);
                Assert.AreEqual(o1.PropertyString3, o2.PropertyString3);
                Assert.AreEqual(o1.PropertyString4, o2.PropertyString4);
                Assert.AreEqual(o1.PropertyString5, o2.PropertyString5);
                Assert.AreEqual(o1.PropertyString6, o2.PropertyString6);
                Assert.AreEqual(o1.PropertyString7, o2.PropertyString7);
                Assert.AreEqual(o1.PropertyString8, o2.PropertyString8);
                Assert.AreEqual(o1.PropertyString9, o2.PropertyString9);
                Assert.AreEqual(o1.PropertyString10, o2.PropertyString10);
                Assert.AreEqual(o1.PropertyString11, o2.PropertyString11);
                CollectionAssert.AreEqual(o1.PrFieldStringListList10, o2.PrFieldStringListList10);
                CollectionAssert.AreEqual(o1.PrFieldStringListList11, o2.PrFieldStringListList11);
                CollectionAssert.AreEqual(o1.PrFieldStringListList12, o2.PrFieldStringListList12);
                CollectionAssert.AreEqual(o1.PrFieldStringListList13, o2.PrFieldStringListList13);
                CollectionAssert.AreEqual(o1.PrFieldStringListList14, o2.PrFieldStringListList14);
                CollectionAssert.AreEqual(o1._pubFieldStringListList10, o2._pubFieldStringListList10);
                CollectionAssert.AreEqual(o1._pubFieldStringListList11, o2._pubFieldStringListList11);
                CollectionAssert.AreEqual(o1._pubFieldStringListList12, o2._pubFieldStringListList12);
                CollectionAssert.AreEqual(o1._pubFieldStringListList13, o2._pubFieldStringListList13);
                CollectionAssert.AreEqual(o1._pubFieldStringListList14, o2._pubFieldStringListList14);
                CollectionAssert.AreEqual(o1.PropertyStringListList10, o2.PropertyStringListList10);
                CollectionAssert.AreEqual(o1.PropertyStringListList11, o2.PropertyStringListList11);
                CollectionAssert.AreEqual(o1.PropertyStringListList12, o2.PropertyStringListList12);
                CollectionAssert.AreEqual(o1.PropertyStringListList13, o2.PropertyStringListList13);
                CollectionAssert.AreEqual(o1.PropertyStringListList14, o2.PropertyStringListList14);
                CollectionAssert.AreEqual(o1.PropertyStringListList15, o2.PropertyStringListList15);
                CollectionAssert.AreEqual(o1.PropertyStringListList16, o2.PropertyStringListList16);
                CollectionAssert.AreEqual(o1.PropertyStringListList17, o2.PropertyStringListList17);
                CollectionAssert.AreEqual(o1.PropertyStringListList18, o2.PropertyStringListList18);
                CollectionAssert.AreEqual(o1.PropertyStringListList19, o2.PropertyStringListList19);
                CollectionAssert.AreEqual(o1.PropertyStringListList110, o2.PropertyStringListList110);
                CollectionAssert.AreEqual(o1.PropertyStringListList111, o2.PropertyStringListList111);
            }
        }
    }

    [Serializable]
    internal class NewMixedClass {
        private Byte[] _prFieldByteByteArr0;
        private Byte[] _prFieldByteByteArr1;
        private Byte[] _prFieldByteByteArr2;
        private Byte[] _prFieldByteByteArr3;
        private Byte[] _prFieldByteByteArr4;

        public Byte[] PrFieldByteByteArr0 {
            get { return _prFieldByteByteArr0; }
        }

        public Byte[] PrFieldByteByteArr1 {
            get { return _prFieldByteByteArr1; }
        }

        public Byte[] PrFieldByteByteArr2 {
            get { return _prFieldByteByteArr2; }
        }

        public Byte[] PrFieldByteByteArr3 {
            get { return _prFieldByteByteArr3; }
        }

        public Byte[] PrFieldByteByteArr4 {
            get { return _prFieldByteByteArr4; }
        }

        public Byte[] _pubFieldByteByteArr0;
        public Byte[] _pubFieldByteByteArr1;
        public Byte[] _pubFieldByteByteArr2;
        public Byte[] _pubFieldByteByteArr3;
        public Byte[] _pubFieldByteByteArr4;
        public Byte[] PropertyByteByteArr0 { get; set; }
        public Byte[] PropertyByteByteArr1 { get; set; }
        public Byte[] PropertyByteByteArr2 { get; set; }
        public Byte[] PropertyByteByteArr3 { get; set; }
        public Byte[] PropertyByteByteArr4 { get; set; }
        public Byte[] PropertyByteByteArr5 { get; set; }
        public Byte[] PropertyByteByteArr6 { get; set; }
        public Byte[] PropertyByteByteArr7 { get; set; }
        public Byte[] PropertyByteByteArr8 { get; set; }
        public Byte[] PropertyByteByteArr9 { get; set; }
        public Byte[] PropertyByteByteArr10 { get; set; }
        public Byte[] PropertyByteByteArr11 { get; set; }
        private Byte _prFieldByte0;
        private Byte _prFieldByte1;
        private Byte _prFieldByte2;
        private Byte _prFieldByte3;
        private Byte _prFieldByte4;

        public Byte PrFieldByte0 {
            get { return _prFieldByte0; }
        }

        public Byte PrFieldByte1 {
            get { return _prFieldByte1; }
        }

        public Byte PrFieldByte2 {
            get { return _prFieldByte2; }
        }

        public Byte PrFieldByte3 {
            get { return _prFieldByte3; }
        }

        public Byte PrFieldByte4 {
            get { return _prFieldByte4; }
        }

        public Byte _pubFieldByte0;
        public Byte _pubFieldByte1;
        public Byte _pubFieldByte2;
        public Byte _pubFieldByte3;
        public Byte _pubFieldByte4;
        public Byte PropertyByte0 { get; set; }
        public Byte PropertyByte1 { get; set; }
        public Byte PropertyByte2 { get; set; }
        public Byte PropertyByte3 { get; set; }
        public Byte PropertyByte4 { get; set; }
        public Byte PropertyByte5 { get; set; }
        public Byte PropertyByte6 { get; set; }
        public Byte PropertyByte7 { get; set; }
        public Byte PropertyByte8 { get; set; }
        public Byte PropertyByte9 { get; set; }
        public Byte PropertyByte10 { get; set; }
        public Byte PropertyByte11 { get; set; }
        private List<Byte> _prFieldByteListList10;
        private List<Byte> _prFieldByteListList11;
        private List<Byte> _prFieldByteListList12;
        private List<Byte> _prFieldByteListList13;
        private List<Byte> _prFieldByteListList14;

        public List<Byte> PrFieldByteListList10 {
            get { return _prFieldByteListList10; }
        }

        public List<Byte> PrFieldByteListList11 {
            get { return _prFieldByteListList11; }
        }

        public List<Byte> PrFieldByteListList12 {
            get { return _prFieldByteListList12; }
        }

        public List<Byte> PrFieldByteListList13 {
            get { return _prFieldByteListList13; }
        }

        public List<Byte> PrFieldByteListList14 {
            get { return _prFieldByteListList14; }
        }

        public List<Byte> _pubFieldByteListList10;
        public List<Byte> _pubFieldByteListList11;
        public List<Byte> _pubFieldByteListList12;
        public List<Byte> _pubFieldByteListList13;
        public List<Byte> _pubFieldByteListList14;
        public List<Byte> PropertyByteListList10 { get; set; }
        public List<Byte> PropertyByteListList11 { get; set; }
        public List<Byte> PropertyByteListList12 { get; set; }
        public List<Byte> PropertyByteListList13 { get; set; }
        public List<Byte> PropertyByteListList14 { get; set; }
        public List<Byte> PropertyByteListList15 { get; set; }
        public List<Byte> PropertyByteListList16 { get; set; }
        public List<Byte> PropertyByteListList17 { get; set; }
        public List<Byte> PropertyByteListList18 { get; set; }
        public List<Byte> PropertyByteListList19 { get; set; }
        public List<Byte> PropertyByteListList110 { get; set; }
        public List<Byte> PropertyByteListList111 { get; set; }
        private SByte[] _prFieldSByteSByteArr0;
        private SByte[] _prFieldSByteSByteArr1;
        private SByte[] _prFieldSByteSByteArr2;
        private SByte[] _prFieldSByteSByteArr3;
        private SByte[] _prFieldSByteSByteArr4;

        public SByte[] PrFieldSByteSByteArr0 {
            get { return _prFieldSByteSByteArr0; }
        }

        public SByte[] PrFieldSByteSByteArr1 {
            get { return _prFieldSByteSByteArr1; }
        }

        public SByte[] PrFieldSByteSByteArr2 {
            get { return _prFieldSByteSByteArr2; }
        }

        public SByte[] PrFieldSByteSByteArr3 {
            get { return _prFieldSByteSByteArr3; }
        }

        public SByte[] PrFieldSByteSByteArr4 {
            get { return _prFieldSByteSByteArr4; }
        }

        public SByte[] _pubFieldSByteSByteArr0;
        public SByte[] _pubFieldSByteSByteArr1;
        public SByte[] _pubFieldSByteSByteArr2;
        public SByte[] _pubFieldSByteSByteArr3;
        public SByte[] _pubFieldSByteSByteArr4;
        public SByte[] PropertySByteSByteArr0 { get; set; }
        public SByte[] PropertySByteSByteArr1 { get; set; }
        public SByte[] PropertySByteSByteArr2 { get; set; }
        public SByte[] PropertySByteSByteArr3 { get; set; }
        public SByte[] PropertySByteSByteArr4 { get; set; }
        public SByte[] PropertySByteSByteArr5 { get; set; }
        public SByte[] PropertySByteSByteArr6 { get; set; }
        public SByte[] PropertySByteSByteArr7 { get; set; }
        public SByte[] PropertySByteSByteArr8 { get; set; }
        public SByte[] PropertySByteSByteArr9 { get; set; }
        public SByte[] PropertySByteSByteArr10 { get; set; }
        public SByte[] PropertySByteSByteArr11 { get; set; }
        private SByte _prFieldSByte0;
        private SByte _prFieldSByte1;
        private SByte _prFieldSByte2;
        private SByte _prFieldSByte3;
        private SByte _prFieldSByte4;

        public SByte PrFieldSByte0 {
            get { return _prFieldSByte0; }
        }

        public SByte PrFieldSByte1 {
            get { return _prFieldSByte1; }
        }

        public SByte PrFieldSByte2 {
            get { return _prFieldSByte2; }
        }

        public SByte PrFieldSByte3 {
            get { return _prFieldSByte3; }
        }

        public SByte PrFieldSByte4 {
            get { return _prFieldSByte4; }
        }

        public SByte _pubFieldSByte0;
        public SByte _pubFieldSByte1;
        public SByte _pubFieldSByte2;
        public SByte _pubFieldSByte3;
        public SByte _pubFieldSByte4;
        public SByte PropertySByte0 { get; set; }
        public SByte PropertySByte1 { get; set; }
        public SByte PropertySByte2 { get; set; }
        public SByte PropertySByte3 { get; set; }
        public SByte PropertySByte4 { get; set; }
        public SByte PropertySByte5 { get; set; }
        public SByte PropertySByte6 { get; set; }
        public SByte PropertySByte7 { get; set; }
        public SByte PropertySByte8 { get; set; }
        public SByte PropertySByte9 { get; set; }
        public SByte PropertySByte10 { get; set; }
        public SByte PropertySByte11 { get; set; }
        private List<SByte> _prFieldSByteListList10;
        private List<SByte> _prFieldSByteListList11;
        private List<SByte> _prFieldSByteListList12;
        private List<SByte> _prFieldSByteListList13;
        private List<SByte> _prFieldSByteListList14;

        public List<SByte> PrFieldSByteListList10 {
            get { return _prFieldSByteListList10; }
        }

        public List<SByte> PrFieldSByteListList11 {
            get { return _prFieldSByteListList11; }
        }

        public List<SByte> PrFieldSByteListList12 {
            get { return _prFieldSByteListList12; }
        }

        public List<SByte> PrFieldSByteListList13 {
            get { return _prFieldSByteListList13; }
        }

        public List<SByte> PrFieldSByteListList14 {
            get { return _prFieldSByteListList14; }
        }

        public List<SByte> _pubFieldSByteListList10;
        public List<SByte> _pubFieldSByteListList11;
        public List<SByte> _pubFieldSByteListList12;
        public List<SByte> _pubFieldSByteListList13;
        public List<SByte> _pubFieldSByteListList14;
        public List<SByte> PropertySByteListList10 { get; set; }
        public List<SByte> PropertySByteListList11 { get; set; }
        public List<SByte> PropertySByteListList12 { get; set; }
        public List<SByte> PropertySByteListList13 { get; set; }
        public List<SByte> PropertySByteListList14 { get; set; }
        public List<SByte> PropertySByteListList15 { get; set; }
        public List<SByte> PropertySByteListList16 { get; set; }
        public List<SByte> PropertySByteListList17 { get; set; }
        public List<SByte> PropertySByteListList18 { get; set; }
        public List<SByte> PropertySByteListList19 { get; set; }
        public List<SByte> PropertySByteListList110 { get; set; }
        public List<SByte> PropertySByteListList111 { get; set; }
        private UInt16[] _prFieldUInt16UInt16Arr0;
        private UInt16[] _prFieldUInt16UInt16Arr1;
        private UInt16[] _prFieldUInt16UInt16Arr2;
        private UInt16[] _prFieldUInt16UInt16Arr3;
        private UInt16[] _prFieldUInt16UInt16Arr4;

        public UInt16[] PrFieldUInt16UInt16Arr0 {
            get { return _prFieldUInt16UInt16Arr0; }
        }

        public UInt16[] PrFieldUInt16UInt16Arr1 {
            get { return _prFieldUInt16UInt16Arr1; }
        }

        public UInt16[] PrFieldUInt16UInt16Arr2 {
            get { return _prFieldUInt16UInt16Arr2; }
        }

        public UInt16[] PrFieldUInt16UInt16Arr3 {
            get { return _prFieldUInt16UInt16Arr3; }
        }

        public UInt16[] PrFieldUInt16UInt16Arr4 {
            get { return _prFieldUInt16UInt16Arr4; }
        }

        public UInt16[] _pubFieldUInt16UInt16Arr0;
        public UInt16[] _pubFieldUInt16UInt16Arr1;
        public UInt16[] _pubFieldUInt16UInt16Arr2;
        public UInt16[] _pubFieldUInt16UInt16Arr3;
        public UInt16[] _pubFieldUInt16UInt16Arr4;
        public UInt16[] PropertyUInt16UInt16Arr0 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr1 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr2 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr3 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr4 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr5 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr6 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr7 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr8 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr9 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr10 { get; set; }
        public UInt16[] PropertyUInt16UInt16Arr11 { get; set; }
        private UInt16 _prFieldUInt160;
        private UInt16 _prFieldUInt161;
        private UInt16 _prFieldUInt162;
        private UInt16 _prFieldUInt163;
        private UInt16 _prFieldUInt164;

        public UInt16 PrFieldUInt160 {
            get { return _prFieldUInt160; }
        }

        public UInt16 PrFieldUInt161 {
            get { return _prFieldUInt161; }
        }

        public UInt16 PrFieldUInt162 {
            get { return _prFieldUInt162; }
        }

        public UInt16 PrFieldUInt163 {
            get { return _prFieldUInt163; }
        }

        public UInt16 PrFieldUInt164 {
            get { return _prFieldUInt164; }
        }

        public UInt16 _pubFieldUInt160;
        public UInt16 _pubFieldUInt161;
        public UInt16 _pubFieldUInt162;
        public UInt16 _pubFieldUInt163;
        public UInt16 _pubFieldUInt164;
        public UInt16 PropertyUInt160 { get; set; }
        public UInt16 PropertyUInt161 { get; set; }
        public UInt16 PropertyUInt162 { get; set; }
        public UInt16 PropertyUInt163 { get; set; }
        public UInt16 PropertyUInt164 { get; set; }
        public UInt16 PropertyUInt165 { get; set; }
        public UInt16 PropertyUInt166 { get; set; }
        public UInt16 PropertyUInt167 { get; set; }
        public UInt16 PropertyUInt168 { get; set; }
        public UInt16 PropertyUInt169 { get; set; }
        public UInt16 PropertyUInt1610 { get; set; }
        public UInt16 PropertyUInt1611 { get; set; }
        private List<UInt16> _prFieldUInt16ListList10;
        private List<UInt16> _prFieldUInt16ListList11;
        private List<UInt16> _prFieldUInt16ListList12;
        private List<UInt16> _prFieldUInt16ListList13;
        private List<UInt16> _prFieldUInt16ListList14;

        public List<UInt16> PrFieldUInt16ListList10 {
            get { return _prFieldUInt16ListList10; }
        }

        public List<UInt16> PrFieldUInt16ListList11 {
            get { return _prFieldUInt16ListList11; }
        }

        public List<UInt16> PrFieldUInt16ListList12 {
            get { return _prFieldUInt16ListList12; }
        }

        public List<UInt16> PrFieldUInt16ListList13 {
            get { return _prFieldUInt16ListList13; }
        }

        public List<UInt16> PrFieldUInt16ListList14 {
            get { return _prFieldUInt16ListList14; }
        }

        public List<UInt16> _pubFieldUInt16ListList10;
        public List<UInt16> _pubFieldUInt16ListList11;
        public List<UInt16> _pubFieldUInt16ListList12;
        public List<UInt16> _pubFieldUInt16ListList13;
        public List<UInt16> _pubFieldUInt16ListList14;
        public List<UInt16> PropertyUInt16ListList10 { get; set; }
        public List<UInt16> PropertyUInt16ListList11 { get; set; }
        public List<UInt16> PropertyUInt16ListList12 { get; set; }
        public List<UInt16> PropertyUInt16ListList13 { get; set; }
        public List<UInt16> PropertyUInt16ListList14 { get; set; }
        public List<UInt16> PropertyUInt16ListList15 { get; set; }
        public List<UInt16> PropertyUInt16ListList16 { get; set; }
        public List<UInt16> PropertyUInt16ListList17 { get; set; }
        public List<UInt16> PropertyUInt16ListList18 { get; set; }
        public List<UInt16> PropertyUInt16ListList19 { get; set; }
        public List<UInt16> PropertyUInt16ListList110 { get; set; }
        public List<UInt16> PropertyUInt16ListList111 { get; set; }
        private Int16[] _prFieldInt16Int16Arr0;
        private Int16[] _prFieldInt16Int16Arr1;
        private Int16[] _prFieldInt16Int16Arr2;
        private Int16[] _prFieldInt16Int16Arr3;
        private Int16[] _prFieldInt16Int16Arr4;

        public Int16[] PrFieldInt16Int16Arr0 {
            get { return _prFieldInt16Int16Arr0; }
        }

        public Int16[] PrFieldInt16Int16Arr1 {
            get { return _prFieldInt16Int16Arr1; }
        }

        public Int16[] PrFieldInt16Int16Arr2 {
            get { return _prFieldInt16Int16Arr2; }
        }

        public Int16[] PrFieldInt16Int16Arr3 {
            get { return _prFieldInt16Int16Arr3; }
        }

        public Int16[] PrFieldInt16Int16Arr4 {
            get { return _prFieldInt16Int16Arr4; }
        }

        public Int16[] _pubFieldInt16Int16Arr0;
        public Int16[] _pubFieldInt16Int16Arr1;
        public Int16[] _pubFieldInt16Int16Arr2;
        public Int16[] _pubFieldInt16Int16Arr3;
        public Int16[] _pubFieldInt16Int16Arr4;
        public Int16[] PropertyInt16Int16Arr0 { get; set; }
        public Int16[] PropertyInt16Int16Arr1 { get; set; }
        public Int16[] PropertyInt16Int16Arr2 { get; set; }
        public Int16[] PropertyInt16Int16Arr3 { get; set; }
        public Int16[] PropertyInt16Int16Arr4 { get; set; }
        public Int16[] PropertyInt16Int16Arr5 { get; set; }
        public Int16[] PropertyInt16Int16Arr6 { get; set; }
        public Int16[] PropertyInt16Int16Arr7 { get; set; }
        public Int16[] PropertyInt16Int16Arr8 { get; set; }
        public Int16[] PropertyInt16Int16Arr9 { get; set; }
        public Int16[] PropertyInt16Int16Arr10 { get; set; }
        public Int16[] PropertyInt16Int16Arr11 { get; set; }
        private Int16 _prFieldInt160;
        private Int16 _prFieldInt161;
        private Int16 _prFieldInt162;
        private Int16 _prFieldInt163;
        private Int16 _prFieldInt164;

        public Int16 PrFieldInt160 {
            get { return _prFieldInt160; }
        }

        public Int16 PrFieldInt161 {
            get { return _prFieldInt161; }
        }

        public Int16 PrFieldInt162 {
            get { return _prFieldInt162; }
        }

        public Int16 PrFieldInt163 {
            get { return _prFieldInt163; }
        }

        public Int16 PrFieldInt164 {
            get { return _prFieldInt164; }
        }

        public Int16 _pubFieldInt160;
        public Int16 _pubFieldInt161;
        public Int16 _pubFieldInt162;
        public Int16 _pubFieldInt163;
        public Int16 _pubFieldInt164;
        public Int16 PropertyInt160 { get; set; }
        public Int16 PropertyInt161 { get; set; }
        public Int16 PropertyInt162 { get; set; }
        public Int16 PropertyInt163 { get; set; }
        public Int16 PropertyInt164 { get; set; }
        public Int16 PropertyInt165 { get; set; }
        public Int16 PropertyInt166 { get; set; }
        public Int16 PropertyInt167 { get; set; }
        public Int16 PropertyInt168 { get; set; }
        public Int16 PropertyInt169 { get; set; }
        public Int16 PropertyInt1610 { get; set; }
        public Int16 PropertyInt1611 { get; set; }
        private List<Int16> _prFieldInt16ListList10;
        private List<Int16> _prFieldInt16ListList11;
        private List<Int16> _prFieldInt16ListList12;
        private List<Int16> _prFieldInt16ListList13;
        private List<Int16> _prFieldInt16ListList14;

        public List<Int16> PrFieldInt16ListList10 {
            get { return _prFieldInt16ListList10; }
        }

        public List<Int16> PrFieldInt16ListList11 {
            get { return _prFieldInt16ListList11; }
        }

        public List<Int16> PrFieldInt16ListList12 {
            get { return _prFieldInt16ListList12; }
        }

        public List<Int16> PrFieldInt16ListList13 {
            get { return _prFieldInt16ListList13; }
        }

        public List<Int16> PrFieldInt16ListList14 {
            get { return _prFieldInt16ListList14; }
        }

        public List<Int16> _pubFieldInt16ListList10;
        public List<Int16> _pubFieldInt16ListList11;
        public List<Int16> _pubFieldInt16ListList12;
        public List<Int16> _pubFieldInt16ListList13;
        public List<Int16> _pubFieldInt16ListList14;
        public List<Int16> PropertyInt16ListList10 { get; set; }
        public List<Int16> PropertyInt16ListList11 { get; set; }
        public List<Int16> PropertyInt16ListList12 { get; set; }
        public List<Int16> PropertyInt16ListList13 { get; set; }
        public List<Int16> PropertyInt16ListList14 { get; set; }
        public List<Int16> PropertyInt16ListList15 { get; set; }
        public List<Int16> PropertyInt16ListList16 { get; set; }
        public List<Int16> PropertyInt16ListList17 { get; set; }
        public List<Int16> PropertyInt16ListList18 { get; set; }
        public List<Int16> PropertyInt16ListList19 { get; set; }
        public List<Int16> PropertyInt16ListList110 { get; set; }
        public List<Int16> PropertyInt16ListList111 { get; set; }
        private UInt32[] _prFieldUInt32UInt32Arr0;
        private UInt32[] _prFieldUInt32UInt32Arr1;
        private UInt32[] _prFieldUInt32UInt32Arr2;
        private UInt32[] _prFieldUInt32UInt32Arr3;
        private UInt32[] _prFieldUInt32UInt32Arr4;

        public UInt32[] PrFieldUInt32UInt32Arr0 {
            get { return _prFieldUInt32UInt32Arr0; }
        }

        public UInt32[] PrFieldUInt32UInt32Arr1 {
            get { return _prFieldUInt32UInt32Arr1; }
        }

        public UInt32[] PrFieldUInt32UInt32Arr2 {
            get { return _prFieldUInt32UInt32Arr2; }
        }

        public UInt32[] PrFieldUInt32UInt32Arr3 {
            get { return _prFieldUInt32UInt32Arr3; }
        }

        public UInt32[] PrFieldUInt32UInt32Arr4 {
            get { return _prFieldUInt32UInt32Arr4; }
        }

        public UInt32[] _pubFieldUInt32UInt32Arr0;
        public UInt32[] _pubFieldUInt32UInt32Arr1;
        public UInt32[] _pubFieldUInt32UInt32Arr2;
        public UInt32[] _pubFieldUInt32UInt32Arr3;
        public UInt32[] _pubFieldUInt32UInt32Arr4;
        public UInt32[] PropertyUInt32UInt32Arr0 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr1 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr2 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr3 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr4 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr5 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr6 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr7 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr8 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr9 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr10 { get; set; }
        public UInt32[] PropertyUInt32UInt32Arr11 { get; set; }
        private UInt32 _prFieldUInt320;
        private UInt32 _prFieldUInt321;
        private UInt32 _prFieldUInt322;
        private UInt32 _prFieldUInt323;
        private UInt32 _prFieldUInt324;

        public UInt32 PrFieldUInt320 {
            get { return _prFieldUInt320; }
        }

        public UInt32 PrFieldUInt321 {
            get { return _prFieldUInt321; }
        }

        public UInt32 PrFieldUInt322 {
            get { return _prFieldUInt322; }
        }

        public UInt32 PrFieldUInt323 {
            get { return _prFieldUInt323; }
        }

        public UInt32 PrFieldUInt324 {
            get { return _prFieldUInt324; }
        }

        public UInt32 _pubFieldUInt320;
        public UInt32 _pubFieldUInt321;
        public UInt32 _pubFieldUInt322;
        public UInt32 _pubFieldUInt323;
        public UInt32 _pubFieldUInt324;
        public UInt32 PropertyUInt320 { get; set; }
        public UInt32 PropertyUInt321 { get; set; }
        public UInt32 PropertyUInt322 { get; set; }
        public UInt32 PropertyUInt323 { get; set; }
        public UInt32 PropertyUInt324 { get; set; }
        public UInt32 PropertyUInt325 { get; set; }
        public UInt32 PropertyUInt326 { get; set; }
        public UInt32 PropertyUInt327 { get; set; }
        public UInt32 PropertyUInt328 { get; set; }
        public UInt32 PropertyUInt329 { get; set; }
        public UInt32 PropertyUInt3210 { get; set; }
        public UInt32 PropertyUInt3211 { get; set; }
        private List<UInt32> _prFieldUInt32ListList10;
        private List<UInt32> _prFieldUInt32ListList11;
        private List<UInt32> _prFieldUInt32ListList12;
        private List<UInt32> _prFieldUInt32ListList13;
        private List<UInt32> _prFieldUInt32ListList14;

        public List<UInt32> PrFieldUInt32ListList10 {
            get { return _prFieldUInt32ListList10; }
        }

        public List<UInt32> PrFieldUInt32ListList11 {
            get { return _prFieldUInt32ListList11; }
        }

        public List<UInt32> PrFieldUInt32ListList12 {
            get { return _prFieldUInt32ListList12; }
        }

        public List<UInt32> PrFieldUInt32ListList13 {
            get { return _prFieldUInt32ListList13; }
        }

        public List<UInt32> PrFieldUInt32ListList14 {
            get { return _prFieldUInt32ListList14; }
        }

        public List<UInt32> _pubFieldUInt32ListList10;
        public List<UInt32> _pubFieldUInt32ListList11;
        public List<UInt32> _pubFieldUInt32ListList12;
        public List<UInt32> _pubFieldUInt32ListList13;
        public List<UInt32> _pubFieldUInt32ListList14;
        public List<UInt32> PropertyUInt32ListList10 { get; set; }
        public List<UInt32> PropertyUInt32ListList11 { get; set; }
        public List<UInt32> PropertyUInt32ListList12 { get; set; }
        public List<UInt32> PropertyUInt32ListList13 { get; set; }
        public List<UInt32> PropertyUInt32ListList14 { get; set; }
        public List<UInt32> PropertyUInt32ListList15 { get; set; }
        public List<UInt32> PropertyUInt32ListList16 { get; set; }
        public List<UInt32> PropertyUInt32ListList17 { get; set; }
        public List<UInt32> PropertyUInt32ListList18 { get; set; }
        public List<UInt32> PropertyUInt32ListList19 { get; set; }
        public List<UInt32> PropertyUInt32ListList110 { get; set; }
        public List<UInt32> PropertyUInt32ListList111 { get; set; }
        private Int32[] _prFieldInt32Int32Arr0;
        private Int32[] _prFieldInt32Int32Arr1;
        private Int32[] _prFieldInt32Int32Arr2;
        private Int32[] _prFieldInt32Int32Arr3;
        private Int32[] _prFieldInt32Int32Arr4;

        public Int32[] PrFieldInt32Int32Arr0 {
            get { return _prFieldInt32Int32Arr0; }
        }

        public Int32[] PrFieldInt32Int32Arr1 {
            get { return _prFieldInt32Int32Arr1; }
        }

        public Int32[] PrFieldInt32Int32Arr2 {
            get { return _prFieldInt32Int32Arr2; }
        }

        public Int32[] PrFieldInt32Int32Arr3 {
            get { return _prFieldInt32Int32Arr3; }
        }

        public Int32[] PrFieldInt32Int32Arr4 {
            get { return _prFieldInt32Int32Arr4; }
        }

        public Int32[] _pubFieldInt32Int32Arr0;
        public Int32[] _pubFieldInt32Int32Arr1;
        public Int32[] _pubFieldInt32Int32Arr2;
        public Int32[] _pubFieldInt32Int32Arr3;
        public Int32[] _pubFieldInt32Int32Arr4;
        public Int32[] PropertyInt32Int32Arr0 { get; set; }
        public Int32[] PropertyInt32Int32Arr1 { get; set; }
        public Int32[] PropertyInt32Int32Arr2 { get; set; }
        public Int32[] PropertyInt32Int32Arr3 { get; set; }
        public Int32[] PropertyInt32Int32Arr4 { get; set; }
        public Int32[] PropertyInt32Int32Arr5 { get; set; }
        public Int32[] PropertyInt32Int32Arr6 { get; set; }
        public Int32[] PropertyInt32Int32Arr7 { get; set; }
        public Int32[] PropertyInt32Int32Arr8 { get; set; }
        public Int32[] PropertyInt32Int32Arr9 { get; set; }
        public Int32[] PropertyInt32Int32Arr10 { get; set; }
        public Int32[] PropertyInt32Int32Arr11 { get; set; }
        private Int32 _prFieldInt320;
        private Int32 _prFieldInt321;
        private Int32 _prFieldInt322;
        private Int32 _prFieldInt323;
        private Int32 _prFieldInt324;

        public Int32 PrFieldInt320 {
            get { return _prFieldInt320; }
        }

        public Int32 PrFieldInt321 {
            get { return _prFieldInt321; }
        }

        public Int32 PrFieldInt322 {
            get { return _prFieldInt322; }
        }

        public Int32 PrFieldInt323 {
            get { return _prFieldInt323; }
        }

        public Int32 PrFieldInt324 {
            get { return _prFieldInt324; }
        }

        public Int32 _pubFieldInt320;
        public Int32 _pubFieldInt321;
        public Int32 _pubFieldInt322;
        public Int32 _pubFieldInt323;
        public Int32 _pubFieldInt324;
        public Int32 PropertyInt320 { get; set; }
        public Int32 PropertyInt321 { get; set; }
        public Int32 PropertyInt322 { get; set; }
        public Int32 PropertyInt323 { get; set; }
        public Int32 PropertyInt324 { get; set; }
        public Int32 PropertyInt325 { get; set; }
        public Int32 PropertyInt326 { get; set; }
        public Int32 PropertyInt327 { get; set; }
        public Int32 PropertyInt328 { get; set; }
        public Int32 PropertyInt329 { get; set; }
        public Int32 PropertyInt3210 { get; set; }
        public Int32 PropertyInt3211 { get; set; }
        private List<Int32> _prFieldInt32ListList10;
        private List<Int32> _prFieldInt32ListList11;
        private List<Int32> _prFieldInt32ListList12;
        private List<Int32> _prFieldInt32ListList13;
        private List<Int32> _prFieldInt32ListList14;

        public List<Int32> PrFieldInt32ListList10 {
            get { return _prFieldInt32ListList10; }
        }

        public List<Int32> PrFieldInt32ListList11 {
            get { return _prFieldInt32ListList11; }
        }

        public List<Int32> PrFieldInt32ListList12 {
            get { return _prFieldInt32ListList12; }
        }

        public List<Int32> PrFieldInt32ListList13 {
            get { return _prFieldInt32ListList13; }
        }

        public List<Int32> PrFieldInt32ListList14 {
            get { return _prFieldInt32ListList14; }
        }

        public List<Int32> _pubFieldInt32ListList10;
        public List<Int32> _pubFieldInt32ListList11;
        public List<Int32> _pubFieldInt32ListList12;
        public List<Int32> _pubFieldInt32ListList13;
        public List<Int32> _pubFieldInt32ListList14;
        public List<Int32> PropertyInt32ListList10 { get; set; }
        public List<Int32> PropertyInt32ListList11 { get; set; }
        public List<Int32> PropertyInt32ListList12 { get; set; }
        public List<Int32> PropertyInt32ListList13 { get; set; }
        public List<Int32> PropertyInt32ListList14 { get; set; }
        public List<Int32> PropertyInt32ListList15 { get; set; }
        public List<Int32> PropertyInt32ListList16 { get; set; }
        public List<Int32> PropertyInt32ListList17 { get; set; }
        public List<Int32> PropertyInt32ListList18 { get; set; }
        public List<Int32> PropertyInt32ListList19 { get; set; }
        public List<Int32> PropertyInt32ListList110 { get; set; }
        public List<Int32> PropertyInt32ListList111 { get; set; }
        private UInt64[] _prFieldUInt64UInt64Arr0;
        private UInt64[] _prFieldUInt64UInt64Arr1;
        private UInt64[] _prFieldUInt64UInt64Arr2;
        private UInt64[] _prFieldUInt64UInt64Arr3;
        private UInt64[] _prFieldUInt64UInt64Arr4;

        public UInt64[] PrFieldUInt64UInt64Arr0 {
            get { return _prFieldUInt64UInt64Arr0; }
        }

        public UInt64[] PrFieldUInt64UInt64Arr1 {
            get { return _prFieldUInt64UInt64Arr1; }
        }

        public UInt64[] PrFieldUInt64UInt64Arr2 {
            get { return _prFieldUInt64UInt64Arr2; }
        }

        public UInt64[] PrFieldUInt64UInt64Arr3 {
            get { return _prFieldUInt64UInt64Arr3; }
        }

        public UInt64[] PrFieldUInt64UInt64Arr4 {
            get { return _prFieldUInt64UInt64Arr4; }
        }

        public UInt64[] _pubFieldUInt64UInt64Arr0;
        public UInt64[] _pubFieldUInt64UInt64Arr1;
        public UInt64[] _pubFieldUInt64UInt64Arr2;
        public UInt64[] _pubFieldUInt64UInt64Arr3;
        public UInt64[] _pubFieldUInt64UInt64Arr4;
        public UInt64[] PropertyUInt64UInt64Arr0 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr1 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr2 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr3 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr4 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr5 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr6 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr7 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr8 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr9 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr10 { get; set; }
        public UInt64[] PropertyUInt64UInt64Arr11 { get; set; }
        private UInt64 _prFieldUInt640;
        private UInt64 _prFieldUInt641;
        private UInt64 _prFieldUInt642;
        private UInt64 _prFieldUInt643;
        private UInt64 _prFieldUInt644;

        public UInt64 PrFieldUInt640 {
            get { return _prFieldUInt640; }
        }

        public UInt64 PrFieldUInt641 {
            get { return _prFieldUInt641; }
        }

        public UInt64 PrFieldUInt642 {
            get { return _prFieldUInt642; }
        }

        public UInt64 PrFieldUInt643 {
            get { return _prFieldUInt643; }
        }

        public UInt64 PrFieldUInt644 {
            get { return _prFieldUInt644; }
        }

        public UInt64 _pubFieldUInt640;
        public UInt64 _pubFieldUInt641;
        public UInt64 _pubFieldUInt642;
        public UInt64 _pubFieldUInt643;
        public UInt64 _pubFieldUInt644;
        public UInt64 PropertyUInt640 { get; set; }
        public UInt64 PropertyUInt641 { get; set; }
        public UInt64 PropertyUInt642 { get; set; }
        public UInt64 PropertyUInt643 { get; set; }
        public UInt64 PropertyUInt644 { get; set; }
        public UInt64 PropertyUInt645 { get; set; }
        public UInt64 PropertyUInt646 { get; set; }
        public UInt64 PropertyUInt647 { get; set; }
        public UInt64 PropertyUInt648 { get; set; }
        public UInt64 PropertyUInt649 { get; set; }
        public UInt64 PropertyUInt6410 { get; set; }
        public UInt64 PropertyUInt6411 { get; set; }
        private List<UInt64> _prFieldUInt64ListList10;
        private List<UInt64> _prFieldUInt64ListList11;
        private List<UInt64> _prFieldUInt64ListList12;
        private List<UInt64> _prFieldUInt64ListList13;
        private List<UInt64> _prFieldUInt64ListList14;

        public List<UInt64> PrFieldUInt64ListList10 {
            get { return _prFieldUInt64ListList10; }
        }

        public List<UInt64> PrFieldUInt64ListList11 {
            get { return _prFieldUInt64ListList11; }
        }

        public List<UInt64> PrFieldUInt64ListList12 {
            get { return _prFieldUInt64ListList12; }
        }

        public List<UInt64> PrFieldUInt64ListList13 {
            get { return _prFieldUInt64ListList13; }
        }

        public List<UInt64> PrFieldUInt64ListList14 {
            get { return _prFieldUInt64ListList14; }
        }

        public List<UInt64> _pubFieldUInt64ListList10;
        public List<UInt64> _pubFieldUInt64ListList11;
        public List<UInt64> _pubFieldUInt64ListList12;
        public List<UInt64> _pubFieldUInt64ListList13;
        public List<UInt64> _pubFieldUInt64ListList14;
        public List<UInt64> PropertyUInt64ListList10 { get; set; }
        public List<UInt64> PropertyUInt64ListList11 { get; set; }
        public List<UInt64> PropertyUInt64ListList12 { get; set; }
        public List<UInt64> PropertyUInt64ListList13 { get; set; }
        public List<UInt64> PropertyUInt64ListList14 { get; set; }
        public List<UInt64> PropertyUInt64ListList15 { get; set; }
        public List<UInt64> PropertyUInt64ListList16 { get; set; }
        public List<UInt64> PropertyUInt64ListList17 { get; set; }
        public List<UInt64> PropertyUInt64ListList18 { get; set; }
        public List<UInt64> PropertyUInt64ListList19 { get; set; }
        public List<UInt64> PropertyUInt64ListList110 { get; set; }
        public List<UInt64> PropertyUInt64ListList111 { get; set; }
        private Int64[] _prFieldInt64Int64Arr0;
        private Int64[] _prFieldInt64Int64Arr1;
        private Int64[] _prFieldInt64Int64Arr2;
        private Int64[] _prFieldInt64Int64Arr3;
        private Int64[] _prFieldInt64Int64Arr4;

        public Int64[] PrFieldInt64Int64Arr0 {
            get { return _prFieldInt64Int64Arr0; }
        }

        public Int64[] PrFieldInt64Int64Arr1 {
            get { return _prFieldInt64Int64Arr1; }
        }

        public Int64[] PrFieldInt64Int64Arr2 {
            get { return _prFieldInt64Int64Arr2; }
        }

        public Int64[] PrFieldInt64Int64Arr3 {
            get { return _prFieldInt64Int64Arr3; }
        }

        public Int64[] PrFieldInt64Int64Arr4 {
            get { return _prFieldInt64Int64Arr4; }
        }

        public Int64[] _pubFieldInt64Int64Arr0;
        public Int64[] _pubFieldInt64Int64Arr1;
        public Int64[] _pubFieldInt64Int64Arr2;
        public Int64[] _pubFieldInt64Int64Arr3;
        public Int64[] _pubFieldInt64Int64Arr4;
        public Int64[] PropertyInt64Int64Arr0 { get; set; }
        public Int64[] PropertyInt64Int64Arr1 { get; set; }
        public Int64[] PropertyInt64Int64Arr2 { get; set; }
        public Int64[] PropertyInt64Int64Arr3 { get; set; }
        public Int64[] PropertyInt64Int64Arr4 { get; set; }
        public Int64[] PropertyInt64Int64Arr5 { get; set; }
        public Int64[] PropertyInt64Int64Arr6 { get; set; }
        public Int64[] PropertyInt64Int64Arr7 { get; set; }
        public Int64[] PropertyInt64Int64Arr8 { get; set; }
        public Int64[] PropertyInt64Int64Arr9 { get; set; }
        public Int64[] PropertyInt64Int64Arr10 { get; set; }
        public Int64[] PropertyInt64Int64Arr11 { get; set; }
        private Int64 _prFieldInt640;
        private Int64 _prFieldInt641;
        private Int64 _prFieldInt642;
        private Int64 _prFieldInt643;
        private Int64 _prFieldInt644;

        public Int64 PrFieldInt640 {
            get { return _prFieldInt640; }
        }

        public Int64 PrFieldInt641 {
            get { return _prFieldInt641; }
        }

        public Int64 PrFieldInt642 {
            get { return _prFieldInt642; }
        }

        public Int64 PrFieldInt643 {
            get { return _prFieldInt643; }
        }

        public Int64 PrFieldInt644 {
            get { return _prFieldInt644; }
        }

        public Int64 _pubFieldInt640;
        public Int64 _pubFieldInt641;
        public Int64 _pubFieldInt642;
        public Int64 _pubFieldInt643;
        public Int64 _pubFieldInt644;
        public Int64 PropertyInt640 { get; set; }
        public Int64 PropertyInt641 { get; set; }
        public Int64 PropertyInt642 { get; set; }
        public Int64 PropertyInt643 { get; set; }
        public Int64 PropertyInt644 { get; set; }
        public Int64 PropertyInt645 { get; set; }
        public Int64 PropertyInt646 { get; set; }
        public Int64 PropertyInt647 { get; set; }
        public Int64 PropertyInt648 { get; set; }
        public Int64 PropertyInt649 { get; set; }
        public Int64 PropertyInt6410 { get; set; }
        public Int64 PropertyInt6411 { get; set; }
        private List<Int64> _prFieldInt64ListList10;
        private List<Int64> _prFieldInt64ListList11;
        private List<Int64> _prFieldInt64ListList12;
        private List<Int64> _prFieldInt64ListList13;
        private List<Int64> _prFieldInt64ListList14;

        public List<Int64> PrFieldInt64ListList10 {
            get { return _prFieldInt64ListList10; }
        }

        public List<Int64> PrFieldInt64ListList11 {
            get { return _prFieldInt64ListList11; }
        }

        public List<Int64> PrFieldInt64ListList12 {
            get { return _prFieldInt64ListList12; }
        }

        public List<Int64> PrFieldInt64ListList13 {
            get { return _prFieldInt64ListList13; }
        }

        public List<Int64> PrFieldInt64ListList14 {
            get { return _prFieldInt64ListList14; }
        }

        public List<Int64> _pubFieldInt64ListList10;
        public List<Int64> _pubFieldInt64ListList11;
        public List<Int64> _pubFieldInt64ListList12;
        public List<Int64> _pubFieldInt64ListList13;
        public List<Int64> _pubFieldInt64ListList14;
        public List<Int64> PropertyInt64ListList10 { get; set; }
        public List<Int64> PropertyInt64ListList11 { get; set; }
        public List<Int64> PropertyInt64ListList12 { get; set; }
        public List<Int64> PropertyInt64ListList13 { get; set; }
        public List<Int64> PropertyInt64ListList14 { get; set; }
        public List<Int64> PropertyInt64ListList15 { get; set; }
        public List<Int64> PropertyInt64ListList16 { get; set; }
        public List<Int64> PropertyInt64ListList17 { get; set; }
        public List<Int64> PropertyInt64ListList18 { get; set; }
        public List<Int64> PropertyInt64ListList19 { get; set; }
        public List<Int64> PropertyInt64ListList110 { get; set; }
        public List<Int64> PropertyInt64ListList111 { get; set; }
        private Single[] _prFieldSingleSingleArr0;
        private Single[] _prFieldSingleSingleArr1;
        private Single[] _prFieldSingleSingleArr2;
        private Single[] _prFieldSingleSingleArr3;
        private Single[] _prFieldSingleSingleArr4;

        public Single[] PrFieldSingleSingleArr0 {
            get { return _prFieldSingleSingleArr0; }
        }

        public Single[] PrFieldSingleSingleArr1 {
            get { return _prFieldSingleSingleArr1; }
        }

        public Single[] PrFieldSingleSingleArr2 {
            get { return _prFieldSingleSingleArr2; }
        }

        public Single[] PrFieldSingleSingleArr3 {
            get { return _prFieldSingleSingleArr3; }
        }

        public Single[] PrFieldSingleSingleArr4 {
            get { return _prFieldSingleSingleArr4; }
        }

        public Single[] _pubFieldSingleSingleArr0;
        public Single[] _pubFieldSingleSingleArr1;
        public Single[] _pubFieldSingleSingleArr2;
        public Single[] _pubFieldSingleSingleArr3;
        public Single[] _pubFieldSingleSingleArr4;
        public Single[] PropertySingleSingleArr0 { get; set; }
        public Single[] PropertySingleSingleArr1 { get; set; }
        public Single[] PropertySingleSingleArr2 { get; set; }
        public Single[] PropertySingleSingleArr3 { get; set; }
        public Single[] PropertySingleSingleArr4 { get; set; }
        public Single[] PropertySingleSingleArr5 { get; set; }
        public Single[] PropertySingleSingleArr6 { get; set; }
        public Single[] PropertySingleSingleArr7 { get; set; }
        public Single[] PropertySingleSingleArr8 { get; set; }
        public Single[] PropertySingleSingleArr9 { get; set; }
        public Single[] PropertySingleSingleArr10 { get; set; }
        public Single[] PropertySingleSingleArr11 { get; set; }
        private Single _prFieldSingle0;
        private Single _prFieldSingle1;
        private Single _prFieldSingle2;
        private Single _prFieldSingle3;
        private Single _prFieldSingle4;

        public Single PrFieldSingle0 {
            get { return _prFieldSingle0; }
        }

        public Single PrFieldSingle1 {
            get { return _prFieldSingle1; }
        }

        public Single PrFieldSingle2 {
            get { return _prFieldSingle2; }
        }

        public Single PrFieldSingle3 {
            get { return _prFieldSingle3; }
        }

        public Single PrFieldSingle4 {
            get { return _prFieldSingle4; }
        }

        public Single _pubFieldSingle0;
        public Single _pubFieldSingle1;
        public Single _pubFieldSingle2;
        public Single _pubFieldSingle3;
        public Single _pubFieldSingle4;
        public Single PropertySingle0 { get; set; }
        public Single PropertySingle1 { get; set; }
        public Single PropertySingle2 { get; set; }
        public Single PropertySingle3 { get; set; }
        public Single PropertySingle4 { get; set; }
        public Single PropertySingle5 { get; set; }
        public Single PropertySingle6 { get; set; }
        public Single PropertySingle7 { get; set; }
        public Single PropertySingle8 { get; set; }
        public Single PropertySingle9 { get; set; }
        public Single PropertySingle10 { get; set; }
        public Single PropertySingle11 { get; set; }
        private List<Single> _prFieldSingleListList10;
        private List<Single> _prFieldSingleListList11;
        private List<Single> _prFieldSingleListList12;
        private List<Single> _prFieldSingleListList13;
        private List<Single> _prFieldSingleListList14;

        public List<Single> PrFieldSingleListList10 {
            get { return _prFieldSingleListList10; }
        }

        public List<Single> PrFieldSingleListList11 {
            get { return _prFieldSingleListList11; }
        }

        public List<Single> PrFieldSingleListList12 {
            get { return _prFieldSingleListList12; }
        }

        public List<Single> PrFieldSingleListList13 {
            get { return _prFieldSingleListList13; }
        }

        public List<Single> PrFieldSingleListList14 {
            get { return _prFieldSingleListList14; }
        }

        public List<Single> _pubFieldSingleListList10;
        public List<Single> _pubFieldSingleListList11;
        public List<Single> _pubFieldSingleListList12;
        public List<Single> _pubFieldSingleListList13;
        public List<Single> _pubFieldSingleListList14;
        public List<Single> PropertySingleListList10 { get; set; }
        public List<Single> PropertySingleListList11 { get; set; }
        public List<Single> PropertySingleListList12 { get; set; }
        public List<Single> PropertySingleListList13 { get; set; }
        public List<Single> PropertySingleListList14 { get; set; }
        public List<Single> PropertySingleListList15 { get; set; }
        public List<Single> PropertySingleListList16 { get; set; }
        public List<Single> PropertySingleListList17 { get; set; }
        public List<Single> PropertySingleListList18 { get; set; }
        public List<Single> PropertySingleListList19 { get; set; }
        public List<Single> PropertySingleListList110 { get; set; }
        public List<Single> PropertySingleListList111 { get; set; }
        private Double[] _prFieldDoubleDoubleArr0;
        private Double[] _prFieldDoubleDoubleArr1;
        private Double[] _prFieldDoubleDoubleArr2;
        private Double[] _prFieldDoubleDoubleArr3;
        private Double[] _prFieldDoubleDoubleArr4;

        public Double[] PrFieldDoubleDoubleArr0 {
            get { return _prFieldDoubleDoubleArr0; }
        }

        public Double[] PrFieldDoubleDoubleArr1 {
            get { return _prFieldDoubleDoubleArr1; }
        }

        public Double[] PrFieldDoubleDoubleArr2 {
            get { return _prFieldDoubleDoubleArr2; }
        }

        public Double[] PrFieldDoubleDoubleArr3 {
            get { return _prFieldDoubleDoubleArr3; }
        }

        public Double[] PrFieldDoubleDoubleArr4 {
            get { return _prFieldDoubleDoubleArr4; }
        }

        public Double[] _pubFieldDoubleDoubleArr0;
        public Double[] _pubFieldDoubleDoubleArr1;
        public Double[] _pubFieldDoubleDoubleArr2;
        public Double[] _pubFieldDoubleDoubleArr3;
        public Double[] _pubFieldDoubleDoubleArr4;
        public Double[] PropertyDoubleDoubleArr0 { get; set; }
        public Double[] PropertyDoubleDoubleArr1 { get; set; }
        public Double[] PropertyDoubleDoubleArr2 { get; set; }
        public Double[] PropertyDoubleDoubleArr3 { get; set; }
        public Double[] PropertyDoubleDoubleArr4 { get; set; }
        public Double[] PropertyDoubleDoubleArr5 { get; set; }
        public Double[] PropertyDoubleDoubleArr6 { get; set; }
        public Double[] PropertyDoubleDoubleArr7 { get; set; }
        public Double[] PropertyDoubleDoubleArr8 { get; set; }
        public Double[] PropertyDoubleDoubleArr9 { get; set; }
        public Double[] PropertyDoubleDoubleArr10 { get; set; }
        public Double[] PropertyDoubleDoubleArr11 { get; set; }
        private Double _prFieldDouble0;
        private Double _prFieldDouble1;
        private Double _prFieldDouble2;
        private Double _prFieldDouble3;
        private Double _prFieldDouble4;

        public Double PrFieldDouble0 {
            get { return _prFieldDouble0; }
        }

        public Double PrFieldDouble1 {
            get { return _prFieldDouble1; }
        }

        public Double PrFieldDouble2 {
            get { return _prFieldDouble2; }
        }

        public Double PrFieldDouble3 {
            get { return _prFieldDouble3; }
        }

        public Double PrFieldDouble4 {
            get { return _prFieldDouble4; }
        }

        public Double _pubFieldDouble0;
        public Double _pubFieldDouble1;
        public Double _pubFieldDouble2;
        public Double _pubFieldDouble3;
        public Double _pubFieldDouble4;
        public Double PropertyDouble0 { get; set; }
        public Double PropertyDouble1 { get; set; }
        public Double PropertyDouble2 { get; set; }
        public Double PropertyDouble3 { get; set; }
        public Double PropertyDouble4 { get; set; }
        public Double PropertyDouble5 { get; set; }
        public Double PropertyDouble6 { get; set; }
        public Double PropertyDouble7 { get; set; }
        public Double PropertyDouble8 { get; set; }
        public Double PropertyDouble9 { get; set; }
        public Double PropertyDouble10 { get; set; }
        public Double PropertyDouble11 { get; set; }
        private List<Double> _prFieldDoubleListList10;
        private List<Double> _prFieldDoubleListList11;
        private List<Double> _prFieldDoubleListList12;
        private List<Double> _prFieldDoubleListList13;
        private List<Double> _prFieldDoubleListList14;

        public List<Double> PrFieldDoubleListList10 {
            get { return _prFieldDoubleListList10; }
        }

        public List<Double> PrFieldDoubleListList11 {
            get { return _prFieldDoubleListList11; }
        }

        public List<Double> PrFieldDoubleListList12 {
            get { return _prFieldDoubleListList12; }
        }

        public List<Double> PrFieldDoubleListList13 {
            get { return _prFieldDoubleListList13; }
        }

        public List<Double> PrFieldDoubleListList14 {
            get { return _prFieldDoubleListList14; }
        }

        public List<Double> _pubFieldDoubleListList10;
        public List<Double> _pubFieldDoubleListList11;
        public List<Double> _pubFieldDoubleListList12;
        public List<Double> _pubFieldDoubleListList13;
        public List<Double> _pubFieldDoubleListList14;
        public List<Double> PropertyDoubleListList10 { get; set; }
        public List<Double> PropertyDoubleListList11 { get; set; }
        public List<Double> PropertyDoubleListList12 { get; set; }
        public List<Double> PropertyDoubleListList13 { get; set; }
        public List<Double> PropertyDoubleListList14 { get; set; }
        public List<Double> PropertyDoubleListList15 { get; set; }
        public List<Double> PropertyDoubleListList16 { get; set; }
        public List<Double> PropertyDoubleListList17 { get; set; }
        public List<Double> PropertyDoubleListList18 { get; set; }
        public List<Double> PropertyDoubleListList19 { get; set; }
        public List<Double> PropertyDoubleListList110 { get; set; }
        public List<Double> PropertyDoubleListList111 { get; set; }
        private Boolean[] _prFieldBooleanBooleanArr0;
        private Boolean[] _prFieldBooleanBooleanArr1;
        private Boolean[] _prFieldBooleanBooleanArr2;
        private Boolean[] _prFieldBooleanBooleanArr3;
        private Boolean[] _prFieldBooleanBooleanArr4;

        public Boolean[] PrFieldBooleanBooleanArr0 {
            get { return _prFieldBooleanBooleanArr0; }
        }

        public Boolean[] PrFieldBooleanBooleanArr1 {
            get { return _prFieldBooleanBooleanArr1; }
        }

        public Boolean[] PrFieldBooleanBooleanArr2 {
            get { return _prFieldBooleanBooleanArr2; }
        }

        public Boolean[] PrFieldBooleanBooleanArr3 {
            get { return _prFieldBooleanBooleanArr3; }
        }

        public Boolean[] PrFieldBooleanBooleanArr4 {
            get { return _prFieldBooleanBooleanArr4; }
        }

        public Boolean[] _pubFieldBooleanBooleanArr0;
        public Boolean[] _pubFieldBooleanBooleanArr1;
        public Boolean[] _pubFieldBooleanBooleanArr2;
        public Boolean[] _pubFieldBooleanBooleanArr3;
        public Boolean[] _pubFieldBooleanBooleanArr4;
        public Boolean[] PropertyBooleanBooleanArr0 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr1 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr2 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr3 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr4 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr5 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr6 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr7 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr8 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr9 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr10 { get; set; }
        public Boolean[] PropertyBooleanBooleanArr11 { get; set; }
        private Boolean _prFieldBoolean0;
        private Boolean _prFieldBoolean1;
        private Boolean _prFieldBoolean2;
        private Boolean _prFieldBoolean3;
        private Boolean _prFieldBoolean4;

        public Boolean PrFieldBoolean0 {
            get { return _prFieldBoolean0; }
        }

        public Boolean PrFieldBoolean1 {
            get { return _prFieldBoolean1; }
        }

        public Boolean PrFieldBoolean2 {
            get { return _prFieldBoolean2; }
        }

        public Boolean PrFieldBoolean3 {
            get { return _prFieldBoolean3; }
        }

        public Boolean PrFieldBoolean4 {
            get { return _prFieldBoolean4; }
        }

        public Boolean _pubFieldBoolean0;
        public Boolean _pubFieldBoolean1;
        public Boolean _pubFieldBoolean2;
        public Boolean _pubFieldBoolean3;
        public Boolean _pubFieldBoolean4;
        public Boolean PropertyBoolean0 { get; set; }
        public Boolean PropertyBoolean1 { get; set; }
        public Boolean PropertyBoolean2 { get; set; }
        public Boolean PropertyBoolean3 { get; set; }
        public Boolean PropertyBoolean4 { get; set; }
        public Boolean PropertyBoolean5 { get; set; }
        public Boolean PropertyBoolean6 { get; set; }
        public Boolean PropertyBoolean7 { get; set; }
        public Boolean PropertyBoolean8 { get; set; }
        public Boolean PropertyBoolean9 { get; set; }
        public Boolean PropertyBoolean10 { get; set; }
        public Boolean PropertyBoolean11 { get; set; }
        private List<Boolean> _prFieldBooleanListList10;
        private List<Boolean> _prFieldBooleanListList11;
        private List<Boolean> _prFieldBooleanListList12;
        private List<Boolean> _prFieldBooleanListList13;
        private List<Boolean> _prFieldBooleanListList14;

        public List<Boolean> PrFieldBooleanListList10 {
            get { return _prFieldBooleanListList10; }
        }

        public List<Boolean> PrFieldBooleanListList11 {
            get { return _prFieldBooleanListList11; }
        }

        public List<Boolean> PrFieldBooleanListList12 {
            get { return _prFieldBooleanListList12; }
        }

        public List<Boolean> PrFieldBooleanListList13 {
            get { return _prFieldBooleanListList13; }
        }

        public List<Boolean> PrFieldBooleanListList14 {
            get { return _prFieldBooleanListList14; }
        }

        public List<Boolean> _pubFieldBooleanListList10;
        public List<Boolean> _pubFieldBooleanListList11;
        public List<Boolean> _pubFieldBooleanListList12;
        public List<Boolean> _pubFieldBooleanListList13;
        public List<Boolean> _pubFieldBooleanListList14;
        public List<Boolean> PropertyBooleanListList10 { get; set; }
        public List<Boolean> PropertyBooleanListList11 { get; set; }
        public List<Boolean> PropertyBooleanListList12 { get; set; }
        public List<Boolean> PropertyBooleanListList13 { get; set; }
        public List<Boolean> PropertyBooleanListList14 { get; set; }
        public List<Boolean> PropertyBooleanListList15 { get; set; }
        public List<Boolean> PropertyBooleanListList16 { get; set; }
        public List<Boolean> PropertyBooleanListList17 { get; set; }
        public List<Boolean> PropertyBooleanListList18 { get; set; }
        public List<Boolean> PropertyBooleanListList19 { get; set; }
        public List<Boolean> PropertyBooleanListList110 { get; set; }
        public List<Boolean> PropertyBooleanListList111 { get; set; }
        private Char[] _prFieldCharCharArr0;
        private Char[] _prFieldCharCharArr1;
        private Char[] _prFieldCharCharArr2;
        private Char[] _prFieldCharCharArr3;
        private Char[] _prFieldCharCharArr4;

        public Char[] PrFieldCharCharArr0 {
            get { return _prFieldCharCharArr0; }
        }

        public Char[] PrFieldCharCharArr1 {
            get { return _prFieldCharCharArr1; }
        }

        public Char[] PrFieldCharCharArr2 {
            get { return _prFieldCharCharArr2; }
        }

        public Char[] PrFieldCharCharArr3 {
            get { return _prFieldCharCharArr3; }
        }

        public Char[] PrFieldCharCharArr4 {
            get { return _prFieldCharCharArr4; }
        }

        public Char[] _pubFieldCharCharArr0;
        public Char[] _pubFieldCharCharArr1;
        public Char[] _pubFieldCharCharArr2;
        public Char[] _pubFieldCharCharArr3;
        public Char[] _pubFieldCharCharArr4;
        public Char[] PropertyCharCharArr0 { get; set; }
        public Char[] PropertyCharCharArr1 { get; set; }
        public Char[] PropertyCharCharArr2 { get; set; }
        public Char[] PropertyCharCharArr3 { get; set; }
        public Char[] PropertyCharCharArr4 { get; set; }
        public Char[] PropertyCharCharArr5 { get; set; }
        public Char[] PropertyCharCharArr6 { get; set; }
        public Char[] PropertyCharCharArr7 { get; set; }
        public Char[] PropertyCharCharArr8 { get; set; }
        public Char[] PropertyCharCharArr9 { get; set; }
        public Char[] PropertyCharCharArr10 { get; set; }
        public Char[] PropertyCharCharArr11 { get; set; }
        private Char _prFieldChar0;
        private Char _prFieldChar1;
        private Char _prFieldChar2;
        private Char _prFieldChar3;
        private Char _prFieldChar4;

        public Char PrFieldChar0 {
            get { return _prFieldChar0; }
        }

        public Char PrFieldChar1 {
            get { return _prFieldChar1; }
        }

        public Char PrFieldChar2 {
            get { return _prFieldChar2; }
        }

        public Char PrFieldChar3 {
            get { return _prFieldChar3; }
        }

        public Char PrFieldChar4 {
            get { return _prFieldChar4; }
        }

        public Char _pubFieldChar0;
        public Char _pubFieldChar1;
        public Char _pubFieldChar2;
        public Char _pubFieldChar3;
        public Char _pubFieldChar4;
        public Char PropertyChar0 { get; set; }
        public Char PropertyChar1 { get; set; }
        public Char PropertyChar2 { get; set; }
        public Char PropertyChar3 { get; set; }
        public Char PropertyChar4 { get; set; }
        public Char PropertyChar5 { get; set; }
        public Char PropertyChar6 { get; set; }
        public Char PropertyChar7 { get; set; }
        public Char PropertyChar8 { get; set; }
        public Char PropertyChar9 { get; set; }
        public Char PropertyChar10 { get; set; }
        public Char PropertyChar11 { get; set; }
        private List<Char> _prFieldCharListList10;
        private List<Char> _prFieldCharListList11;
        private List<Char> _prFieldCharListList12;
        private List<Char> _prFieldCharListList13;
        private List<Char> _prFieldCharListList14;

        public List<Char> PrFieldCharListList10 {
            get { return _prFieldCharListList10; }
        }

        public List<Char> PrFieldCharListList11 {
            get { return _prFieldCharListList11; }
        }

        public List<Char> PrFieldCharListList12 {
            get { return _prFieldCharListList12; }
        }

        public List<Char> PrFieldCharListList13 {
            get { return _prFieldCharListList13; }
        }

        public List<Char> PrFieldCharListList14 {
            get { return _prFieldCharListList14; }
        }

        public List<Char> _pubFieldCharListList10;
        public List<Char> _pubFieldCharListList11;
        public List<Char> _pubFieldCharListList12;
        public List<Char> _pubFieldCharListList13;
        public List<Char> _pubFieldCharListList14;
        public List<Char> PropertyCharListList10 { get; set; }
        public List<Char> PropertyCharListList11 { get; set; }
        public List<Char> PropertyCharListList12 { get; set; }
        public List<Char> PropertyCharListList13 { get; set; }
        public List<Char> PropertyCharListList14 { get; set; }
        public List<Char> PropertyCharListList15 { get; set; }
        public List<Char> PropertyCharListList16 { get; set; }
        public List<Char> PropertyCharListList17 { get; set; }
        public List<Char> PropertyCharListList18 { get; set; }
        public List<Char> PropertyCharListList19 { get; set; }
        public List<Char> PropertyCharListList110 { get; set; }
        public List<Char> PropertyCharListList111 { get; set; }
        private DateTime[] _prFieldDateTimeDateTimeArr0;
        private DateTime[] _prFieldDateTimeDateTimeArr1;
        private DateTime[] _prFieldDateTimeDateTimeArr2;
        private DateTime[] _prFieldDateTimeDateTimeArr3;
        private DateTime[] _prFieldDateTimeDateTimeArr4;

        public DateTime[] PrFieldDateTimeDateTimeArr0 {
            get { return _prFieldDateTimeDateTimeArr0; }
        }

        public DateTime[] PrFieldDateTimeDateTimeArr1 {
            get { return _prFieldDateTimeDateTimeArr1; }
        }

        public DateTime[] PrFieldDateTimeDateTimeArr2 {
            get { return _prFieldDateTimeDateTimeArr2; }
        }

        public DateTime[] PrFieldDateTimeDateTimeArr3 {
            get { return _prFieldDateTimeDateTimeArr3; }
        }

        public DateTime[] PrFieldDateTimeDateTimeArr4 {
            get { return _prFieldDateTimeDateTimeArr4; }
        }

        public DateTime[] _pubFieldDateTimeDateTimeArr0;
        public DateTime[] _pubFieldDateTimeDateTimeArr1;
        public DateTime[] _pubFieldDateTimeDateTimeArr2;
        public DateTime[] _pubFieldDateTimeDateTimeArr3;
        public DateTime[] _pubFieldDateTimeDateTimeArr4;
        public DateTime[] PropertyDateTimeDateTimeArr0 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr1 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr2 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr3 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr4 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr5 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr6 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr7 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr8 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr9 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr10 { get; set; }
        public DateTime[] PropertyDateTimeDateTimeArr11 { get; set; }
        private DateTime _prFieldDateTime0;
        private DateTime _prFieldDateTime1;
        private DateTime _prFieldDateTime2;
        private DateTime _prFieldDateTime3;
        private DateTime _prFieldDateTime4;

        public DateTime PrFieldDateTime0 {
            get { return _prFieldDateTime0; }
        }

        public DateTime PrFieldDateTime1 {
            get { return _prFieldDateTime1; }
        }

        public DateTime PrFieldDateTime2 {
            get { return _prFieldDateTime2; }
        }

        public DateTime PrFieldDateTime3 {
            get { return _prFieldDateTime3; }
        }

        public DateTime PrFieldDateTime4 {
            get { return _prFieldDateTime4; }
        }

        public DateTime _pubFieldDateTime0;
        public DateTime _pubFieldDateTime1;
        public DateTime _pubFieldDateTime2;
        public DateTime _pubFieldDateTime3;
        public DateTime _pubFieldDateTime4;
        public DateTime PropertyDateTime0 { get; set; }
        public DateTime PropertyDateTime1 { get; set; }
        public DateTime PropertyDateTime2 { get; set; }
        public DateTime PropertyDateTime3 { get; set; }
        public DateTime PropertyDateTime4 { get; set; }
        public DateTime PropertyDateTime5 { get; set; }
        public DateTime PropertyDateTime6 { get; set; }
        public DateTime PropertyDateTime7 { get; set; }
        public DateTime PropertyDateTime8 { get; set; }
        public DateTime PropertyDateTime9 { get; set; }
        public DateTime PropertyDateTime10 { get; set; }
        public DateTime PropertyDateTime11 { get; set; }
        private List<DateTime> _prFieldDateTimeListList10;
        private List<DateTime> _prFieldDateTimeListList11;
        private List<DateTime> _prFieldDateTimeListList12;
        private List<DateTime> _prFieldDateTimeListList13;
        private List<DateTime> _prFieldDateTimeListList14;

        public List<DateTime> PrFieldDateTimeListList10 {
            get { return _prFieldDateTimeListList10; }
        }

        public List<DateTime> PrFieldDateTimeListList11 {
            get { return _prFieldDateTimeListList11; }
        }

        public List<DateTime> PrFieldDateTimeListList12 {
            get { return _prFieldDateTimeListList12; }
        }

        public List<DateTime> PrFieldDateTimeListList13 {
            get { return _prFieldDateTimeListList13; }
        }

        public List<DateTime> PrFieldDateTimeListList14 {
            get { return _prFieldDateTimeListList14; }
        }

        public List<DateTime> _pubFieldDateTimeListList10;
        public List<DateTime> _pubFieldDateTimeListList11;
        public List<DateTime> _pubFieldDateTimeListList12;
        public List<DateTime> _pubFieldDateTimeListList13;
        public List<DateTime> _pubFieldDateTimeListList14;
        public List<DateTime> PropertyDateTimeListList10 { get; set; }
        public List<DateTime> PropertyDateTimeListList11 { get; set; }
        public List<DateTime> PropertyDateTimeListList12 { get; set; }
        public List<DateTime> PropertyDateTimeListList13 { get; set; }
        public List<DateTime> PropertyDateTimeListList14 { get; set; }
        public List<DateTime> PropertyDateTimeListList15 { get; set; }
        public List<DateTime> PropertyDateTimeListList16 { get; set; }
        public List<DateTime> PropertyDateTimeListList17 { get; set; }
        public List<DateTime> PropertyDateTimeListList18 { get; set; }
        public List<DateTime> PropertyDateTimeListList19 { get; set; }
        public List<DateTime> PropertyDateTimeListList110 { get; set; }
        public List<DateTime> PropertyDateTimeListList111 { get; set; }
        private Decimal[] _prFieldDecimalDecimalArr0;
        private Decimal[] _prFieldDecimalDecimalArr1;
        private Decimal[] _prFieldDecimalDecimalArr2;
        private Decimal[] _prFieldDecimalDecimalArr3;
        private Decimal[] _prFieldDecimalDecimalArr4;

        public Decimal[] PrFieldDecimalDecimalArr0 {
            get { return _prFieldDecimalDecimalArr0; }
        }

        public Decimal[] PrFieldDecimalDecimalArr1 {
            get { return _prFieldDecimalDecimalArr1; }
        }

        public Decimal[] PrFieldDecimalDecimalArr2 {
            get { return _prFieldDecimalDecimalArr2; }
        }

        public Decimal[] PrFieldDecimalDecimalArr3 {
            get { return _prFieldDecimalDecimalArr3; }
        }

        public Decimal[] PrFieldDecimalDecimalArr4 {
            get { return _prFieldDecimalDecimalArr4; }
        }

        public Decimal[] _pubFieldDecimalDecimalArr0;
        public Decimal[] _pubFieldDecimalDecimalArr1;
        public Decimal[] _pubFieldDecimalDecimalArr2;
        public Decimal[] _pubFieldDecimalDecimalArr3;
        public Decimal[] _pubFieldDecimalDecimalArr4;
        public Decimal[] PropertyDecimalDecimalArr0 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr1 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr2 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr3 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr4 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr5 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr6 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr7 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr8 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr9 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr10 { get; set; }
        public Decimal[] PropertyDecimalDecimalArr11 { get; set; }
        private Decimal _prFieldDecimal0;
        private Decimal _prFieldDecimal1;
        private Decimal _prFieldDecimal2;
        private Decimal _prFieldDecimal3;
        private Decimal _prFieldDecimal4;

        public Decimal PrFieldDecimal0 {
            get { return _prFieldDecimal0; }
        }

        public Decimal PrFieldDecimal1 {
            get { return _prFieldDecimal1; }
        }

        public Decimal PrFieldDecimal2 {
            get { return _prFieldDecimal2; }
        }

        public Decimal PrFieldDecimal3 {
            get { return _prFieldDecimal3; }
        }

        public Decimal PrFieldDecimal4 {
            get { return _prFieldDecimal4; }
        }

        public Decimal _pubFieldDecimal0;
        public Decimal _pubFieldDecimal1;
        public Decimal _pubFieldDecimal2;
        public Decimal _pubFieldDecimal3;
        public Decimal _pubFieldDecimal4;
        public Decimal PropertyDecimal0 { get; set; }
        public Decimal PropertyDecimal1 { get; set; }
        public Decimal PropertyDecimal2 { get; set; }
        public Decimal PropertyDecimal3 { get; set; }
        public Decimal PropertyDecimal4 { get; set; }
        public Decimal PropertyDecimal5 { get; set; }
        public Decimal PropertyDecimal6 { get; set; }
        public Decimal PropertyDecimal7 { get; set; }
        public Decimal PropertyDecimal8 { get; set; }
        public Decimal PropertyDecimal9 { get; set; }
        public Decimal PropertyDecimal10 { get; set; }
        public Decimal PropertyDecimal11 { get; set; }
        private List<Decimal> _prFieldDecimalListList10;
        private List<Decimal> _prFieldDecimalListList11;
        private List<Decimal> _prFieldDecimalListList12;
        private List<Decimal> _prFieldDecimalListList13;
        private List<Decimal> _prFieldDecimalListList14;

        public List<Decimal> PrFieldDecimalListList10 {
            get { return _prFieldDecimalListList10; }
        }

        public List<Decimal> PrFieldDecimalListList11 {
            get { return _prFieldDecimalListList11; }
        }

        public List<Decimal> PrFieldDecimalListList12 {
            get { return _prFieldDecimalListList12; }
        }

        public List<Decimal> PrFieldDecimalListList13 {
            get { return _prFieldDecimalListList13; }
        }

        public List<Decimal> PrFieldDecimalListList14 {
            get { return _prFieldDecimalListList14; }
        }

        public List<Decimal> _pubFieldDecimalListList10;
        public List<Decimal> _pubFieldDecimalListList11;
        public List<Decimal> _pubFieldDecimalListList12;
        public List<Decimal> _pubFieldDecimalListList13;
        public List<Decimal> _pubFieldDecimalListList14;
        public List<Decimal> PropertyDecimalListList10 { get; set; }
        public List<Decimal> PropertyDecimalListList11 { get; set; }
        public List<Decimal> PropertyDecimalListList12 { get; set; }
        public List<Decimal> PropertyDecimalListList13 { get; set; }
        public List<Decimal> PropertyDecimalListList14 { get; set; }
        public List<Decimal> PropertyDecimalListList15 { get; set; }
        public List<Decimal> PropertyDecimalListList16 { get; set; }
        public List<Decimal> PropertyDecimalListList17 { get; set; }
        public List<Decimal> PropertyDecimalListList18 { get; set; }
        public List<Decimal> PropertyDecimalListList19 { get; set; }
        public List<Decimal> PropertyDecimalListList110 { get; set; }
        public List<Decimal> PropertyDecimalListList111 { get; set; }
        private SomeEnum[] _prFieldSomeEnumSomeEnumArr0;
        private SomeEnum[] _prFieldSomeEnumSomeEnumArr1;
        private SomeEnum[] _prFieldSomeEnumSomeEnumArr2;
        private SomeEnum[] _prFieldSomeEnumSomeEnumArr3;
        private SomeEnum[] _prFieldSomeEnumSomeEnumArr4;

        public SomeEnum[] PrFieldSomeEnumSomeEnumArr0 {
            get { return _prFieldSomeEnumSomeEnumArr0; }
        }

        public SomeEnum[] PrFieldSomeEnumSomeEnumArr1 {
            get { return _prFieldSomeEnumSomeEnumArr1; }
        }

        public SomeEnum[] PrFieldSomeEnumSomeEnumArr2 {
            get { return _prFieldSomeEnumSomeEnumArr2; }
        }

        public SomeEnum[] PrFieldSomeEnumSomeEnumArr3 {
            get { return _prFieldSomeEnumSomeEnumArr3; }
        }

        public SomeEnum[] PrFieldSomeEnumSomeEnumArr4 {
            get { return _prFieldSomeEnumSomeEnumArr4; }
        }

        public SomeEnum[] _pubFieldSomeEnumSomeEnumArr0;
        public SomeEnum[] _pubFieldSomeEnumSomeEnumArr1;
        public SomeEnum[] _pubFieldSomeEnumSomeEnumArr2;
        public SomeEnum[] _pubFieldSomeEnumSomeEnumArr3;
        public SomeEnum[] _pubFieldSomeEnumSomeEnumArr4;
        public SomeEnum[] PropertySomeEnumSomeEnumArr0 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr1 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr2 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr3 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr4 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr5 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr6 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr7 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr8 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr9 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr10 { get; set; }
        public SomeEnum[] PropertySomeEnumSomeEnumArr11 { get; set; }
        private SomeEnum _prFieldSomeEnum0;
        private SomeEnum _prFieldSomeEnum1;
        private SomeEnum _prFieldSomeEnum2;
        private SomeEnum _prFieldSomeEnum3;
        private SomeEnum _prFieldSomeEnum4;

        public SomeEnum PrFieldSomeEnum0 {
            get { return _prFieldSomeEnum0; }
        }

        public SomeEnum PrFieldSomeEnum1 {
            get { return _prFieldSomeEnum1; }
        }

        public SomeEnum PrFieldSomeEnum2 {
            get { return _prFieldSomeEnum2; }
        }

        public SomeEnum PrFieldSomeEnum3 {
            get { return _prFieldSomeEnum3; }
        }

        public SomeEnum PrFieldSomeEnum4 {
            get { return _prFieldSomeEnum4; }
        }

        public SomeEnum _pubFieldSomeEnum0;
        public SomeEnum _pubFieldSomeEnum1;
        public SomeEnum _pubFieldSomeEnum2;
        public SomeEnum _pubFieldSomeEnum3;
        public SomeEnum _pubFieldSomeEnum4;
        public SomeEnum PropertySomeEnum0 { get; set; }
        public SomeEnum PropertySomeEnum1 { get; set; }
        public SomeEnum PropertySomeEnum2 { get; set; }
        public SomeEnum PropertySomeEnum3 { get; set; }
        public SomeEnum PropertySomeEnum4 { get; set; }
        public SomeEnum PropertySomeEnum5 { get; set; }
        public SomeEnum PropertySomeEnum6 { get; set; }
        public SomeEnum PropertySomeEnum7 { get; set; }
        public SomeEnum PropertySomeEnum8 { get; set; }
        public SomeEnum PropertySomeEnum9 { get; set; }
        public SomeEnum PropertySomeEnum10 { get; set; }
        public SomeEnum PropertySomeEnum11 { get; set; }
        private List<SomeEnum> _prFieldSomeEnumListList10;
        private List<SomeEnum> _prFieldSomeEnumListList11;
        private List<SomeEnum> _prFieldSomeEnumListList12;
        private List<SomeEnum> _prFieldSomeEnumListList13;
        private List<SomeEnum> _prFieldSomeEnumListList14;

        public List<SomeEnum> PrFieldSomeEnumListList10 {
            get { return _prFieldSomeEnumListList10; }
        }

        public List<SomeEnum> PrFieldSomeEnumListList11 {
            get { return _prFieldSomeEnumListList11; }
        }

        public List<SomeEnum> PrFieldSomeEnumListList12 {
            get { return _prFieldSomeEnumListList12; }
        }

        public List<SomeEnum> PrFieldSomeEnumListList13 {
            get { return _prFieldSomeEnumListList13; }
        }

        public List<SomeEnum> PrFieldSomeEnumListList14 {
            get { return _prFieldSomeEnumListList14; }
        }

        public List<SomeEnum> _pubFieldSomeEnumListList10;
        public List<SomeEnum> _pubFieldSomeEnumListList11;
        public List<SomeEnum> _pubFieldSomeEnumListList12;
        public List<SomeEnum> _pubFieldSomeEnumListList13;
        public List<SomeEnum> _pubFieldSomeEnumListList14;
        public List<SomeEnum> PropertySomeEnumListList10 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList11 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList12 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList13 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList14 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList15 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList16 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList17 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList18 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList19 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList110 { get; set; }
        public List<SomeEnum> PropertySomeEnumListList111 { get; set; }
        private RandomizableStruct[] _prFieldRandomizableStructRandomizableStructArr0;
        private RandomizableStruct[] _prFieldRandomizableStructRandomizableStructArr1;
        private RandomizableStruct[] _prFieldRandomizableStructRandomizableStructArr2;
        private RandomizableStruct[] _prFieldRandomizableStructRandomizableStructArr3;
        private RandomizableStruct[] _prFieldRandomizableStructRandomizableStructArr4;

        public RandomizableStruct[] PrFieldRandomizableStructRandomizableStructArr0 {
            get { return _prFieldRandomizableStructRandomizableStructArr0; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructRandomizableStructArr1 {
            get { return _prFieldRandomizableStructRandomizableStructArr1; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructRandomizableStructArr2 {
            get { return _prFieldRandomizableStructRandomizableStructArr2; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructRandomizableStructArr3 {
            get { return _prFieldRandomizableStructRandomizableStructArr3; }
        }

        public RandomizableStruct[] PrFieldRandomizableStructRandomizableStructArr4 {
            get { return _prFieldRandomizableStructRandomizableStructArr4; }
        }

        public RandomizableStruct[] _pubFieldRandomizableStructRandomizableStructArr0;
        public RandomizableStruct[] _pubFieldRandomizableStructRandomizableStructArr1;
        public RandomizableStruct[] _pubFieldRandomizableStructRandomizableStructArr2;
        public RandomizableStruct[] _pubFieldRandomizableStructRandomizableStructArr3;
        public RandomizableStruct[] _pubFieldRandomizableStructRandomizableStructArr4;
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr0 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr1 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr2 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr3 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr4 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr5 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr6 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr7 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr8 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr9 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr10 { get; set; }
        public RandomizableStruct[] PropertyRandomizableStructRandomizableStructArr11 { get; set; }
        private RandomizableStruct _prFieldRandomizableStruct0;
        private RandomizableStruct _prFieldRandomizableStruct1;
        private RandomizableStruct _prFieldRandomizableStruct2;
        private RandomizableStruct _prFieldRandomizableStruct3;
        private RandomizableStruct _prFieldRandomizableStruct4;

        public RandomizableStruct PrFieldRandomizableStruct0 {
            get { return _prFieldRandomizableStruct0; }
        }

        public RandomizableStruct PrFieldRandomizableStruct1 {
            get { return _prFieldRandomizableStruct1; }
        }

        public RandomizableStruct PrFieldRandomizableStruct2 {
            get { return _prFieldRandomizableStruct2; }
        }

        public RandomizableStruct PrFieldRandomizableStruct3 {
            get { return _prFieldRandomizableStruct3; }
        }

        public RandomizableStruct PrFieldRandomizableStruct4 {
            get { return _prFieldRandomizableStruct4; }
        }

        public RandomizableStruct _pubFieldRandomizableStruct0;
        public RandomizableStruct _pubFieldRandomizableStruct1;
        public RandomizableStruct _pubFieldRandomizableStruct2;
        public RandomizableStruct _pubFieldRandomizableStruct3;
        public RandomizableStruct _pubFieldRandomizableStruct4;
        public RandomizableStruct PropertyRandomizableStruct0 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct1 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct2 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct3 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct4 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct5 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct6 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct7 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct8 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct9 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct10 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct11 { get; set; }
        private List<RandomizableStruct> _prFieldRandomizableStructListList10;
        private List<RandomizableStruct> _prFieldRandomizableStructListList11;
        private List<RandomizableStruct> _prFieldRandomizableStructListList12;
        private List<RandomizableStruct> _prFieldRandomizableStructListList13;
        private List<RandomizableStruct> _prFieldRandomizableStructListList14;

        public List<RandomizableStruct> PrFieldRandomizableStructListList10 {
            get { return _prFieldRandomizableStructListList10; }
        }

        public List<RandomizableStruct> PrFieldRandomizableStructListList11 {
            get { return _prFieldRandomizableStructListList11; }
        }

        public List<RandomizableStruct> PrFieldRandomizableStructListList12 {
            get { return _prFieldRandomizableStructListList12; }
        }

        public List<RandomizableStruct> PrFieldRandomizableStructListList13 {
            get { return _prFieldRandomizableStructListList13; }
        }

        public List<RandomizableStruct> PrFieldRandomizableStructListList14 {
            get { return _prFieldRandomizableStructListList14; }
        }

        public List<RandomizableStruct> _pubFieldRandomizableStructListList10;
        public List<RandomizableStruct> _pubFieldRandomizableStructListList11;
        public List<RandomizableStruct> _pubFieldRandomizableStructListList12;
        public List<RandomizableStruct> _pubFieldRandomizableStructListList13;
        public List<RandomizableStruct> _pubFieldRandomizableStructListList14;
        public List<RandomizableStruct> PropertyRandomizableStructListList10 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList11 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList12 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList13 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList14 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList15 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList16 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList17 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList18 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList19 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList110 { get; set; }
        public List<RandomizableStruct> PropertyRandomizableStructListList111 { get; set; }
        private Randomizable[] _prFieldRandomizableRandomizableArr0;
        private Randomizable[] _prFieldRandomizableRandomizableArr1;
        private Randomizable[] _prFieldRandomizableRandomizableArr2;
        private Randomizable[] _prFieldRandomizableRandomizableArr3;
        private Randomizable[] _prFieldRandomizableRandomizableArr4;

        public Randomizable[] PrFieldRandomizableRandomizableArr0 {
            get { return _prFieldRandomizableRandomizableArr0; }
        }

        public Randomizable[] PrFieldRandomizableRandomizableArr1 {
            get { return _prFieldRandomizableRandomizableArr1; }
        }

        public Randomizable[] PrFieldRandomizableRandomizableArr2 {
            get { return _prFieldRandomizableRandomizableArr2; }
        }

        public Randomizable[] PrFieldRandomizableRandomizableArr3 {
            get { return _prFieldRandomizableRandomizableArr3; }
        }

        public Randomizable[] PrFieldRandomizableRandomizableArr4 {
            get { return _prFieldRandomizableRandomizableArr4; }
        }

        public Randomizable[] _pubFieldRandomizableRandomizableArr0;
        public Randomizable[] _pubFieldRandomizableRandomizableArr1;
        public Randomizable[] _pubFieldRandomizableRandomizableArr2;
        public Randomizable[] _pubFieldRandomizableRandomizableArr3;
        public Randomizable[] _pubFieldRandomizableRandomizableArr4;
        public Randomizable[] PropertyRandomizableRandomizableArr0 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr1 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr2 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr3 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr4 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr5 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr6 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr7 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr8 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr9 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr10 { get; set; }
        public Randomizable[] PropertyRandomizableRandomizableArr11 { get; set; }
        private Randomizable _prFieldRandomizable0;
        private Randomizable _prFieldRandomizable1;
        private Randomizable _prFieldRandomizable2;
        private Randomizable _prFieldRandomizable3;
        private Randomizable _prFieldRandomizable4;

        public Randomizable PrFieldRandomizable0 {
            get { return _prFieldRandomizable0; }
        }

        public Randomizable PrFieldRandomizable1 {
            get { return _prFieldRandomizable1; }
        }

        public Randomizable PrFieldRandomizable2 {
            get { return _prFieldRandomizable2; }
        }

        public Randomizable PrFieldRandomizable3 {
            get { return _prFieldRandomizable3; }
        }

        public Randomizable PrFieldRandomizable4 {
            get { return _prFieldRandomizable4; }
        }

        public Randomizable _pubFieldRandomizable0;
        public Randomizable _pubFieldRandomizable1;
        public Randomizable _pubFieldRandomizable2;
        public Randomizable _pubFieldRandomizable3;
        public Randomizable _pubFieldRandomizable4;
        public Randomizable PropertyRandomizable0 { get; set; }
        public Randomizable PropertyRandomizable1 { get; set; }
        public Randomizable PropertyRandomizable2 { get; set; }
        public Randomizable PropertyRandomizable3 { get; set; }
        public Randomizable PropertyRandomizable4 { get; set; }
        public Randomizable PropertyRandomizable5 { get; set; }
        public Randomizable PropertyRandomizable6 { get; set; }
        public Randomizable PropertyRandomizable7 { get; set; }
        public Randomizable PropertyRandomizable8 { get; set; }
        public Randomizable PropertyRandomizable9 { get; set; }
        public Randomizable PropertyRandomizable10 { get; set; }
        public Randomizable PropertyRandomizable11 { get; set; }
        private List<Randomizable> _prFieldRandomizableListList10;
        private List<Randomizable> _prFieldRandomizableListList11;
        private List<Randomizable> _prFieldRandomizableListList12;
        private List<Randomizable> _prFieldRandomizableListList13;
        private List<Randomizable> _prFieldRandomizableListList14;

        public List<Randomizable> PrFieldRandomizableListList10 {
            get { return _prFieldRandomizableListList10; }
        }

        public List<Randomizable> PrFieldRandomizableListList11 {
            get { return _prFieldRandomizableListList11; }
        }

        public List<Randomizable> PrFieldRandomizableListList12 {
            get { return _prFieldRandomizableListList12; }
        }

        public List<Randomizable> PrFieldRandomizableListList13 {
            get { return _prFieldRandomizableListList13; }
        }

        public List<Randomizable> PrFieldRandomizableListList14 {
            get { return _prFieldRandomizableListList14; }
        }

        public List<Randomizable> _pubFieldRandomizableListList10;
        public List<Randomizable> _pubFieldRandomizableListList11;
        public List<Randomizable> _pubFieldRandomizableListList12;
        public List<Randomizable> _pubFieldRandomizableListList13;
        public List<Randomizable> _pubFieldRandomizableListList14;
        public List<Randomizable> PropertyRandomizableListList10 { get; set; }
        public List<Randomizable> PropertyRandomizableListList11 { get; set; }
        public List<Randomizable> PropertyRandomizableListList12 { get; set; }
        public List<Randomizable> PropertyRandomizableListList13 { get; set; }
        public List<Randomizable> PropertyRandomizableListList14 { get; set; }
        public List<Randomizable> PropertyRandomizableListList15 { get; set; }
        public List<Randomizable> PropertyRandomizableListList16 { get; set; }
        public List<Randomizable> PropertyRandomizableListList17 { get; set; }
        public List<Randomizable> PropertyRandomizableListList18 { get; set; }
        public List<Randomizable> PropertyRandomizableListList19 { get; set; }
        public List<Randomizable> PropertyRandomizableListList110 { get; set; }
        public List<Randomizable> PropertyRandomizableListList111 { get; set; }
        private String[] _prFieldStringStringArr0;
        private String[] _prFieldStringStringArr1;
        private String[] _prFieldStringStringArr2;
        private String[] _prFieldStringStringArr3;
        private String[] _prFieldStringStringArr4;

        public String[] PrFieldStringStringArr0 {
            get { return _prFieldStringStringArr0; }
        }

        public String[] PrFieldStringStringArr1 {
            get { return _prFieldStringStringArr1; }
        }

        public String[] PrFieldStringStringArr2 {
            get { return _prFieldStringStringArr2; }
        }

        public String[] PrFieldStringStringArr3 {
            get { return _prFieldStringStringArr3; }
        }

        public String[] PrFieldStringStringArr4 {
            get { return _prFieldStringStringArr4; }
        }

        public String[] _pubFieldStringStringArr0;
        public String[] _pubFieldStringStringArr1;
        public String[] _pubFieldStringStringArr2;
        public String[] _pubFieldStringStringArr3;
        public String[] _pubFieldStringStringArr4;
        public String[] PropertyStringStringArr0 { get; set; }
        public String[] PropertyStringStringArr1 { get; set; }
        public String[] PropertyStringStringArr2 { get; set; }
        public String[] PropertyStringStringArr3 { get; set; }
        public String[] PropertyStringStringArr4 { get; set; }
        public String[] PropertyStringStringArr5 { get; set; }
        public String[] PropertyStringStringArr6 { get; set; }
        public String[] PropertyStringStringArr7 { get; set; }
        public String[] PropertyStringStringArr8 { get; set; }
        public String[] PropertyStringStringArr9 { get; set; }
        public String[] PropertyStringStringArr10 { get; set; }
        public String[] PropertyStringStringArr11 { get; set; }
        private String _prFieldString0;
        private String _prFieldString1;
        private String _prFieldString2;
        private String _prFieldString3;
        private String _prFieldString4;

        public String PrFieldString0 {
            get { return _prFieldString0; }
        }

        public String PrFieldString1 {
            get { return _prFieldString1; }
        }

        public String PrFieldString2 {
            get { return _prFieldString2; }
        }

        public String PrFieldString3 {
            get { return _prFieldString3; }
        }

        public String PrFieldString4 {
            get { return _prFieldString4; }
        }

        public String _pubFieldString0;
        public String _pubFieldString1;
        public String _pubFieldString2;
        public String _pubFieldString3;
        public String _pubFieldString4;
        public String PropertyString0 { get; set; }
        public String PropertyString1 { get; set; }
        public String PropertyString2 { get; set; }
        public String PropertyString3 { get; set; }
        public String PropertyString4 { get; set; }
        public String PropertyString5 { get; set; }
        public String PropertyString6 { get; set; }
        public String PropertyString7 { get; set; }
        public String PropertyString8 { get; set; }
        public String PropertyString9 { get; set; }
        public String PropertyString10 { get; set; }
        public String PropertyString11 { get; set; }
        private List<String> _prFieldStringListList10;
        private List<String> _prFieldStringListList11;
        private List<String> _prFieldStringListList12;
        private List<String> _prFieldStringListList13;
        private List<String> _prFieldStringListList14;

        public List<String> PrFieldStringListList10 {
            get { return _prFieldStringListList10; }
        }

        public List<String> PrFieldStringListList11 {
            get { return _prFieldStringListList11; }
        }

        public List<String> PrFieldStringListList12 {
            get { return _prFieldStringListList12; }
        }

        public List<String> PrFieldStringListList13 {
            get { return _prFieldStringListList13; }
        }

        public List<String> PrFieldStringListList14 {
            get { return _prFieldStringListList14; }
        }

        public List<String> _pubFieldStringListList10;
        public List<String> _pubFieldStringListList11;
        public List<String> _pubFieldStringListList12;
        public List<String> _pubFieldStringListList13;
        public List<String> _pubFieldStringListList14;
        public List<String> PropertyStringListList10 { get; set; }
        public List<String> PropertyStringListList11 { get; set; }
        public List<String> PropertyStringListList12 { get; set; }
        public List<String> PropertyStringListList13 { get; set; }
        public List<String> PropertyStringListList14 { get; set; }
        public List<String> PropertyStringListList15 { get; set; }
        public List<String> PropertyStringListList16 { get; set; }
        public List<String> PropertyStringListList17 { get; set; }
        public List<String> PropertyStringListList18 { get; set; }
        public List<String> PropertyStringListList19 { get; set; }
        public List<String> PropertyStringListList110 { get; set; }
        public List<String> PropertyStringListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldByteByteArr0 = Utils.Default<Byte[]>();
            _prFieldByteByteArr1 = Utils.RandomArray<Byte>(r);
            _prFieldByteByteArr2 = Utils.Default<Byte[]>();
            _prFieldByteByteArr3 = Utils.EmptyArray<Byte>();
            _prFieldByteByteArr4 = Utils.Default<Byte[]>();
            _pubFieldByteByteArr0 = Utils.Default<Byte[]>();
            _pubFieldByteByteArr1 = Utils.RandomArray<Byte>(r);
            _pubFieldByteByteArr2 = Utils.Default<Byte[]>();
            _pubFieldByteByteArr3 = Utils.EmptyArray<Byte>();
            _pubFieldByteByteArr4 = Utils.Default<Byte[]>();
            PropertyByteByteArr0 = Utils.Default<Byte[]>();
            PropertyByteByteArr1 = Utils.RandomArray<Byte>(r);
            PropertyByteByteArr2 = Utils.Default<Byte[]>();
            PropertyByteByteArr3 = Utils.EmptyArray<Byte>();
            PropertyByteByteArr4 = Utils.Default<Byte[]>();
            PropertyByteByteArr5 = Utils.RandomArray<Byte>(r);
            PropertyByteByteArr6 = Utils.Default<Byte[]>();
            PropertyByteByteArr7 = Utils.RandomArray<Byte>(r);
            PropertyByteByteArr8 = Utils.Default<Byte[]>();
            PropertyByteByteArr9 = Utils.EmptyArray<Byte>();
            PropertyByteByteArr10 = Utils.Default<Byte[]>();
            PropertyByteByteArr11 = Utils.RandomArray<Byte>(r);
            _prFieldByte0 = Utils.Default<Byte>();
            _prFieldByte1 = Utils.RandomValue<Byte>(r);
            _prFieldByte2 = Utils.RandomValue<Byte>(r);
            _prFieldByte3 = Utils.Default<Byte>();
            _prFieldByte4 = Utils.RandomValue<Byte>(r);
            _pubFieldByte0 = Utils.Default<Byte>();
            _pubFieldByte1 = Utils.RandomValue<Byte>(r);
            _pubFieldByte2 = Utils.Default<Byte>();
            _pubFieldByte3 = Utils.RandomValue<Byte>(r);
            _pubFieldByte4 = Utils.Default<Byte>();
            PropertyByte0 = Utils.Default<Byte>();
            PropertyByte1 = Utils.RandomValue<Byte>(r);
            PropertyByte2 = Utils.RandomValue<Byte>(r);
            PropertyByte3 = Utils.Default<Byte>();
            PropertyByte4 = Utils.RandomValue<Byte>(r);
            PropertyByte5 = Utils.RandomValue<Byte>(r);
            PropertyByte6 = Utils.Default<Byte>();
            PropertyByte7 = Utils.RandomValue<Byte>(r);
            PropertyByte8 = Utils.RandomValue<Byte>(r);
            PropertyByte9 = Utils.Default<Byte>();
            PropertyByte10 = Utils.RandomValue<Byte>(r);
            PropertyByte11 = Utils.RandomValue<Byte>(r);
            _prFieldByteListList10 = Utils.Default<List<Byte>>();
            _prFieldByteListList11 = Utils.RandomList<Byte>(r);
            _prFieldByteListList12 = Utils.Default<List<Byte>>();
            _prFieldByteListList13 = Utils.EmptyList<Byte>();
            _prFieldByteListList14 = Utils.Default<List<Byte>>();
            _pubFieldByteListList10 = Utils.Default<List<Byte>>();
            _pubFieldByteListList11 = Utils.RandomList<Byte>(r);
            _pubFieldByteListList12 = Utils.Default<List<Byte>>();
            _pubFieldByteListList13 = Utils.EmptyList<Byte>();
            _pubFieldByteListList14 = Utils.Default<List<Byte>>();
            PropertyByteListList10 = Utils.Default<List<Byte>>();
            PropertyByteListList11 = Utils.RandomList<Byte>(r);
            PropertyByteListList12 = Utils.Default<List<Byte>>();
            PropertyByteListList13 = Utils.EmptyList<Byte>();
            PropertyByteListList14 = Utils.Default<List<Byte>>();
            PropertyByteListList15 = Utils.RandomList<Byte>(r);
            PropertyByteListList16 = Utils.Default<List<Byte>>();
            PropertyByteListList17 = Utils.RandomList<Byte>(r);
            PropertyByteListList18 = Utils.Default<List<Byte>>();
            PropertyByteListList19 = Utils.EmptyList<Byte>();
            PropertyByteListList110 = Utils.Default<List<Byte>>();
            PropertyByteListList111 = Utils.RandomList<Byte>(r);
            _prFieldSByteSByteArr0 = Utils.Default<SByte[]>();
            _prFieldSByteSByteArr1 = Utils.RandomArray<SByte>(r);
            _prFieldSByteSByteArr2 = Utils.Default<SByte[]>();
            _prFieldSByteSByteArr3 = Utils.EmptyArray<SByte>();
            _prFieldSByteSByteArr4 = Utils.Default<SByte[]>();
            _pubFieldSByteSByteArr0 = Utils.Default<SByte[]>();
            _pubFieldSByteSByteArr1 = Utils.RandomArray<SByte>(r);
            _pubFieldSByteSByteArr2 = Utils.Default<SByte[]>();
            _pubFieldSByteSByteArr3 = Utils.EmptyArray<SByte>();
            _pubFieldSByteSByteArr4 = Utils.Default<SByte[]>();
            PropertySByteSByteArr0 = Utils.Default<SByte[]>();
            PropertySByteSByteArr1 = Utils.RandomArray<SByte>(r);
            PropertySByteSByteArr2 = Utils.Default<SByte[]>();
            PropertySByteSByteArr3 = Utils.EmptyArray<SByte>();
            PropertySByteSByteArr4 = Utils.Default<SByte[]>();
            PropertySByteSByteArr5 = Utils.RandomArray<SByte>(r);
            PropertySByteSByteArr6 = Utils.Default<SByte[]>();
            PropertySByteSByteArr7 = Utils.RandomArray<SByte>(r);
            PropertySByteSByteArr8 = Utils.Default<SByte[]>();
            PropertySByteSByteArr9 = Utils.EmptyArray<SByte>();
            PropertySByteSByteArr10 = Utils.Default<SByte[]>();
            PropertySByteSByteArr11 = Utils.RandomArray<SByte>(r);
            _prFieldSByte0 = Utils.Default<SByte>();
            _prFieldSByte1 = Utils.RandomValue<SByte>(r);
            _prFieldSByte2 = Utils.RandomValue<SByte>(r);
            _prFieldSByte3 = Utils.Default<SByte>();
            _prFieldSByte4 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte0 = Utils.Default<SByte>();
            _pubFieldSByte1 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte2 = Utils.Default<SByte>();
            _pubFieldSByte3 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte4 = Utils.Default<SByte>();
            PropertySByte0 = Utils.Default<SByte>();
            PropertySByte1 = Utils.RandomValue<SByte>(r);
            PropertySByte2 = Utils.RandomValue<SByte>(r);
            PropertySByte3 = Utils.Default<SByte>();
            PropertySByte4 = Utils.RandomValue<SByte>(r);
            PropertySByte5 = Utils.RandomValue<SByte>(r);
            PropertySByte6 = Utils.Default<SByte>();
            PropertySByte7 = Utils.RandomValue<SByte>(r);
            PropertySByte8 = Utils.RandomValue<SByte>(r);
            PropertySByte9 = Utils.Default<SByte>();
            PropertySByte10 = Utils.RandomValue<SByte>(r);
            PropertySByte11 = Utils.RandomValue<SByte>(r);
            _prFieldSByteListList10 = Utils.Default<List<SByte>>();
            _prFieldSByteListList11 = Utils.RandomList<SByte>(r);
            _prFieldSByteListList12 = Utils.Default<List<SByte>>();
            _prFieldSByteListList13 = Utils.EmptyList<SByte>();
            _prFieldSByteListList14 = Utils.Default<List<SByte>>();
            _pubFieldSByteListList10 = Utils.Default<List<SByte>>();
            _pubFieldSByteListList11 = Utils.RandomList<SByte>(r);
            _pubFieldSByteListList12 = Utils.Default<List<SByte>>();
            _pubFieldSByteListList13 = Utils.EmptyList<SByte>();
            _pubFieldSByteListList14 = Utils.Default<List<SByte>>();
            PropertySByteListList10 = Utils.Default<List<SByte>>();
            PropertySByteListList11 = Utils.RandomList<SByte>(r);
            PropertySByteListList12 = Utils.Default<List<SByte>>();
            PropertySByteListList13 = Utils.EmptyList<SByte>();
            PropertySByteListList14 = Utils.Default<List<SByte>>();
            PropertySByteListList15 = Utils.RandomList<SByte>(r);
            PropertySByteListList16 = Utils.Default<List<SByte>>();
            PropertySByteListList17 = Utils.RandomList<SByte>(r);
            PropertySByteListList18 = Utils.Default<List<SByte>>();
            PropertySByteListList19 = Utils.EmptyList<SByte>();
            PropertySByteListList110 = Utils.Default<List<SByte>>();
            PropertySByteListList111 = Utils.RandomList<SByte>(r);
            _prFieldUInt16UInt16Arr0 = Utils.Default<UInt16[]>();
            _prFieldUInt16UInt16Arr1 = Utils.RandomArray<UInt16>(r);
            _prFieldUInt16UInt16Arr2 = Utils.Default<UInt16[]>();
            _prFieldUInt16UInt16Arr3 = Utils.EmptyArray<UInt16>();
            _prFieldUInt16UInt16Arr4 = Utils.Default<UInt16[]>();
            _pubFieldUInt16UInt16Arr0 = Utils.Default<UInt16[]>();
            _pubFieldUInt16UInt16Arr1 = Utils.RandomArray<UInt16>(r);
            _pubFieldUInt16UInt16Arr2 = Utils.Default<UInt16[]>();
            _pubFieldUInt16UInt16Arr3 = Utils.EmptyArray<UInt16>();
            _pubFieldUInt16UInt16Arr4 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr0 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr1 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16UInt16Arr2 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr3 = Utils.EmptyArray<UInt16>();
            PropertyUInt16UInt16Arr4 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr5 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16UInt16Arr6 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr7 = Utils.RandomArray<UInt16>(r);
            PropertyUInt16UInt16Arr8 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr9 = Utils.EmptyArray<UInt16>();
            PropertyUInt16UInt16Arr10 = Utils.Default<UInt16[]>();
            PropertyUInt16UInt16Arr11 = Utils.RandomArray<UInt16>(r);
            _prFieldUInt160 = Utils.Default<UInt16>();
            _prFieldUInt161 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt162 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt163 = Utils.Default<UInt16>();
            _prFieldUInt164 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt160 = Utils.Default<UInt16>();
            _pubFieldUInt161 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt162 = Utils.Default<UInt16>();
            _pubFieldUInt163 = Utils.RandomValue<UInt16>(r);
            _pubFieldUInt164 = Utils.Default<UInt16>();
            PropertyUInt160 = Utils.Default<UInt16>();
            PropertyUInt161 = Utils.RandomValue<UInt16>(r);
            PropertyUInt162 = Utils.RandomValue<UInt16>(r);
            PropertyUInt163 = Utils.Default<UInt16>();
            PropertyUInt164 = Utils.RandomValue<UInt16>(r);
            PropertyUInt165 = Utils.RandomValue<UInt16>(r);
            PropertyUInt166 = Utils.Default<UInt16>();
            PropertyUInt167 = Utils.RandomValue<UInt16>(r);
            PropertyUInt168 = Utils.RandomValue<UInt16>(r);
            PropertyUInt169 = Utils.Default<UInt16>();
            PropertyUInt1610 = Utils.RandomValue<UInt16>(r);
            PropertyUInt1611 = Utils.RandomValue<UInt16>(r);
            _prFieldUInt16ListList10 = Utils.Default<List<UInt16>>();
            _prFieldUInt16ListList11 = Utils.RandomList<UInt16>(r);
            _prFieldUInt16ListList12 = Utils.Default<List<UInt16>>();
            _prFieldUInt16ListList13 = Utils.EmptyList<UInt16>();
            _prFieldUInt16ListList14 = Utils.Default<List<UInt16>>();
            _pubFieldUInt16ListList10 = Utils.Default<List<UInt16>>();
            _pubFieldUInt16ListList11 = Utils.RandomList<UInt16>(r);
            _pubFieldUInt16ListList12 = Utils.Default<List<UInt16>>();
            _pubFieldUInt16ListList13 = Utils.EmptyList<UInt16>();
            _pubFieldUInt16ListList14 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList10 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList11 = Utils.RandomList<UInt16>(r);
            PropertyUInt16ListList12 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList13 = Utils.EmptyList<UInt16>();
            PropertyUInt16ListList14 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList15 = Utils.RandomList<UInt16>(r);
            PropertyUInt16ListList16 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList17 = Utils.RandomList<UInt16>(r);
            PropertyUInt16ListList18 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList19 = Utils.EmptyList<UInt16>();
            PropertyUInt16ListList110 = Utils.Default<List<UInt16>>();
            PropertyUInt16ListList111 = Utils.RandomList<UInt16>(r);
            _prFieldInt16Int16Arr0 = Utils.Default<Int16[]>();
            _prFieldInt16Int16Arr1 = Utils.RandomArray<Int16>(r);
            _prFieldInt16Int16Arr2 = Utils.Default<Int16[]>();
            _prFieldInt16Int16Arr3 = Utils.EmptyArray<Int16>();
            _prFieldInt16Int16Arr4 = Utils.Default<Int16[]>();
            _pubFieldInt16Int16Arr0 = Utils.Default<Int16[]>();
            _pubFieldInt16Int16Arr1 = Utils.RandomArray<Int16>(r);
            _pubFieldInt16Int16Arr2 = Utils.Default<Int16[]>();
            _pubFieldInt16Int16Arr3 = Utils.EmptyArray<Int16>();
            _pubFieldInt16Int16Arr4 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr0 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr1 = Utils.RandomArray<Int16>(r);
            PropertyInt16Int16Arr2 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr3 = Utils.EmptyArray<Int16>();
            PropertyInt16Int16Arr4 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr5 = Utils.RandomArray<Int16>(r);
            PropertyInt16Int16Arr6 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr7 = Utils.RandomArray<Int16>(r);
            PropertyInt16Int16Arr8 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr9 = Utils.EmptyArray<Int16>();
            PropertyInt16Int16Arr10 = Utils.Default<Int16[]>();
            PropertyInt16Int16Arr11 = Utils.RandomArray<Int16>(r);
            _prFieldInt160 = Utils.Default<Int16>();
            _prFieldInt161 = Utils.RandomValue<Int16>(r);
            _prFieldInt162 = Utils.RandomValue<Int16>(r);
            _prFieldInt163 = Utils.Default<Int16>();
            _prFieldInt164 = Utils.RandomValue<Int16>(r);
            _pubFieldInt160 = Utils.Default<Int16>();
            _pubFieldInt161 = Utils.RandomValue<Int16>(r);
            _pubFieldInt162 = Utils.Default<Int16>();
            _pubFieldInt163 = Utils.RandomValue<Int16>(r);
            _pubFieldInt164 = Utils.Default<Int16>();
            PropertyInt160 = Utils.Default<Int16>();
            PropertyInt161 = Utils.RandomValue<Int16>(r);
            PropertyInt162 = Utils.RandomValue<Int16>(r);
            PropertyInt163 = Utils.Default<Int16>();
            PropertyInt164 = Utils.RandomValue<Int16>(r);
            PropertyInt165 = Utils.RandomValue<Int16>(r);
            PropertyInt166 = Utils.Default<Int16>();
            PropertyInt167 = Utils.RandomValue<Int16>(r);
            PropertyInt168 = Utils.RandomValue<Int16>(r);
            PropertyInt169 = Utils.Default<Int16>();
            PropertyInt1610 = Utils.RandomValue<Int16>(r);
            PropertyInt1611 = Utils.RandomValue<Int16>(r);
            _prFieldInt16ListList10 = Utils.Default<List<Int16>>();
            _prFieldInt16ListList11 = Utils.RandomList<Int16>(r);
            _prFieldInt16ListList12 = Utils.Default<List<Int16>>();
            _prFieldInt16ListList13 = Utils.EmptyList<Int16>();
            _prFieldInt16ListList14 = Utils.Default<List<Int16>>();
            _pubFieldInt16ListList10 = Utils.Default<List<Int16>>();
            _pubFieldInt16ListList11 = Utils.RandomList<Int16>(r);
            _pubFieldInt16ListList12 = Utils.Default<List<Int16>>();
            _pubFieldInt16ListList13 = Utils.EmptyList<Int16>();
            _pubFieldInt16ListList14 = Utils.Default<List<Int16>>();
            PropertyInt16ListList10 = Utils.Default<List<Int16>>();
            PropertyInt16ListList11 = Utils.RandomList<Int16>(r);
            PropertyInt16ListList12 = Utils.Default<List<Int16>>();
            PropertyInt16ListList13 = Utils.EmptyList<Int16>();
            PropertyInt16ListList14 = Utils.Default<List<Int16>>();
            PropertyInt16ListList15 = Utils.RandomList<Int16>(r);
            PropertyInt16ListList16 = Utils.Default<List<Int16>>();
            PropertyInt16ListList17 = Utils.RandomList<Int16>(r);
            PropertyInt16ListList18 = Utils.Default<List<Int16>>();
            PropertyInt16ListList19 = Utils.EmptyList<Int16>();
            PropertyInt16ListList110 = Utils.Default<List<Int16>>();
            PropertyInt16ListList111 = Utils.RandomList<Int16>(r);
            _prFieldUInt32UInt32Arr0 = Utils.Default<UInt32[]>();
            _prFieldUInt32UInt32Arr1 = Utils.RandomArray<UInt32>(r);
            _prFieldUInt32UInt32Arr2 = Utils.Default<UInt32[]>();
            _prFieldUInt32UInt32Arr3 = Utils.EmptyArray<UInt32>();
            _prFieldUInt32UInt32Arr4 = Utils.Default<UInt32[]>();
            _pubFieldUInt32UInt32Arr0 = Utils.Default<UInt32[]>();
            _pubFieldUInt32UInt32Arr1 = Utils.RandomArray<UInt32>(r);
            _pubFieldUInt32UInt32Arr2 = Utils.Default<UInt32[]>();
            _pubFieldUInt32UInt32Arr3 = Utils.EmptyArray<UInt32>();
            _pubFieldUInt32UInt32Arr4 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr0 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr1 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32UInt32Arr2 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr3 = Utils.EmptyArray<UInt32>();
            PropertyUInt32UInt32Arr4 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr5 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32UInt32Arr6 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr7 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32UInt32Arr8 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr9 = Utils.EmptyArray<UInt32>();
            PropertyUInt32UInt32Arr10 = Utils.Default<UInt32[]>();
            PropertyUInt32UInt32Arr11 = Utils.RandomArray<UInt32>(r);
            _prFieldUInt320 = Utils.Default<UInt32>();
            _prFieldUInt321 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt322 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt323 = Utils.Default<UInt32>();
            _prFieldUInt324 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt320 = Utils.Default<UInt32>();
            _pubFieldUInt321 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt322 = Utils.Default<UInt32>();
            _pubFieldUInt323 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt324 = Utils.Default<UInt32>();
            PropertyUInt320 = Utils.Default<UInt32>();
            PropertyUInt321 = Utils.RandomValue<UInt32>(r);
            PropertyUInt322 = Utils.RandomValue<UInt32>(r);
            PropertyUInt323 = Utils.Default<UInt32>();
            PropertyUInt324 = Utils.RandomValue<UInt32>(r);
            PropertyUInt325 = Utils.RandomValue<UInt32>(r);
            PropertyUInt326 = Utils.Default<UInt32>();
            PropertyUInt327 = Utils.RandomValue<UInt32>(r);
            PropertyUInt328 = Utils.RandomValue<UInt32>(r);
            PropertyUInt329 = Utils.Default<UInt32>();
            PropertyUInt3210 = Utils.RandomValue<UInt32>(r);
            PropertyUInt3211 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt32ListList10 = Utils.Default<List<UInt32>>();
            _prFieldUInt32ListList11 = Utils.RandomList<UInt32>(r);
            _prFieldUInt32ListList12 = Utils.Default<List<UInt32>>();
            _prFieldUInt32ListList13 = Utils.EmptyList<UInt32>();
            _prFieldUInt32ListList14 = Utils.Default<List<UInt32>>();
            _pubFieldUInt32ListList10 = Utils.Default<List<UInt32>>();
            _pubFieldUInt32ListList11 = Utils.RandomList<UInt32>(r);
            _pubFieldUInt32ListList12 = Utils.Default<List<UInt32>>();
            _pubFieldUInt32ListList13 = Utils.EmptyList<UInt32>();
            _pubFieldUInt32ListList14 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList10 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList11 = Utils.RandomList<UInt32>(r);
            PropertyUInt32ListList12 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList13 = Utils.EmptyList<UInt32>();
            PropertyUInt32ListList14 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList15 = Utils.RandomList<UInt32>(r);
            PropertyUInt32ListList16 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList17 = Utils.RandomList<UInt32>(r);
            PropertyUInt32ListList18 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList19 = Utils.EmptyList<UInt32>();
            PropertyUInt32ListList110 = Utils.Default<List<UInt32>>();
            PropertyUInt32ListList111 = Utils.RandomList<UInt32>(r);
            _prFieldInt32Int32Arr0 = Utils.Default<Int32[]>();
            _prFieldInt32Int32Arr1 = Utils.RandomArray<Int32>(r);
            _prFieldInt32Int32Arr2 = Utils.Default<Int32[]>();
            _prFieldInt32Int32Arr3 = Utils.EmptyArray<Int32>();
            _prFieldInt32Int32Arr4 = Utils.Default<Int32[]>();
            _pubFieldInt32Int32Arr0 = Utils.Default<Int32[]>();
            _pubFieldInt32Int32Arr1 = Utils.RandomArray<Int32>(r);
            _pubFieldInt32Int32Arr2 = Utils.Default<Int32[]>();
            _pubFieldInt32Int32Arr3 = Utils.EmptyArray<Int32>();
            _pubFieldInt32Int32Arr4 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr0 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr1 = Utils.RandomArray<Int32>(r);
            PropertyInt32Int32Arr2 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr3 = Utils.EmptyArray<Int32>();
            PropertyInt32Int32Arr4 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr5 = Utils.RandomArray<Int32>(r);
            PropertyInt32Int32Arr6 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr7 = Utils.RandomArray<Int32>(r);
            PropertyInt32Int32Arr8 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr9 = Utils.EmptyArray<Int32>();
            PropertyInt32Int32Arr10 = Utils.Default<Int32[]>();
            PropertyInt32Int32Arr11 = Utils.RandomArray<Int32>(r);
            _prFieldInt320 = Utils.Default<Int32>();
            _prFieldInt321 = Utils.RandomValue<Int32>(r);
            _prFieldInt322 = Utils.RandomValue<Int32>(r);
            _prFieldInt323 = Utils.Default<Int32>();
            _prFieldInt324 = Utils.RandomValue<Int32>(r);
            _pubFieldInt320 = Utils.Default<Int32>();
            _pubFieldInt321 = Utils.RandomValue<Int32>(r);
            _pubFieldInt322 = Utils.Default<Int32>();
            _pubFieldInt323 = Utils.RandomValue<Int32>(r);
            _pubFieldInt324 = Utils.Default<Int32>();
            PropertyInt320 = Utils.Default<Int32>();
            PropertyInt321 = Utils.RandomValue<Int32>(r);
            PropertyInt322 = Utils.RandomValue<Int32>(r);
            PropertyInt323 = Utils.Default<Int32>();
            PropertyInt324 = Utils.RandomValue<Int32>(r);
            PropertyInt325 = Utils.RandomValue<Int32>(r);
            PropertyInt326 = Utils.Default<Int32>();
            PropertyInt327 = Utils.RandomValue<Int32>(r);
            PropertyInt328 = Utils.RandomValue<Int32>(r);
            PropertyInt329 = Utils.Default<Int32>();
            PropertyInt3210 = Utils.RandomValue<Int32>(r);
            PropertyInt3211 = Utils.RandomValue<Int32>(r);
            _prFieldInt32ListList10 = Utils.Default<List<Int32>>();
            _prFieldInt32ListList11 = Utils.RandomList<Int32>(r);
            _prFieldInt32ListList12 = Utils.Default<List<Int32>>();
            _prFieldInt32ListList13 = Utils.EmptyList<Int32>();
            _prFieldInt32ListList14 = Utils.Default<List<Int32>>();
            _pubFieldInt32ListList10 = Utils.Default<List<Int32>>();
            _pubFieldInt32ListList11 = Utils.RandomList<Int32>(r);
            _pubFieldInt32ListList12 = Utils.Default<List<Int32>>();
            _pubFieldInt32ListList13 = Utils.EmptyList<Int32>();
            _pubFieldInt32ListList14 = Utils.Default<List<Int32>>();
            PropertyInt32ListList10 = Utils.Default<List<Int32>>();
            PropertyInt32ListList11 = Utils.RandomList<Int32>(r);
            PropertyInt32ListList12 = Utils.Default<List<Int32>>();
            PropertyInt32ListList13 = Utils.EmptyList<Int32>();
            PropertyInt32ListList14 = Utils.Default<List<Int32>>();
            PropertyInt32ListList15 = Utils.RandomList<Int32>(r);
            PropertyInt32ListList16 = Utils.Default<List<Int32>>();
            PropertyInt32ListList17 = Utils.RandomList<Int32>(r);
            PropertyInt32ListList18 = Utils.Default<List<Int32>>();
            PropertyInt32ListList19 = Utils.EmptyList<Int32>();
            PropertyInt32ListList110 = Utils.Default<List<Int32>>();
            PropertyInt32ListList111 = Utils.RandomList<Int32>(r);
            _prFieldUInt64UInt64Arr0 = Utils.Default<UInt64[]>();
            _prFieldUInt64UInt64Arr1 = Utils.RandomArray<UInt64>(r);
            _prFieldUInt64UInt64Arr2 = Utils.Default<UInt64[]>();
            _prFieldUInt64UInt64Arr3 = Utils.EmptyArray<UInt64>();
            _prFieldUInt64UInt64Arr4 = Utils.Default<UInt64[]>();
            _pubFieldUInt64UInt64Arr0 = Utils.Default<UInt64[]>();
            _pubFieldUInt64UInt64Arr1 = Utils.RandomArray<UInt64>(r);
            _pubFieldUInt64UInt64Arr2 = Utils.Default<UInt64[]>();
            _pubFieldUInt64UInt64Arr3 = Utils.EmptyArray<UInt64>();
            _pubFieldUInt64UInt64Arr4 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr0 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr1 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64UInt64Arr2 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr3 = Utils.EmptyArray<UInt64>();
            PropertyUInt64UInt64Arr4 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr5 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64UInt64Arr6 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr7 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64UInt64Arr8 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr9 = Utils.EmptyArray<UInt64>();
            PropertyUInt64UInt64Arr10 = Utils.Default<UInt64[]>();
            PropertyUInt64UInt64Arr11 = Utils.RandomArray<UInt64>(r);
            _prFieldUInt640 = Utils.Default<UInt64>();
            _prFieldUInt641 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt642 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt643 = Utils.Default<UInt64>();
            _prFieldUInt644 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt640 = Utils.Default<UInt64>();
            _pubFieldUInt641 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt642 = Utils.Default<UInt64>();
            _pubFieldUInt643 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt644 = Utils.Default<UInt64>();
            PropertyUInt640 = Utils.Default<UInt64>();
            PropertyUInt641 = Utils.RandomValue<UInt64>(r);
            PropertyUInt642 = Utils.RandomValue<UInt64>(r);
            PropertyUInt643 = Utils.Default<UInt64>();
            PropertyUInt644 = Utils.RandomValue<UInt64>(r);
            PropertyUInt645 = Utils.RandomValue<UInt64>(r);
            PropertyUInt646 = Utils.Default<UInt64>();
            PropertyUInt647 = Utils.RandomValue<UInt64>(r);
            PropertyUInt648 = Utils.RandomValue<UInt64>(r);
            PropertyUInt649 = Utils.Default<UInt64>();
            PropertyUInt6410 = Utils.RandomValue<UInt64>(r);
            PropertyUInt6411 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt64ListList10 = Utils.Default<List<UInt64>>();
            _prFieldUInt64ListList11 = Utils.RandomList<UInt64>(r);
            _prFieldUInt64ListList12 = Utils.Default<List<UInt64>>();
            _prFieldUInt64ListList13 = Utils.EmptyList<UInt64>();
            _prFieldUInt64ListList14 = Utils.Default<List<UInt64>>();
            _pubFieldUInt64ListList10 = Utils.Default<List<UInt64>>();
            _pubFieldUInt64ListList11 = Utils.RandomList<UInt64>(r);
            _pubFieldUInt64ListList12 = Utils.Default<List<UInt64>>();
            _pubFieldUInt64ListList13 = Utils.EmptyList<UInt64>();
            _pubFieldUInt64ListList14 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList10 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList11 = Utils.RandomList<UInt64>(r);
            PropertyUInt64ListList12 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList13 = Utils.EmptyList<UInt64>();
            PropertyUInt64ListList14 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList15 = Utils.RandomList<UInt64>(r);
            PropertyUInt64ListList16 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList17 = Utils.RandomList<UInt64>(r);
            PropertyUInt64ListList18 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList19 = Utils.EmptyList<UInt64>();
            PropertyUInt64ListList110 = Utils.Default<List<UInt64>>();
            PropertyUInt64ListList111 = Utils.RandomList<UInt64>(r);
            _prFieldInt64Int64Arr0 = Utils.Default<Int64[]>();
            _prFieldInt64Int64Arr1 = Utils.RandomArray<Int64>(r);
            _prFieldInt64Int64Arr2 = Utils.Default<Int64[]>();
            _prFieldInt64Int64Arr3 = Utils.EmptyArray<Int64>();
            _prFieldInt64Int64Arr4 = Utils.Default<Int64[]>();
            _pubFieldInt64Int64Arr0 = Utils.Default<Int64[]>();
            _pubFieldInt64Int64Arr1 = Utils.RandomArray<Int64>(r);
            _pubFieldInt64Int64Arr2 = Utils.Default<Int64[]>();
            _pubFieldInt64Int64Arr3 = Utils.EmptyArray<Int64>();
            _pubFieldInt64Int64Arr4 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr0 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr1 = Utils.RandomArray<Int64>(r);
            PropertyInt64Int64Arr2 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr3 = Utils.EmptyArray<Int64>();
            PropertyInt64Int64Arr4 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr5 = Utils.RandomArray<Int64>(r);
            PropertyInt64Int64Arr6 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr7 = Utils.RandomArray<Int64>(r);
            PropertyInt64Int64Arr8 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr9 = Utils.EmptyArray<Int64>();
            PropertyInt64Int64Arr10 = Utils.Default<Int64[]>();
            PropertyInt64Int64Arr11 = Utils.RandomArray<Int64>(r);
            _prFieldInt640 = Utils.Default<Int64>();
            _prFieldInt641 = Utils.RandomValue<Int64>(r);
            _prFieldInt642 = Utils.RandomValue<Int64>(r);
            _prFieldInt643 = Utils.Default<Int64>();
            _prFieldInt644 = Utils.RandomValue<Int64>(r);
            _pubFieldInt640 = Utils.Default<Int64>();
            _pubFieldInt641 = Utils.RandomValue<Int64>(r);
            _pubFieldInt642 = Utils.Default<Int64>();
            _pubFieldInt643 = Utils.RandomValue<Int64>(r);
            _pubFieldInt644 = Utils.Default<Int64>();
            PropertyInt640 = Utils.Default<Int64>();
            PropertyInt641 = Utils.RandomValue<Int64>(r);
            PropertyInt642 = Utils.RandomValue<Int64>(r);
            PropertyInt643 = Utils.Default<Int64>();
            PropertyInt644 = Utils.RandomValue<Int64>(r);
            PropertyInt645 = Utils.RandomValue<Int64>(r);
            PropertyInt646 = Utils.Default<Int64>();
            PropertyInt647 = Utils.RandomValue<Int64>(r);
            PropertyInt648 = Utils.RandomValue<Int64>(r);
            PropertyInt649 = Utils.Default<Int64>();
            PropertyInt6410 = Utils.RandomValue<Int64>(r);
            PropertyInt6411 = Utils.RandomValue<Int64>(r);
            _prFieldInt64ListList10 = Utils.Default<List<Int64>>();
            _prFieldInt64ListList11 = Utils.RandomList<Int64>(r);
            _prFieldInt64ListList12 = Utils.Default<List<Int64>>();
            _prFieldInt64ListList13 = Utils.EmptyList<Int64>();
            _prFieldInt64ListList14 = Utils.Default<List<Int64>>();
            _pubFieldInt64ListList10 = Utils.Default<List<Int64>>();
            _pubFieldInt64ListList11 = Utils.RandomList<Int64>(r);
            _pubFieldInt64ListList12 = Utils.Default<List<Int64>>();
            _pubFieldInt64ListList13 = Utils.EmptyList<Int64>();
            _pubFieldInt64ListList14 = Utils.Default<List<Int64>>();
            PropertyInt64ListList10 = Utils.Default<List<Int64>>();
            PropertyInt64ListList11 = Utils.RandomList<Int64>(r);
            PropertyInt64ListList12 = Utils.Default<List<Int64>>();
            PropertyInt64ListList13 = Utils.EmptyList<Int64>();
            PropertyInt64ListList14 = Utils.Default<List<Int64>>();
            PropertyInt64ListList15 = Utils.RandomList<Int64>(r);
            PropertyInt64ListList16 = Utils.Default<List<Int64>>();
            PropertyInt64ListList17 = Utils.RandomList<Int64>(r);
            PropertyInt64ListList18 = Utils.Default<List<Int64>>();
            PropertyInt64ListList19 = Utils.EmptyList<Int64>();
            PropertyInt64ListList110 = Utils.Default<List<Int64>>();
            PropertyInt64ListList111 = Utils.RandomList<Int64>(r);
            _prFieldSingleSingleArr0 = Utils.Default<Single[]>();
            _prFieldSingleSingleArr1 = Utils.RandomArray<Single>(r);
            _prFieldSingleSingleArr2 = Utils.Default<Single[]>();
            _prFieldSingleSingleArr3 = Utils.EmptyArray<Single>();
            _prFieldSingleSingleArr4 = Utils.Default<Single[]>();
            _pubFieldSingleSingleArr0 = Utils.Default<Single[]>();
            _pubFieldSingleSingleArr1 = Utils.RandomArray<Single>(r);
            _pubFieldSingleSingleArr2 = Utils.Default<Single[]>();
            _pubFieldSingleSingleArr3 = Utils.EmptyArray<Single>();
            _pubFieldSingleSingleArr4 = Utils.Default<Single[]>();
            PropertySingleSingleArr0 = Utils.Default<Single[]>();
            PropertySingleSingleArr1 = Utils.RandomArray<Single>(r);
            PropertySingleSingleArr2 = Utils.Default<Single[]>();
            PropertySingleSingleArr3 = Utils.EmptyArray<Single>();
            PropertySingleSingleArr4 = Utils.Default<Single[]>();
            PropertySingleSingleArr5 = Utils.RandomArray<Single>(r);
            PropertySingleSingleArr6 = Utils.Default<Single[]>();
            PropertySingleSingleArr7 = Utils.RandomArray<Single>(r);
            PropertySingleSingleArr8 = Utils.Default<Single[]>();
            PropertySingleSingleArr9 = Utils.EmptyArray<Single>();
            PropertySingleSingleArr10 = Utils.Default<Single[]>();
            PropertySingleSingleArr11 = Utils.RandomArray<Single>(r);
            _prFieldSingle0 = Utils.Default<Single>();
            _prFieldSingle1 = Utils.RandomValue<Single>(r);
            _prFieldSingle2 = Utils.RandomValue<Single>(r);
            _prFieldSingle3 = Utils.Default<Single>();
            _prFieldSingle4 = Utils.RandomValue<Single>(r);
            _pubFieldSingle0 = Utils.Default<Single>();
            _pubFieldSingle1 = Utils.RandomValue<Single>(r);
            _pubFieldSingle2 = Utils.Default<Single>();
            _pubFieldSingle3 = Utils.RandomValue<Single>(r);
            _pubFieldSingle4 = Utils.Default<Single>();
            PropertySingle0 = Utils.Default<Single>();
            PropertySingle1 = Utils.RandomValue<Single>(r);
            PropertySingle2 = Utils.RandomValue<Single>(r);
            PropertySingle3 = Utils.Default<Single>();
            PropertySingle4 = Utils.RandomValue<Single>(r);
            PropertySingle5 = Utils.RandomValue<Single>(r);
            PropertySingle6 = Utils.Default<Single>();
            PropertySingle7 = Utils.RandomValue<Single>(r);
            PropertySingle8 = Utils.RandomValue<Single>(r);
            PropertySingle9 = Utils.Default<Single>();
            PropertySingle10 = Utils.RandomValue<Single>(r);
            PropertySingle11 = Utils.RandomValue<Single>(r);
            _prFieldSingleListList10 = Utils.Default<List<Single>>();
            _prFieldSingleListList11 = Utils.RandomList<Single>(r);
            _prFieldSingleListList12 = Utils.Default<List<Single>>();
            _prFieldSingleListList13 = Utils.EmptyList<Single>();
            _prFieldSingleListList14 = Utils.Default<List<Single>>();
            _pubFieldSingleListList10 = Utils.Default<List<Single>>();
            _pubFieldSingleListList11 = Utils.RandomList<Single>(r);
            _pubFieldSingleListList12 = Utils.Default<List<Single>>();
            _pubFieldSingleListList13 = Utils.EmptyList<Single>();
            _pubFieldSingleListList14 = Utils.Default<List<Single>>();
            PropertySingleListList10 = Utils.Default<List<Single>>();
            PropertySingleListList11 = Utils.RandomList<Single>(r);
            PropertySingleListList12 = Utils.Default<List<Single>>();
            PropertySingleListList13 = Utils.EmptyList<Single>();
            PropertySingleListList14 = Utils.Default<List<Single>>();
            PropertySingleListList15 = Utils.RandomList<Single>(r);
            PropertySingleListList16 = Utils.Default<List<Single>>();
            PropertySingleListList17 = Utils.RandomList<Single>(r);
            PropertySingleListList18 = Utils.Default<List<Single>>();
            PropertySingleListList19 = Utils.EmptyList<Single>();
            PropertySingleListList110 = Utils.Default<List<Single>>();
            PropertySingleListList111 = Utils.RandomList<Single>(r);
            _prFieldDoubleDoubleArr0 = Utils.Default<Double[]>();
            _prFieldDoubleDoubleArr1 = Utils.RandomArray<Double>(r);
            _prFieldDoubleDoubleArr2 = Utils.Default<Double[]>();
            _prFieldDoubleDoubleArr3 = Utils.EmptyArray<Double>();
            _prFieldDoubleDoubleArr4 = Utils.Default<Double[]>();
            _pubFieldDoubleDoubleArr0 = Utils.Default<Double[]>();
            _pubFieldDoubleDoubleArr1 = Utils.RandomArray<Double>(r);
            _pubFieldDoubleDoubleArr2 = Utils.Default<Double[]>();
            _pubFieldDoubleDoubleArr3 = Utils.EmptyArray<Double>();
            _pubFieldDoubleDoubleArr4 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr0 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr1 = Utils.RandomArray<Double>(r);
            PropertyDoubleDoubleArr2 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr3 = Utils.EmptyArray<Double>();
            PropertyDoubleDoubleArr4 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr5 = Utils.RandomArray<Double>(r);
            PropertyDoubleDoubleArr6 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr7 = Utils.RandomArray<Double>(r);
            PropertyDoubleDoubleArr8 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr9 = Utils.EmptyArray<Double>();
            PropertyDoubleDoubleArr10 = Utils.Default<Double[]>();
            PropertyDoubleDoubleArr11 = Utils.RandomArray<Double>(r);
            _prFieldDouble0 = Utils.Default<Double>();
            _prFieldDouble1 = Utils.RandomValue<Double>(r);
            _prFieldDouble2 = Utils.RandomValue<Double>(r);
            _prFieldDouble3 = Utils.Default<Double>();
            _prFieldDouble4 = Utils.RandomValue<Double>(r);
            _pubFieldDouble0 = Utils.Default<Double>();
            _pubFieldDouble1 = Utils.RandomValue<Double>(r);
            _pubFieldDouble2 = Utils.Default<Double>();
            _pubFieldDouble3 = Utils.RandomValue<Double>(r);
            _pubFieldDouble4 = Utils.Default<Double>();
            PropertyDouble0 = Utils.Default<Double>();
            PropertyDouble1 = Utils.RandomValue<Double>(r);
            PropertyDouble2 = Utils.RandomValue<Double>(r);
            PropertyDouble3 = Utils.Default<Double>();
            PropertyDouble4 = Utils.RandomValue<Double>(r);
            PropertyDouble5 = Utils.RandomValue<Double>(r);
            PropertyDouble6 = Utils.Default<Double>();
            PropertyDouble7 = Utils.RandomValue<Double>(r);
            PropertyDouble8 = Utils.RandomValue<Double>(r);
            PropertyDouble9 = Utils.Default<Double>();
            PropertyDouble10 = Utils.RandomValue<Double>(r);
            PropertyDouble11 = Utils.RandomValue<Double>(r);
            _prFieldDoubleListList10 = Utils.Default<List<Double>>();
            _prFieldDoubleListList11 = Utils.RandomList<Double>(r);
            _prFieldDoubleListList12 = Utils.Default<List<Double>>();
            _prFieldDoubleListList13 = Utils.EmptyList<Double>();
            _prFieldDoubleListList14 = Utils.Default<List<Double>>();
            _pubFieldDoubleListList10 = Utils.Default<List<Double>>();
            _pubFieldDoubleListList11 = Utils.RandomList<Double>(r);
            _pubFieldDoubleListList12 = Utils.Default<List<Double>>();
            _pubFieldDoubleListList13 = Utils.EmptyList<Double>();
            _pubFieldDoubleListList14 = Utils.Default<List<Double>>();
            PropertyDoubleListList10 = Utils.Default<List<Double>>();
            PropertyDoubleListList11 = Utils.RandomList<Double>(r);
            PropertyDoubleListList12 = Utils.Default<List<Double>>();
            PropertyDoubleListList13 = Utils.EmptyList<Double>();
            PropertyDoubleListList14 = Utils.Default<List<Double>>();
            PropertyDoubleListList15 = Utils.RandomList<Double>(r);
            PropertyDoubleListList16 = Utils.Default<List<Double>>();
            PropertyDoubleListList17 = Utils.RandomList<Double>(r);
            PropertyDoubleListList18 = Utils.Default<List<Double>>();
            PropertyDoubleListList19 = Utils.EmptyList<Double>();
            PropertyDoubleListList110 = Utils.Default<List<Double>>();
            PropertyDoubleListList111 = Utils.RandomList<Double>(r);
            _prFieldBooleanBooleanArr0 = Utils.Default<Boolean[]>();
            _prFieldBooleanBooleanArr1 = Utils.RandomArray<Boolean>(r);
            _prFieldBooleanBooleanArr2 = Utils.Default<Boolean[]>();
            _prFieldBooleanBooleanArr3 = Utils.EmptyArray<Boolean>();
            _prFieldBooleanBooleanArr4 = Utils.Default<Boolean[]>();
            _pubFieldBooleanBooleanArr0 = Utils.Default<Boolean[]>();
            _pubFieldBooleanBooleanArr1 = Utils.RandomArray<Boolean>(r);
            _pubFieldBooleanBooleanArr2 = Utils.Default<Boolean[]>();
            _pubFieldBooleanBooleanArr3 = Utils.EmptyArray<Boolean>();
            _pubFieldBooleanBooleanArr4 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr0 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr1 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanBooleanArr2 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr3 = Utils.EmptyArray<Boolean>();
            PropertyBooleanBooleanArr4 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr5 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanBooleanArr6 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr7 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanBooleanArr8 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr9 = Utils.EmptyArray<Boolean>();
            PropertyBooleanBooleanArr10 = Utils.Default<Boolean[]>();
            PropertyBooleanBooleanArr11 = Utils.RandomArray<Boolean>(r);
            _prFieldBoolean0 = Utils.Default<Boolean>();
            _prFieldBoolean1 = Utils.RandomValue<Boolean>(r);
            _prFieldBoolean2 = Utils.RandomValue<Boolean>(r);
            _prFieldBoolean3 = Utils.Default<Boolean>();
            _prFieldBoolean4 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean0 = Utils.Default<Boolean>();
            _pubFieldBoolean1 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean2 = Utils.Default<Boolean>();
            _pubFieldBoolean3 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean4 = Utils.Default<Boolean>();
            PropertyBoolean0 = Utils.Default<Boolean>();
            PropertyBoolean1 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean2 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean3 = Utils.Default<Boolean>();
            PropertyBoolean4 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean5 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean6 = Utils.Default<Boolean>();
            PropertyBoolean7 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean8 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean9 = Utils.Default<Boolean>();
            PropertyBoolean10 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean11 = Utils.RandomValue<Boolean>(r);
            _prFieldBooleanListList10 = Utils.Default<List<Boolean>>();
            _prFieldBooleanListList11 = Utils.RandomList<Boolean>(r);
            _prFieldBooleanListList12 = Utils.Default<List<Boolean>>();
            _prFieldBooleanListList13 = Utils.EmptyList<Boolean>();
            _prFieldBooleanListList14 = Utils.Default<List<Boolean>>();
            _pubFieldBooleanListList10 = Utils.Default<List<Boolean>>();
            _pubFieldBooleanListList11 = Utils.RandomList<Boolean>(r);
            _pubFieldBooleanListList12 = Utils.Default<List<Boolean>>();
            _pubFieldBooleanListList13 = Utils.EmptyList<Boolean>();
            _pubFieldBooleanListList14 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList10 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList11 = Utils.RandomList<Boolean>(r);
            PropertyBooleanListList12 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList13 = Utils.EmptyList<Boolean>();
            PropertyBooleanListList14 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList15 = Utils.RandomList<Boolean>(r);
            PropertyBooleanListList16 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList17 = Utils.RandomList<Boolean>(r);
            PropertyBooleanListList18 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList19 = Utils.EmptyList<Boolean>();
            PropertyBooleanListList110 = Utils.Default<List<Boolean>>();
            PropertyBooleanListList111 = Utils.RandomList<Boolean>(r);
            _prFieldCharCharArr0 = Utils.Default<Char[]>();
            _prFieldCharCharArr1 = Utils.RandomArray<Char>(r);
            _prFieldCharCharArr2 = Utils.Default<Char[]>();
            _prFieldCharCharArr3 = Utils.EmptyArray<Char>();
            _prFieldCharCharArr4 = Utils.Default<Char[]>();
            _pubFieldCharCharArr0 = Utils.Default<Char[]>();
            _pubFieldCharCharArr1 = Utils.RandomArray<Char>(r);
            _pubFieldCharCharArr2 = Utils.Default<Char[]>();
            _pubFieldCharCharArr3 = Utils.EmptyArray<Char>();
            _pubFieldCharCharArr4 = Utils.Default<Char[]>();
            PropertyCharCharArr0 = Utils.Default<Char[]>();
            PropertyCharCharArr1 = Utils.RandomArray<Char>(r);
            PropertyCharCharArr2 = Utils.Default<Char[]>();
            PropertyCharCharArr3 = Utils.EmptyArray<Char>();
            PropertyCharCharArr4 = Utils.Default<Char[]>();
            PropertyCharCharArr5 = Utils.RandomArray<Char>(r);
            PropertyCharCharArr6 = Utils.Default<Char[]>();
            PropertyCharCharArr7 = Utils.RandomArray<Char>(r);
            PropertyCharCharArr8 = Utils.Default<Char[]>();
            PropertyCharCharArr9 = Utils.EmptyArray<Char>();
            PropertyCharCharArr10 = Utils.Default<Char[]>();
            PropertyCharCharArr11 = Utils.RandomArray<Char>(r);
            _prFieldChar0 = Utils.Default<Char>();
            _prFieldChar1 = Utils.RandomValue<Char>(r);
            _prFieldChar2 = Utils.RandomValue<Char>(r);
            _prFieldChar3 = Utils.Default<Char>();
            _prFieldChar4 = Utils.RandomValue<Char>(r);
            _pubFieldChar0 = Utils.Default<Char>();
            _pubFieldChar1 = Utils.RandomValue<Char>(r);
            _pubFieldChar2 = Utils.Default<Char>();
            _pubFieldChar3 = Utils.RandomValue<Char>(r);
            _pubFieldChar4 = Utils.Default<Char>();
            PropertyChar0 = Utils.Default<Char>();
            PropertyChar1 = Utils.RandomValue<Char>(r);
            PropertyChar2 = Utils.RandomValue<Char>(r);
            PropertyChar3 = Utils.Default<Char>();
            PropertyChar4 = Utils.RandomValue<Char>(r);
            PropertyChar5 = Utils.RandomValue<Char>(r);
            PropertyChar6 = Utils.Default<Char>();
            PropertyChar7 = Utils.RandomValue<Char>(r);
            PropertyChar8 = Utils.RandomValue<Char>(r);
            PropertyChar9 = Utils.Default<Char>();
            PropertyChar10 = Utils.RandomValue<Char>(r);
            PropertyChar11 = Utils.RandomValue<Char>(r);
            _prFieldCharListList10 = Utils.Default<List<Char>>();
            _prFieldCharListList11 = Utils.RandomList<Char>(r);
            _prFieldCharListList12 = Utils.Default<List<Char>>();
            _prFieldCharListList13 = Utils.EmptyList<Char>();
            _prFieldCharListList14 = Utils.Default<List<Char>>();
            _pubFieldCharListList10 = Utils.Default<List<Char>>();
            _pubFieldCharListList11 = Utils.RandomList<Char>(r);
            _pubFieldCharListList12 = Utils.Default<List<Char>>();
            _pubFieldCharListList13 = Utils.EmptyList<Char>();
            _pubFieldCharListList14 = Utils.Default<List<Char>>();
            PropertyCharListList10 = Utils.Default<List<Char>>();
            PropertyCharListList11 = Utils.RandomList<Char>(r);
            PropertyCharListList12 = Utils.Default<List<Char>>();
            PropertyCharListList13 = Utils.EmptyList<Char>();
            PropertyCharListList14 = Utils.Default<List<Char>>();
            PropertyCharListList15 = Utils.RandomList<Char>(r);
            PropertyCharListList16 = Utils.Default<List<Char>>();
            PropertyCharListList17 = Utils.RandomList<Char>(r);
            PropertyCharListList18 = Utils.Default<List<Char>>();
            PropertyCharListList19 = Utils.EmptyList<Char>();
            PropertyCharListList110 = Utils.Default<List<Char>>();
            PropertyCharListList111 = Utils.RandomList<Char>(r);
            _prFieldDateTimeDateTimeArr0 = Utils.Default<DateTime[]>();
            _prFieldDateTimeDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            _prFieldDateTimeDateTimeArr2 = Utils.Default<DateTime[]>();
            _prFieldDateTimeDateTimeArr3 = Utils.EmptyArray<DateTime>();
            _prFieldDateTimeDateTimeArr4 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeDateTimeArr0 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            _pubFieldDateTimeDateTimeArr2 = Utils.Default<DateTime[]>();
            _pubFieldDateTimeDateTimeArr3 = Utils.EmptyArray<DateTime>();
            _pubFieldDateTimeDateTimeArr4 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr0 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr1 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeDateTimeArr2 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr3 = Utils.EmptyArray<DateTime>();
            PropertyDateTimeDateTimeArr4 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr5 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeDateTimeArr6 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr7 = Utils.RandomArray<DateTime>(r);
            PropertyDateTimeDateTimeArr8 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr9 = Utils.EmptyArray<DateTime>();
            PropertyDateTimeDateTimeArr10 = Utils.Default<DateTime[]>();
            PropertyDateTimeDateTimeArr11 = Utils.RandomArray<DateTime>(r);
            _prFieldDateTime0 = Utils.Default<DateTime>();
            _prFieldDateTime1 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTime2 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTime3 = Utils.Default<DateTime>();
            _prFieldDateTime4 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime0 = Utils.Default<DateTime>();
            _pubFieldDateTime1 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime2 = Utils.Default<DateTime>();
            _pubFieldDateTime3 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime4 = Utils.Default<DateTime>();
            PropertyDateTime0 = Utils.Default<DateTime>();
            PropertyDateTime1 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime2 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime3 = Utils.Default<DateTime>();
            PropertyDateTime4 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime5 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime6 = Utils.Default<DateTime>();
            PropertyDateTime7 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime8 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime9 = Utils.Default<DateTime>();
            PropertyDateTime10 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime11 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTimeListList10 = Utils.Default<List<DateTime>>();
            _prFieldDateTimeListList11 = Utils.RandomList<DateTime>(r);
            _prFieldDateTimeListList12 = Utils.Default<List<DateTime>>();
            _prFieldDateTimeListList13 = Utils.EmptyList<DateTime>();
            _prFieldDateTimeListList14 = Utils.Default<List<DateTime>>();
            _pubFieldDateTimeListList10 = Utils.Default<List<DateTime>>();
            _pubFieldDateTimeListList11 = Utils.RandomList<DateTime>(r);
            _pubFieldDateTimeListList12 = Utils.Default<List<DateTime>>();
            _pubFieldDateTimeListList13 = Utils.EmptyList<DateTime>();
            _pubFieldDateTimeListList14 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList10 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList11 = Utils.RandomList<DateTime>(r);
            PropertyDateTimeListList12 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList13 = Utils.EmptyList<DateTime>();
            PropertyDateTimeListList14 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList15 = Utils.RandomList<DateTime>(r);
            PropertyDateTimeListList16 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList17 = Utils.RandomList<DateTime>(r);
            PropertyDateTimeListList18 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList19 = Utils.EmptyList<DateTime>();
            PropertyDateTimeListList110 = Utils.Default<List<DateTime>>();
            PropertyDateTimeListList111 = Utils.RandomList<DateTime>(r);
            _prFieldDecimalDecimalArr0 = Utils.Default<Decimal[]>();
            _prFieldDecimalDecimalArr1 = Utils.RandomArray<Decimal>(r);
            _prFieldDecimalDecimalArr2 = Utils.Default<Decimal[]>();
            _prFieldDecimalDecimalArr3 = Utils.EmptyArray<Decimal>();
            _prFieldDecimalDecimalArr4 = Utils.Default<Decimal[]>();
            _pubFieldDecimalDecimalArr0 = Utils.Default<Decimal[]>();
            _pubFieldDecimalDecimalArr1 = Utils.RandomArray<Decimal>(r);
            _pubFieldDecimalDecimalArr2 = Utils.Default<Decimal[]>();
            _pubFieldDecimalDecimalArr3 = Utils.EmptyArray<Decimal>();
            _pubFieldDecimalDecimalArr4 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr0 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr1 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalDecimalArr2 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr3 = Utils.EmptyArray<Decimal>();
            PropertyDecimalDecimalArr4 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr5 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalDecimalArr6 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr7 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalDecimalArr8 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr9 = Utils.EmptyArray<Decimal>();
            PropertyDecimalDecimalArr10 = Utils.Default<Decimal[]>();
            PropertyDecimalDecimalArr11 = Utils.RandomArray<Decimal>(r);
            _prFieldDecimal0 = Utils.Default<Decimal>();
            _prFieldDecimal1 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimal2 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimal3 = Utils.Default<Decimal>();
            _prFieldDecimal4 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal0 = Utils.Default<Decimal>();
            _pubFieldDecimal1 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal2 = Utils.Default<Decimal>();
            _pubFieldDecimal3 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal4 = Utils.Default<Decimal>();
            PropertyDecimal0 = Utils.Default<Decimal>();
            PropertyDecimal1 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal2 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal3 = Utils.Default<Decimal>();
            PropertyDecimal4 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal5 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal6 = Utils.Default<Decimal>();
            PropertyDecimal7 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal8 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal9 = Utils.Default<Decimal>();
            PropertyDecimal10 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal11 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimalListList10 = Utils.Default<List<Decimal>>();
            _prFieldDecimalListList11 = Utils.RandomList<Decimal>(r);
            _prFieldDecimalListList12 = Utils.Default<List<Decimal>>();
            _prFieldDecimalListList13 = Utils.EmptyList<Decimal>();
            _prFieldDecimalListList14 = Utils.Default<List<Decimal>>();
            _pubFieldDecimalListList10 = Utils.Default<List<Decimal>>();
            _pubFieldDecimalListList11 = Utils.RandomList<Decimal>(r);
            _pubFieldDecimalListList12 = Utils.Default<List<Decimal>>();
            _pubFieldDecimalListList13 = Utils.EmptyList<Decimal>();
            _pubFieldDecimalListList14 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList10 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList11 = Utils.RandomList<Decimal>(r);
            PropertyDecimalListList12 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList13 = Utils.EmptyList<Decimal>();
            PropertyDecimalListList14 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList15 = Utils.RandomList<Decimal>(r);
            PropertyDecimalListList16 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList17 = Utils.RandomList<Decimal>(r);
            PropertyDecimalListList18 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList19 = Utils.EmptyList<Decimal>();
            PropertyDecimalListList110 = Utils.Default<List<Decimal>>();
            PropertyDecimalListList111 = Utils.RandomList<Decimal>(r);
            _prFieldSomeEnumSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            _prFieldSomeEnumSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            _prFieldSomeEnumSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            _prFieldSomeEnumSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            _prFieldSomeEnumSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            _pubFieldSomeEnumSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            _pubFieldSomeEnumSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            PropertySomeEnumSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr5 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumSomeEnumArr6 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr7 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumSomeEnumArr8 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr9 = Utils.EmptyArray<SomeEnum>();
            PropertySomeEnumSomeEnumArr10 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumSomeEnumArr11 = Utils.RandomArray<SomeEnum>(r);
            _prFieldSomeEnum0 = Utils.Default<SomeEnum>();
            _prFieldSomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnum2 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnum3 = Utils.Default<SomeEnum>();
            _prFieldSomeEnum4 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum0 = Utils.Default<SomeEnum>();
            _pubFieldSomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum2 = Utils.Default<SomeEnum>();
            _pubFieldSomeEnum3 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum4 = Utils.Default<SomeEnum>();
            PropertySomeEnum0 = Utils.Default<SomeEnum>();
            PropertySomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum2 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum3 = Utils.Default<SomeEnum>();
            PropertySomeEnum4 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum5 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum6 = Utils.Default<SomeEnum>();
            PropertySomeEnum7 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum8 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum9 = Utils.Default<SomeEnum>();
            PropertySomeEnum10 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum11 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnumListList10 = Utils.Default<List<SomeEnum>>();
            _prFieldSomeEnumListList11 = Utils.RandomList<SomeEnum>(r);
            _prFieldSomeEnumListList12 = Utils.Default<List<SomeEnum>>();
            _prFieldSomeEnumListList13 = Utils.EmptyList<SomeEnum>();
            _prFieldSomeEnumListList14 = Utils.Default<List<SomeEnum>>();
            _pubFieldSomeEnumListList10 = Utils.Default<List<SomeEnum>>();
            _pubFieldSomeEnumListList11 = Utils.RandomList<SomeEnum>(r);
            _pubFieldSomeEnumListList12 = Utils.Default<List<SomeEnum>>();
            _pubFieldSomeEnumListList13 = Utils.EmptyList<SomeEnum>();
            _pubFieldSomeEnumListList14 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList10 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList11 = Utils.RandomList<SomeEnum>(r);
            PropertySomeEnumListList12 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList13 = Utils.EmptyList<SomeEnum>();
            PropertySomeEnumListList14 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList15 = Utils.RandomList<SomeEnum>(r);
            PropertySomeEnumListList16 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList17 = Utils.RandomList<SomeEnum>(r);
            PropertySomeEnumListList18 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList19 = Utils.EmptyList<SomeEnum>();
            PropertySomeEnumListList110 = Utils.Default<List<SomeEnum>>();
            PropertySomeEnumListList111 = Utils.RandomList<SomeEnum>(r);
            _prFieldRandomizableStructRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            _prFieldRandomizableStructRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            _prFieldRandomizableStructRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            _prFieldRandomizableStructRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            _prFieldRandomizableStructRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            _pubFieldRandomizableStructRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            _pubFieldRandomizableStructRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            _pubFieldRandomizableStructRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr0 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr1 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructRandomizableStructArr2 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr3 = Utils.EmptyArray<RandomizableStruct>();
            PropertyRandomizableStructRandomizableStructArr4 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr5 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructRandomizableStructArr6 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr7 = Utils.RandomArray<RandomizableStruct>(r);
            PropertyRandomizableStructRandomizableStructArr8 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr9 = Utils.EmptyArray<RandomizableStruct>();
            PropertyRandomizableStructRandomizableStructArr10 = Utils.Default<RandomizableStruct[]>();
            PropertyRandomizableStructRandomizableStructArr11 = Utils.RandomArray<RandomizableStruct>(r);
            _prFieldRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            _prFieldRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStruct2 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStruct3 = Utils.Default<RandomizableStruct>();
            _prFieldRandomizableStruct4 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            _pubFieldRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct2 = Utils.Default<RandomizableStruct>();
            _pubFieldRandomizableStruct3 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct4 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct2 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct3 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct4 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct5 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct6 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct7 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct8 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct9 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct10 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct11 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStructListList10 = Utils.Default<List<RandomizableStruct>>();
            _prFieldRandomizableStructListList11 = Utils.RandomList<RandomizableStruct>(r);
            _prFieldRandomizableStructListList12 = Utils.Default<List<RandomizableStruct>>();
            _prFieldRandomizableStructListList13 = Utils.EmptyList<RandomizableStruct>();
            _prFieldRandomizableStructListList14 = Utils.Default<List<RandomizableStruct>>();
            _pubFieldRandomizableStructListList10 = Utils.Default<List<RandomizableStruct>>();
            _pubFieldRandomizableStructListList11 = Utils.RandomList<RandomizableStruct>(r);
            _pubFieldRandomizableStructListList12 = Utils.Default<List<RandomizableStruct>>();
            _pubFieldRandomizableStructListList13 = Utils.EmptyList<RandomizableStruct>();
            _pubFieldRandomizableStructListList14 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList10 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList11 = Utils.RandomList<RandomizableStruct>(r);
            PropertyRandomizableStructListList12 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList13 = Utils.EmptyList<RandomizableStruct>();
            PropertyRandomizableStructListList14 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList15 = Utils.RandomList<RandomizableStruct>(r);
            PropertyRandomizableStructListList16 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList17 = Utils.RandomList<RandomizableStruct>(r);
            PropertyRandomizableStructListList18 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList19 = Utils.EmptyList<RandomizableStruct>();
            PropertyRandomizableStructListList110 = Utils.Default<List<RandomizableStruct>>();
            PropertyRandomizableStructListList111 = Utils.RandomList<RandomizableStruct>(r);
            _prFieldRandomizableRandomizableArr0 = Utils.Default<Randomizable[]>();
            _prFieldRandomizableRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            _prFieldRandomizableRandomizableArr2 = Utils.Default<Randomizable[]>();
            _prFieldRandomizableRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            _prFieldRandomizableRandomizableArr4 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableRandomizableArr0 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            _pubFieldRandomizableRandomizableArr2 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            _pubFieldRandomizableRandomizableArr4 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr0 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableRandomizableArr2 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            PropertyRandomizableRandomizableArr4 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr5 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableRandomizableArr6 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr7 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableRandomizableArr8 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr9 = Utils.EmptyArray<Randomizable>();
            PropertyRandomizableRandomizableArr10 = Utils.Default<Randomizable[]>();
            PropertyRandomizableRandomizableArr11 = Utils.RandomArray<Randomizable>(r);
            _prFieldRandomizable0 = Utils.Default<Randomizable>();
            _prFieldRandomizable1 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizable2 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizable3 = Utils.Default<Randomizable>();
            _prFieldRandomizable4 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable0 = Utils.Default<Randomizable>();
            _pubFieldRandomizable1 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable2 = Utils.Default<Randomizable>();
            _pubFieldRandomizable3 = Utils.RandomValue<Randomizable>(r);
            _pubFieldRandomizable4 = Utils.Default<Randomizable>();
            PropertyRandomizable0 = Utils.Default<Randomizable>();
            PropertyRandomizable1 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable2 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable3 = Utils.Default<Randomizable>();
            PropertyRandomizable4 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable5 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable6 = Utils.Default<Randomizable>();
            PropertyRandomizable7 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable8 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable9 = Utils.Default<Randomizable>();
            PropertyRandomizable10 = Utils.RandomValue<Randomizable>(r);
            PropertyRandomizable11 = Utils.RandomValue<Randomizable>(r);
            _prFieldRandomizableListList10 = Utils.Default<List<Randomizable>>();
            _prFieldRandomizableListList11 = Utils.RandomList<Randomizable>(r);
            _prFieldRandomizableListList12 = Utils.Default<List<Randomizable>>();
            _prFieldRandomizableListList13 = Utils.EmptyList<Randomizable>();
            _prFieldRandomizableListList14 = Utils.Default<List<Randomizable>>();
            _pubFieldRandomizableListList10 = Utils.Default<List<Randomizable>>();
            _pubFieldRandomizableListList11 = Utils.RandomList<Randomizable>(r);
            _pubFieldRandomizableListList12 = Utils.Default<List<Randomizable>>();
            _pubFieldRandomizableListList13 = Utils.EmptyList<Randomizable>();
            _pubFieldRandomizableListList14 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList10 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList11 = Utils.RandomList<Randomizable>(r);
            PropertyRandomizableListList12 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList13 = Utils.EmptyList<Randomizable>();
            PropertyRandomizableListList14 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList15 = Utils.RandomList<Randomizable>(r);
            PropertyRandomizableListList16 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList17 = Utils.RandomList<Randomizable>(r);
            PropertyRandomizableListList18 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList19 = Utils.EmptyList<Randomizable>();
            PropertyRandomizableListList110 = Utils.Default<List<Randomizable>>();
            PropertyRandomizableListList111 = Utils.RandomList<Randomizable>(r);
            _prFieldStringStringArr0 = Utils.Default<String[]>();
            _prFieldStringStringArr1 = Utils.RandomArray<String>(r);
            _prFieldStringStringArr2 = Utils.Default<String[]>();
            _prFieldStringStringArr3 = Utils.EmptyArray<String>();
            _prFieldStringStringArr4 = Utils.Default<String[]>();
            _pubFieldStringStringArr0 = Utils.Default<String[]>();
            _pubFieldStringStringArr1 = Utils.RandomArray<String>(r);
            _pubFieldStringStringArr2 = Utils.Default<String[]>();
            _pubFieldStringStringArr3 = Utils.EmptyArray<String>();
            _pubFieldStringStringArr4 = Utils.Default<String[]>();
            PropertyStringStringArr0 = Utils.Default<String[]>();
            PropertyStringStringArr1 = Utils.RandomArray<String>(r);
            PropertyStringStringArr2 = Utils.Default<String[]>();
            PropertyStringStringArr3 = Utils.EmptyArray<String>();
            PropertyStringStringArr4 = Utils.Default<String[]>();
            PropertyStringStringArr5 = Utils.RandomArray<String>(r);
            PropertyStringStringArr6 = Utils.Default<String[]>();
            PropertyStringStringArr7 = Utils.RandomArray<String>(r);
            PropertyStringStringArr8 = Utils.Default<String[]>();
            PropertyStringStringArr9 = Utils.EmptyArray<String>();
            PropertyStringStringArr10 = Utils.Default<String[]>();
            PropertyStringStringArr11 = Utils.RandomArray<String>(r);
            _prFieldString0 = Utils.Default<String>();
            _prFieldString1 = Utils.RandomValue<String>(r);
            _prFieldString2 = Utils.RandomValue<String>(r);
            _prFieldString3 = Utils.Default<String>();
            _prFieldString4 = Utils.RandomValue<String>(r);
            _pubFieldString0 = Utils.Default<String>();
            _pubFieldString1 = Utils.RandomValue<String>(r);
            _pubFieldString2 = Utils.Default<String>();
            _pubFieldString3 = Utils.RandomValue<String>(r);
            _pubFieldString4 = Utils.Default<String>();
            PropertyString0 = Utils.Default<String>();
            PropertyString1 = Utils.RandomValue<String>(r);
            PropertyString2 = Utils.RandomValue<String>(r);
            PropertyString3 = Utils.Default<String>();
            PropertyString4 = Utils.RandomValue<String>(r);
            PropertyString5 = Utils.RandomValue<String>(r);
            PropertyString6 = Utils.Default<String>();
            PropertyString7 = Utils.RandomValue<String>(r);
            PropertyString8 = Utils.RandomValue<String>(r);
            PropertyString9 = Utils.Default<String>();
            PropertyString10 = Utils.RandomValue<String>(r);
            PropertyString11 = Utils.RandomValue<String>(r);
            _prFieldStringListList10 = Utils.Default<List<String>>();
            _prFieldStringListList11 = Utils.RandomList<String>(r);
            _prFieldStringListList12 = Utils.Default<List<String>>();
            _prFieldStringListList13 = Utils.EmptyList<String>();
            _prFieldStringListList14 = Utils.Default<List<String>>();
            _pubFieldStringListList10 = Utils.Default<List<String>>();
            _pubFieldStringListList11 = Utils.RandomList<String>(r);
            _pubFieldStringListList12 = Utils.Default<List<String>>();
            _pubFieldStringListList13 = Utils.EmptyList<String>();
            _pubFieldStringListList14 = Utils.Default<List<String>>();
            PropertyStringListList10 = Utils.Default<List<String>>();
            PropertyStringListList11 = Utils.RandomList<String>(r);
            PropertyStringListList12 = Utils.Default<List<String>>();
            PropertyStringListList13 = Utils.EmptyList<String>();
            PropertyStringListList14 = Utils.Default<List<String>>();
            PropertyStringListList15 = Utils.RandomList<String>(r);
            PropertyStringListList16 = Utils.Default<List<String>>();
            PropertyStringListList17 = Utils.RandomList<String>(r);
            PropertyStringListList18 = Utils.Default<List<String>>();
            PropertyStringListList19 = Utils.EmptyList<String>();
            PropertyStringListList110 = Utils.Default<List<String>>();
            PropertyStringListList111 = Utils.RandomList<String>(r);
        }
    }
}