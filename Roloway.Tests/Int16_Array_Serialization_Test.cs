﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Int16_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new Int16ArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (Int16ArrayClass) s2.Deserialize(typeof (Int16ArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldInt16Arr0, o2.PrFieldInt16Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr1, o2.PrFieldInt16Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr2, o2.PrFieldInt16Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr3, o2.PrFieldInt16Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt16Arr4, o2.PrFieldInt16Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr0, o2._pubFieldInt16Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr1, o2._pubFieldInt16Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr2, o2._pubFieldInt16Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr3, o2._pubFieldInt16Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt16Arr4, o2._pubFieldInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr0, o2.PropertyInt16Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr1, o2.PropertyInt16Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr2, o2.PropertyInt16Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr3, o2.PropertyInt16Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr4, o2.PropertyInt16Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr5, o2.PropertyInt16Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr6, o2.PropertyInt16Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr7, o2.PropertyInt16Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr8, o2.PropertyInt16Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr9, o2.PropertyInt16Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr10, o2.PropertyInt16Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt16Arr11, o2.PropertyInt16Arr11);
            }
        }
    }

    [Serializable]
    internal class Int16ArrayClass {
        private Int16[] _prFieldInt16Arr0;
        private Int16[] _prFieldInt16Arr1;
        private Int16[] _prFieldInt16Arr2;
        private Int16[] _prFieldInt16Arr3;
        private Int16[] _prFieldInt16Arr4;

        public Int16[] PrFieldInt16Arr0 {
            get { return _prFieldInt16Arr0; }
        }

        public Int16[] PrFieldInt16Arr1 {
            get { return _prFieldInt16Arr1; }
        }

        public Int16[] PrFieldInt16Arr2 {
            get { return _prFieldInt16Arr2; }
        }

        public Int16[] PrFieldInt16Arr3 {
            get { return _prFieldInt16Arr3; }
        }

        public Int16[] PrFieldInt16Arr4 {
            get { return _prFieldInt16Arr4; }
        }

        public Int16[] _pubFieldInt16Arr0;
        public Int16[] _pubFieldInt16Arr1;
        public Int16[] _pubFieldInt16Arr2;
        public Int16[] _pubFieldInt16Arr3;
        public Int16[] _pubFieldInt16Arr4;
        public Int16[] PropertyInt16Arr0 { get; set; }
        public Int16[] PropertyInt16Arr1 { get; set; }
        public Int16[] PropertyInt16Arr2 { get; set; }
        public Int16[] PropertyInt16Arr3 { get; set; }
        public Int16[] PropertyInt16Arr4 { get; set; }
        public Int16[] PropertyInt16Arr5 { get; set; }
        public Int16[] PropertyInt16Arr6 { get; set; }
        public Int16[] PropertyInt16Arr7 { get; set; }
        public Int16[] PropertyInt16Arr8 { get; set; }
        public Int16[] PropertyInt16Arr9 { get; set; }
        public Int16[] PropertyInt16Arr10 { get; set; }
        public Int16[] PropertyInt16Arr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldInt16Arr0 = Utils.Default<Int16[]>();
            _prFieldInt16Arr1 = Utils.RandomArray<Int16>(r);
            _prFieldInt16Arr2 = Utils.Default<Int16[]>();
            _prFieldInt16Arr3 = Utils.EmptyArray<Int16>();
            _prFieldInt16Arr4 = Utils.Default<Int16[]>();
            _pubFieldInt16Arr0 = Utils.Default<Int16[]>();
            _pubFieldInt16Arr1 = Utils.RandomArray<Int16>(r);
            _pubFieldInt16Arr2 = Utils.Default<Int16[]>();
            _pubFieldInt16Arr3 = Utils.EmptyArray<Int16>();
            _pubFieldInt16Arr4 = Utils.Default<Int16[]>();
            PropertyInt16Arr0 = Utils.Default<Int16[]>();
            PropertyInt16Arr1 = Utils.RandomArray<Int16>(r);
            PropertyInt16Arr2 = Utils.Default<Int16[]>();
            PropertyInt16Arr3 = Utils.EmptyArray<Int16>();
            PropertyInt16Arr4 = Utils.Default<Int16[]>();
            PropertyInt16Arr5 = Utils.RandomArray<Int16>(r);
            PropertyInt16Arr6 = Utils.Default<Int16[]>();
            PropertyInt16Arr7 = Utils.RandomArray<Int16>(r);
            PropertyInt16Arr8 = Utils.Default<Int16[]>();
            PropertyInt16Arr9 = Utils.EmptyArray<Int16>();
            PropertyInt16Arr10 = Utils.Default<Int16[]>();
            PropertyInt16Arr11 = Utils.RandomArray<Int16>(r);
        }
    }
}