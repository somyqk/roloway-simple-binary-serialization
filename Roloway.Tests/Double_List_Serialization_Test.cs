using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Double_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListDoubleClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListDoubleClass) s2.Deserialize(typeof (ListDoubleClass));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListDoubleClass {
        private List<Double> _prFieldListList10;
        private List<Double> _prFieldListList11;
        private List<Double> _prFieldListList12;
        private List<Double> _prFieldListList13;
        private List<Double> _prFieldListList14;

        public List<Double> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<Double> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<Double> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<Double> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<Double> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<Double> _pubFieldListList10;
        public List<Double> _pubFieldListList11;
        public List<Double> _pubFieldListList12;
        public List<Double> _pubFieldListList13;
        public List<Double> _pubFieldListList14;
        public List<Double> PropertyListList10 { get; set; }
        public List<Double> PropertyListList11 { get; set; }
        public List<Double> PropertyListList12 { get; set; }
        public List<Double> PropertyListList13 { get; set; }
        public List<Double> PropertyListList14 { get; set; }
        public List<Double> PropertyListList15 { get; set; }
        public List<Double> PropertyListList16 { get; set; }
        public List<Double> PropertyListList17 { get; set; }
        public List<Double> PropertyListList18 { get; set; }
        public List<Double> PropertyListList19 { get; set; }
        public List<Double> PropertyListList110 { get; set; }
        public List<Double> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<Double>>();
            _prFieldListList11 = Utils.RandomList<Double>(r);
            _prFieldListList12 = Utils.Default<List<Double>>();
            _prFieldListList13 = Utils.EmptyList<Double>();
            _prFieldListList14 = Utils.Default<List<Double>>();
            _pubFieldListList10 = Utils.Default<List<Double>>();
            _pubFieldListList11 = Utils.RandomList<Double>(r);
            _pubFieldListList12 = Utils.Default<List<Double>>();
            _pubFieldListList13 = Utils.EmptyList<Double>();
            _pubFieldListList14 = Utils.Default<List<Double>>();
            PropertyListList10 = Utils.Default<List<Double>>();
            PropertyListList11 = Utils.RandomList<Double>(r);
            PropertyListList12 = Utils.Default<List<Double>>();
            PropertyListList13 = Utils.EmptyList<Double>();
            PropertyListList14 = Utils.Default<List<Double>>();
            PropertyListList15 = Utils.RandomList<Double>(r);
            PropertyListList16 = Utils.Default<List<Double>>();
            PropertyListList17 = Utils.RandomList<Double>(r);
            PropertyListList18 = Utils.Default<List<Double>>();
            PropertyListList19 = Utils.EmptyList<Double>();
            PropertyListList110 = Utils.Default<List<Double>>();
            PropertyListList111 = Utils.RandomList<Double>(r);
        }
    }
}