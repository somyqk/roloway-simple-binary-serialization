﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class SByte_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new SByteArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (SByteArrayClass) s2.Deserialize(typeof (SByteArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldSByteArr0, o2.PrFieldSByteArr0);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr1, o2.PrFieldSByteArr1);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr2, o2.PrFieldSByteArr2);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr3, o2.PrFieldSByteArr3);
                CollectionAssert.AreEqual(o1.PrFieldSByteArr4, o2.PrFieldSByteArr4);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr0, o2._pubFieldSByteArr0);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr1, o2._pubFieldSByteArr1);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr2, o2._pubFieldSByteArr2);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr3, o2._pubFieldSByteArr3);
                CollectionAssert.AreEqual(o1._pubFieldSByteArr4, o2._pubFieldSByteArr4);
                CollectionAssert.AreEqual(o1.PropertySByteArr0, o2.PropertySByteArr0);
                CollectionAssert.AreEqual(o1.PropertySByteArr1, o2.PropertySByteArr1);
                CollectionAssert.AreEqual(o1.PropertySByteArr2, o2.PropertySByteArr2);
                CollectionAssert.AreEqual(o1.PropertySByteArr3, o2.PropertySByteArr3);
                CollectionAssert.AreEqual(o1.PropertySByteArr4, o2.PropertySByteArr4);
                CollectionAssert.AreEqual(o1.PropertySByteArr5, o2.PropertySByteArr5);
                CollectionAssert.AreEqual(o1.PropertySByteArr6, o2.PropertySByteArr6);
                CollectionAssert.AreEqual(o1.PropertySByteArr7, o2.PropertySByteArr7);
                CollectionAssert.AreEqual(o1.PropertySByteArr8, o2.PropertySByteArr8);
                CollectionAssert.AreEqual(o1.PropertySByteArr9, o2.PropertySByteArr9);
                CollectionAssert.AreEqual(o1.PropertySByteArr10, o2.PropertySByteArr10);
                CollectionAssert.AreEqual(o1.PropertySByteArr11, o2.PropertySByteArr11);
            }
        }
    }

    [Serializable]
    internal class SByteArrayClass {
        private SByte[] _prFieldSByteArr0;
        private SByte[] _prFieldSByteArr1;
        private SByte[] _prFieldSByteArr2;
        private SByte[] _prFieldSByteArr3;
        private SByte[] _prFieldSByteArr4;

        public SByte[] PrFieldSByteArr0 {
            get { return _prFieldSByteArr0; }
        }

        public SByte[] PrFieldSByteArr1 {
            get { return _prFieldSByteArr1; }
        }

        public SByte[] PrFieldSByteArr2 {
            get { return _prFieldSByteArr2; }
        }

        public SByte[] PrFieldSByteArr3 {
            get { return _prFieldSByteArr3; }
        }

        public SByte[] PrFieldSByteArr4 {
            get { return _prFieldSByteArr4; }
        }

        public SByte[] _pubFieldSByteArr0;
        public SByte[] _pubFieldSByteArr1;
        public SByte[] _pubFieldSByteArr2;
        public SByte[] _pubFieldSByteArr3;
        public SByte[] _pubFieldSByteArr4;
        public SByte[] PropertySByteArr0 { get; set; }
        public SByte[] PropertySByteArr1 { get; set; }
        public SByte[] PropertySByteArr2 { get; set; }
        public SByte[] PropertySByteArr3 { get; set; }
        public SByte[] PropertySByteArr4 { get; set; }
        public SByte[] PropertySByteArr5 { get; set; }
        public SByte[] PropertySByteArr6 { get; set; }
        public SByte[] PropertySByteArr7 { get; set; }
        public SByte[] PropertySByteArr8 { get; set; }
        public SByte[] PropertySByteArr9 { get; set; }
        public SByte[] PropertySByteArr10 { get; set; }
        public SByte[] PropertySByteArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldSByteArr0 = Utils.Default<SByte[]>();
            _prFieldSByteArr1 = Utils.RandomArray<SByte>(r);
            _prFieldSByteArr2 = Utils.Default<SByte[]>();
            _prFieldSByteArr3 = Utils.EmptyArray<SByte>();
            _prFieldSByteArr4 = Utils.Default<SByte[]>();
            _pubFieldSByteArr0 = Utils.Default<SByte[]>();
            _pubFieldSByteArr1 = Utils.RandomArray<SByte>(r);
            _pubFieldSByteArr2 = Utils.Default<SByte[]>();
            _pubFieldSByteArr3 = Utils.EmptyArray<SByte>();
            _pubFieldSByteArr4 = Utils.Default<SByte[]>();
            PropertySByteArr0 = Utils.Default<SByte[]>();
            PropertySByteArr1 = Utils.RandomArray<SByte>(r);
            PropertySByteArr2 = Utils.Default<SByte[]>();
            PropertySByteArr3 = Utils.EmptyArray<SByte>();
            PropertySByteArr4 = Utils.Default<SByte[]>();
            PropertySByteArr5 = Utils.RandomArray<SByte>(r);
            PropertySByteArr6 = Utils.Default<SByte[]>();
            PropertySByteArr7 = Utils.RandomArray<SByte>(r);
            PropertySByteArr8 = Utils.Default<SByte[]>();
            PropertySByteArr9 = Utils.EmptyArray<SByte>();
            PropertySByteArr10 = Utils.Default<SByte[]>();
            PropertySByteArr11 = Utils.RandomArray<SByte>(r);
        }
    }
}