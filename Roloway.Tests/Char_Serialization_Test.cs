﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Char_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new CharsClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (CharsClass) s2.Deserialize(typeof (CharsClass));

                Assert.AreEqual(o1.PrFieldChar0, o2.PrFieldChar0);
                Assert.AreEqual(o1.PrFieldChar1, o2.PrFieldChar1);
                Assert.AreEqual(o1.PrFieldChar2, o2.PrFieldChar2);
                Assert.AreEqual(o1.PrFieldChar3, o2.PrFieldChar3);
                Assert.AreEqual(o1.PrFieldChar4, o2.PrFieldChar4);
                Assert.AreEqual(o1._pubFieldChar0, o2._pubFieldChar0);
                Assert.AreEqual(o1._pubFieldChar1, o2._pubFieldChar1);
                Assert.AreEqual(o1._pubFieldChar2, o2._pubFieldChar2);
                Assert.AreEqual(o1._pubFieldChar3, o2._pubFieldChar3);
                Assert.AreEqual(o1._pubFieldChar4, o2._pubFieldChar4);
                Assert.AreEqual(o1.PropertyChar0, o2.PropertyChar0);
                Assert.AreEqual(o1.PropertyChar1, o2.PropertyChar1);
                Assert.AreEqual(o1.PropertyChar2, o2.PropertyChar2);
                Assert.AreEqual(o1.PropertyChar3, o2.PropertyChar3);
                Assert.AreEqual(o1.PropertyChar4, o2.PropertyChar4);
                Assert.AreEqual(o1.PropertyChar5, o2.PropertyChar5);
                Assert.AreEqual(o1.PropertyChar6, o2.PropertyChar6);
                Assert.AreEqual(o1.PropertyChar7, o2.PropertyChar7);
                Assert.AreEqual(o1.PropertyChar8, o2.PropertyChar8);
                Assert.AreEqual(o1.PropertyChar9, o2.PropertyChar9);
                Assert.AreEqual(o1.PropertyChar10, o2.PropertyChar10);
                Assert.AreEqual(o1.PropertyChar11, o2.PropertyChar11);
            }
        }
    }

    [Serializable]
    internal class CharsClass {
        private Char _prFieldChar0;
        private Char _prFieldChar1;
        private Char _prFieldChar2;
        private Char _prFieldChar3;
        private Char _prFieldChar4;

        public Char PrFieldChar0 {
            get { return _prFieldChar0; }
        }

        public Char PrFieldChar1 {
            get { return _prFieldChar1; }
        }

        public Char PrFieldChar2 {
            get { return _prFieldChar2; }
        }

        public Char PrFieldChar3 {
            get { return _prFieldChar3; }
        }

        public Char PrFieldChar4 {
            get { return _prFieldChar4; }
        }

        public Char _pubFieldChar0;
        public Char _pubFieldChar1;
        public Char _pubFieldChar2;
        public Char _pubFieldChar3;
        public Char _pubFieldChar4;
        public Char PropertyChar0 { get; set; }
        public Char PropertyChar1 { get; set; }
        public Char PropertyChar2 { get; set; }
        public Char PropertyChar3 { get; set; }
        public Char PropertyChar4 { get; set; }
        public Char PropertyChar5 { get; set; }
        public Char PropertyChar6 { get; set; }
        public Char PropertyChar7 { get; set; }
        public Char PropertyChar8 { get; set; }
        public Char PropertyChar9 { get; set; }
        public Char PropertyChar10 { get; set; }
        public Char PropertyChar11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldChar0 = Utils.Default<Char>();
            _prFieldChar1 = Utils.RandomValue<Char>(r);
            _prFieldChar2 = Utils.Default<Char>();
            _prFieldChar3 = Utils.RandomValue<Char>(r);
            _prFieldChar4 = Utils.Default<Char>();
            _pubFieldChar0 = Utils.Default<Char>();
            _pubFieldChar1 = Utils.RandomValue<Char>(r);
            _pubFieldChar2 = Utils.Default<Char>();
            _pubFieldChar3 = Utils.RandomValue<Char>(r);
            _pubFieldChar4 = Utils.Default<Char>();
            PropertyChar0 = Utils.Default<Char>();
            PropertyChar1 = Utils.RandomValue<Char>(r);
            PropertyChar2 = Utils.RandomValue<Char>(r);
            PropertyChar3 = Utils.Default<Char>();
            PropertyChar4 = Utils.RandomValue<Char>(r);
            PropertyChar5 = Utils.RandomValue<Char>(r);
            PropertyChar6 = Utils.Default<Char>();
            PropertyChar7 = Utils.RandomValue<Char>(r);
            PropertyChar8 = Utils.RandomValue<Char>(r);
            PropertyChar9 = Utils.Default<Char>();
            PropertyChar10 = Utils.RandomValue<Char>(r);
            PropertyChar11 = Utils.RandomValue<Char>(r);
        }
    }
}