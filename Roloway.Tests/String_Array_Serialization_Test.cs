﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class String_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new StringArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (StringArrayClass) s2.Deserialize(typeof (StringArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldStringArr0, o2.PrFieldStringArr0);
                CollectionAssert.AreEqual(o1.PrFieldStringArr1, o2.PrFieldStringArr1);
                CollectionAssert.AreEqual(o1.PrFieldStringArr2, o2.PrFieldStringArr2);
                CollectionAssert.AreEqual(o1.PrFieldStringArr3, o2.PrFieldStringArr3);
                CollectionAssert.AreEqual(o1.PrFieldStringArr4, o2.PrFieldStringArr4);
                CollectionAssert.AreEqual(o1._pubFieldStringArr0, o2._pubFieldStringArr0);
                CollectionAssert.AreEqual(o1._pubFieldStringArr1, o2._pubFieldStringArr1);
                CollectionAssert.AreEqual(o1._pubFieldStringArr2, o2._pubFieldStringArr2);
                CollectionAssert.AreEqual(o1._pubFieldStringArr3, o2._pubFieldStringArr3);
                CollectionAssert.AreEqual(o1._pubFieldStringArr4, o2._pubFieldStringArr4);
                CollectionAssert.AreEqual(o1.PropertyStringArr0, o2.PropertyStringArr0);
                CollectionAssert.AreEqual(o1.PropertyStringArr1, o2.PropertyStringArr1);
                CollectionAssert.AreEqual(o1.PropertyStringArr2, o2.PropertyStringArr2);
                CollectionAssert.AreEqual(o1.PropertyStringArr3, o2.PropertyStringArr3);
                CollectionAssert.AreEqual(o1.PropertyStringArr4, o2.PropertyStringArr4);
                CollectionAssert.AreEqual(o1.PropertyStringArr5, o2.PropertyStringArr5);
                CollectionAssert.AreEqual(o1.PropertyStringArr6, o2.PropertyStringArr6);
                CollectionAssert.AreEqual(o1.PropertyStringArr7, o2.PropertyStringArr7);
                CollectionAssert.AreEqual(o1.PropertyStringArr8, o2.PropertyStringArr8);
                CollectionAssert.AreEqual(o1.PropertyStringArr9, o2.PropertyStringArr9);
                CollectionAssert.AreEqual(o1.PropertyStringArr10, o2.PropertyStringArr10);
                CollectionAssert.AreEqual(o1.PropertyStringArr11, o2.PropertyStringArr11);
            }
        }
    }

    [Serializable]
    internal class StringArrayClass {
        private String[] _prFieldStringArr0;
        private String[] _prFieldStringArr1;
        private String[] _prFieldStringArr2;
        private String[] _prFieldStringArr3;
        private String[] _prFieldStringArr4;

        public String[] PrFieldStringArr0 {
            get { return _prFieldStringArr0; }
        }

        public String[] PrFieldStringArr1 {
            get { return _prFieldStringArr1; }
        }

        public String[] PrFieldStringArr2 {
            get { return _prFieldStringArr2; }
        }

        public String[] PrFieldStringArr3 {
            get { return _prFieldStringArr3; }
        }

        public String[] PrFieldStringArr4 {
            get { return _prFieldStringArr4; }
        }

        public String[] _pubFieldStringArr0;
        public String[] _pubFieldStringArr1;
        public String[] _pubFieldStringArr2;
        public String[] _pubFieldStringArr3;
        public String[] _pubFieldStringArr4;
        public String[] PropertyStringArr0 { get; set; }
        public String[] PropertyStringArr1 { get; set; }
        public String[] PropertyStringArr2 { get; set; }
        public String[] PropertyStringArr3 { get; set; }
        public String[] PropertyStringArr4 { get; set; }
        public String[] PropertyStringArr5 { get; set; }
        public String[] PropertyStringArr6 { get; set; }
        public String[] PropertyStringArr7 { get; set; }
        public String[] PropertyStringArr8 { get; set; }
        public String[] PropertyStringArr9 { get; set; }
        public String[] PropertyStringArr10 { get; set; }
        public String[] PropertyStringArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldStringArr0 = Utils.Default<String[]>();
            _prFieldStringArr1 = Utils.RandomArray<String>(r);
            _prFieldStringArr2 = Utils.Default<String[]>();
            _prFieldStringArr3 = Utils.EmptyArray<String>();
            _prFieldStringArr4 = Utils.Default<String[]>();
            _pubFieldStringArr0 = Utils.Default<String[]>();
            _pubFieldStringArr1 = Utils.RandomArray<String>(r);
            _pubFieldStringArr2 = Utils.Default<String[]>();
            _pubFieldStringArr3 = Utils.EmptyArray<String>();
            _pubFieldStringArr4 = Utils.Default<String[]>();
            PropertyStringArr0 = Utils.Default<String[]>();
            PropertyStringArr1 = Utils.RandomArray<String>(r);
            PropertyStringArr2 = Utils.Default<String[]>();
            PropertyStringArr3 = Utils.EmptyArray<String>();
            PropertyStringArr4 = Utils.Default<String[]>();
            PropertyStringArr5 = Utils.RandomArray<String>(r);
            PropertyStringArr6 = Utils.Default<String[]>();
            PropertyStringArr7 = Utils.RandomArray<String>(r);
            PropertyStringArr8 = Utils.Default<String[]>();
            PropertyStringArr9 = Utils.EmptyArray<String>();
            PropertyStringArr10 = Utils.Default<String[]>();
            PropertyStringArr11 = Utils.RandomArray<String>(r);
        }
    }
}