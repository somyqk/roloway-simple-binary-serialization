<Query Kind="Program" />

enum SomeEnum {}
struct RandomizableStruct {}
class Randomizable{}

void Main()
{
	
}

void GenerateUnitTest(string testClassName, string testMethodName, string modelClassName, params Type[] types){
	var builder = new StringBuilder();
	builder.Append("using System;\nusing System.IO;\nusing Microsoft.VisualStudio.TestTools.UnitTesting;\nusing Roloway.Lib.Impl;\nusing Roloway.Tests.Others;\nusing System.Collections.Generic;\n\n");
	builder.Append("namespace Roloway.Tests {\n");
	
	var @class = GenerateTestClass(testClassName, testMethodName, modelClassName, types);
	builder.Append(@class);
	
	builder.Append("\n}\n");
	
	using(var fstream = new FileStream(@"C:\Users\samat\Documents\GitHub\roloway-simple-binary-serialization\Roloway.Tests\"+testClassName+".cs", FileMode.Create)) {
		using(var writer = new StreamWriter(fstream)){
			writer.Write(builder.ToString());
		}
	}
}

string GenerateTestClass(string testClassName, string testMethodName, string modelClassName, params Type[] types) {
	var builder = new StringBuilder();
	builder.Append("\t[TestClass]\n");
	builder.AppendFormat("\tpublic class {0} {{\n\n", testClassName);
	var testMethod = GenerateTestMethod(testMethodName, modelClassName, types);
	builder.Append(testMethod);
	builder.Append("\t}\n\n");
	
	var modelClass = GenerateRandomClass(modelClassName, types);
	builder.Append(modelClass);
	return builder.ToString();
}

string GenerateTestMethod(string methodName, string className, params Type[] fieldTypes){
	var builder = new StringBuilder();
	builder.Append("\t\t[TestMethod]\n");
	builder.AppendFormat("\t\tpublic void {0}() {{\n", methodName);
	builder.Append("\t\t\tusing (var stream = new MemoryStream()) {\n");
	builder.AppendFormat("\t\t\t\tvar o1 = new {0}();\n", className);
	builder.Append("\t\t\t\to1.Init();\n\n");
	builder.Append("\t\t\t\tvar s1 = new SbSerializer(stream);\n");
	builder.Append("\t\t\t\ts1.Serialize(o1);\n\n");
	
	builder.Append("\t\t\t\tstream.Position = 0;\n");
	builder.Append("\t\t\t\tvar s2 = new SbSerializer(stream);\n");
	builder.AppendFormat("\t\t\t\tvar o2 = ({0}) s2.Deserialize(typeof ({0}));\n\n", className);
	
	foreach(var fieldType in fieldTypes) {
		PrivateFieldAsserts(builder, fieldType);
		PublicFieldAsserts(builder, fieldType);
		PublicPropertyAsserts(builder, fieldType);
	}
	
	builder.Append("\t\t\t}\n");
	builder.Append("\t\t}\n");
	
	return builder.ToString();
}

string GenerateRandomClass(string name, params Type[] fieldTypes){
	var builder = new StringBuilder();
	builder.AppendFormat("\t[Serializable]\n\tinternal class {0} {{\n", name);
	
	foreach(var fieldType in fieldTypes){
		InsertPrivateFields(builder, fieldType, false);
		InsertPublicFields(builder, fieldType, false);
		InsertPublicProperties(builder, fieldType, false);
	}
	
	builder.Append("\n\t\t\tpublic void Init() {\n");
	builder.Append("\t\t\t\tvar r = new Random();\n");
	foreach(var fieldType in fieldTypes){
		InsertPrivateFields(builder, fieldType, true);
		InsertPublicFields(builder, fieldType, true);
		InsertPublicProperties(builder, fieldType, true);
	}
	builder.Append("\t\t\t}\n");
	builder.Append("}");
	
	return builder.ToString();
}

void PrivateFieldAsserts(StringBuilder builder, Type fieldType){
	if(IsArrayOrList(fieldType)) {
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\t\tCollectionAssert.AreEqual(o1.PrField{0}{1}, o2.PrField{0}{1});\n", MemberName(fieldType), i);
		}
	}
	else {
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\t\tAssert.AreEqual(o1.PrField{0}{1}, o2.PrField{0}{1});\n", MemberName(fieldType), i);
		}
	}
}

void InsertPrivateFields(StringBuilder builder, Type fieldType, bool initCode){
	if(!initCode) {
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\tprivate {0} _prField{1}{2};\n", MemberTypeName(fieldType), MemberName(fieldType), i);
		}
		
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\tpublic {0} PrField{1}{2} {{get {{return _prField{1}{2}; }} }}\n", MemberTypeName(fieldType), MemberName(fieldType), i);
		}
		return;
	}
	
	if(IsArrayOrList(fieldType)) {
		for(int i = 0; i < 5; i++){
			if(i%2 == 0) builder.AppendFormat("\t\t\t\t_prField{0}{1} = Utils.Default<{2}>();\n", MemberName(fieldType), i, MemberTypeName(fieldType));
			else if(i%3 == 0) builder.AppendFormat("\t\t\t\t_prField{0}{1} = Utils.Empty{2}<{3}>();\n", MemberName(fieldType), i, ArrayOrList(fieldType), ElementName(fieldType));
			else builder.AppendFormat("\t\t\t\t_prField{0}{1} = Utils.Random{2}<{3}>(r);\n", MemberName(fieldType), i, ArrayOrList(fieldType),  ElementName(fieldType));
		}
	}
	else {
		for(int i = 0; i < 5; i++){
			if(i%3 == 0) builder.AppendFormat("\t\t\t\t_prField{0}{1} = Utils.Default<{2}>();\n", MemberName(fieldType), i, MemberTypeName(fieldType));
			else builder.AppendFormat("\t\t\t\t_prField{0}{1} = Utils.RandomValue<{2}>(r);\n", MemberName(fieldType), i, MemberTypeName(fieldType));
		}
	}
}

void PublicFieldAsserts(StringBuilder builder, Type fieldType){
	if(IsArrayOrList(fieldType)) {
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\t\tCollectionAssert.AreEqual(o1._pubField{0}{1}, o2._pubField{0}{1});\n", MemberName(fieldType), i);
		}
	}
	else {
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\t\tAssert.AreEqual(o1._pubField{0}{1}, o2._pubField{0}{1});\n", MemberName(fieldType), i);
		}
	}
}

void InsertPublicFields(StringBuilder builder, Type fieldType, bool initCode){
	if(!initCode) {
		for(int i = 0; i < 5; i++){
			builder.AppendFormat("\t\t\tpublic {0} _pubField{1}{2};\n", MemberTypeName(fieldType), MemberName(fieldType), i);
		}
		return;
	}
	
	if(IsArrayOrList(fieldType)) {
		for(int i = 0; i < 5; i++){
			if(i%2 == 0) builder.AppendFormat("\t\t\t\t_pubField{0}{1} = Utils.Default<{2}>();\n", MemberName(fieldType), i, MemberTypeName(fieldType));
			else if(i%3 == 0) builder.AppendFormat("\t\t\t\t_pubField{0}{1} = Utils.Empty{2}<{3}>();\n", MemberName(fieldType), i, ArrayOrList(fieldType), ElementName(fieldType));
			else builder.AppendFormat("\t\t\t\t_pubField{0}{1} = Utils.Random{2}<{3}>(r);\n", MemberName(fieldType), i, ArrayOrList(fieldType), ElementName(fieldType));
		}
	}
	else {
		for(int i = 0; i < 5; i++){
			if(i%2 == 0) builder.AppendFormat("\t\t\t\t_pubField{0}{1} = Utils.Default<{2}>();\n", MemberName(fieldType), i, MemberTypeName(fieldType));
			else builder.AppendFormat("\t\t\t\t_pubField{0}{1} = Utils.RandomValue<{2}>(r);\n", MemberName(fieldType), i, MemberTypeName(fieldType));
		}
	}
}

void PublicPropertyAsserts(StringBuilder builder, Type fieldType){
	if(IsArrayOrList(fieldType)){
		for(int i = 0; i < 12; i++){
			builder.AppendFormat("\t\t\t\tCollectionAssert.AreEqual(o1.Property{0}{1}, o2.Property{0}{1});\n", MemberName(fieldType), i);
		}
	}
	else {
		for(int i = 0; i < 12; i++){
			builder.AppendFormat("\t\t\t\tAssert.AreEqual(o1.Property{0}{1}, o2.Property{0}{1});\n", MemberName(fieldType), i);
		}
	}
}

void InsertPublicProperties(StringBuilder builder, Type fieldType, bool initCode){
	if(!initCode) {
		for(int i = 0; i < 12; i++){
			builder.AppendFormat("\t\t\tpublic {0} Property{1}{2} {{get; set;}}\n", MemberTypeName(fieldType), MemberName(fieldType), i);
		}
		return;
	}
	
	if(IsArrayOrList(fieldType)) {
		for(int i = 0; i < 12; i++){
			if(i%2 == 0) builder.AppendFormat("\t\t\t\tProperty{0}{1} = Utils.Default<{2}>();\n", MemberName(fieldType), i, MemberTypeName(fieldType));
			else if(i%3 == 0) builder.AppendFormat("\t\t\t\tProperty{0}{1} = Utils.Empty{2}<{3}>();\n", MemberName(fieldType), i, ArrayOrList(fieldType), ElementName(fieldType));
			else builder.AppendFormat("\t\t\t\tProperty{0}{1} = Utils.Random{2}<{3}>(r);\n", MemberName(fieldType), i, ArrayOrList(fieldType), ElementName(fieldType));
		}
	}
	else {
		for(int i = 0; i < 12; i++){
			if(i%3 == 0) builder.AppendFormat("\t\t\t\tProperty{0}{1} = Utils.Default<{2}>();\n", MemberName(fieldType), i, MemberTypeName(fieldType));
			else builder.AppendFormat("\t\t\t\tProperty{0}{1} = Utils.RandomValue<{2}>(r);\n", MemberName(fieldType), i, MemberTypeName(fieldType));
		}
	}
}

string ElementName(Type t) {
	if (t.IsArray){
		return t.GetElementType().Name;
	}
	return t.GetGenericArguments()[0].Name;
}

string ArrayOrList(Type type) {
	if(type.IsArray)return "Array";
	return "List";
}

bool IsArrayOrList(Type type) {
	return type.IsArray || (type.IsGenericType && typeof(IList).IsAssignableFrom(type));
}

string MemberTypeName(Type type) {
	if(type.IsGenericType && typeof(IList).IsAssignableFrom(type)) {
		var t = type.GetGenericArguments()[0];
		return "List<"+t.Name+">";
	}
	return type.Name;
}

string MemberName(Type type) {
	if(type.IsArray) {
		return type.GetElementType().Name+type.Name.Replace("[]", "Arr");
	}
	else if(type.IsGenericType && typeof(IList).IsAssignableFrom(type)) {
		return type.GenericTypeArguments[0].Name+type.Name.Replace("`", "List");
	}
	return type.Name;
}

T[] EmptyArray<T>() {
	return new T[0];
}

T[] RandomArray<T>(Random random) {
	var res = new T[100];
	for(int i = 0; i < 100; i++) {
		res[i] = RandomValue<T>(random);
	}
	
	return res;
}

T RandomValue<T>(Random random) {
	string chars = "abcd efgh ijkl mnop qrsu vwxy zABC DEFG HIJK LMNO PQRS TUVW XYZ0 1234 5678 9!@# $%^& *()- _=+{ }:\" <>., /?|\\`";
	object res = null;
	
	if(typeof(T) == typeof(Byte)){
		res = (Byte) random.Next(Byte.MinValue, Byte.MaxValue);
	}
	else if(typeof(T) == typeof(SByte)){
		res = (SByte) random.Next(SByte.MinValue, SByte.MaxValue);
	}
	else if(typeof(T) == typeof(UInt16)){
		res = (UInt16) random.Next(UInt16.MinValue, UInt16.MaxValue);
	}
	else if(typeof(T) == typeof(Int16)){
		res = (Int16) random.Next(Int16.MinValue, Int16.MaxValue);
	}
	else if(typeof(T) == typeof(UInt32)){
		var bytes = new byte[4];
		random.NextBytes(bytes);
		res = BitConverter.ToUInt32(bytes, 0);
	}
	else if(typeof(T) == typeof(Int32)){
		res = (Int32) random.Next(Int32.MinValue, Int32.MaxValue);
	}
	else if(typeof(T) == typeof(UInt64)){
		var bytes = new byte[8];
		random.NextBytes(bytes);
		res = BitConverter.ToUInt64(bytes, 0);
	}
	else if(typeof(T) == typeof(Int64)){
		var bytes = new byte[8];
		random.NextBytes(bytes);
		res = BitConverter.ToInt64(bytes, 0);
	}
	else if(typeof(T) ==  typeof(Single)){
		var bytes = new byte[4];
		random.NextBytes(bytes);
		res = BitConverter.ToSingle(bytes, 0);
	}
	else if(typeof(T) ==  typeof(Double)){
		var bytes = new byte[8];
		random.NextBytes(bytes);
		res = BitConverter.ToDouble(bytes, 0);
	}
	else if(typeof(T) == typeof(Boolean)){
		res = random.Next()%2 == 0;
	}
	else if(typeof(T) == typeof(Char)){
		res = chars[random.Next(0, chars.Length-1)];
	}
	else if(typeof(T) == typeof(DateTime)){
		long l = DateTime.MinValue.Ticks;
		while(l <= DateTime.MinValue.Ticks || l >= DateTime.MaxValue.Ticks){
			l = RandomValue<long>(random);
		}
		res = DateTime.FromBinary(l);
	}
	else if(typeof(T) ==  typeof(Decimal)){
		res = NextDecimal(random);
	}
	else if(typeof(T) == typeof(String)){
		var length = random.Next(0, 1000);
		var builder = new StringBuilder();
		for(int i = 0; i < length; i++){
			builder.Append(chars[random.Next(0, chars.Length-1)]);
		}
		res = builder.ToString();
	}
	return (T)res;
}

T Default<T>() {
	return default(T);
}

int NextInt32(Random random) {
	unchecked {
		int firstBits = random.Next(0, 1 << 4) << 28;
		int lastBits = random.Next(0, 1 << 28);
		return firstBits | lastBits;
     }
}

decimal NextDecimal(Random random) {
	byte scale = (byte) random.Next(29);
	bool sign = random.Next(2) == 1;
	return new decimal(NextInt32(random), NextInt32(random), NextInt32(random), sign, scale);
}