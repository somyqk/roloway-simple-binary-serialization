﻿using System;
using System.Collections.Generic;

namespace Roloway.Lib.Models {
    public sealed class Tables {
        private Dictionary<int, string> _names = new Dictionary<int, string>();
        private Dictionary<int, Type> _types = new Dictionary<int, Type>();

        public Dictionary<int, string> Names {
            get { return _names; }
            set { _names = value; }
        }

        public Dictionary<int, Type> Types {
            get { return _types; }
            set { _types = value; }
        }
    }
}