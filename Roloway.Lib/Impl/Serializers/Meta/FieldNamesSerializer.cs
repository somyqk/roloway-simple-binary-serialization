﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.Meta {
    internal sealed class FieldNamesSerializer : ISbSerializer {
        public void Serialize(Stream stream, object obj, Tables tables) {
            var fields = Utils.GetFields(obj.GetType());
            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                tables.Names.Add(i, field.Name);
                var nameBytes = Encoding.UTF8.GetBytes(field.Name);
                stream.Write(nameBytes, 0, nameBytes.Length);
                stream.WriteByte(0x7c);
            }
        }

        public object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var fields = Utils.GetFields(type);

            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                var nameBytes = Encoding.UTF8.GetBytes(field.Name);
                var readBytes = new byte[nameBytes.Length];
                var read = stream.Read(readBytes, 0, readBytes.Length);
                if (read <= 0) {
                    throw new SerializationException("Unexpected end of stream");
                }
                if (!Utils.BytesAreSame(nameBytes, readBytes)) {
                    throw new SerializationException("Field names are not matched. Maybe you are trying to deserialize other type");
                }

                read = stream.Read(byt, 0, 1);
                if (read <= 0) {
                    throw new SerializationException("Unexpected end of stream");
                }
                if (byt[0] != 0x7c) {
                    throw new SerializationException("Incorrect binary format");
                }
                tables.Names.Add(i, field.Name);
            }

            return null;
        }
    }
}