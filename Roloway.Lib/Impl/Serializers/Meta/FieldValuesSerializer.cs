﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.Meta {
    internal sealed class FieldValuesSerializer : ISbSerializer {
        public void Serialize(Stream stream, object obj, Tables tables) {
            var fields = Utils.GetFields(obj.GetType());

            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                var fieldType = field.FieldType;
                var serializer = Factory.Instance.GetSerializer(fieldType, i);
                if (serializer != null) {
                    serializer.Serialize(stream, field.GetValue(obj), tables);
                }
            }
        }

        public object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var fields = Utils.GetFields(type);

            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                var fieldType = field.FieldType;
                var serializer = Factory.Instance.GetSerializer(fieldType, i);
                if (serializer != null) {
                    serializer.Deserialize(stream, type, tables, instance);
                }
            }

            return null;
        }
    }
}