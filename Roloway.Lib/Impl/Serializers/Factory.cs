﻿using System;
using System.Collections.Generic;
using Roloway.Lib.Abstract;
using Roloway.Lib.Impl.Serializers.RefType;
using Roloway.Lib.Impl.Serializers.ValueType;
using Roloway.Lib.Impl.Serializers.ValueType.Numeric;
using Roloway.Lib.Infrastructure;

namespace Roloway.Lib.Impl.Serializers {
    internal sealed class Factory {
        private Dictionary<Type, Func<int, ISbSerializer>> _serializers = new Dictionary<Type, Func<Int32, ISbSerializer>>();

        private Factory() {
            _serializers.Add(typeof (Int32), index => new Int32Serializer(index));
            _serializers.Add(typeof (Boolean), index => new BooleanSerializer(index));
            _serializers.Add(typeof (Byte), index => new ByteSerializer(index));
            _serializers.Add(typeof (Char), index => new CharSerializer(index));
            _serializers.Add(typeof (Decimal), index => new DecimalSerializer(index));
            _serializers.Add(typeof (Double), index => new DoubleSerializer(index));
            _serializers.Add(typeof (Single), index => new SingleSerializer(index));
            _serializers.Add(typeof (Int64), index => new Int64Serializer(index));
            _serializers.Add(typeof (SByte), index => new SByteSerializer(index));
            _serializers.Add(typeof (Int16), index => new Int16Serializer(index));
            _serializers.Add(typeof (UInt32), index => new UInt32Serializer(index));
            _serializers.Add(typeof (UInt64), index => new UInt64Serializer(index));
            _serializers.Add(typeof (UInt16), index => new UInt16Serializer(index));
            _serializers.Add(typeof (String), index => new StringSerializer(index));
            _serializers.Add(typeof (DateTime), index => new DateTimeSerializer(index));
        }

        public static Factory Instance {
            get { return Nested.instance; }
        }

        private class Nested {
            internal static readonly Factory instance = new Factory();

            static Nested() {}
        }

        public ISbSerializer GetSerializer(Type t, int index) {
            if (_serializers.ContainsKey(t)) {
                return _serializers[t](index);
            }
            if (t.IsEnum) {
                return new EnumSerializer(index);
            }
            if (Utils.IsListOrArray(t)) {
                return new ArraySerializer(index);
            }
            if (t.IsClass) {
                return new ClassSerializer(index);
            }
            if (t.IsValueType) {
                return new StructSerializer(index);
            }

            return null;
        }
    }
}