﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType {
    internal sealed class DecimalSerializer : AbstractSerializer {
        public DecimalSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            decimal value = (decimal) obj;
            var bytes = GetDecimalBytes(value);
            Utils.WriteBytesReversed(bytes, stream, tables);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var bytes = Utils.ReadBytesReversed(stream, 16);
            decimal result = GetDecimal(bytes);
            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            return result;
        }

        private decimal GetDecimal(byte[] bytes) {
            int[] bits = new int[4];
            for (int i = 0; i < bits.Length; i++) {
                var b = new[] {bytes[i*4 + 0], bytes[i*4 + 1], bytes[i*4 + 2], bytes[i*4 + 3]};
                bits[i] = BitConverter.ToInt32(b, 0);
            }
            return new decimal(bits);
        }

        private byte[] GetDecimalBytes(decimal d) {
            var bint = Decimal.GetBits(d);
            var result = new byte[16];
            for (int i = 0; i < bint.Length; i++) {
                var bytes = BitConverter.GetBytes(bint[i]);
                for (int j = 0; j < bytes.Length; j++) {
                    result[4*i + j] = bytes[j];
                }
            }
            return result;
        }
    }
}