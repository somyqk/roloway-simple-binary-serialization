﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType {
    internal sealed class BooleanSerializer : AbstractSerializer {
        public BooleanSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            var value = (Boolean) obj;
            if (value) {
                stream.WriteByte(1);
            }
            else {
                stream.WriteByte(0);
            }
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, byt[0] != 0);
                return null;
            }
            return byt[0] != 0;
        }
    }
}