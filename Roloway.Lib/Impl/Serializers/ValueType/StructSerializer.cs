﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType {
    internal sealed class StructSerializer : AbstractSerializer {
        public StructSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            var cs = new RolowaySerializer(stream, false);
            cs.Serialize(obj);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var cs = new RolowaySerializer(stream, false);
            object result;
            if (instance != null) {
                result = cs.Deserialize(tables.Types[Index]);
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            result = cs.Deserialize(type);
            return result;
        }
    }
}