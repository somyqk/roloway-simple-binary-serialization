﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType.Numeric {
    internal sealed class ByteSerializer : AbstractSerializer {
        public ByteSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            byte value = (byte) obj;
            stream.WriteByte(value);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, byt[0]);
                return null;
            }
            return byt[0];
        }
    }
}