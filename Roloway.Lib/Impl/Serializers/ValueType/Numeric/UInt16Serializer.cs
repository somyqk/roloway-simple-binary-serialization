﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType.Numeric {
    internal sealed class UInt16Serializer : AbstractSerializer {
        public UInt16Serializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            var value = (UInt16) obj;
            var bytes = BitConverter.GetBytes(value);
            Utils.WriteBytesReversed(bytes, stream, tables);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var bytes = Utils.ReadBytesReversed(stream, 2);
            ushort result = BitConverter.ToUInt16(bytes, 0);
            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            return result;
        }
    }
}