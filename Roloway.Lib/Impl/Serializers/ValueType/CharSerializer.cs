﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType {
    internal sealed class CharSerializer : AbstractSerializer {
        public CharSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            Char value = (Char) obj;
            var bytes = BitConverter.GetBytes(value);
            stream.Write(bytes, 0, bytes.Length);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var bytes = new byte[2];
            var read = stream.Read(bytes, 0, 2);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            var value = BitConverter.ToChar(bytes, 0);
            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, value);
                return null;
            }
            return value;
        }
    }
}