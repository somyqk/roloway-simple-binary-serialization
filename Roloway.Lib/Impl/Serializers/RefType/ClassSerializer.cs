﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.RefType {
    internal sealed class ClassSerializer : AbstractSerializer {
        public ClassSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            if (obj != null) {
                stream.WriteByte(1);
                var cs = new RolowaySerializer(stream, false);
                cs.Serialize(obj);
            }
            else {
                stream.WriteByte(0);
            }
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }
            if (byt[0] == 0) {
                return null;
            }

            var cs = new RolowaySerializer(stream, false);
            object result;
            if (instance != null) {
                result = cs.Deserialize(tables.Types[Index]);
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            result = cs.Deserialize(type);
            return result;
        }
    }
}