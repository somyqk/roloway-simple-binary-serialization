﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.RefType {
    internal sealed class StringSerializer : AbstractSerializer {
        public StringSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            if (obj == null) {
                stream.WriteByte(0);
                return;
            }
            stream.WriteByte(1);

            var value = (string) obj;
            if (value == string.Empty) {
                stream.WriteByte(0);
                return;
            }
            stream.WriteByte(1);

            var bytes = Encoding.UTF8.GetBytes(value);
            var lbytes = BitConverter.GetBytes(bytes.Length);
            Utils.WriteBytesReversed(lbytes, stream, tables);
            stream.Write(bytes, 0, bytes.Length);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }
            if (byt[0] == 0) {
                return null;
            }

            read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            string result;
            if (byt[0] == 0) {
                result = string.Empty;
            }
            else {
                var lbytes = Utils.ReadBytesReversed(stream, 4);
                int length = BitConverter.ToInt32(lbytes, 0);
                var bytes = new byte[length];
                read = stream.Read(bytes, 0, length);
                if (read <= 0) {
                    throw new SerializationException("Unexpected end of stream");
                }
                result = Encoding.UTF8.GetString(bytes);
            }
            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            return result;
        }
    }
}