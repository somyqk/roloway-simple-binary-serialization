﻿using System;
using System.Collections.Generic;
using System.IO;
using Roloway.Lib.Impl;

namespace Roloway.Console {
    [Serializable]
    internal sealed class Person {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime BirthDate { get; set; }
        public int Experience { get; set; }
        public double Height { get; set; }
        public List<string> Keywords { get; set; }

        public override string ToString() {
            return string.Format("First name: {0}\nLast name: {1}\nMiddle name: {2}\nBirthDate: {3}\nExperience: {4}\nHeight: {5}\nKeywords: {6}",
                FirstName, LastName, MiddleName, BirthDate, Experience, Height, string.Join(", ", Keywords)
                );
        }
    }

    internal class Program {
        private static void Main(string[] args) {
            var person = new Person {
                FirstName = "Samat",
                LastName = "Ait",
                MiddleName = "Tanat",
                BirthDate = DateTime.Now,
                Experience = 5,
                Height = 7.7,
                Keywords = new List<string> {".net", "c#", "asp.net", "asp.net mvc", "linqpad"}
            };
            var tempFile = Path.GetTempFileName();

            using (var stream = new FileStream(tempFile, FileMode.Create)) {
                var serializer = new RolowaySerializer(stream);
                serializer.Serialize(person);
            }

            Person deserialized;
            using (var stream = new FileStream(tempFile, FileMode.Open)) {
                var serializer = new RolowaySerializer(stream);
                deserialized = (Person) serializer.Deserialize(typeof (Person));
            }

            if (deserialized != null) {
                System.Console.WriteLine("Temp path: {0}", tempFile);
                System.Console.WriteLine(deserialized);
            }

            System.Console.WriteLine("Press any key...");
            System.Console.ReadKey(false);
        }
    }
}